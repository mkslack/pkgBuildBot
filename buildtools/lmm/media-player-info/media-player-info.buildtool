#!/bin/sh
##
## This is a buildtool to create a package for Slackware Linux.
##
## The author make no claims as to the fitness or correctness of
## this software for any use whatsoever, and it is provided as is.
## Any use of this software is at the user's own risk. 
##

# Initialize the script
BUILDTOOL_NAME="$0"
BUILDTOOL_OPTIONS="$*"
BUILDTOOL_FUNCTIONS_PATH="${BUILDTOOL_FUNCTIONS_PATH:-$(pwd)}"
if [ -e "${BUILDTOOL_FUNCTIONS_PATH}/functions" ]; then
  . "${BUILDTOOL_FUNCTIONS_PATH}/functions"
  BUILDTOOL_INIT
else
  echo "Cannot find 'functions'!"
  echo "Exiting now!"
  exit 1
fi

# What files to include in package and where to look for those?
DOCFILES=""

# Set package information
PACKAGE_NAME=media-player-info
PACKAGE_NAME_SOURCE=$PACKAGE_NAME
PACKAGE_VERSION=24
PACKAGE_VERSION_SOURCE=$PACKAGE_VERSION
PACKAGE_RELEASE_TAG=git180821
PACKAGE_RELEASE_TAG_SOURCE=$PACKAGE_RELEASE_TAG
PACKAGE_RELEASE_REV_GIT=337c758d95cc37f5a1b7a2a065e2e2cd8a75c4d8
PACKAGE_ARCH=${SLACK_ARCH:-i586}
PACKAGE_BUILD=1
PACKAGE_TAG=${CUSTOMPKGTAG:-slp}
PACKAGE_TYPE=tar.gz
PACKAGE_USES_SCHEMAS=false
PACKAGE_STRIP_BINARIES=true
PACKAGE_SUPPORTS_DISTCC=true

# Split PACKAGE_VERSION into MAJOR, MINOR, MICRO and NANO version.
PACKAGE_VERSION_MAJOR=$(echo $PACKAGE_VERSION | awk -F. '{ print $1 }')
PACKAGE_VERSION_MINOR=$(echo $PACKAGE_VERSION | awk -F. '{ print $2 }')
PACKAGE_VERSION_MICRO=$(echo $PACKAGE_VERSION | awk -F. '{ print $3 }')
PACKAGE_VERSION_NANO=$( echo $PACKAGE_VERSION | awk -F. '{ print $4 }')

# Define the download URL for the source package.
PACKAGE_URL[0]=https://gitlab.freedesktop.org/$PACKAGE_NAME_SOURCE/$PACKAGE_NAME_SOURCE/-/archive/$PACKAGE_RELEASE_REV_GIT/$PACKAGE_NAME_SOURCE-$PACKAGE_RELEASE_REV_GIT.$PACKAGE_TYPE
PACKAGE_URL[1]=
PACKAGE_URL[2]=
PACKAGE_URL[3]=

# Define the name of the source archive required to build this package.
PACKAGE_SOURCE[0]=$PACKAGE_NAME_SOURCE-$PACKAGE_VERSION_SOURCE$PACKAGE_RELEASE_TAG_SOURCE.$PACKAGE_TYPE
PACKAGE_SOURCE[1]=
PACKAGE_SOURCE[2]=
PACKAGE_SOURCE[3]=

# Define the name of the source directory.
PACKAGE_SOURCE_DIR=$PACKAGE_NAME_SOURCE-*




# Download required sources to build the package.
# If a custom download code is required (for example for svn/cvs)
# then make sure to copy the source package to '$SOURCE_PATH'.
# Use return code '0' for 'Download OK' or '1' for 'Download failed'.
DOWNLOAD_SOURCE_PACKAGE() {
  # This is the default download function that should
  # work for 99.9% of all packages.
  #BUILDTOOL_DOWNLOAD_STD
  # Sometimes you may need to give some extra options.
  # This is also being used when download a github tarball.
  #BUILDTOOL_DOWNLOAD_STD "-O ${SOURCE_PATH}/${PACKAGE_SOURCE[0]}"
  # Sometimes default wget does not work, use a custom command.
  wget ${DEFAULT_WGET_OPTIONS} \
       -O ${SOURCE_PATH}/${PACKAGE_SOURCE[0]} \
       -P ${SOURCE_PATH} \
       ${PACKAGE_URL[${url}]} 2>&1
  # Return the result of the download function.
  return $?
}




# Initialize install directories.
BUILDTOOL_INIT_DEFAULTS "usr" "etc"




# Let the user know what we are going to do.
echo "Compiling package $PACKAGE_NAME..."

# Create logfiles and build directories.
BUILDTOOL_CREATE_BUILD_ENVIRONMENT

# Extract the source package.
BUILDTOOL_UNPACK_SOURCES

# Set source file permissions.
BUILDTOOL_PREPARE_SOURCES




# Apply some patches
# BUILDTOOL_APPLY_PATCH "1" $CWD/files/nameofthepatch




# Rebuild configure...
autoreconf -vfi 1>>$LOG_STAGE1 2>>$LOG_STAGE1

# Compile the package
if (   CFLAGS="$SLKCFLAGS" \
       CXXFLAGS="$SLKCFLAGS" \
       ./configure \
            --prefix=/$PKPREFIX \
            --libdir=/$PKPRELIB \
            --sysconfdir=/$PKPREFETC \
            --mandir=/$PKPREMAN \
            --infodir=/$PKPREINF \
            --localstatedir=/$PKPRELSD \
            --build=${PACKAGE_BUILD_TARGET} \
            1>>$LOG_STAGE1 2>>$LOG_STAGE1 \
         && make $MAKEOPTS 1>>$LOG_STAGE2 2>>$LOG_STAGE2 \
         && make install DESTDIR=$BUILD_PACKAGE_DIR 1>>$LOG_STAGE2 2>>$LOG_STAGE2 \
  );  then
    echo "Done!"
  else
    # Build has failed, show some error info.
    BUILDTOOL_BUILD_ERROR
fi




# Prepare to build the package
echo "Building package..."

# Create the package.
BUILDTOOL_MAKEPKG_OPTIONS=""
BUILDTOOL_SLACKDESC_FILE=""
BUILDTOOL_MAKE_PACKAGE

# Remove temporary stuff if requested.
BUILDTOOL_FINALIZE

# Package created.
echo "Done"; echo

# All done.
exit 0

