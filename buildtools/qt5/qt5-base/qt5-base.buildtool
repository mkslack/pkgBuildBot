#!/bin/sh
##
## This is a buildtool to create a package for Slackware Linux.
##
## The author make no claims as to the fitness or correctness of
## this software for any use whatsoever, and it is provided as is.
## Any use of this software is at the user's own risk. 
##

# Initialize the script
BUILDTOOL_NAME="$0"
BUILDTOOL_OPTIONS="$*"
BUILDTOOL_FUNCTIONS_PATH="${BUILDTOOL_FUNCTIONS_PATH:-$(pwd)}"
if [ -e "${BUILDTOOL_FUNCTIONS_PATH}/functions" ]; then
  . "${BUILDTOOL_FUNCTIONS_PATH}/functions"
  BUILDTOOL_INIT
else
  echo "Cannot find 'functions'!"
  echo "Exiting now!"
  exit 1
fi

# What files to include in package and where to look for those?
DOCFILES=""

# Set package information
PACKAGE_NAME=qt5-base
PACKAGE_NAME_SOURCE=qtbase-everywhere-src
PACKAGE_VERSION=5.15.0
PACKAGE_VERSION_SOURCE=$PACKAGE_VERSION
PACKAGE_RELEASE_TAG=
PACKAGE_RELEASE_TAG_SOURCE=
PACKAGE_ARCH=${SLACK_ARCH:-i586}
PACKAGE_BUILD=1
PACKAGE_TAG=${CUSTOMPKGTAG:-slp}
PACKAGE_TYPE=tar.xz
PACKAGE_USES_SCHEMAS=false
PACKAGE_STRIP_BINARIES=true
PACKAGE_SUPPORTS_DISTCC=true

# Split PACKAGE_VERSION into MAJOR, MINOR, MICRO and NANO version.
PACKAGE_VERSION_MAJOR=$(echo $PACKAGE_VERSION | awk -F. '{ print $1 }')
PACKAGE_VERSION_MINOR=$(echo $PACKAGE_VERSION | awk -F. '{ print $2 }')
PACKAGE_VERSION_MICRO=$(echo $PACKAGE_VERSION | awk -F. '{ print $3 }')
PACKAGE_VERSION_NANO=$( echo $PACKAGE_VERSION | awk -F. '{ print $4 }')

# Define the download URL for the source package.
PACKAGE_URL[0]=http://download.qt.io/official_releases/qt/$PACKAGE_VERSION_MAJOR.$PACKAGE_VERSION_MINOR/$PACKAGE_VERSION_SOURCE$PACKAGE_RELEASE_TAG_SOURCE/submodules
PACKAGE_URL[1]=
PACKAGE_URL[2]=
PACKAGE_URL[3]=

# Define the name of the source archive required to build this package.
PACKAGE_SOURCE[0]=$PACKAGE_NAME_SOURCE-$PACKAGE_VERSION_SOURCE$PACKAGE_RELEASE_TAG_SOURCE.$PACKAGE_TYPE
PACKAGE_SOURCE[1]=
PACKAGE_SOURCE[2]=
PACKAGE_SOURCE[3]=

# Define the name of the source directory.
PACKAGE_SOURCE_DIR=$PACKAGE_NAME_SOURCE-$PACKAGE_VERSION_SOURCE




# Download required sources to build the package.
# If a custom download code is required (for example for svn/cvs)
# then make sure to copy the source package to '$SOURCE_PATH'.
# Use return code '0' for 'Download OK' or '1' for 'Download failed'.
DOWNLOAD_SOURCE_PACKAGE() {
  # This is the default download function that should
  # work for 99.9% of all packages.
  BUILDTOOL_DOWNLOAD_STD
  # Sometimes you may need to give some extra options.
  # This is also being used when download a github tarball.
  #BUILDTOOL_DOWNLOAD_STD "-O ${SOURCE_PATH}/${PACKAGE_SOURCE[0]}"
  # Sometimes default wget does not work, use a custom command.
  #wget ${DEFAULT_WGET_OPTIONS} \
  #     -O ${SOURCE_PATH}/${PACKAGE_SOURCE[0]} \
  #     -P ${SOURCE_PATH} \
  #     ${PACKAGE_URL[${url}]} 2>&1
  # Return the result of the download function.
  return $?
}




# Initialize install directories.
BUILDTOOL_INIT_DEFAULTS "usr" "etc/xdg"




# Let the user know what we are going to do.
echo "Compiling package $PACKAGE_NAME..."

# Create logfiles and build directories.
BUILDTOOL_CREATE_BUILD_ENVIRONMENT

# Extract the source package.
BUILDTOOL_UNPACK_SOURCES

# Set source file permissions.
BUILDTOOL_PREPARE_SOURCES




# Apply some patches
# BUILDTOOL_APPLY_PATCH "1" $CWD/files/nameofthepatch

# Fix location of MySQL headers.
BUILDTOOL_APPLY_PATCH "1" $CWD/patches/qt-5.8.0-mysql_headers.patch

# Apply patch to fix include/header issues with cmake.
BUILDTOOL_APPLY_PATCH "1" $CWD/patches/qt-5.15.0-qtbase_cmake_isystem_includes.patch




# Build qmake using Arch {C,LD}FLAGS
# This also sets default {C,CXX,LD}FLAGS for projects built using qmake
sed -i -e "s,^\(QMAKE_CFLAGS_RELEASE .*+=\).*,\1 $SLKCFLAGS," \
    mkspecs/common/gcc-base.conf 1>>$LOG_STAGE1 2>>$LOG_STAGE1
sed -i -e "s,^\(QMAKE_CFLAGS_RELEASE_WITH_DEBUGINFO .*+=\).*,\1 $SLKCFLAGS -g," \
    mkspecs/common/gcc-base.conf 1>>$LOG_STAGE1 2>>$LOG_STAGE1
sed -i -e "s,^\(QMAKE_LFLAGS_RELEASE.*\),\1 ${LDFLAGS}," \
    mkspecs/common/g++-unix.conf 1>>$LOG_STAGE1 2>>$LOG_STAGE1

# Use internal qdoc/qhelpgenerator/qmlplugindump when creating docs.
# Note: Creating docs does not work right now, so apply these changes
#       might not be necessary.
sed -i "s,\$\$QDOC,$(pwd)/qtbase/bin/qdoc,g" \
    mkspecs/features/qt_docs.prf
sed -i "s,\$\$QHELPGENERATOR,$(pwd)/qttools/bin/qhelpgenerator,g" \
    mkspecs/features/qt_docs.prf
sed -i "s,\$\$QMLPLUGINDUMP,$(pwd)/qtdeclarative/bin/qmlplugindump,g" \
    mkspecs/features/qml_plugin.prf

# Prepare some variables used while compiling the sources.
export LD_LIBRARY_PATH="$(pwd)/qtbase/lib:$(pwd)/qttools/lib:${LD_LIBRARY_PATH}"
export QT_PLUGIN_PATH="$(pwd)/qtbase/plugins"

# Strip binaries?
if ${BUILDTOOL_STRIP_BINARIES:-true}; then
  QT5_DEBUG="-strip"
else
  QT5_DEBUG="-no-strip"
fi
BUILDTOOL_PRINTLOG "  * Setting debugging features..."
BUILDTOOL_PRINTLOG "    => ${QT5_DEBUG}"

# Disable SSE2 on x86, see bug report:
# https://bugs.archlinux.org/task/38796
case $(uname -m) in
  i?86)		QT5_SSE2="-no-sse2";;
  *)		QT5_SSE2="";;
esac

# Agree to the GPL and compile the package.
# Deprecated with 5.15: -system-xcb, -qt-xcb, -xkb, -xcb-input
if (   CFLAGS="$SLKCFLAGS" \
       CXXFLAGS="$SLKCFLAGS" \
       ./configure \
            -prefix /$PKPREFIX \
            -sysconfdir /$PKPREFETC \
            -libdir /$PKPRELIB \
            -archdatadir /$PKPRELIB/qt5 \
            -bindir /$PKPRELIB/qt5/bin \
            -libexecdir /$PKPREFIX/libexec/qt5 \
            -headerdir /$PKPREFIX/include/qt5 \
            -datadir /$PKPREFIX/share/qt5 \
            -docdir /$PKPREDOC \
            -confirm-license \
            -opensource \
            -release \
            -silent \
            -shared \
            -reduce-relocations \
            ${QT5_DEBUG} \
            -no-rpath \
            -no-separate-debug-info \
            -no-use-gold-linker \
            -no-warnings-are-errors \
            -no-pch \
            -no-journald \
            -no-system-proxies \
            -linuxfb \
            -no-directfb \
            -xcb \
            -qpa xcb \
            -xcb-xlib \
            -fontconfig \
            -system-freetype \
            -system-harfbuzz \
            -system-libpng \
            -system-libjpeg \
            -qt-pcre \
            -system-zlib \
            -system-sqlite \
            -plugin-sql-mysql \
            -plugin-sql-sqlite \
            -nomake examples \
            -nomake tests \
            ${QT5_SSE2} \
            1>>$LOG_STAGE1 2>>$LOG_STAGE1 \
         && make $MAKEOPTS 1>>$LOG_STAGE2 2>>$LOG_STAGE2 \
         && make install INSTALL_ROOT=$BUILD_PACKAGE_DIR 1>>$LOG_STAGE2 2>>$LOG_STAGE2 \
  );  then
    echo "Done!"
  else
    # Build has failed, show some error info.
    BUILDTOOL_BUILD_ERROR
fi




# Prepare to build the package
echo "Building package..."

# Name of the QT symlink.
QT5NAME=qt5
QT5LINK=${QT5NAME}-${PACKAGE_VERSION}${PACKAGE_RELEASE_TAG}

# Create symlink /usr/lib/qt5-version to /usr/lib/qt5
ln -sf ${QT5NAME} $BUILD_PACKAGE_DIR/$PKPRELIB/${QT5LINK}

# Remove references to the build directory
find $BUILD_PACKAGE_DIR/ \
     -name "*.pc" \
     -exec perl -pi -e "s, -L$(pwd)/?\S+,,g" {} \;
find $BUILD_PACKAGE_DIR/$PKPRELIB/ \
     -type f \
     -name '*.prl' \
     -exec sed -i -e '/^QMAKE_PRL_BUILD_DIR/d' {} \;
sed  -e "s,$(pwd)/qtbase,/$QT5DIR,g" \
     -i $BUILD_PACKAGE_DIR/$QT5DIR/mkspecs/modules/qt_lib_bootstrap_private.pri
sed  -e "s,$(pwd)/qtdeclarative,/$QT5DIR,g" \
     -i $BUILD_PACKAGE_DIR/$QT5DIR/mkspecs/features/qml_plugin.prf
sed  -e "s,$(pwd)/qtbase,/$QT5DIR,g" \
     -i $BUILD_PACKAGE_DIR/$QT5DIR/mkspecs/features/qt_docs.prf
sed  -e "s,$(pwd)/qttools,/$QT5DIR,g" \
     -i $BUILD_PACKAGE_DIR/$QT5DIR/mkspecs/features/qt_docs.prf

# Useful symlinks
mkdir -p $BUILD_PACKAGE_DIR/usr/bin
( cd $BUILD_PACKAGE_DIR/usr/bin
  for binary in `find $BUILD_PACKAGE_DIR/$QT5DIR/bin -type f -printf "%f\n"`; do
    ln -s ../$PKLIBDIR/${QT5NAME}/bin/${binary} ${binary}-qt5
  done
)

# Add the profile scripts.
mkdir -p $BUILD_PACKAGE_DIR/etc/profile.d
cat <<EOF >$BUILD_PACKAGE_DIR/etc/profile.d/qt5.csh
#!/bin/csh
# It's best to use the generic directory to avoid
# compiling in a version-containing path:
setenv QT5DIR $QT5DIR
set path = ( \$path \${QT5DIR}/bin )

# Note: Disabled for now... most packages should find
#       the headers using pkgconfig or cmake.
#if ( \$?CPLUS_INCLUDE_PATH ) then
#    setenv CPLUS_INCLUDE_PATH /$PKPREFIX/include/qt5:\${CPLUS_INCLUDE_PATH}
#else
#    setenv CPLUS_INCLUDE_PATH /$PKPREFIX/include/qt5
#endif
EOF
cat <<EOF >$BUILD_PACKAGE_DIR/etc/profile.d/qt5.sh
#!/bin/sh
# It's best to use the generic directory to avoid
# compiling in a version-containing path:
export QT5DIR=$QT5DIR
export PATH="\${PATH}:\${QT5DIR}/bin"

# Note: Disabled for now... most packages should find
#       the headers using pkgconfig or cmake.
#if [ ! "\$CPLUS_INCLUDE_PATH" = "" ]; then
#  export CPLUS_INCLUDE_PATH=/$PKPREFIX/include/qt5:\${CPLUS_INCLUDE_PATH}
#else
#  export CPLUS_INCLUDE_PATH=/$PKPREFIX/include/qt5
#fi
EOF
chmod 755 $BUILD_PACKAGE_DIR/etc/profile.d/*
chown root.root $BUILD_PACKAGE_DIR/etc/profile.d/*

# Create the package.
BUILDTOOL_MAKEPKG_OPTIONS=""
BUILDTOOL_SLACKDESC_FILE=""
BUILDTOOL_MAKE_PACKAGE

# Remove temporary stuff if requested.
BUILDTOOL_FINALIZE

# Package created.
echo "Done"; echo

# All done.
exit 0

