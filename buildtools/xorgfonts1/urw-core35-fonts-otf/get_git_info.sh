#!/bin/sh
CWD=$(pwd)

# This is the repository name on quickgit.kde.org.
GITPROJECT=urw-core35-fonts

# Get the commit history.
wget -O ${CWD}/tmp -q http://git.ghostscript.com/?p=${GITPROJECT}.git;a=commit

# Get the date of last commit.
GIT_DATE=`grep -m1 "last change.*datetime" ${CWD}/tmp | sed "s,.*datetime\">,,g" | sed "s,<.*,,g"`
GIT_DATE=`date --date="$GIT_DATE" +%Y%m%d`
# Get the commit id.
GIT_REV=`grep -m1 "link.*commit" ${CWD}/tmp | sed "s,.*commit;h=,,g" | sed "s,\".*,,g"`

# Remove the temporary stuff...
rm ${CWD}/tmp

# Display or Update?
BUILDTOOL=`find . -type f -maxdepth 1 -name "*.buildtool" -printf "%f\n" 2>/dev/null | grep -m1 "."`
if test x"$BUILDTOOL" == x""; then
  echo "Buildtool not found!"
  exit 1
fi
if test x"$1" == x"--version" -o x"$1" == x"-v"; then
  echo "$GIT_DATE"
  echo "$GIT_REV"
else
  if test x"`grep -e \"^PACKAGE_RELEASE_REV_GIT=$GIT_REV\" $BUILDTOOL`" == x""; then
    sed -i -e "s,^PACKAGE_RELEASE_REV_GIT=.*,PACKAGE_RELEASE_REV_GIT=$GIT_REV," \
           -e "s,^PACKAGE_VERSION=.*,PACKAGE_VERSION=$GIT_DATE," \
           $BUILDTOOL
    echo "Buildtool updated:"
    echo "  Date    : $GIT_DATE"
    echo "  Revision: $GIT_REV"
    echo "Download new source package?"
    echo "Hit 'CTRL+C' to cancel, press <ENTER> to download."
    read key
    sh $BUILDTOOL -n
  else
    echo "Nothing to update ($GIT_DATE)."
  fi
fi
