
# Place init and config files.
config_note=false
config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
  if [ -e $NEW ]; then
    if ! $config_note; then
      echo "Will not replace existing configuration files!"
      config_note=true
    fi
    echo "  => $NEW"
  fi
}

# Keep permissions of existing configuration files.
# REMOVE THIS CODE IF NOT NEEDED!
#config_exec() {
#  if test -e $(dirname $1)/$(basename $1 .new) ; then
#    if [ ! -x $(dirname $1)/$(basename $1 .new) ]; then
#      chmod 644 $1
#     else
#      chmod 755 $1
#    fi
#  else
#    chmod 644 $1
#  fi
#  config $1
#}

config etc/configuration.new


# Create user and group if they do not exist.
group_name=name
group_id=70
group_exists=$(grep ^$group_name etc/group)
if [ x"${group_exists}" == x"" ]; then
  if ! ( groupadd -g $group_id $group_name ); then
    echo "Warning: Could not add group '$group_name'!"
  fi
fi
user_name=name
user_text="Sample User"
user_exists=$(grep ^$user_name etc/passwd)
if [ x"${user_exists}" == x"" ]; then
  if ! ( useradd -c "$user_text" -s /bin/false -d /dev/null -u $group_id -g $group_name $user_name ); then
    echo "Warning: Could not add user '$user_name'!"
  fi
fi

