#!/bin/sh
CWD=$(pwd)

# This is the username for the github project.
GITUSER=darktable-org
# This is the repository name on github.
GITPROJECT=darktable
GITPROJECT_RAWSPEED=rawspeed

# Get the commit history for darktable.
wget -O ${CWD}/tmp -q https://github.com/${GITUSER}/${GITPROJECT}/commits/master
# Get the date of last commit.
GIT_DATE=`grep -m1 ">Commits on" ${CWD}/tmp | sed -e "s,.*on ,,g" -e "s,<.*,,g"`
GIT_DATE=`date --date="$GIT_DATE" +%y%m%d`
# Get the commit id.
GIT_REV=`grep -m1 "/commit/" ${CWD}/tmp | sed "s,.*/commit/,,g" | sed "s,\".*,,g"`

# Get the commit history for rawspeed submodule.
wget -O ${CWD}/tmp -q https://github.com/${GITUSER}/${GITPROJECT_RAWSPEED}/commits/develop
# Get the date of last commit.
GIT_DATE_RS=`grep -m1 ">Commits on" ${CWD}/tmp | sed -e "s,.*on ,,g" -e "s,<.*,,g"`
GIT_DATE_RS=`date --date="$GIT_DATE_RS" +%y%m%d`
# Get the commit id.
GIT_REV_RS=`grep -m1 "/commit/" ${CWD}/tmp | sed "s,.*/commit/,,g" | sed "s,\".*,,g"`

# Remove the temporary stuff...
rm ${CWD}/tmp

# Display or Update?
BUILDTOOL=`find . -type f -maxdepth 1 -name "*.buildtool" -printf "%f\n" 2>/dev/null | grep -m1 "."`
if test x"$BUILDTOOL" == x""; then
  echo "Buildtool not found!"
  exit 1
fi
if test x"$1" == x"--version" -o x"$1" == x"-v"; then
  echo "DarkTable:"
  echo "  $GIT_DATE"
  echo "  $GIT_REV"
  echo "RawSpeed sub module:"
  echo "  $GIT_DATE_RS"
  echo "  $GIT_REV_RS"
else
  if test x"`grep -e \"^PACKAGE_RELEASE_REV_GIT=$GIT_REV\" $BUILDTOOL`" == x"" \
       -o x"`grep -e \"^PACKAGE_RELEASE_REV_GIT_RS=$GIT_REV_RS\" $BUILDTOOL`" == x"" ; then
    sed -i -e "s,^PACKAGE_RELEASE_REV_GIT=.*,PACKAGE_RELEASE_REV_GIT=$GIT_REV," \
           -e "s,^PACKAGE_RELEASE_TAG=.*,PACKAGE_RELEASE_TAG=git$GIT_DATE," \
           -e "s,^PACKAGE_RELEASE_REV_GIT_RS=.*,PACKAGE_RELEASE_REV_GIT_RS=$GIT_REV_RS," \
           -e "s,^PACKAGE_RELEASE_TAG_RS=.*,PACKAGE_RELEASE_TAG_RS=git$GIT_DATE_RS," \
           $BUILDTOOL
    echo "Buildtool updated:"
    echo "  DarkTable Date    : $GIT_DATE"
    echo "            Revision: $GIT_REV"
    echo "  RawSpeed  Date    : $GIT_DATE_RS"
    echo "            Revision: $GIT_REV_RS"
    echo "Download new source package?"
    echo "Hit 'CTRL+C' to cancel, press <ENTER> to download."
    read key
    sh $BUILDTOOL -n
  else
    echo "Nothing to update ($GIT_DATE)."
  fi
fi
