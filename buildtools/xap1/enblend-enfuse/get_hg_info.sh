#!/bin/sh
CWD=$(pwd)

# This is the repository name on sourceforge.
HGPROJECT=enblend

# Get the commit history.
wget -O ${CWD}/tmp -q https://sourceforge.net/p/${HGPROJECT}/code/ci/default/tree
# Get the commit id.
HG_REV=`grep -m1 "Tree <a href=" ${CWD}/tmp | sed "s,.*/code/ci/,,g" | sed "s,/.*,,g"`
wget -O ${CWD}/tmp -q https://sourceforge.net/p/${HGPROJECT}/code/ci/${HG_REV}
# Get the date of last commit.
HG_DATE=`grep -m1 "span title=" ${CWD}/tmp | sed "s,.*title=\",,g" | sed "s,\".*,,g"`
HG_DATE=`date -u -d "${HG_DATE}" +"%Y%m%d" | sed "s,^20,,g"`

# Remove the temporary stuff...
rm ${CWD}/tmp

# Display or Update?
BUILDTOOL=`find . -type f -maxdepth 1 -name "*.buildtool" -printf "%f\n" 2>/dev/null | grep -m1 "."`
if test x"${BUILDTOOL}" == x""; then
  echo "Buildtool not found!"
  exit 1
fi
if test x"$1" == x"--version" -o x"$1" == x"-v"; then
  echo "${HG_DATE}"
  echo "${HG_REV}"
else
  if test x"`grep -e \"^PACKAGE_RELEASE_REV_HG=${HG_REV}\" ${BUILDTOOL}`" == x""; then
    sed -i -e "s,^PACKAGE_RELEASE_REV_HG=.*,PACKAGE_RELEASE_REV_HG=${HG_REV}," \
           -e "s,^PACKAGE_RELEASE_TAG=.*,PACKAGE_RELEASE_TAG=rev${HG_DATE}_${HG_REV:0:8}," \
           ${BUILDTOOL}
    echo "Buildtool updated:"
    echo "  Date    : ${HG_DATE}"
    echo "  Revision: ${HG_REV}"
    echo "Download new source package?"
    echo "Hit 'CTRL+C' to cancel, press <ENTER> to download."
    read key
    sh ${BUILDTOOL} -n
  else
    echo "Nothing to update (${HG_DATE})."
  fi
fi
