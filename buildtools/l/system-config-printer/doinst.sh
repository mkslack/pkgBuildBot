
# Place init and config files.
config_note=false
config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
  if [ -e $NEW ]; then
    if ! $config_note; then
      echo "Will not replace existing configuration files!"
      config_note=true
    fi
    echo "  => $NEW"
  fi
}

# Rename existing configuration files.
if test -e etc/dbus-1/system.d/newprinternotification.conf ; then
  mv etc/dbus-1/system.d/newprinternotification.conf \
     etc/dbus-1/system.d/com.redhat.NewPrinterNotification.conf
fi
if test -e etc/dbus-1/system.d/printerdriversinstaller.conf ; then
  mv etc/dbus-1/system.d/printerdriversinstaller.conf \
     etc/dbus-1/system.d/com.redhat.PrinterDriversInstaller.conf
fi

config etc/dbus-1/system.d/com.redhat.NewPrinterNotification.conf.new
config etc/dbus-1/system.d/com.redhat.PrinterDriversInstaller.conf.new

