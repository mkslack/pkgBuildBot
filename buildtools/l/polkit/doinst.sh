
# Place init and config files.
config_note=false
config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
  if [ -e $NEW ]; then
    if ! $config_note; then
      echo "Will not replace existing configuration files!"
      config_note=true
    fi
    echo "  => $NEW"
  fi
}

config etc/polkit-1/rules.d/10-org.freedesktop.NetworkManager.rules.new
config etc/polkit-1/rules.d/20-plugdev-group-mount-override.rules.new

# PAM is optional.
if test -e etc/pam.d/polkit-1.new; then
  config etc/pam.d/polkit-1.new
fi


# Create user and group if they do not exist.
group_name=polkitd
group_id=87
group_exists=$(grep ^$group_name etc/group)
if [ x"${group_exists}" == x"" ]; then
  if ! ( groupadd -g $group_id $group_name ); then
    echo "Warning: Could not add group '$group_name'!"
  fi
fi
user_name=polkitd
user_text="PolicyKit daemon owner"
user_exists=$(grep ^$user_name etc/passwd)
if [ x"${user_exists}" == x"" ]; then
  if ! ( useradd -c "$user_text" -s /bin/false -d /var/lib/polkit -u $group_id -g $group_name $user_name ); then
    echo "Warning: Could not add user '$user_name'!"
  fi
fi


