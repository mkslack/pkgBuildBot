#!/bin/csh

if ( $?XDG_CONFIG_DIRS ) then
  echo "${XDG_CONFIG_DIRS}" | grep "/etc/xfce/xdg" >& /dev/null
  if ( "$?" != "0" ) then
    setenv XDG_CONFIG_DIRS ${XDG_CONFIG_DIRS}:/etc/xfce/xdg
  endif
else
  setenv XDG_CONFIG_DIRS /etc/xdg:/etc/xfce/xdg
endif

if ( $?XDG_DATA_DIRS ) then
  echo "${XDG_DATA_DIRS}" | grep "/usr/share/" >& /dev/null
  if ( "$?" != "0" ) then
    setenv XDG_DATA_DIRS /usr/share/:${XDG_DATA_DIRS}
  endif
else
  setenv XDG_DATA_DIRS /usr/local/share/:/usr/share/
endif
