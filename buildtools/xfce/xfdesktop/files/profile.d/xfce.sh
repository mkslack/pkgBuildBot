#!/bin/sh
echo "${XDG_CONFIG_DIRS}" | grep "/etc/xfce/xdg" >/dev/null 2>&1
if [ "$?" -ne "0" ]; then
  if [ ! x"${XDG_CONFIG_DIRS}" == x"" ]; then
    XDG_CONFIG_DIRS=${XDG_CONFIG_DIRS}:/etc/xfce/xdg
  else
    XDG_CONFIG_DIRS=/etc/xdg:/etc/xfce/xdg
  fi
  export XDG_CONFIG_DIRS
fi

echo "${XDG_DATA_DIRS}" | grep "/usr/share/" >/dev/null 2>&1
if [ "$?" -ne "0" ]; then
  if [ ! x"${XDG_DATA_DIRS}" == x"" ]; then
    XDG_DATA_DIRS=/usr/share/:${XDG_DATA_DIRS}
  else
    XDG_DATA_DIRS=/usr/local/share/:/usr/share/
  fi
  export XDG_DATA_DIRS
fi
