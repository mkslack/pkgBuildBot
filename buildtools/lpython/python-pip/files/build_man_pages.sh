#!/bin/sh
# This script will install python/sphinx to
# create the man pagesfor python/pip.
# Note: You need python/pip to be installed
# since the required dependencies will be
# downloaded/installed using python/pip.
CWD=$(pwd)
if test ! -d tmp-source; then
    echo "Error: No directory named 'tmp-source' found!"
    echo "       You need to run this script from inside"
    echo "       the package directory after this package"
    echo "       has beed compiled and installed/upgraded."
    echo "       You also need the tmp-source directory to"
    echo "       create the man pages."
    echo " !!!!! Make sure latest python-pip is installed!"
    echo ""
    echo "Exiting now!"
    exit 1
fi

# Get the name of the source directory.
PIP_SRC_DIR=$(cd tmp-source; find -type d -mindepth 1 -maxdepth 1 -printf "%f\n")
if test ! -d tmp-source/$PIP_SRC_DIR/docs/man; then
    echo "Error: Source directory including the docs/man"
    echo "       files needed to build man pages not found!"
    echo ""
    echo "Exiting now!"
    exit 1
fi

# Get the version of the python/pip package.
if test ! -f python-pip.buildtool; then
    echo "Error: Source directory including the python/pip"
    echo "       buildtool not found!"
    echo ""
    echo "Exiting now!"
    exit 1
fi
PIP_SRC_VER=$(grep "^PACKAGE_VERSION=" python-pip.buildtool | sed "s,.*=,,g")

# Install python/sphinx and all of the required dependencies
# to build the man pages of python/pip.
echo "Everything OK."
echo "Now this script will install python/sphinx needed"
echo "to build the man pages. The required dependencies"
echo "will be installed automatically."
echo ""
echo "Hit <RETURN> to continue, hit <CTRL+C> to abort."
read key
pip install sphinx || exit 1

# Sphinx installed, build the man pages.
( cd tmp-source/$PIP_SRC_DIR/docs \
  && mkdir -p build || exit
  PYTHONPATH="${CWD}/tmp-source/$PIP_SRC_DIR/docs" sphinx-build -W -b man -d build/doctrees/man man build/man -c html
  mkdir -p $CWD/files/man-$PIP_SRC_VER \
  && rm -f $CWD/files/man/* \
  && cp -a build/man/* $CWD/files/man-$PIP_SRC_VER || exit 1
)

# All done!
echo ""
echo "All done! The man pages have been copied to:"
echo "-> $CWD/files/man-$PIP_SRC_VER"
echo ""
exit 0
