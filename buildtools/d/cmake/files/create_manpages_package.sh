#!/bin/sh

# This is a script to extract the manpages from cmake-*-Linux-x86_64.tar.gz
# and output them as cmake.manpages.tar.xz in the current directory.

PACKAGE_NAME=cmake
PACKAGE_VERSION=`grep "^PACKAGE_VERSION=" ../cmake.buildtool | awk -F= '{ print $2 }'`
PACKAGE_VERSION_MAJOR=`echo $PACKAGE_VERSION | awk -F. '{ print $1 }'`
PACKAGE_VERSION_MINOR=`echo $PACKAGE_VERSION | awk -F. '{ print $2 }'`
PACKAGE_RELEASE_TAG=`grep "^PACKAGE_RELEASE_TAG=" ../cmake.buildtool | awk -F= '{ print $2 }'`
PACKAGE_TYPE=tar.gz
PACKAGE_URL=`grep "^PACKAGE_URL\\[0\\]="  ../cmake.buildtool | sed "s,.*=,,g"`
PACKAGE_URL=`eval echo $PACKAGE_URL`
PACKAGE_MANPAGE_NAME=$PACKAGE_NAME-$PACKAGE_VERSION$PACKAGE_RELEASE_TAG.manpages.tar.xz
PACKAGE_SOURCE_NAME=$PACKAGE_NAME-$PACKAGE_VERSION$PACKAGE_RELEASE_TAG-Linux-x86_64
PACKAGE_SOURCE=$PACKAGE_SOURCE_NAME.$PACKAGE_TYPE

# Download location.
mkdir -p ../files_other
PACKAGE_SOURCE_DIR=`cd ../files_other; pwd`

# Download binary release.
if ! test  -e $PACKAGE_SOURCE_DIR/$PACKAGE_SOURCE; then
  wget -P $PACKAGE_SOURCE_DIR $PACKAGE_URL/$PACKAGE_SOURCE || exit 1
fi

# Prepare extracting process.
rm -rf tmp-manpages
mkdir -p tmp-manpages/usr

# Unpackag the binary package.
tar xf $PACKAGE_SOURCE_DIR/$PACKAGE_SOURCE

# Extract the manpages.
mv $PACKAGE_SOURCE_NAME/man tmp-manpages/usr
rm -r $PACKAGE_SOURCE_NAME
chown -R root:root tmp-manpages

# Create new manpages package.
rm -f $PACKAGE_SOURCE_DIR/$PACKAGE_MANPAGE_NAME
cd tmp-manpages
makepkg -l n -c n $PACKAGE_SOURCE_DIR/$PACKAGE_MANPAGE_NAME
cd ..

# Remove temporary package files.
rm -r tmp-manpages
rm $PACKAGE_SOURCE_DIR/$PACKAGE_SOURCE

# Done!
echo "All done!"
exit 0
