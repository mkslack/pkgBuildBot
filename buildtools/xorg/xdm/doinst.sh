
# Place init and config files.
config_note=false
config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
  if [ -e $NEW ]; then
    if ! $config_note; then
      echo "Will not replace existing configuration files!"
      config_note=true
    fi
    echo "  => $NEW"
  fi
}

# Install configuration files.
for conffile in Xaccess \
                Xreset \
                Xresources \
                Xservers \
                Xsession \
                Xsetup_0 \
                Xstartup \
                Xwilling \
                GiveConsole \
                TakeConsole \
                xdm-config \
                ../app-defaults/Chooser ; do
  config etc/X11/xdm/${conffile}.new
done

config etc/pam.d/xdm.new


