# Add group adm to sudoers file...
add_sudo_entry() {
  if [ -e ${SUDOERS} ]; then
    SUDO=$(grep "${EXECFILE}" ${SUDOERS})
    if [ x"$SUDO" == x"" ]; then
      echo "" >>${SUDOERS}
      echo "# ${COMMENT}" >>${SUDOERS}
      echo "%${GROUP} ${SERVER}=${PASSMODE} ${EXECFILE}"    >>${SUDOERS}
    fi
  else
    echo "=> SUDO configuration file '/etc/sudoers' not found!" 1>&2
    echo "   Please install SUDO and add access policies manually:" 1>&2
    echo "   %${GROUP} ${SERVER}=${PASSMODE} ${EXECFILE}" 1>&2
  fi
}

# Define the default sudoers file and the
# group to use for admin tasks.
SUDOERS=etc/sudoers
GROUP=adm

# This can either be a server name, alias name or ALL.
SERVER=${HOSTNAME//.*/}

# This can be PASSWD: (always ask) or NOPASSWD: (never ask).
PASSMODE="PASSWD:"


# Define which commands should be available
# for users. COMMENT is some text for the sudoers file.
EXECFILE=/usr/bin/luckybackup
COMMENT="Allow users in group 'adm' to backup files..."
add_sudo_entry

