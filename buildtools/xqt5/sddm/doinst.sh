
# Place init and config files.
config_note=false
config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
  if [ -e $NEW ]; then
    if ! $config_note; then
      echo "Will not replace existing configuration files!"
      config_note=true
    fi
    echo "  => $NEW"
  fi
}

config etc/pam.d/sddm.new
config etc/pam.d/sddm-autologin.new
config etc/pam.d/sddm-greeter.new


# Make sure /etc/rc.d/rc.4.local does exist.
if ! test -e etc/rc.d/rc.4.local; then
  cat <<EOF >etc/rc.d/rc.4.local
#!/bin/sh
#
# /etc/rc.d/rc.4.local:  This file is executed by init(8) when the system
#                        is being initialized for run level 4 (XDM)
#
# Put any local startup commands in here.


EOF
chmod +x etc/rc.d/rc.4.local
fi

# Add SDDM to runlevel rc.4.local startup script.
if [ x"$(grep /usr/bin/sddm etc/rc.d/rc.4.local)" == x"" ]; then
  echo "Add SDDM to /etc/rc.d/rc.4.local..."
  cat <<EOF >>etc/rc.d/rc.4.local
# Try SDDM session manager.
if [ -x /usr/bin/sddm ]; then
  exec /usr/bin/sddm
fi

EOF
fi

# Create user and group if they do not exist.
group_name=sddm
group_id=64
group_exists=$(grep ^$group_name etc/group)
if [ x"${group_exists}" == x"" ]; then
  if ! ( groupadd -g $group_id $group_name ); then
    echo "Warning: Could not add group '$group_name'!"
  fi
fi
user_name=sddm
user_id=64
user_text="SDDM Daemon Owner"
user_exists=$(grep ^$user_name etc/passwd)
if [ x"${user_exists}" == x"" ]; then
  if ! ( useradd -c "$user_text" -s /bin/false -d /var/lib/sddm -u $user_id -g $group_name -G video $user_name ); then
    echo "Warning: Could not add user '$user_name'!"
  fi
fi

# Make 100% sure that sddm home directory exist. If not
# sddm may fail to start.
if ! test -d var/lib/sddm; then
  mkdir -p var/lib/sddm
  chown sddm.sddm var/lib/sddm
fi

