#! /bin/sh
# Xsession - run as user

# This file is extracted from kde-workspace (kdm/kfrontend/genkdmconf.c)
# Copyright (C) 2001-2005 Oswald Buddenhagen <ossi@kde.org>

session=$1

# redirect errors to a file in user's home directory if we can
errfile="$HOME/.xsession-errors"
if ( umask 077 && cp /dev/null "$errfile" 2> /dev/null )
then
        exec > "$errfile" 2>&1
else

        mktemp=/usr/bin/mktemp
        for errfile in "${TMPDIR-/tmp}/xses-$USER" "/tmp/xses-$USER"
        do
                if ef="$( umask 077 && $mktemp "$errfile.XXXXXX" 2> /dev/null)"
                then
                        exec > "$ef" 2>&1
                        mv "$ef" "$errfile" 2> /dev/null
                        break
                fi
        done
fi

# This section is borrowed from the old X11 Xsession file:
userresources=$HOME/.Xresources
usermodmap=$HOME/.Xmodmap
sysresources=/usr/lib64/X11/xinit/.Xresources
sysmodmap=/usr/lib64/X11/xinit/.Xmodmap

# merge in defaults and keymaps
if [ -r $sysresources ]; then
        /usr/bin/xrdb -merge $sysresources
fi
if [ -r $sysmodmap ]; then
        /usr/bin/xmodmap $sysmodmap
fi
if [ -r $userresources ]; then
        /usr/bin/xrdb -merge $userresources
fi
if [ -r $usermodmap ]; then
        /usr/bin/xmodmap $usermodmap
fi

# Note that the respective logout scripts are not sourced.
case $SHELL in
  */bash)
    [ -z "$BASH" ] && exec $SHELL $0 "$@"
    set +o posix
    [ -f /etc/profile ] && . /etc/profile
    if [ -f $HOME/.bash_profile ]; then
      . $HOME/.bash_profile
    elif [ -f $HOME/.bash_login ]; then
      . $HOME/.bash_login
    elif [ -f $HOME/.profile ]; then
      . $HOME/.profile
    fi
    ;;
  */zsh)
    [ -z "$ZSH_NAME" ] && exec $SHELL $0 "$@"
    [ -d /etc/zsh ] && zdir=/etc/zsh || zdir=/etc
    zhome=${ZDOTDIR:-$HOME}
    # zshenv is always sourced automatically.
    [ -f $zdir/zprofile ] && . $zdir/zprofile
    [ -f $zhome/.zprofile ] && . $zhome/.zprofile
    [ -f $zdir/zlogin ] && . $zdir/zlogin
    [ -f $zhome/.zlogin ] && . $zhome/.zlogin
    emulate -R sh
    ;;
  */csh|*/tcsh)
    # [t]cshrc is always sourced automatically.
    # Note that sourcing csh.login after .cshrc is non-standard.
    xsess_tmp=`mktemp /tmp/xsess-env-XXXXXX`
    $SHELL -c "if (-f /etc/csh.login) source /etc/csh.login; if (-f ~/.login) source ~/.login; /bin/sh -c 'export -p' >! $xsess_tmp"
    . $xsess_tmp
    rm -f $xsess_tmp
    ;;
  *) # Plain sh, ksh, and anything we do not know.
    [ -f /etc/profile ] && . /etc/profile
    [ -f $HOME/.profile ] && . $HOME/.profile
    ;;
esac

[ -f /etc/xprofile ] && . /etc/xprofile
[ -f $HOME/.xprofile ] && . $HOME/.xprofile

case $session in
  "")
    exec xmessage -center -buttons OK:0 -default OK "Sorry, $DESKTOP_SESSION is no valid session."
    ;;
  failsafe)
    exec xterm -geometry 80x24-0-0
    ;;
  *)
    exec "$session"
    ;;
esac
exec xmessage -center -buttons OK:0 -default OK "Sorry, cannot execute $session. Check $DESKTOP_SESSION.desktop."
