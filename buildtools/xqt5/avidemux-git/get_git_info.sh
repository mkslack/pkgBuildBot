#!/bin/sh
CWD=$(pwd)

# This is the username for the github project.
GITUSER=mean00
# This is the repository name on github.
GITPROJECT=avidemux2
GITPROJECT_I18N=avidemux2_i18n

# Get the commit history.
wget -O ${CWD}/tmp -q https://github.com/${GITUSER}/${GITPROJECT}/commits/master
# Get the date of last commit.
GIT_DATE=`grep -m1 ">Commits on" ${CWD}/tmp | sed -e "s,.*on ,,g" -e "s,<.*,,g"`
GIT_DATE=`date --date="$GIT_DATE" +%y%m%d`
# Get the commit id.
GIT_REV=`grep -m1 "/commit/" ${CWD}/tmp | sed "s,.*/commit/,,g" | sed "s,\".*,,g"`

# Get the commit history.
wget -O ${CWD}/tmp -q https://github.com/${GITUSER}/${GITPROJECT_I18N}/commits/master
# Get the commit id.
GIT_REV_I18N=`grep -m1 "/commit/" ${CWD}/tmp | sed "s,.*/commit/,,g" | sed "s,\".*,,g"`

# Remove the temporary stuff...
rm ${CWD}/tmp

# Display or Update?
BUILDTOOL=`find . -type f -maxdepth 1 -name "*.buildtool" -printf "%f\n" 2>/dev/null | grep -m1 "."`
if test x"$BUILDTOOL" == x""; then
  echo "Buildtool not found!"
  exit 1
fi
if test x"$1" == x"--version" -o x"$1" == x"-v"; then
  echo "$GIT_DATE"
  echo "$GIT_REV"
  echo "$GIT_REV_I18N"
else
  if test x"`grep -e \"^PACKAGE_RELEASE_REV_GIT=$GIT_REV\" $BUILDTOOL`" == x""; then
    sed -i -e "s,^PACKAGE_RELEASE_REV_GIT=.*,PACKAGE_RELEASE_REV_GIT=$GIT_REV," \
           -e "s,^PACKAGE_RELEASE_TAG=.*,PACKAGE_RELEASE_TAG=git$GIT_DATE," \
           -e "s,^PACKAGE_RELEASE_REV_GIT_I18N=.*,PACKAGE_RELEASE_REV_GIT_I18N=$GIT_REV_I18N," \
           $BUILDTOOL
    echo "Buildtool updated:"
    echo "  Date    : $GIT_DATE"
    echo "  Revision: $GIT_REV"
    echo "  Revision: $GIT_REV_I18N"
    echo "Download new source package?"
    echo "Hit 'CTRL+C' to cancel, press <ENTER> to download."
    read key
    sh $BUILDTOOL -n
  else
    echo "Nothing to update ($GIT_DATE)."
  fi
fi
