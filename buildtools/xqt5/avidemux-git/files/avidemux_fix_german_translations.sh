#!/bin/sh
QT_TS_FILE=avidemux/qt4/i18n/avidemux_de.ts
sed -i "s,Ä,Ae,g" $QT_TS_FILE
sed -i "s,Ö,Oe,g" $QT_TS_FILE
sed -i "s,Ü,Ue,g" $QT_TS_FILE
sed -i "s,ä,ae,g" $QT_TS_FILE
sed -i "s,ö,oe,g" $QT_TS_FILE
sed -i "s,ü,ue,g" $QT_TS_FILE
sed -i "s,ß,ss,g" $QT_TS_FILE


