#!/bin/sh
CWD=$(pwd)

# This is the root directory for the freedesktop.org repository.
# If there is no root project then use GITROOT=/
GITROOT=xorg/driver/
# This is the repository name on freedesktop.org.
GITPROJECT=xf86-video-qxl

# Get the commit history.
wget -O ${CWD}/tmp -q https://gitlab.freedesktop.org/${GITROOT}${GITPROJECT}/-/commits/master
# Get the date of last commit.
GIT_DATE=`grep -m1 "data-day" ${CWD}/tmp | sed "s,.*data-day=\",,g" | sed "s,\".*,,g" | sed "s,-,,g" | sed "s,^20,,g"`
# Get the commit id.
GIT_REV=`grep -m1 "/-/commit/" ${CWD}/tmp | sed "s,.*/-/commit/,,g" | sed "s,\".*,,g"`

# Remove the temporary stuff...
rm ${CWD}/tmp

# Display or Update?
BUILDTOOL=`find . -type f -maxdepth 1 -name "*.buildtool" -printf "%f\n" 2>/dev/null | grep -m1 "."`
if test x"$BUILDTOOL" == x""; then
  echo "Buildtool not found!"
  exit 1
fi
if test x"$1" == x"--version" -o x"$1" == x"-v"; then
  echo "$GIT_DATE"
  echo "$GIT_REV"
else
  if test x"`grep -e \"^PACKAGE_RELEASE_REV_GIT=$GIT_REV\" $BUILDTOOL`" == x""; then
    sed -i -e "s,^PACKAGE_RELEASE_REV_GIT=.*,PACKAGE_RELEASE_REV_GIT=$GIT_REV," \
           -e "s,^PACKAGE_RELEASE_TAG=.*,PACKAGE_RELEASE_TAG=git$GIT_DATE," \
           $BUILDTOOL
    echo "Buildtool updated:"
    echo "  Date    : $GIT_DATE"
    echo "  Revision: $GIT_REV"
    echo "Download new source package?"
    echo "Hit 'CTRL+C' to cancel, press <ENTER> to download."
    read key
    sh $BUILDTOOL -n
  else
    echo "Nothing to update ($GIT_DATE)."
  fi
fi
