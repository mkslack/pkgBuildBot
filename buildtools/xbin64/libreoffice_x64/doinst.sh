
# Place init and config files.
config_note=false
config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
  if [ -e $NEW ]; then
    if ! $config_note; then
      echo "Will not replace existing configuration files!"
      config_note=true
    fi
    echo "  => $NEW"
  fi
}

# Keep permissions of existing configuration files.
config_exec() {
  if test -e $(dirname $1)/$(basename $1 .new) ; then
    if [ ! -x $(dirname $1)/$(basename $1 .new) ]; then
      chmod 644 $1
     else
      chmod 755 $1
    fi
  else
    chmod 755 $1
  fi
  config $1
}

config_exec etc/profile.d/libreoffice.csh.new
config_exec etc/profile.d/libreoffice.sh.new


