#!/bin/csh
# The KDE integration looks ugly, use GTK design.
if ( ! $?SAL_USE_VCLPLUGIN ) then
  setenv SAL_USE_VCLPLUGIN gtk
  #setenv SAL_USE_VCLPLUGIN gtk3
  #setenv SAL_USE_VCLPLUGIN gen
  #setenv SAL_USE_VCLPLUGIN kde4
endif
