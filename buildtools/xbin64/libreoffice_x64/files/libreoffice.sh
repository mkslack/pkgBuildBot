#!/bin/sh
# The KDE integration looks ugly, use GTK design.
if test x"$SAL_USE_VCLPLUGIN" == x""; then
  export SAL_USE_VCLPLUGIN=gtk
  #export SAL_USE_VCLPLUGIN=gtk3
  #export SAL_USE_VCLPLUGIN=gen
  #export SAL_USE_VCLPLUGIN=kde4
fi
