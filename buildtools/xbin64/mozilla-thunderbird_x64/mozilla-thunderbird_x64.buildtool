#!/bin/sh
##
## This is a buildtool to create a package for Slackware Linux.
##
## The author make no claims as to the fitness or correctness of
## this software for any use whatsoever, and it is provided as is.
## Any use of this software is at the user's own risk. 
##

# Initialize the script
BUILDTOOL_NAME="$0"
BUILDTOOL_OPTIONS="$*"
BUILDTOOL_FUNCTIONS_PATH="${BUILDTOOL_FUNCTIONS_PATH:-$(pwd)}"
if [ -e "${BUILDTOOL_FUNCTIONS_PATH}/functions" ]; then
  . "${BUILDTOOL_FUNCTIONS_PATH}/functions"
  BUILDTOOL_INIT
else
  echo "Cannot find 'functions'!"
  echo "Exiting now!"
  exit 1
fi

# What files to include in package and where to look for those?
DOCFILES=""

# Set package information
PACKAGE_NAME=mozilla-thunderbird
PACKAGE_NAME_SOURCE=thunderbird
PACKAGE_LANGFILE=""
PACKAGE_VERSION=78.2.1
PACKAGE_VERSION_SOURCE=$PACKAGE_VERSION
PACKAGE_RELEASE_TAG=
PACKAGE_RELEASE_TAG_SOURCE=
PACKAGE_ARCH=x86_64
PACKAGE_BUILD=1
PACKAGE_TAG=${CUSTOMPKGTAG:-slp}
PACKAGE_TYPE=tar.bz2
PACKAGE_USES_SCHEMAS=false
PACKAGE_STRIP_BINARIES=false
PACKAGE_SUPPORTS_DISTCC=false

# Split PACKAGE_VERSION into MAJOR, MINOR, MICRO and NANO version.
PACKAGE_VERSION_MAJOR=$(echo $PACKAGE_VERSION | awk -F. '{ print $1 }')
PACKAGE_VERSION_MINOR=$(echo $PACKAGE_VERSION | awk -F. '{ print $2 }')
PACKAGE_VERSION_MICRO=$(echo $PACKAGE_VERSION | awk -F. '{ print $3 }')
PACKAGE_VERSION_NANO=$( echo $PACKAGE_VERSION | awk -F. '{ print $4 }')

# Define the download URL for the source package.
PACKAGE_URL[0]=http://archive.mozilla.org/pub/thunderbird/releases/$PACKAGE_VERSION/linux-x86_64/en-US
PACKAGE_URL[1]=${SOURCE_MIRROR_GENTOO_DISTFILES}
PACKAGE_URL[2]=
PACKAGE_URL[3]=

# Define the name of the source archive required to build this package.
PACKAGE_SOURCE[0]=$PACKAGE_NAME_SOURCE-$PACKAGE_VERSION_SOURCE$PACKAGE_RELEASE_TAG_SOURCE.$PACKAGE_TYPE
PACKAGE_SOURCE[1]=
PACKAGE_SOURCE[2]=
PACKAGE_SOURCE[3]=

# Define the name of the source directory.
PACKAGE_SOURCE_DIR=$PACKAGE_NAME_SOURCE




# Download required sources to build the package.
# If a custom download code is required (for example for svn/cvs)
# then make sure to copy the source package to '$SOURCE_PATH'.
# Use return code '0' for 'Download OK' or '1' for 'Download failed'.
DOWNLOAD_SOURCE_PACKAGE() {
  # This is the default download function that should
  # work for 99.9% of all packages.
  BUILDTOOL_DOWNLOAD_STD
  # Sometimes you may need to give some extra options.
  # This is also being used when download a github tarball.
  #BUILDTOOL_DOWNLOAD_STD "-O ${SOURCE_PATH}/${PACKAGE_SOURCE[0]}"
  # Sometimes default wget does not work, use a custom command.
  #wget ${DEFAULT_WGET_OPTIONS} \
  #     -O ${SOURCE_PATH}/${PACKAGE_SOURCE[0]} \
  #     -P ${SOURCE_PATH} \
  #     ${PACKAGE_URL[${url}]} 2>&1
  # Return the result of the download function.
  return $?
}




# Initialize install directories.
BUILDTOOL_INIT_DEFAULTS "usr" "etc"




# Let the user know what we are going to do.
echo "Compiling package $PACKAGE_NAME..."

# Create logfiles and build directories.
BUILDTOOL_CREATE_BUILD_ENVIRONMENT

# Extract the source package.
BUILDTOOL_UNPACK_SOURCES

# Set source file permissions.
BUILDTOOL_PREPARE_SOURCES




# Apply some patches
# BUILDTOOL_APPLY_PATCH "1" $CWD/files/nameofthepatch




# Compile the package
if ( mkdir -p $BUILD_PACKAGE_DIR/$PKPRELIB/$PACKAGE_NAME_SOURCE-$PACKAGE_VERSION_SOURCE \
       && cp -aR . $BUILD_PACKAGE_DIR/$PKPRELIB/$PACKAGE_NAME_SOURCE-$PACKAGE_VERSION_SOURCE \
          1>>$LOG_STAGE2 2>>$LOG_STAGE2 \
  );  then
    echo "Done!"
  else
    # Build has failed, show some error info.
    BUILDTOOL_BUILD_ERROR
fi




# Prepare to build the package
echo "Building package..."

# Patch the package to use with slackware-defaults
sed -i "s,moz_libdir=.*,moz_libdir=/$PKPRELIB/$PACKAGE_NAME_SOURCE-$PACKAGE_VERSION_SOURCE," \
       $BUILD_PACKAGE_DIR/$PKPRELIB/$PACKAGE_NAME_SOURCE-$PACKAGE_VERSION_SOURCE/thunderbird

cat << EOF >>$BUILD_PACKAGE_DIR/$PKPRELIB/$PACKAGE_NAME_SOURCE-$PACKAGE_VERSION_SOURCE/defaults/pref/all-thunderbird.js

// Use Firefox as the default external browser app
pref("network.protocol-handler.app.http",  "/usr/bin/firefox");
pref("network.protocol-handler.app.https", "/usr/bin/firefox");

EOF

# Create executable.
( mkdir -p $BUILD_PACKAGE_DIR/usr/bin
  cd $BUILD_PACKAGE_DIR/usr/bin
  ln -sf ../$PKLIBDIR/$PACKAGE_NAME_SOURCE-$PACKAGE_VERSION_SOURCE/thunderbird .
  chown -R root:root .
)

# Create symlink thunderbird-<version> => thunderbird.
( cd $BUILD_PACKAGE_DIR/$PKPRELIB
  ln -sf $PACKAGE_NAME_SOURCE-$PACKAGE_VERSION_SOURCE thunderbird
)

# Disable application updates.
mkdir -p $BUILD_PACKAGE_DIR/$PKPRELIB/thunderbird/distribution
cat $CWD/files/policies.json \
    >$BUILD_PACKAGE_DIR/$PKPRELIB/thunderbird/distribution/policies.json

# Create menu entry.
mkdir -p $BUILD_PACKAGE_DIR/usr/share/applications
cat $CWD/files/mozilla-thunderbird.desktop > $BUILD_PACKAGE_DIR/usr/share/applications/mozilla-thunderbird.desktop

mkdir -p $BUILD_PACKAGE_DIR/usr/share/pixmaps
cat $CWD/files/thunderbird.png > $BUILD_PACKAGE_DIR/usr/share/pixmaps/thunderbird.png

# Add the dictionary
if [ x"$PACKAGE_LANGFILE" != x"" ]; then
  LANGFILE=$(basename $PACKAGE_LANGFILE)
  mkdir -p $CWD/files_other
  if [ ! -e $CWD/files_other/$LANGFILE ]; then
    ( cd $CWD/files_other
      wget --no-check-certificate "$PACKAGE_LANGFILE" 1>>$LOG_STAGE2 2>>$LOG_STAGE2
    )
  fi

  if [ -e $CWD/files_other/$LANGFILE ]; then
    echo "  * Adding dictionary: $(basename $LANGFILE .xpi)"
    ( cd $BUILD_PACKAGE_DIR/$PKPRELIB/$PACKAGE_NAME_SOURCE-$PACKAGE_VERSION_SOURCE
      unzip -o "$CWD/files_other/$LANGFILE" "*.aff" "*.dic" 1>>$LOG_STAGE2 2>>$LOG_STAGE2
    )
    chown root.root $BUILD_PACKAGE_DIR/$PKPRELIB/$PACKAGE_NAME_SOURCE-$PACKAGE_VERSION_SOURCE/dictionaries/*
    chmod 644       $BUILD_PACKAGE_DIR/$PKPRELIB/$PACKAGE_NAME_SOURCE-$PACKAGE_VERSION_SOURCE/dictionaries/*
  fi
fi


# Create the package.
BUILDTOOL_MAKEPKG_OPTIONS=""
BUILDTOOL_SLACKDESC_FILE=""
BUILDTOOL_MAKE_PACKAGE

# Remove temporary stuff if requested.
BUILDTOOL_FINALIZE

# Package created.
echo "Done"; echo

# All done.
exit 0

