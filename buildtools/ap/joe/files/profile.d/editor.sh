#!/bin/sh
# Make Joe's Own Editor the default editor on the shell.
# Some versions of cron use EDITOR, some use VISUAL.
EDITOR=/usr/bin/joe
export EDITOR
VISUAL=/usr/bin/joe
export VISUAL
