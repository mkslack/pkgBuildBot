#!/bin/csh
# Make Joe's Own Editor the default editor on the shell.
# Some versions of cron use EDITOR, some use VISUAL.
setenv EDITOR /usr/bin/joe
setenv VISUAL /usr/bin/joe
