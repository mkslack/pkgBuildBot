# Register gstreamer-plugins
if [ -x usr/bin/gst-inspect-1.0 ]; then
  usr/bin/gst-inspect-1.0 1> dev/null 2> dev/null
else
  echo "Cannot find't gst-inspect-1.0, this should be part of gstreamer1."
  echo "Run 'gst-inspect-1.0' after you have installed gstreamer1 to register the plugins"
fi

