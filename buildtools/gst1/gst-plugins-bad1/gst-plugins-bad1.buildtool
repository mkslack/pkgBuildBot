#!/bin/sh
##
## This is a buildtool to create a package for Slackware Linux.
##
## The author make no claims as to the fitness or correctness of
## this software for any use whatsoever, and it is provided as is.
## Any use of this software is at the user's own risk. 
##

# Initialize the script
BUILDTOOL_NAME="$0"
BUILDTOOL_OPTIONS="$*"
BUILDTOOL_FUNCTIONS_PATH="${BUILDTOOL_FUNCTIONS_PATH:-$(pwd)}"
if [ -e "${BUILDTOOL_FUNCTIONS_PATH}/functions" ]; then
  . "${BUILDTOOL_FUNCTIONS_PATH}/functions"
  BUILDTOOL_INIT
else
  echo "Cannot find 'functions'!"
  echo "Exiting now!"
  exit 1
fi

# What files to include in package and where to look for those?
DOCFILES=""

# Set package information
PACKAGE_NAME=gst-plugins-bad1
PACKAGE_NAME_SOURCE=gst-plugins-bad
PACKAGE_VERSION=1.16.2
PACKAGE_VERSION_SOURCE=$PACKAGE_VERSION
PACKAGE_RELEASE_TAG=
PACKAGE_RELEASE_TAG_SOURCE=
PACKAGE_ARCH=${SLACK_ARCH:-i586}
PACKAGE_BUILD=1
PACKAGE_TAG=${CUSTOMPKGTAG:-slp}
PACKAGE_TYPE=tar.xz
PACKAGE_USES_SCHEMAS=false
PACKAGE_STRIP_BINARIES=true
PACKAGE_SUPPORTS_DISTCC=true

# Split PACKAGE_VERSION into MAJOR, MINOR, MICRO and NANO version.
PACKAGE_VERSION_MAJOR=$(echo $PACKAGE_VERSION | awk -F. '{ print $1 }')
PACKAGE_VERSION_MINOR=$(echo $PACKAGE_VERSION | awk -F. '{ print $2 }')
PACKAGE_VERSION_MICRO=$(echo $PACKAGE_VERSION | awk -F. '{ print $3 }')
PACKAGE_VERSION_NANO=$( echo $PACKAGE_VERSION | awk -F. '{ print $4 }')

# Define the download URL for the source package.
PACKAGE_URL[0]=http://gstreamer.freedesktop.org/src/$PACKAGE_NAME_SOURCE
PACKAGE_URL[1]=${SOURCE_MIRROR_GENTOO_DISTFILES}
PACKAGE_URL[2]=${SOURCE_MIRROR_GNOME}/sources/$PACKAGE_NAME_SOURCE/$PACKAGE_VERSION_MAJOR.$PACKAGE_VERSION_MINOR
PACKAGE_URL[3]=

# Define the name of the source archive required to build this package.
PACKAGE_SOURCE[0]=$PACKAGE_NAME_SOURCE-$PACKAGE_VERSION_SOURCE$PACKAGE_RELEASE_TAG_SOURCE.$PACKAGE_TYPE
PACKAGE_SOURCE[1]=
PACKAGE_SOURCE[2]=
PACKAGE_SOURCE[3]=

# Define the name of the source directory.
PACKAGE_SOURCE_DIR=$PACKAGE_NAME_SOURCE-$PACKAGE_VERSION_SOURCE$PACKAGE_RELEASE_TAG_SOURCE




# Download required sources to build the package.
# If a custom download code is required (for example for svn/cvs)
# then make sure to copy the source package to '$SOURCE_PATH'.
# Use return code '0' for 'Download OK' or '1' for 'Download failed'.
DOWNLOAD_SOURCE_PACKAGE() {
  # This is the default download function that should
  # work for 99.9% of all packages.
  BUILDTOOL_DOWNLOAD_STD
  # Sometimes you may need to give some extra options.
  # This is also being used when download a github tarball.
  #BUILDTOOL_DOWNLOAD_STD "-O ${SOURCE_PATH}/${PACKAGE_SOURCE[0]}"
  # Sometimes default wget does not work, use a custom command.
  #wget ${DEFAULT_WGET_OPTIONS} \
  #     -O ${SOURCE_PATH}/${PACKAGE_SOURCE[0]} \
  #     -P ${SOURCE_PATH} \
  #     ${PACKAGE_URL[${url}]} 2>&1
  # Return the result of the download function.
  return $?
}




# Initialize install directories.
BUILDTOOL_INIT_DEFAULTS "usr" "etc"




# Let the user know what we are going to do.
echo "Compiling package $PACKAGE_NAME..."

# Create logfiles and build directories.
BUILDTOOL_CREATE_BUILD_ENVIRONMENT

# Extract the source package.
BUILDTOOL_UNPACK_SOURCES

# Set source file permissions.
BUILDTOOL_PREPARE_SOURCES




# Apply some patches
# BUILDTOOL_APPLY_PATCH "1" $CWD/files/nameofthepatch

# Fix build with faac >= 1.29.
BUILDTOOL_APPLY_PATCH "1" $CWD/patches/gst-plugins-bad-1.12.3-faac129.patch

# Fix build with recent VULKAN-SDK.
BUILDTOOL_APPLY_PATCH "1" $CWD/patches/gst-plugins-bad-1.16.2-vulkan_headers.patch




# Check for optional packages.
BUILDTOOL_PRINTLOG "  * Checking for optional features..."

OPTIONAL_CHROMAP=$(   if ( pkg-config --exists libchromaprint ); then echo "--enable-chromaprint"; else echo "--disable-chromaprint"; fi)
BUILDTOOL_PRINTLOG "    => ${OPTIONAL_CHROMAP}"
BUILDTOOL_PRINTLOG "       (This option requires CHROMAPRINT to be installed)"
OPTIONAL_LIBOFA=$(    if ( pkg-config --exists libofa ); then echo "--enable-ofa"; else echo "--disable-ofa"; fi)
BUILDTOOL_PRINTLOG "    => ${OPTIONAL_LIBOFA}"
BUILDTOOL_PRINTLOG "       (This option requires LIBOFA to be installed)"
OPTIONAL_FAAC=$(      if [ -e /usr/include/faac.h ]; then echo "--enable-faac"; else echo "--disable-faac"; fi)
BUILDTOOL_PRINTLOG "    => ${OPTIONAL_FAAC}"
BUILDTOOL_PRINTLOG "       (This option requires LIBFAAC to be installed)"
OPTIONAL_FAAD2=$(     if [ -e /usr/include/faad.h ]; then echo "--enable-faad"; else echo "--disable-faad"; fi)
BUILDTOOL_PRINTLOG "    => ${OPTIONAL_FAAD2}"
BUILDTOOL_PRINTLOG "       (This option requires LIBFAAD2 to be installed)"
OPTIONAL_LIBWEBP=$(   if ( pkg-config --exists libwebp ); then echo "--enable-webp"; else echo "--disable-webp"; fi)
BUILDTOOL_PRINTLOG "    => ${OPTIONAL_LIBWEBP}"
BUILDTOOL_PRINTLOG "       (This option requires LIBWEBP to be installed)"
OPTIONAL_MPEG2ENC=$(  if ( pkg-config --exists mjpegtools ); then echo "--enable-mpeg2enc"; else echo "--disable-mpeg2enc"; fi)
BUILDTOOL_PRINTLOG "    => ${OPTIONAL_MPEG2ENC}"
BUILDTOOL_PRINTLOG "       (This option requires MJPEGTOOLS to be installed)"
OPTIONAL_OPUS=$(      if ( pkg-config --exists opus ); then echo "--enable-opus"; else echo "--disable-opus"; fi)
BUILDTOOL_PRINTLOG "    => ${OPTIONAL_OPUS}"
BUILDTOOL_PRINTLOG "       (This option requires OPUS to be installed)"
OPTIONAL_OPENAL=$(    if ( pkg-config --exists openal ); then echo "--enable-openal"; else echo "--disable-openal"; fi)
BUILDTOOL_PRINTLOG "    => ${OPTIONAL_OPENAL}"
BUILDTOOL_PRINTLOG "       (This option requires OPENAL to be installed)"
OPTIONAL_OPENCV=$(    if ( pkg-config --exists opencv ); then echo "--enable-opencv"; else echo "--disable-opencv"; fi)
BUILDTOOL_PRINTLOG "    => ${OPTIONAL_OPENCV}"
BUILDTOOL_PRINTLOG "       (This option requires OPENCV to be installed)"
OPTIONAL_X265=$(      if ( pkg-config --exists x265 ); then echo "--enable-x265"; else echo "--disable-x265"; fi)
BUILDTOOL_PRINTLOG "    => ${OPTIONAL_X265}"
BUILDTOOL_PRINTLOG "       (This option requires x265 to be installed)"
OPTIONAL_OPENJPEG2=$( if ( pkg-config --exists libopenjp2 ); then echo "--enable-openjpeg"; else echo "--disable-openjpeg"; fi)
BUILDTOOL_PRINTLOG "    => ${OPTIONAL_OPENJPEG2}"
BUILDTOOL_PRINTLOG "       (This option requires OPENJPEG2 to be installed)"
OPTIONAL_QT5=$(       if ( pkg-config --exists Qt5Core ); then echo "--enable-qt"; else echo "--disable-qt"; fi)
BUILDTOOL_PRINTLOG "    => ${OPTIONAL_QT5}"
BUILDTOOL_PRINTLOG "       (This option requires QT5 to be installed)"
OPTIONAL_MUSEPACK=$(  if [ -e /usr/include/mpcdec/mpcdec.h ]; then echo "--enable-musepack"; else echo "--disable-musepack"; fi)
BUILDTOOL_PRINTLOG "    => ${OPTIONAL_MUSEPACK}"
BUILDTOOL_PRINTLOG "       (This option requires LIBMPCDEC (MUSEPACK) to be installed)"

# Reconfigure the sources.
NOCONFIGURE=1 sh autogen.sh 1>>$LOG_STAGE1 2>>$LOG_STAGE1

# Compile the package
if (   CFLAGS="$SLKCFLAGS" \
       CXXFLAGS="$SLKCFLAGS -std=c++11" \
       ./configure \
            --prefix=/$PKPREFIX \
            --libdir=/$PKPRELIB \
            --sysconfdir=/$PKPREFETC \
            --mandir=/$PKPREMAN \
            --localstatedir=/$PKPRELSD \
            --disable-static \
            --enable-introspection=yes \
            --enable-external \
            --disable-examples \
            --disable-wayland \
            --enable-orc \
            --disable-kate \
            ${OPTIONAL_CHROMAP} \
            ${OPTIONAL_LIBOFA} \
            ${OPTIONAL_FAAC} \
            ${OPTIONAL_FAAD2} \
            ${OPTIONAL_LIBWEBP} \
            ${OPTIONAL_MJPG2ENC} \
            ${OPTIONAL_OPENJPEG2} \
            ${OPTIONAL_OPUS} \
            ${OPTIONAL_OPENAL} \
            ${OPTIONAL_OPENCV} \
            ${OPTIONAL_MUSEPACK} \
            ${OPTIONAL_XVID} \
            ${OPTIONAL_X265} \
            ${OPTIONAL_QT5} \
            --build=${PACKAGE_BUILD_TARGET} \
            1>>$LOG_STAGE1 2>>$LOG_STAGE1 \
         && make $MAKEOPTS 1>>$LOG_STAGE2 2>>$LOG_STAGE2 \
         && make install DESTDIR=$BUILD_PACKAGE_DIR 1>>$LOG_STAGE2 2>>$LOG_STAGE2 \
  );  then
    echo "Done!"
  else
    # Build has failed, show some error info.
    BUILDTOOL_BUILD_ERROR
fi




# Prepare to build the package
echo "Building package..."

# Create the package.
BUILDTOOL_MAKEPKG_OPTIONS=""
BUILDTOOL_SLACKDESC_FILE=""
BUILDTOOL_MAKE_PACKAGE

# Remove temporary stuff if requested.
BUILDTOOL_FINALIZE

# Package created.
echo "Done"; echo

# All done.
exit 0

