#!/bin/sh
# https://help.vivaldi.com/article/html5-proprietary-media-on-linux/
# HTML5 “proprietary” audio and video includes all media types that are patented, such
# as MP4 (H.264/AAC). Under Linux, Vivaldi requires a suitable, third-party support file to play these.
#
# To correct this situation, start Vivaldi from a “Terminal” using the command vivaldi.
# The output printed on the terminal will include steps on how to install a support file.
#
# For Vivaldi 2.4.1488.40:
#
curl https://launchpadlibrarian.net/414953672/chromium-codecs-ffmpeg-extra_73.0.3683.75-0ubuntu0.16.04.1_amd64.deb |\
  tail -c+1077 | tar JxC ~ --wildcards \*libffmpeg.so --xform 's,.*/,.local/lib/vivaldi/,'
