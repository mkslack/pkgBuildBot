
# Path to system-wide default mime applications list
MIMEAPPS=usr/share/applications/mimeapps.list

# Delete existing default application and
# set new default application
MIME_ENTRY_SET() {
  sed -i "\%$1% d" $MIMEAPPS
  cat <<EOF >>$MIMEAPPS
$1=$2
EOF
}

# Create empty mimeapps list if not already there...
if ! test -e $MIMEAPPS; then
  cat <<EOF >$MIMEAPPS
[Default Applications]
EOF
fi

# Set thunderbird as default for various mime types:
MIME_ENTRY_SET "x-scheme-handler/mailto" mozilla-thunderbird.desktop
MIME_ENTRY_SET "x-scheme-handler/news" mozilla-thunderbird.desktop
MIME_ENTRY_SET "message/rfc822" mozilla-thunderbird.desktop

