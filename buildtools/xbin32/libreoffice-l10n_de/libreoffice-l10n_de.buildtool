#!/bin/sh
##
## This is a buildtool to create a package for Slackware Linux.
##
## The author make no claims as to the fitness or correctness of
## this software for any use whatsoever, and it is provided as is.
## Any use of this software is at the user's own risk. 
##

# Initialize the script
BUILDTOOL_NAME="$0"
BUILDTOOL_OPTIONS="$*"
BUILDTOOL_FUNCTIONS_PATH="${BUILDTOOL_FUNCTIONS_PATH:-$(pwd)}"
if [ -e "${BUILDTOOL_FUNCTIONS_PATH}/functions" ]; then
  . "${BUILDTOOL_FUNCTIONS_PATH}/functions"
  BUILDTOOL_INIT
else
  echo "Cannot find 'functions'!"
  echo "Exiting now!"
  exit 1
fi

# What files to include in package and where to look for those?
DOCFILES=""

# Set package information
PACKAGE_LANG=${LIBREOFFICE_LANGPACK:-de}
PACKAGE_NAME=libreoffice-l10n
PACKAGE_NAME_SOURCE=LibreOffice
PACKAGE_VERSION=6.2.8.2
PACKAGE_VERSION_SOURCE=$PACKAGE_VERSION
PACKAGE_RELEASE_TAG=
PACKAGE_RELEASE_TAG_SOURCE=$PACKAGE_RELEASE_TAG
PACKAGE_ARCH=i686
PACKAGE_BUILD=1
PACKAGE_TAG=${CUSTOMPKGTAG:-slp}
PACKAGE_TYPE=tar.gz
PACKAGE_USES_SCHEMAS=false
PACKAGE_STRIP_BINARIES=false
PACKAGE_SUPPORTS_DISTCC=false

# Split PACKAGE_VERSION into MAJOR, MINOR, MICRO and NANO version.
PACKAGE_VERSION_MAJOR=$(echo $PACKAGE_VERSION | awk -F. '{ print $1 }')
PACKAGE_VERSION_MINOR=$(echo $PACKAGE_VERSION | awk -F. '{ print $2 }')
PACKAGE_VERSION_MICRO=$(echo $PACKAGE_VERSION | awk -F. '{ print $3 }')
PACKAGE_VERSION_NANO=$( echo $PACKAGE_VERSION | awk -F. '{ print $4 }')

# Define the download URL for the source package.
PACKAGE_URL[0]=https://downloadarchive.documentfoundation.org/libreoffice/old/$PACKAGE_VERSION_SOURCE/rpm/x86
PACKAGE_URL[1]=
PACKAGE_URL[2]=
PACKAGE_URL[3]=

# Define the name of the source archive required to build this package.
PACKAGE_SOURCE[0]=$PACKAGE_NAME_SOURCE\_$PACKAGE_VERSION_SOURCE$PACKAGE_RELEASE_TAG_SOURCE\_Linux_x86_rpm_langpack_$PACKAGE_LANG.$PACKAGE_TYPE
PACKAGE_SOURCE[1]=
PACKAGE_SOURCE[2]=
PACKAGE_SOURCE[3]=

# Define the name of the source directory.
PACKAGE_SOURCE_DIR=LibreOffice*rpm*langpack*




# Download required sources to build the package.
# If a custom download code is required (for example for svn/cvs)
# then make sure to copy the source package to '$SOURCE_PATH'.
# Use return code '0' for 'Download OK' or '1' for 'Download failed'.
DOWNLOAD_SOURCE_PACKAGE() {
  # This is the default download function that should
  # work for 99.9% of all packages.
  BUILDTOOL_DOWNLOAD_STD
  # Sometimes you may need to give some extra options.
  # This is also being used when download a github tarball.
  #BUILDTOOL_DOWNLOAD_STD "-O ${SOURCE_PATH}/${PACKAGE_SOURCE[0]}"
  # Sometimes default wget does not work, use a custom command.
  #wget ${DEFAULT_WGET_OPTIONS} \
  #     -O ${SOURCE_PATH}/${PACKAGE_SOURCE[0]} \
  #     -P ${SOURCE_PATH} \
  #     ${PACKAGE_URL[${url}]} 2>&1
  # Return the result of the download function.
  return $?
}




# Initialize install directories.
BUILDTOOL_INIT_DEFAULTS "opt/$PACKAGE_NAME" "etc"

# Name of the package to build
SLACKWARE_PACKAGE=$PACKAGE_NAME-${PACKAGE_LANG//@/_}-$PACKAGE_VERSION-$PACKAGE_ARCH-$PACKAGE_BUILD$PACKAGE_TAG.$PACKAGE_EXTENSION




# Let the user know what we are going to do.
echo "Compiling package $PACKAGE_NAME..."

# Create logfiles and build directories.
BUILDTOOL_CREATE_BUILD_ENVIRONMENT

# Extract the source package.
BUILDTOOL_UNPACK_SOURCES

# Change to source directory
if ! [ -d $PACKAGE_SOURCE_DIR ] ; then
  echo "*** Source directory $PACKAGE_SOURCE_DIR not found! ***"
  echo
  exit 1
fi
cd $PACKAGE_SOURCE_DIR

# Convert those "installer"-files from rpm to tgz so
# we can handle the files
echo -n "      - Converting distribution files... "
( cd RPMS
  rm -f jre*.rpm
  for a in $(find ./ -type f -maxdepth 1 -name "*.rpm"); do
    rpm2tgz "$a" 1>>$LOG_STAGE1 2>>$LOG_STAGE1 || exit 1
    rm -f "$a"
  done
) || ( echo "failed!"; exit 1 ) || exit 1
echo "OK"

# ...and now extract those archives
echo -n "      - Extracting archives... "
( cd RPMS
  for a in $(find ./ -type f -maxdepth 1 -name "*.tgz"); do
    tar -xzf "$a" 1>>$LOG_STAGE1 2>>$LOG_STAGE1 || exit 1
    rm -f "$a"
  done
) || ( echo "failed!"; exit 1 ) || exit 1
echo "OK"

# Back to $BUILD_SOURCE_DIR
cd ..

# Set source file permissions.
BUILDTOOL_PREPARE_SOURCES




# Move the files to correct location
cat $CWD/slack-desc | sed "s,-@@,-$PACKAGE_LANG,g" >$BUILD_SOURCE_DIR/slack-desc
( mkdir -p $BUILD_PACKAGE_DIR/
  cd RPMS/
  cp -aR . $BUILD_PACKAGE_DIR/
)

echo "Done!"




# Prepare to build the package
echo "Building package..."

# Create the package.
BUILDTOOL_MAKEPKG_OPTIONS="-p"
BUILDTOOL_SLACKDESC_FILE="$BUILD_SOURCE_DIR/slack-desc"
BUILDTOOL_MAKE_PACKAGE

# Remove temporary stuff if requested.
BUILDTOOL_FINALIZE

# Package created.
echo "Done"; echo

# All done.
exit 0

