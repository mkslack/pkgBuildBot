
# Path to system-wide default mime applications list
MIMEAPPS=usr/share/applications/mimeapps.list

# Delete existing default application and
# set new default application
MIME_ENTRY_SET() {
  sed -i "\%$1% d" $MIMEAPPS
  cat <<EOF >>$MIMEAPPS
$1=$2
EOF
}

# Create empty mimeapps list if not already there...
if ! test -e $MIMEAPPS; then
  cat <<EOF >$MIMEAPPS
[Default Applications]
EOF
fi

# Set firefox as default for various mime types:
MIME_ENTRY_SET "text/html" mozilla-firefox.desktop
MIME_ENTRY_SET "x-scheme-handler/http" mozilla-firefox.desktop
MIME_ENTRY_SET "x-scheme-handler/https" mozilla-firefox.desktop

