
#BLURB="Run fc-cache to locate new fonts for Xft"
if [ -x usr/bin/fc-cache ]; then
  echo "Please wait while we generate font.cache-1 files with fc-cache."
  echo "For best results, fc-cache should be run whenever fonts are added"
  echo "to the system."
  sbin/ldconfig 1> /dev/null 2> /dev/null
  usr/bin/fc-cache -f 1>/dev/null 2>/dev/null
fi

