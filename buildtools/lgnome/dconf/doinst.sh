# Do not use locking or the lock file
# for pkgtools/installpkg will remain
# after stop/start messagebus and will
# block installpkg from executing the
# install script for the next package.
# NOLOCK

# Reload messagebus service
if [ -x /etc/rc.d/rc.messagebus ]; then
  # Reloading messagebus does not work if
  # dbus was updated but not restarted.
  # sh /etc/rc.d/rc.messagebus reload
  # Stop and Start the messagebus instead to
  # reload dconf service files.
  sh /etc/rc.d/rc.messagebus stop
  sh /etc/rc.d/rc.messagebus start
fi

