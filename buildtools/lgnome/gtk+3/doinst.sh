
# Place init and config files.
config_note=false
config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
  if [ -e $NEW ]; then
    if ! $config_note; then
      echo "Will not replace existing configuration files!"
      config_note=true
    fi
    echo "  => $NEW"
  fi
}

config etc/gtk-3.0/im-multipress.conf.new

# Update immodules cache.
mkdir -p etc/gtk-3.0
gtk-query-immodules-3.0  > etc/gtk-3.0/gtk.immodules
chown -R root.root etc/gtk-3.0
chmod 644 etc/gtk-3.0/*

