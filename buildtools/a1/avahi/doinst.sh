
# Place init and config files.
config_note=false
config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
  if [ -e $NEW ]; then
    if ! $config_note; then
      echo "Will not replace existing configuration files!"
      config_note=true
    fi
    echo "  => $NEW"
  fi
}
config etc/avahi/avahi-daemon.conf.new


# Create avahi' user and group if they do not exist.
group_exists=$(grep ^avahi etc/group)
if [ x"${group_exists}" == x"" ]; then
  if ! ( groupadd -g 70 avahi ); then
    echo "Warning: Could not add group 'avahi'!"
  fi
fi
group_exists=$(grep ^netdev etc/group)
if [ x"${group_exists}" == x"" ]; then
  if ! ( groupadd -g 71 netdev ); then
    echo "Warning: Could not add group 'netdev'!"
  fi
fi
user_exists=$(grep ^avahi etc/passwd)
if [ x"${user_exists}" == x"" ]; then
  if ! ( useradd -c "Avahi Daemon User" -s /bin/false -d /dev/null -u 70 -g avahi avahi ); then
    echo "Warning: Could not add user 'avahi'!"
  fi
fi


# Use rc.local to start avahi at boot.
if [ ! -e etc/rc.d/rc.local ]; then
	echo "#!/bin/sh" > etc/rc.d/rc.local
	chmod 755 etc/rc.d/rc.local
fi

run=$(grep "/etc/rc.d/rc.avahidaemon" etc/rc.d/rc.local)
if [[ "${run}" == "" ]]; then
  cat <<EOF >>etc/rc.d/rc.local

# Start AVAHI services
if [ -x /etc/rc.d/rc.avahidaemon ]; then
  /etc/rc.d/rc.avahidaemon start
fi

if [ -x /etc/rc.d/rc.avahidnsconfd ]; then
  /etc/rc.d/rc.avahidnsconfd start
fi

EOF
fi

