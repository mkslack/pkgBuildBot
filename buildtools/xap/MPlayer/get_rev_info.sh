#!/bin/sh
CWD=$(pwd)

# This is the username for the github project.
GITPROJECT=FFmpeg
# This is the repository name on github.
GITNAME=FFmpeg

# Get MPlayer rev and date:
# MPLAYER_SVN=`LANG=C svn info svn://svn.mplayerhq.hu/mplayer/trunk | grep "Last Changed" | grep -v "Author" | sed "s,.*: ,,g" | sed "s, .*,,g" | sed "s,-,,g" | sed "s,^20,,g"`
# Get MPlayer date only:
MPLAYER_DATE=`LANG=C svn info svn://svn.mplayerhq.hu/mplayer/trunk | grep "Last Changed Date" | sed "s,.*: ,,g" | sed "s, .*,,g" | sed "s,-,,g" | sed "s,^20,,g"`
# Get MPlayer rev only:
MPLAYER_SVN=`LANG=C svn info svn://svn.mplayerhq.hu/mplayer/trunk | grep "Last Changed Rev" | sed "s,.*: ,,g" | sed "s, .*,,g"`

# Get FFmpeg revision.
wget -O ${CWD}/tmp -q https://github.com/${GITPROJECT}/${GITNAME}/commits/master
FFMPEG_GIT=`grep -m1 "a href=.*/commit/" ${CWD}/tmp | sed "s,.*/commit/,,g" | sed "s,\".*,,g"`

rm ${CWD}/tmp

# Display or Update?
BUILDTOOL=`find . -type f -maxdepth 1 -name "*.buildtool" -printf "%f\n" 2>/dev/null | grep -m1 "."`
if test x"$BUILDTOOL" == x""; then
  echo "Buildtool not found!"
  exit 1
fi
if test x"$1" == x"--version" -o x"$1" == x"-v"; then
  echo "$MPLAYER_DATE"
  echo "$MPLAYER_SVN"
  echo "$FFMPEG_GIT"
else
  if test x"`grep -e \"^PACKAGE_RELEASE_REV_GIT=$GIT_REV\" $BUILDTOOL`" == x""; then
    sed -i -e "s,^PACKAGE_RELEASE_REV_FFMPEG=.*,PACKAGE_RELEASE_REV_FFMPEG=$FFMPEG_GIT," \
           -e "s,^PACKAGE_RELEASE_TAG=.*,PACKAGE_RELEASE_TAG=svn$MPLAYER_SVN," \
           $BUILDTOOL
    echo "Buildtool updated:"
    echo "  MPlayer : $MPLAYER_SVN"
    echo "  Updated : $MPLAYER_DATE"
    echo "  FFMPEG  : $FFMPEG_GIT"
    echo "Download new source package?"
    echo "Hit 'CTRL+C' to cancel, press <ENTER> to download."
    read key
    sh $BUILDTOOL -n
  else
    echo "Nothing to update ($GIT_DATE)."
  fi
fi
