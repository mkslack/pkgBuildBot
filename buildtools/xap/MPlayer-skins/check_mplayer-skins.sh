#!/bin/sh
found=false

# List of skin packages included in the current buildtool.
SKIN_BT=$(pwd)/files/README.skins

# Get list of available skin packages.
SKINS=`wget -O - http://www.mplayerhq.hu/MPlayer/skins/ 2>/dev/null | grep ".tar.bz2\"" | sed -e "s,</a>.*,,g" -e "s,.*>,,g"`

# Name of the buildtool.
BUILDTOOL=`find . -type f -maxdepth 1 -name "*.buildtool" -printf "%f\n" 2>/dev/null | grep -m1 "."`
if test x"${BUILDTOOL}" == x""; then
  echo "Buildtool not found!"
  exit 1
fi

# Check for new/updated packages.
for pkgsrc in ${SKINS}; do
  # Do we have the package allready inlcuded in the buildtool?
  check=`grep "^  ${pkgsrc%.tar.bz2}$" ${SKIN_BT}`
  if [ x"${check}" == x"" ]; then
    # No, check package version.
    pkgname=${pkgsrc%-*}
    check=`grep "^  ${pkgname}-[[:digit:]]" ${SKIN_BT} | grep -v "^  ${pkgsrc%.tar.bz2}$" | sed "s,^ *,,g"`
    if [ x"${check}" != x"" ]; then
      # Get the version of the online source package.
      pkg_online=${pkgsrc##*-}
      pkg_online=${pkg_online%.tar.bz2}
      PACKAGE_VERSION_MAJOR=$(echo ${pkg_online##*-} | awk -F. '{ print $1 }')
      PACKAGE_VERSION_MAJOR=${PACKAGE_VERSION_MAJOR:-0}
      PACKAGE_VERSION_MINOR=$(echo ${pkg_online##*-} | awk -F. '{ print $2 }')
      PACKAGE_VERSION_MINOR=${PACKAGE_VERSION_MINOR:-0}
      PACKAGE_VERSION_MICRO=$(echo ${pkg_online##*-} | awk -F. '{ print $3 }')
      PACKAGE_VERSION_MICRO=${PACKAGE_VERSION_MICRO:-0}
      PACKAGE_VERSION_NANO=$( echo ${pkg_online##*-} | awk -F. '{ print $4 }')
      PACKAGE_VERSION_NANO=${PACKAGE_VERSION_NANO:-0}
      let PACKAGE_VERSION_ONLINE=PACKAGE_VERSION_MAJOR*1000000+PACKAGE_VERSION_MINOR*10000+PACKAGE_VERSION_MICRO*100+PACKAGE_VERSION_NANO

      # Get the version of the included source package.
      pkg_buildtool=${check##*-}
      PACKAGE_VERSION_MAJOR_BUILDTOOL=$(echo ${pkg_buildtool##*-} | awk -F. '{ print $1 }')
      PACKAGE_VERSION_MAJOR_BUILDTOOL=${PACKAGE_VERSION_MAJOR_BUILDTOOL:-0}
      PACKAGE_VERSION_MINOR_BUILDTOOL=$(echo ${pkg_buildtool##*-} | awk -F. '{ print $2 }')
      PACKAGE_VERSION_MINOR_BUILDTOOL=${PACKAGE_VERSION_MINOR_BUILDTOOL:-0}
      PACKAGE_VERSION_MICRO_BUILDTOOL=$(echo ${pkg_buildtool##*-} | awk -F. '{ print $3 }')
      PACKAGE_VERSION_MICRO_BUILDTOOL=${PACKAGE_VERSION_MICRO_BUILDTOOL:-0}
      PACKAGE_VERSION_NANO_BUILDTOOL=$( echo ${pkg_buildtool##*-} | awk -F. '{ print $4 }')
      PACKAGE_VERSION_NANO_BUILDTOOL=${PACKAGE_VERSION_NANO_BUILDTOOL:-0}
      let PACKAGE_VERSION_BUILDTOOL=PACKAGE_VERSION_MAJOR_BUILDTOOL*1000000+PACKAGE_VERSION_MINOR_BUILDTOOL*10000+PACKAGE_VERSION_MICRO_BUILDTOOL*100+PACKAGE_VERSION_NANO_BUILDTOOL

      # New package version found?
      if [ ${PACKAGE_VERSION_BUILDTOOL} -ge ${PACKAGE_VERSION_ONLINE} ]; then
        true
      else
        echo " -> New MPlayer skin version     : ${pkgsrc%.tar.bz2}"
        sed -i "s,^  ${check}$,  ${pkgsrc%.tar.bz2}," ${SKIN_BT}
        found=true
      fi
    else
      echo   " -> New MPlayer skin package     : ${pkgsrc%.tar.bz2}"
      echo   "  ${pkgsrc%.tar.bz2}" >>${SKIN_BT}
      found=true
    fi
  fi
done

# Check for removed MPlayer skin packages.
for pkgsrc in `grep "^  " ${SKIN_BT} | sed -e "s,^ *,,g"`; do
  pkg_buildtool=${pkgsrc%-*}
  removed=true
  for check in ${SKINS}; do
    pkg_online=${check%.tar.bz2}
    pkg_online=${pkg_online%-*}
    if [ x"${pkg_buildtool}" == x"${pkg_online}" ]; then
      removed=false
      break
    fi
  done
  if ${removed:-false}; then
    echo " -> Removed MPlayer skin package : ${pkgsrc}"
    sed -i "/^  ${pkgsrc}$/ d" ${SKIN_BT}
    found=true
  fi
done

# Any new/updated packages found?
if ! ${found:-false}; then
  echo "Nothing changed!"
else
  echo ""
  echo "List of MPlayer skins updated."
  echo "Download new source package?"
  echo "Hit 'CTRL+C' to cancel, press <ENTER> to download."
  read key
  sh $BUILDTOOL --purgesrc
  sh $BUILDTOOL -n
fi
