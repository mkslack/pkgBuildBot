#!/bin/sh


TMPFILE=skins
wget http://xinehq.de/index.php/$TMPFILE
SKINS=$(grep "force-download" $TMPFILE | sed "s,.*/index.php,http://xinehq.de/index.php,g" | sed "s,tar.gz.*,tar.gz,g" | grep -v "xinetic")
rm -f $TMPFILE

if [ x"" != x"$SKINS" ]; then
(
  cd ..
  CWD=$(pwd)
  TMP=$CWD/files_other/skins

  rm -rf $TMP
  mkdir -p $TMP

  echo "Downloading skins..."
  (
    cd $TMP
    for a in $SKINS; do
      wget $a
    done
  )
  echo "done!"
  exit 0
)
else
  echo "Cannot download extra skins, don't know why..."
  exit 1
fi
