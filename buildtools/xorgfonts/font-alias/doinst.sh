
# Create the encodings file for the /usr/share/fonts/misc.
if [ x"$(which mkfontdir 2>/dev/null)" != x"" -a x"$(which mkfontscale 2>/dev/null)" != x"" ]; then
  # The following commands need absolute paths.
  # usr/share/font/misc will end in an error:
  #   opendir: file or directory not found
  mkfontscale /usr/share/fonts/misc
  mkfontdir -e /usr/share/fonts/encodings -e /usr/share/fonts/encodings/large /usr/share/fonts/misc
fi

