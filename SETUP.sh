#!/bin/sh
##
##  pkgBuildBot is a buildsystem to create packages for Slackware Linux.
##
##  Copyright (C) 2005-2020  Markus Kanet
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.
##
##  This script requires a few configuration files:
##   packages.conf / packages.conf.orig
##     This file contain the buildorder of the packages. If you want to skip
##     some packages or want to add your own packages, you should edit that
##     file. You can overwrite the defaults using packages.conf.overwrite in
##     the root directory, it will use the same syntax for each package:
##       group/package
##   packages.conf.options
##     Available options to modify packages.conf 'on-the-fly'. This can be
##     used to build the KDE5 desktop environment.
##   system.conf / system.conf.orig
##     This file contain the configuration for the buildsystem.
##   single.conf / single.conf.orig
##     This file contains a list of packages that will be auto-selected
##     when using the "SINGLE" build mode. This might be helpful if someone
##     want to build a pre-defined list of packages.
##   remove.conf / remove.conf.orig
##     This file includes extra package names to be removed.
##   includes/*.conf
##     Configure the buildsystem to build and install selected packages only.
##     Read the 'README' file in the include directory for details.
##
##  This script also requires a bunch of include files that provide functions
##  used in this script.
##

# Define runtime options.
CONFIGURE_DEFINE_RUNTIME_SETTINGS(){

	# Define system root directory.
	SYSTEM_ROOT="$(pwd)"

	# Check VERSION files.
	if test ! -e "${SYSTEM_ROOT}/VERSION" \
		-o  ! -e "${SYSTEM_ROOT}/VERSION_BUILD" \
		-o  ! -e "${SYSTEM_ROOT}/VERSION_RELEASE"; then
			echo
			echo "Error: Cannot find version data files!"
			echo "       (${SYSTEM_ROOT}/VERSION*)"
			echo
			return 1
	fi

	# Set the screen title
	PROJECT_EMAIL="darkvision(at)gmx(dot)eu"
	PROJECT_VERSION="$(cat VERSION)-$(cat VERSION_BUILD)"
	PROJECT_RELEASE="$(cat VERSION_RELEASE)"
	PROJECT_SCRIPT_NAME="$(basename $0)"
	PROJECT_NAME="pkgBuildBot"
	PROJECT_BACKTITLE="${PROJECT_NAME} ${PROJECT_VERSION} '${PROJECT_RELEASE}'"
	PROJECT_ABOUT="\n\
	${PROJECT_NAME} - Copyright (C) 2005 - 2018 by Markus Kanet\n\
	A script to install/compile applications from source.\n\
	\n\
	This program comes with ABSOLUTELY NO WARRANTY.\n\
	This is free software, and you are welcome to redistribute\n\
	it under certain conditions; see LICENSE for details."

	# Set local language to "C" since we catch up some messages.
	export LC_ALL="C"

	# Enable debug mode?
	if test -e "${SYSTEM_ROOT}/__DEBUG__"; then
		RUNTIME_OPTION_DEBUG_MODE=true
		# Disable screen blanking only in debug mode.
		TERM=linux setterm -blank 0
	else
		RUNTIME_OPTION_DEBUG_MODE=false
	fi

	# Define core system directories.
	SYSTEM_CONFIG="${SYSTEM_ROOT}/config"
	SYSTEM_PATH_TEMP="${SYSTEM_ROOT}/temp"
	SYSTEM_CONFIG_CHECKED=false

	# Define system functions file.
	SYSTEM_FUNCTIONS_FILE="functions"

	# Define extra system directories and configuration files.
	SYSTEM_CONFIG_BUILDTOOLS="${SYSTEM_CONFIG}/packages.conf"
	SYSTEM_CONFIG_BUILDTOOLS_OPTIONS="${SYSTEM_CONFIG}/packages.conf.options"
	SYSTEM_CONFIG_SINGLE_MODE="${SYSTEM_CONFIG}/single.conf"
	SYSTEM_CONFIG_REMOVE="${SYSTEM_CONFIG}/remove.conf"
	SYSTEM_CONFIG_DOWNGRADE_WHITELIST="${SYSTEM_CONFIG}/packages.conf.downgrade_whitelist"
	SYSTEM_CONFIG_PACKAGE_BLACKLIST="${SYSTEM_CONFIG}/packages.conf.blacklist"
	SYSTEM_CONFIG_BUILDTOOLS_CACHE="${SYSTEM_PATH_TEMP}/packages.conf.cache"
	SYSTEM_CONFIG_BUILDTOOLS_FILTER="${SYSTEM_PATH_TEMP}/packages.conf.filter"
	SYSTEM_CONFIG_BUILDTOOLS_TEMP="${SYSTEM_PATH_TEMP}/packages.conf.temp"
	SYSTEM_CONFIG_PACKAGE_OPTIONS="${SYSTEM_PATH_TEMP}/package.options"
	SYSTEM_CONFIG_PACKAGE_OPTDATA="${SYSTEM_PATH_TEMP}/package.optdata"
	SYSTEM_CONFIG_OVERWRITE="${SYSTEM_CONFIG}/packages.conf.overwrite"
	SYSTEM_CONFIG_SETTINGS="${SYSTEM_CONFIG}/system.conf"
	SYSTEM_CONFIG_PROFILE="/etc/profile.d/buildtool.sh"
	SYSTEM_CONFIG_ARCHIVE_URL=https://gitlab.com/mkslack/archive/-/raw/master/buildtools
	SYSTEM_CONFIG_LATEST_URL=http://mkslack.gitlab.io/html/archive/LATEST

	SYSTEM_PATH_BUILDTOOLS="${SYSTEM_ROOT}/buildtools"
	SYSTEM_PATH_SOURCES="${SYSTEM_ROOT}/sources"
	SYSTEM_PATH_DOWNLOADS="${SYSTEM_ROOT}/downloads"
	SYSTEM_PATH_PACKAGES="${SYSTEM_PATH_TEMP}/packages"
	SYSTEM_PATH_PACKAGES_DATA="${SYSTEM_PATH_PACKAGES}/packages.orig.dat"
	SYSTEM_PATH_GROUP_EXTRAS_CACHE="${SYSTEM_PATH_TEMP}/group-extras.cache"
	SYSTEM_PATH_INCLUDES_CONFIG="${SYSTEM_ROOT}/includes"
	SYSTEM_PATH_INCLUDES_CONFIG_COPY="${SYSTEM_PATH_TEMP}/includes.conf.copy"
	SYSTEM_PATH_LOGFILES_SYSTEM="${SYSTEM_PATH_TEMP}/log.system"
	SYSTEM_PATH_LOGFILES_BUILDTOOLS="${SYSTEM_PATH_TEMP}/log.buildtools"
	SYSTEM_PATH_PKGDIR=""
	SYSTEM_PATH_TEMP_PACKAGE="${SYSTEM_PATH_TEMP}/final_package"
	SYSTEM_PATH_TEMP_BUILD="${SYSTEM_PATH_TEMP}/build_package"
	SYSTEM_PATH_BTARCHIVE="${SYSTEM_ROOT}/docs/BTARCHIVE.TXT"
	SYSTEM_PATH_RBUILDER="${SYSTEM_PATH_TEMP}/slack-required"
	SYSTEM_PATH_CACHE="${SYSTEM_ROOT}/cache"

	SYSTEM_DEBUG_LOGFILE_PLATFORM="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.debug.platform"
	SYSTEM_DEBUG_LOGFILE_BUILDORDER="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.debug.packages_buildorder"
	SYSTEM_DEBUG_LOGFILE_INSTALLED="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.debug.packages_installed"
	SYSTEM_DEBUG_LOGFILE_INSTALLED_STAGE1="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.debug.packages_installed.stage1"
	SYSTEM_DEBUG_LOGFILE_INSTALLED_STAGE2="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.debug.packages_installed.stage2"
	SYSTEM_DEBUG_LOGFILE_INSTALLED_STAGE3="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.debug.packages_installed.stage3"
	SYSTEM_DEBUG_LOGFILE_BLACKLISTED="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.debug.packages_blacklisted"
	SYSTEM_DEBUG_LOGFILE_DEPOPT="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.debug.depopt_packages"
	SYSTEM_DEBUG_LOGFILE_DEPOPT_INFO="${SYSTEM_PATH_TEMP}/log.debug.depopt_info"
	SYSTEM_DEBUG_LOGFILE_DEPOPT_CHECK="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.debug.depopt_check"
	SYSTEM_DEBUG_LOGFILE_DEPOPT_MISSING="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.debug.depopt_missing"
	SYSTEM_DEBUG_LOGFILE_SKIPPED_PACKAGES="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.debug.skipped_packages"
	SYSTEM_DEBUG_LOGFILE_MISSING_EXTRAS="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.debug.missing_extra_packages"
	SYSTEM_DEBUG_LOGFILE_REMAINING_XORG_PACKAGES="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.debug.xorg_removed_packages"
	SYSTEM_DEBUG_LOGFILE_REMOVED_XORG_DRIVERS="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.debug.xorg_skipped_drivers"

	# Job files.
	SYSTEM_JOB_SETTINGS="${SYSTEM_PATH_TEMP}/job.settings"
	SYSTEM_JOB_PACKAGES="${SYSTEM_PATH_TEMP}/job.packages"

	# Build files.
	SYSTEM_BUILD_FAILED="${SYSTEM_PATH_TEMP}/job.build-failed"
	SYSTEM_BUILD_INSTALL_PACKAGE="${SYSTEM_PATH_TEMP}/job.install.package"
	SYSTEM_BUILD_INSTALL_PACKAGE_ERRORS="${SYSTEM_PATH_TEMP}/job.install.package-error"
	SYSTEM_BUILD_LOGFILE_TEMP1="${SYSTEM_PATH_LOGFILES_SYSTEM}/job.package.info"
	SYSTEM_BUILD_LOGFILE_TEMP2="${SYSTEM_PATH_LOGFILES_SYSTEM}/job.package.error"

	# Buildsystem logfiles.
	SYSTEM_LOGFILE_SETTINGS="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.build.settings"
	SYSTEM_LOGFILE_PKG_CONF_OVERWRITE="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.buildsys.package_conf_overwrite"
	SYSTEM_LOGFILE_DATABASE="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.buildsys.database.csv"
	SYSTEM_LOGFILE_BUILDSYS_SETTINGS="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.buildsys.settings"

	# Info logfiles.
	SYSTEM_LOGFILE_AUTOSELECT="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.info.pkg-autoselected"
	SYSTEM_LOGFILE_USERSELECT="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.info.pkg-userselected"
	SYSTEM_LOGFILE_USERSELECT_RUNTIME="${SYSTEM_LOGFILE_USERSELECT}.$(date +%s)"
	SYSTEM_LOGFILE_BUILDTIME="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.info.buildtime"
	SYSTEM_LOGFILE_MD5SUM="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.info.md5"
	SYSTEM_LOGFILE_SOURCES="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.info.sources"
	SYSTEM_LOGFILE_DOWNLOADS="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.info.downloads"
	SYSTEM_LOGFILE_DOWNLOAD_ERROR="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.warning.download-errors"
	SYSTEM_LOGFILE_REMOVED="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.info.removepkg"
	SYSTEM_LOGFILE_DOWNGRADES="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.info.downgrades"
	SYSTEM_LOGFILE_REMOVED_SOURCES="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.info.removed_sources"
	SYSTEM_LOGFILE_NEW_PACKAGES="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.info.new_packages"
	SYSTEM_LOGFILE_PACKAGE_UPDATES="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.info.updates"
	SYSTEM_LOGFILE_DATA_UPDATE="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.info.data_updates"
	SYSTEM_LOGFILE_SKIPPED_UPDATES="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.info.skipped_updates"

	# Logfiles used by buildtools.
	SYSTEM_LOGFILE_BUILDARCH_WARNINGS="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.build.buildarch-warnings"
	SYSTEM_LOGFILE_SUID_WARNINGS="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.build.suid-warnings"
	SYSTEM_LOGFILE_RPATH_WARNINGS="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.build.rpath-warnings"
	SYSTEM_LOGFILE_SHAREDLIBS_WARNINGS="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.build.sharedlibs-warnings"
	SYSTEM_LOGFILE_EMPTYDIR_WARNINGS="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.build.emptydir-warnings"
	SYSTEM_LOGFILE_DOINST_WARNINGS="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.build.doinst-warnings"
	SYSTEM_LOGFILE_DOCFILES_WARNINGS="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.build.docfile-warnings"
	SYSTEM_LOGFILE_USRLOCAL_WARNINGS="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.build.usrlocal-warnings"
	SYSTEM_LOGFILE_OVERWRITE_FILES_WARNINGS="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.build.overwrite_files-warnings"
	SYSTEM_LOGFILE_APPLIED_PATCHES="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.build.applied-patches"
	SYSTEM_LOGFILE_GTK_ICON_CACHE="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.build.gtk_icon_cache"
	SYSTEM_LOGFILE_RM_LA_FILES="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.build.removed_la_files"
	SYSTEM_LOGFILE_CHECK_LA_FILES="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.build.remaining_la_files"
	SYSTEM_LOGFILE_BUILDSYS_MASTER="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.build.master"
	SYSTEM_LOGFILE_BUILDSYS_CONFIG="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.build.config"
	SYSTEM_LOGFILE_BUILDSYS_ERROR="${SYSTEM_PATH_LOGFILES_SYSTEM}/log.build.error"

	# Package counters.
	SYSDB_PACKAGE_COUNT=0
	SYSDB_PACKAGE_COUNT_ENEBALED=0
	SYSDB_PACKAGE_LIST_COUNT=0

	# Dialog variables.
	DIALOG_KEYCODE=0
	DIALOG_REPLY=""
	DIALOG_SCRIPT="${SYSTEM_PATH_TEMP}/DIALOG_SCRIPT"
	DIALOG_RETURN_VALUE="${SYSTEM_PATH_TEMP}/DIALOG_RETURN_VALUE"
	DIALOG_TEMP_FILE1="${SYSTEM_PATH_TEMP}/dialog_temp_file1"
	DIALOG_TEMP_FILE2="${SYSTEM_PATH_TEMP}/dialog_temp_file2"

	# Some rulers...
	RULER1="------------------------------------------------------------------------------"
	RULER2="---------------------------------------------------------------------"
	SPACEBAR="                                                            "
	SPACER=""

	# Reset runtime settings.
	RUNTIME_SELECT_SOURCE_PACKAGE_MODE="ASK"
	RUNTIME_SYSTEM_RESTART=false
	RUNTIME_SYSTEM_USERMODE=false
	RUNTIME_SYSTEM_CUSTOM_CONFIG=false
	RUNTIME_PACKAGE_CONFIG_FILE="${SYSTEM_CONFIG_BUILDTOOLS}"
	RUNTIME_CLI_MODE=false
	RUNTIME_CLI_OPTION=0
	RUNTIME_CLI_PARAMETER=""
	RUNTIME_SCRIPT_MODE=false
	RUNTIME_SETUP_MODE=""
	RUNTIME_MAIN_MENU_OPTION=""
	RUNTIME_PACKAGE_CODE_CACHE=""
	RUNTIME_PACKAGES_COMPILED=0
	RUNTIME_MAKEOPTS=""
	RUNTIME_DOWNLOAD_ALL_SOURCES_QUIET_MODE=false
	RUNTIME_DEPOPT_STAGE=0
	RUNTIME_QUICK_RESUME=false
	RUNTIME_DOWNLOAD_SINGLE_MODE_ONLY=false
	RUNTIME_REMOVE_EXTRA_PACKAGES=""
	RUNTIME_USE_INCLUDE_CONF_FILES_OVERWRITE=""
	RUNTIME_BUILDTOOL_UPDATE=false

	# If number of threads is >15 reset to auto.
	# Just to make sure there is a valid setting.
	# Note: When using distcc threads could be more then 15.
	#MAX_COMPILE_JOBS=15
	MAX_COMPILE_JOBS=50

	# Counter for missing source packages.
	RUNTIME_MISSING_SOURCE_PACKAGES=0
	RUNTIME_UPDATE_PACKAGES=0
	RUNTIME_INSTALL_PACKAGES=0

	# Build mode.
	BUILDSYS_INSTALL_GROUP=""
	BUILDSYS_INSTALL_MODE=""
	BUILDSYS_INSTALL_USER=""
	BUILDSYS_INSTALL_EVERYTHING=false

	# GTK icon cache check.
	BUILDSYS_GTK_ICON_CACHE="ask"

	# Temporary variables.
	NUMPKG_PACKAGE_VERSION=0
	NUMPKG_INSTALLED_VERSION=0
	PACKAGE_INSTALLED_VERSION=""
	RETURN_VALUE=""
	RETURN_VERSION=0
	RETURN_VERSION_MAJOR=0
	RETURN_VERSION_MINOR=0

	SELECTED_PACKAGES_ETA=0
	SELECTED_PACKAGES_ETA_REMAINING=0
	BUILDTIME_SECONDS_ALL=100
	BUILDTIME_SECONDS_ALL_LASTRUN=100

	# Runtime variables for the config/profile editor.
	RUNTIME_EDITOR_OPTION_TYPE=""
	RUNTIME_EDITOR_OPTION_LABEL=""
	RUNTIME_EDITOR_OPTION_NAME=""
	RUNTIME_EDITOR_OPTION_MODES=""
	RUNTIME_EDITOR_CONFIG_UPDATED=false
	RUNTIME_EDITOR_HELP_TEXT=""

	# XOrg releated runtime settings.
	RUNTIME_XORG_PACKAGES=""
	RUNTIME_XORG_PACKAGE_INFO=""

	# Create required directories.
	mkdir -p ${SYSTEM_PATH_TEMP}
	mkdir -p ${SYSTEM_PATH_LOGFILES_SYSTEM}
	mkdir -p ${SYSTEM_PATH_LOGFILES_BUILDTOOLS}
	mkdir -p ${SYSTEM_PATH_DOWNLOADS}
	mkdir -p ${SYSTEM_PATH_SOURCES}
	mkdir -p ${SYSTEM_PATH_INCLUDES_CONFIG}
	mkdir -p ${SYSTEM_PATH_PACKAGES}

	# Overwrite button labels.
	BUTTON_LABEL_YES="Yes"
	BUTTON_LABEL_NO="No"
	BUTTON_LABEL_OK="OK"
	BUTTON_LABEL_CANCEL="Cancel"
	BUTTON_LABEL_HELP="Help"
	BUTTON_LABEL_INFO="Info"
	BUTTON_LABEL_CONTINUE="Continue"
	BUTTON_LABEL_UPDATE="Update"
	BUTTON_LABEL_EXIT="Exit"
	BUTTON_LABEL_EDIT="Edit"
	BUTTON_LABEL_OPTIONS="Options"
	BUTTON_LABEL_DONE="Done"
	BUTTON_LABEL_GOBACK="Go Back"
	BUTTON_LABEL_RESTART="Restart"
	BUTTON_LABEL_EVERYTHING="All"
	BUTTON_LABEL_NOTHING="None"
	BUTTON_LABEL_SKIP="Skip"
	BUTTON_LABEL_DEFAULT="Default"
	BUTTON_LABEL_REMOVE="Remove"
	BUTTON_LABEL_SELECT="Select"

	# Check for supported platforms.
	OS_PLATFORM=""
	OS_TYPE=""
	OS_VERSION=""
	OS_VERSION_CODE=""
	OS_ARCH=""
	if [ -e /etc/slackware-version ]; then
		if [ x"$(uname -m)" == x"x86_64" ]; then
			OS_PLATFORM="Slackware64"
			OS_TYPE="SLACK"
			OS_VERSION=$(cat /etc/slackware-version | sed "s,.* ,,g")
		else
			OS_PLATFORM="Slackware"
			OS_TYPE="SLACK"
			OS_VERSION=$(cat /etc/slackware-version | sed "s,.* ,,g")
		fi
	else
		echo " => Failed... Unknwon platform!"
		echo "    Only Slackware Linux ist supported."
		return 1
	fi

	# Define library directory and system platform.
	case $(uname -m) in
		x86_64)	SYSTEM_LIBDIR=/usr/lib64
				OS_ARCH="x86_64"
				# We need to export SLACK_ARCH on x86_64 to get
				# correct package_arch when loading package configuration.
				export SLACK_ARCH=${OS_ARCH}
				;;
		*)		SYSTEM_LIBDIR=/usr/lib
				OS_ARCH=${SLACK_ARCH:-"i586"}
				# Do not export SLACK_ARCH since this will overwrite the buildtool settings.
				# For example xine-lib is for i686, not i486 or i586.
				#export SLACK_ARCH=${OS_ARCH}
				;;
	esac

	# Check for supported versions.
	case ${OS_VERSION} in
		#14.2)		OS_VERSION_CODE="142" ;; # Untested
					# After latest update to -current on 2018-04-18 buildtools
					# which do remove *.la files might break packages from
					# the old 14.2 original package tree. This will make 14.2
					# an unsupported platform for now.
		14.2+)		OS_VERSION_CODE="142+";; # -current
		15.0)		OS_VERSION_CODE="150" ;; # Upcoming stable release
		*)			OS_VERSION_CODE="?"   ;; # Unsupported
	esac

	# Platform supoorted?
	if ! ${RUNTIME_OPTION_DEBUG_MODE}; then
		if [ x"${OS_VERSION_CODE}" == x"?" ]; then
			echo " => Failed... Unsupported platform version!"
			echo "    Exiting now!"
			return 1
		fi
	fi

	# Define Platform code like Slackware64_130.
	OS_PLATFORM_CODE="${OS_PLATFORM}_${OS_VERSION_CODE}"

	# Set default for QTDIR.
	QT5DIR=${SYSTEM_LIBDIR}/qt5
	QT4DIR=${SYSTEM_LIBDIR}/qt4
	QTDIR=${QT4DIR}

	# Set default value for KDEDIR.
	KDEDIR=/usr
	KDEDIRS=/usr

	# Set default for package extension.
	SYSTEM_PACKAGE_EXTENSION="tgz"
	if test -e /sbin/makepkg; then
		if [ x"$(grep EXTENSION.*txz /sbin/makepkg)" != x"" ]; then
			# Slackware >= 13.0 uses txz by default.
			SYSTEM_PACKAGE_EXTENSION="txz"
		fi
	fi

	# Name of the install/remove-scripts.
	SCRIPT_NAME_INSTALLPKG="${SYSTEM_PATH_PACKAGES}/install${SYSTEM_PACKAGE_EXTENSION}.sh"
	SCRIPT_NAME_REMOVEPKG="${SYSTEM_PATH_PACKAGES}/remove${SYSTEM_PACKAGE_EXTENSION}.sh"
	SCRIPT_NAME_MAKEINFO="${SYSTEM_PATH_PACKAGES}/makeinfo.sh"

	# If in debug mode, turn on verbose mode.
	BUILDTOOL_OPTION_VERBOSE_MODE=${RUNTIME_OPTION_DEBUG_MODE}

	# Buildtool suffix used by the buildscripts.
	BUILDTOOL_SUFFIX="buildtool"

	# Define blacklist.
	SYSTEM_CATEGORY_BLACKLIST="${SYSTEM_CATEGORY_BLACKLIST} z z1"

}

# Wait a few seconds...
SLEEPMODE() {
	sleep $1
}

# Check command line options.
CONFIGURE_CHECK_CLI_OPTIONS() {
	local exit_setup=false

	# Parse options...
	while [ x"$1" != x"" ]; do

		# Configure Setup...
		case $1 in
			--btcfg|-b)			SYSTEM_CONFIG_BUILDTOOLS="$(cd $(dirname $2); pwd)/$(basename $2)"
								RUNTIME_SYSTEM_CUSTOM_CONFIG=true
								shift; shift; continue;;
			-I)					RUNTIME_USE_INCLUDE_CONF_FILES_OVERWRITE=true
								shift; continue;;
			-q)					if [ -e ${SYSTEM_JOB_SETTINGS} ]; then
									RUNTIME_QUICK_RESUME=true
								else
									RUNTIME_QUICK_RESUME=false
								fi
								shift; continue;;
		esac

		# Buildtool jobs...
		case $1 in
			--download|-d)		RUNTIME_CLI_MODE=true
								RUNTIME_CLI_OPTION="DLALL"
								exit_setup=false; break;;
			-D)					RUNTIME_CLI_MODE=true
								RUNTIME_CLI_OPTION="DLSINGLE"
								exit_setup=false; break;;
			--update|-u)		RUNTIME_CLI_MODE=true
								RUNTIME_CLI_OPTION="UPDATEBT"
								exit_setup=false; break;;
		esac

		# Resume build...
		case $1 in
			--add|-a)			RUNTIME_CLI_OPTION="ADD"
								RUNTIME_CLI_PARAMETER="$2"
								shift; shift; continue;;
			--skip|-s)			RUNTIME_CLI_OPTION="SKIP"
								shift; continue;;
		esac

			# Setup tools...
		case $1 in
			--config|-c)		RUNTIME_CLI_MODE=true
								CONFIG_EDITOR_MAIN_MENU
								FUNC_CLEAR_SCREEN
								exit_setup=true; break;;
			--icache|-i)		RUNTIME_CLI_MODE=true
								OPTION_UPDATE_GTK_ICON_CACHE=true
								FUNC_SYSTEM_UPDATE_ICON_CACHE
								FUNC_CLEAR_SCREEN
								exit_setup=true; break;;
			--newconfig|-n)		RUNTIME_CLI_MODE=true
								CONFIGURE_LOAD_SYSTEM_SETTINGS_CORE
								BUILDSYS_CHECK_ETC_NEW_FILES
								FUNC_CLEAR_SCREEN
								exit_setup=true; break;;
			--scripts|-t)		RUNTIME_CLI_MODE=true
								RUNTIME_SCRIPT_MODE=true
								CONFIGURE_LOAD_SYSTEM_SETTINGS_CORE
								INITIALIZE_PACKAGE_CONFIGURATION
								UTILITY_CREATE_SCRIPT_INSTALLTGZ
								UTILITY_CREATE_SCRIPT_REMOVETGZ
								UTILITY_CREATE_SCRIPT_MAKEINFO
								FUNC_PREPARE_EXIT
								echo "Install scripts have been created here:"
								echo "  -> ${SYSTEM_PATH_PACKAGES}"
								echo ""
								exit_setup=true; break;;
		esac

		# Print version/help messages...
		case $1 in
			--version|-v)		echo ""
								echo " Version: ${PROJECT_VERSION}"
								echo -e ${PROJECT_ABOUT}
								echo ""
								exit_setup=true; break;;
			--help|-h)			cat <<EOF

${PROJECT_NAME} Version ${PROJECT_VERSION}
Usage: sh ${PROJECT_SCRIPT_NAME} [--options]

Valid options are:
-h|--help      Print this page.
-v|--version   Show version info.
-u|--update    Update buildtools from latest online archive.
-d|--download  Update/download all source packages.
-D             Update/download source packages included in '$(basename ${SYSTEM_CONFIG_SINGLE_MODE})' only.
-b|--btcfg     Specify buildtool configuration file. (DANGEROUS!)
               Example: sh ${PROJECT_SCRIPT_NAME} --btcfg mypackages.conf
-I             Use configuration files inside INCLUDES directory instead
               of the default buildorder configuration file.
-i|--icache    Build/update the GTK+ icon cache.
-n|--newconfig Check for new configuration files.
-c|--config    Start the configuration editor.
-t|--scripts   Create the installtgz/removetgz/makeinfo scripts.
               These scripts will be based on the current '$(basename ${SYSTEM_CONFIG_SINGLE_MODE})' file.

If build has failed:
-q             Quick resume a previous build (skip system checks).
               If build has failed because of a missing dependency then the
               build will fail again. Use -a/-s to finetune setup.
-a|--add       Add a package to '$(basename ${SYSTEM_JOB_PACKAGES})' when build has
               failed because a package is not yet installed.
               Example: sh ${PROJECT_SCRIPT_NAME} --add boost [,pkg2,pkg3,...]
-s|--skip      After build has failed skip the broken package and try
               to resume build with the remaining package(s).
               Example: sh ${PROJECT_SCRIPT_NAME} --skip [pkg1,pkg2,pkg3,...]

EOF
								exit_setup=true; break;;
			*)					echo;
								echo "Unknown option: '$1'";
								echo "Use 'sh ${PROJECT_SCRIPT_NAME} --help' to get a list with available options.";
								echo;
								exit_setup=true; break;;
		esac
		shift
	done

	# Exit SETUP?
	if ${exit_setup:-false}; then
		return 1
	else
		return 0
	fi
}

# Check for required system files.
CONFIGURE_CHECK_SYSTEM_FILES() {

	# System files already checked?
	if ${SYSTEM_CONFIG_CHECKED}; then
		return 0
	fi

	# Search for the buildsystem configuration file.
	if ! test -f "${SYSTEM_CONFIG_SETTINGS}"; then
		if test -f "${SYSTEM_CONFIG_SETTINGS}.orig"; then
			SYSTEM_CONFIG_SETTINGS="${SYSTEM_CONFIG_SETTINGS}.orig"
		else
			echo ""
			echo "Error: Missing buildsystem configuration file!"
			echo "       (${SYSTEM_CONFIG_SETTINGS##*/}.orig)"
			echo ""
			return 1
		fi
	fi

	# Search for the single-mode pre-configuration file.
	if ! test -f "${SYSTEM_CONFIG_SINGLE_MODE}"; then
		if test -f "${SYSTEM_CONFIG_SINGLE_MODE}.orig"; then
			SYSTEM_CONFIG_SINGLE_MODE="${SYSTEM_CONFIG_SINGLE_MODE}.orig"
		else
			echo ""
			echo "Error: Missing buildsystem configuration file!"
			echo "       (${SYSTEM_CONFIG_SINGLE_MODE##*/}.orig)"
			echo ""
			return 1
		fi
	fi

	# Search for the remove.conf file.
	if ! test -f "${SYSTEM_CONFIG_REMOVE}"; then
		if test -f "${SYSTEM_CONFIG_REMOVE}.orig"; then
			SYSTEM_CONFIG_REMOVE="${SYSTEM_CONFIG_REMOVE}.orig"
		else
			SYSTEM_CONFIG_REMOVE=""
		fi
	fi

#	# Search for the packages.conf.blacklist file.
#	# Do not check for the default blacklist file
#	# since we do not include this in the release tarball.
#	if ! test -f "${SYSTEM_CONFIG_PACKAGE_BLACKLIST}"; then
#		echo ""
#		echo "Error: Missing package blacklist configuration file!"
#		echo "       (${SYSTEM_CONFIG_PACKAGE_BLACKLIST##*/})"
#		echo ""
#		return 1
#	fi

	SYSTEM_CONFIG_CHECKED=true
	return 0
}

# Check for required system files.
CONFIGURE_CHECK_BUILDTOOL_FILES() {

	# Check if system paths and files do exist.
	if ! test -d ${SYSTEM_PATH_BUILDTOOLS}; then
		BUILDSYS_MAIN_UPDATE_BUILDTOOLS || return 1
		# Just to make sure...
		if ! test -d ${SYSTEM_PATH_BUILDTOOLS}; then
			echo
			echo "Error: Cannot find system directory!"
			echo "       (${SYSTEM_PATH_BUILDTOOLS})"
			echo
			return 1
		fi
	fi

	# Do we have a list of groups/categories?
	if ! test -f ${SYSTEM_PATH_BUILDTOOLS}/GROUPS; then
		echo ""
		echo "Error: Cannot find system group configuration file!"
		echo "       (${SYSTEM_PATH_BUILDTOOLS}/GROUPS)"
		echo ""
		return 1
	fi

	# Search for the buildtool configuration file.
	if ! test -f "${SYSTEM_CONFIG_BUILDTOOLS}"; then
		if test -f "${SYSTEM_CONFIG_BUILDTOOLS}.orig"; then
			SYSTEM_CONFIG_BUILDTOOLS="${SYSTEM_CONFIG_BUILDTOOLS}.orig"
		else
			echo ""
			echo "Error: Missing buildtool configuration file!"
			echo "       (${SYSTEM_CONFIG_BUILDTOOLS##*/}.orig)"
			echo ""
			return 1
		fi
	fi

	# Search for the buildtool functions file.
	if ! test -e ${SYSTEM_FUNCTIONS_FILE}; then
		if ! test -e ${SYSTEM_PATH_BUILDTOOLS}/${SYSTEM_FUNCTIONS_FILE}; then
			# Did we find the 'functions' file used by the buildtools?
			echo ""
			echo "Error: The '${SYSTEM_FUNCTIONS_FILE}'file could not be found!"
			echo ""
			return 1
		else
			export BUILDTOOL_FUNCTIONS_PATH=$(cd ${SYSTEM_PATH_BUILDTOOLS}; pwd)
		fi
	else
		export BUILDTOOL_FUNCTIONS_PATH=$(pwd)
	fi

	return 0
}

# Check for various system requirements.
CONFIGURE_CHECK_SYSTEM_REQUIREMENTS() {
	local fatal_error=false

	# Use 'command' instead of deprecatd 'which'.
	# See http://pubs.opengroup.org/onlinepubs/9699919799/utilities/command.html
	# For reasons not to use which have a look here:
	# https://unix.stackexchange.com/questions/85249/why-not-use-which-what-to-use-then/85250#85250
	if ! ( command echo test &>/dev/null ); then
		cat <<EOF 1>&2
Error: Missing the 'command' function!
       'command' is used to locate executables.
       You should install the 'bash' package!

Exittig now!
EOF
		fatal_error=true
	fi

	if ! ( command -v find &>/dev/null ); then
		cat <<EOF 1>&2
Error: Missing the 'find' utility!
       'find' is used to find files and directories.
       You should install the 'findutils' package!

Exittig now!
EOF
		fatal_error=true
	fi

	if ! ( command -v tar &>/dev/null ); then
		cat <<EOF 1>&2
Error: Missing the 'tar' utility!
       'tar' is used to extract files from tar archives.
       You should install the 'tar' package!

Exittig now!
EOF
		fatal_error=true
	fi

	if ! ( command -v grep &>/dev/null ); then
		cat <<EOF 1>&2
Error: Missing the 'grep' utility!
       'grep' is used to filter a list of data.
       You should install the 'grep' package!

Exittig now!
EOF
		fatal_error=true
	fi

	if ! ( command -v sed &>/dev/null ); then
		cat <<EOF 1>&2
Error: Missing the 'sed' utility!
       'sed' is used to manipulate text files.
       You should install the 'sed' package!

Exittig now!
EOF
		fatal_error=true
	fi

	if ! ( command -v awk &>/dev/null ); then
		cat <<EOF 1>&2
Error: Missing the 'awk' utility!
       'awk' is used for calculate the package versions.
       You should install the 'gawk' package!

Exittig now!
EOF
		fatal_error=true
	fi

	if ! ( command -v readlink &>/dev/null ); then
		cat <<EOF 1>&2
Error: Missing the 'readlink' utility!
       'readlink' is used to resolve symbolic links.
       You should install the 'coreutils' package!

Exittig now!
EOF
		fatal_error=true
	fi

	case ${fatal_error} in
		false)	return 0;;
		*)		return 1;;
	esac

}

# Check buildorder configuration file.
CONFIGURE_CHECK_BUILDTOOL_CONFIGURATION_FILE() {
	local package_list=""
	local package=""
	local error=false

	package_list=$(grep -v "#" ${SYSTEM_CONFIG_BUILDTOOLS} | grep ".")
	for package in ${package_list}; do
		check=$(grep "^${package##*/}$" ${SYSTEM_CONFIG_PACKAGE_BLACKLIST})
		if [ x"${check}" == x"" ]; then
			if [ ! -d ${SYSTEM_PATH_BUILDTOOLS}/${package} ]; then
				error=true
				echo "Error: Unknown package in buildtool configuration file found!"
				echo "       => ${package}"
			else
				if [ ! -f ${SYSTEM_PATH_BUILDTOOLS}/${package}/$(basename ${package}).${BUILDTOOL_SUFFIX} ]; then
					error=true
					echo "Error: Missing buildtool file!"
					echo "       => ${package}/$(basename ${package}).${BUILDTOOL_SUFFIX}"
				fi
			fi
		fi
	done

	if ${error}; then
		echo ""
		echo "Exiting now!"
		return 1
	else
		return 0
	fi
}

# Initialize system run mode.
CONFIGURE_SESSION_MODE() {

	# Restart a previous session?
	if [ -e ${SYSTEM_JOB_SETTINGS} ]; then

		# Don't ask when we want to quick resume a previous build.
		if ! ${RUNTIME_QUICK_RESUME:-false}; then

			# Previous build session was incomplete, restart.
			dialog	--title "${PROJECT_NAME} - INFORMATION" \
					--backtitle "${PROJECT_BACKTITLE}" \
					--cr-wrap \
					--extra-button \
					--extra-label "${BUTTON_LABEL_RESTART}" \
					--yes-label "${BUTTON_LABEL_CONTINUE}" \
					--no-label "${BUTTON_LABEL_CANCEL}" \
					--ok-label "${BUTTON_LABEL_CONTINUE}" \
					--cancel-label "${BUTTON_LABEL_CANCEL}" \
					--yesno "\n\
Previous build was not complete.\n\
\n\
The script will now continue to build the remaining packages using configuration found in 'packages.job'.\n\
\n\
Select 'RESTART' to start a new build or select 'CANCEL' to abort.\n\
" 12 75

			DIALOG_KEYCODE=$?
			rm -f "${DIALOG_RETURN_VALUE}"

			# Restart?
			case ${DIALOG_KEYCODE} in
				3)	rm -f ${SYSTEM_JOB_SETTINGS}
					rm -f ${SYSTEM_JOB_PACKAGES}
					;;
				1)	return 1;;
				0)	true;;
			esac
		fi
	fi


	# Do we have a job file from a previous build?
	if [ -e ${SYSTEM_JOB_SETTINGS} ]; then
		RUNTIME_SYSTEM_RESTART=true
		RUNTIME_SYSTEM_USERMODE=false
		RUNTIME_PACKAGE_CONFIG_FILE="${SYSTEM_JOB_PACKAGES}"
	else
		if [ -e ${SYSTEM_CONFIG_OVERWRITE} ]; then
			RUNTIME_SYSTEM_USERMODE=true
			RUNTIME_SYSTEM_CUSTOM_CONFIG=true
			RUNTIME_PACKAGE_CONFIG_FILE="${SYSTEM_CONFIG_OVERWRITE}"
		else
			RUNTIME_SYSTEM_USERMODE=false
			RUNTIME_PACKAGE_CONFIG_FILE="${SYSTEM_CONFIG_BUILDTOOLS}"
		fi
	fi

	# User config file found...
	if [ x${RUNTIME_SYSTEM_USERMODE} == xtrue -o x${RUNTIME_SYSTEM_CUSTOM_CONFIG} == xtrue ]; then
		dialog	--title "${PROJECT_NAME} - INFORMATION" \
				--backtitle "${PROJECT_BACKTITLE}" \
				--cr-wrap \
				--msgbox "\n\
Note: A modified package list and buildorder will be used:\n\
      Selected config file is '$(basename ${RUNTIME_PACKAGE_CONFIG_FILE})' \
" 8 75
	fi

	# Remove existing logfiles.
	if ! ${RUNTIME_SYSTEM_RESTART}; then
		FUNC_PROGRESS_BOX "Purging existing logfiles..."
		rm -f ${SYSTEM_DEBUG_LOGFILE_INSTALLED_STAGE1}
		rm -f ${SYSTEM_DEBUG_LOGFILE_INSTALLED_STAGE2}
		rm -f ${SYSTEM_DEBUG_LOGFILE_INSTALLED_STAGE3}
		rm -f ${SYSTEM_DEBUG_LOGFILE_MISSING_EXTRAS}
		rm -f ${SYSTEM_DEBUG_LOGFILE_REMOVED_XORG_DRIVERS}
		rm -f ${SYSTEM_DEBUG_LOGFILE_REMAINING_XORG_PACKAGES}
		rm -f ${SYSTEM_DEBUG_LOGFILE_BLACKLISTED}
		rm -f ${SYSTEM_DEBUG_LOGFILE_BUILDORDER}
		rm -f ${SYSTEM_DEBUG_LOGFILE_DEPOPT}
		rm -f ${SYSTEM_DEBUG_LOGFILE_DEPOPT_INFO}
		rm -f ${SYSTEM_DEBUG_LOGFILE_DEPOPT_CHECK}
		rm -f ${SYSTEM_DEBUG_LOGFILE_DEPOPT_MISSING}
		rm -f ${SYSTEM_DEBUG_LOGFILE_SKIPPED_PACKAGES}
		rm -f ${SYSTEM_LOGFILE_AUTOSELECT}
		rm -f ${SYSTEM_LOGFILE_PKG_CONF_OVERWRITE}
		rm -f ${SYSTEM_LOGFILE_SETTINGS}
		rm -f ${SYSTEM_LOGFILE_DOWNLOADS}
		rm -f ${SYSTEM_LOGFILE_DOWNLOAD_ERROR}
		rm -f ${SYSTEM_LOGFILE_MD5SUM}
		rm -f ${SYSTEM_LOGFILE_DATABASE}
		rm -f ${SYSTEM_LOGFILE_REMOVED}
		rm -f ${SYSTEM_LOGFILE_BUILDSYS_CONFIG}
		rm -f ${SYSTEM_LOGFILE_BUILDSYS_ERROR}
		rm -f ${SYSTEM_LOGFILE_BUILDSYS_SETTINGS}
		rm -f ${SYSTEM_LOGFILE_BUILDSYS_MASTER}
		rm -f ${SYSTEM_LOGFILE_BUILDARCH_WARNINGS}
		rm -f ${SYSTEM_LOGFILE_SUID_WARNINGS}
		rm -f ${SYSTEM_LOGFILE_RPATH_WARNINGS}
		rm -f ${SYSTEM_LOGFILE_SHAREDLIBS_WARNINGS}
		rm -f ${SYSTEM_LOGFILE_EMPTYDIR_WARNINGS}
		rm -f ${SYSTEM_LOGFILE_DOINST_WARNINGS}
		rm -f ${SYSTEM_LOGFILE_DOCFILES_WARNINGS}
		rm -f ${SYSTEM_LOGFILE_USRLOCAL_WARNINGS}
		rm -f ${SYSTEM_LOGFILE_OVERWRITE_FILES_WARNINGS}
		rm -f ${SYSTEM_LOGFILE_APPLIED_PATCHES}
		rm -f ${SYSTEM_LOGFILE_GTK_ICON_CACHE}
		rm -f ${SYSTEM_LOGFILE_RM_LA_FILES}
		rm -f ${SYSTEM_LOGFILE_CHECK_LA_FILES}
		rm -f ${SYSTEM_LOGFILE_BUILDTIME}
		rm -f ${SYSTEM_LOGFILE_NEW_PACKAGES}
		rm -f ${SYSTEM_LOGFILE_PACKAGE_UPDATES}
		rm -f ${SYSTEM_LOGFILE_SKIPPED_UPDATES}
		rm -f ${SYSTEM_BUILD_FAILED}
		rm -f ${SYSTEM_BUILD_INSTALL_PACKAGE}
		rm -f ${SYSTEM_BUILD_INSTALL_PACKAGE_ERRORS}
		rm -f ${SYSTEM_BUILD_LOGFILE_TEMP1}
		rm -f ${SYSTEM_BUILD_LOGFILE_TEMP2}
	fi

	return 0
}

# Load core system configuration.
CONFIGURE_LOAD_SYSTEM_SETTINGS_CORE() {
	# Print some messages to screen what is currently being checked.
	FUNC_PROGRESS_BOX "Loading system settings..."
	if . ${SYSTEM_CONFIG_SETTINGS}; then
		SLEEPMODE 1
	else
		FUNC_CLEAR_SCREEN
		echo "Unknown error while loading system settings!"
		echo " => ${SYSTEM_CONFIG_SETTINGS}"
		return 1
    fi

	# Set package tag.
	export PACKAGE_TAG=${PACKAGE_TAG:-${OPTION_PACKAGE_TAG:-"slp"}}
}

# Load system configuration.
CONFIGURE_LOAD_SYSTEM_SETTINGS() {
	# Load core system settings.
	CONFIGURE_LOAD_SYSTEM_SETTINGS_CORE

	# Set package extension.
	BUILDTOOL_PACKAGE_EXTENSION=${BUILDTOOL_PACKAGE_EXTENSION:-${OPTION_PACKAGE_EXT:-${SYSTEM_PACKAGE_EXTENSION}}}
	case ${BUILDTOOL_PACKAGE_EXTENSION} in
		tgz|txz)	true;;
		*)		BUILDTOOL_PACKAGE_EXTENSION=${SYSTEM_PACKAGE_EXTENSION};;
	esac

	# Do we have a previous job file?
	if ${RUNTIME_SYSTEM_RESTART}; then
		FUNC_PROGRESS_BOX "Loading current job settings:"
		if . ${SYSTEM_JOB_SETTINGS}; then
			SLEEPMODE 1
		else
			FUNC_CLEAR_SCREEN
			echo "Unknown error while loading job settings!"
			echo " => ${SYSTEM_JOB_SETTINGS}"
			return 1
		fi
	fi

	# Check for shell variables to create 'slack-required'
	# files during setup.
	if [ x"${BUILDTOOL_RBUILDER_CREATE}" != x"" ]; then
		case ${BUILDTOOL_RBUILDER_CREATE} in
			true|false)	OPTION_RBUILDER_CREATE=${BUILDTOOL_RBUILDER_CREATE};;
		esac
	fi

	# Check "Move packages to path option".
	if [ x"${OPTION_MOVE_INSTALLED_PACKAGES}" == x"true" ]; then
		if [ x"${OPTION_MOVE_INSTALLED_PACKAGES_TO_PATH}" == x"" ]; then
			OPTION_MOVE_INSTALLED_PACKAGES=false
		else
			if [ ! -d "${OPTION_MOVE_INSTALLED_PACKAGES_TO_PATH}" ]; then
				OPTION_MOVE_INSTALLED_PACKAGES=false
				OPTION_MOVE_INSTALLED_PACKAGES_TO_PATH=""
			fi
		fi
	fi

	# Using INCLUDES configuration files requested?
	if [ x"${OPTION_USE_INCLUDE_CONF_FILES}" == x"true" ]; then
		RUNTIME_USE_INCLUDE_CONF_FILES_OVERWRITE=true
	fi
	if [ x"${RUNTIME_USE_INCLUDE_CONF_FILES_OVERWRITE}" != x"" ]; then
		case "${RUNTIME_USE_INCLUDE_CONF_FILES_OVERWRITE}" in
			true|false)	OPTION_USE_INCLUDE_CONF_FILES=${RUNTIME_USE_INCLUDE_CONF_FILES_OVERWRITE};;
		esac
	fi

	# export download mirrors.
	export SOURCE_MIRROR_GNOME=${BUILDTOOL_MIRROR_GNOME:-${MIRROR_GNOME}}
	export SOURCE_MIRROR_KDE=${BUILDTOOL_MIRROR_KDE:-${MIRROR_KDE}}
	export SOURCE_MIRROR_XORG=${BUILDTOOL_MIRROR_XORG:-${MIRROR_XORG}}
	export SOURCE_MIRROR_SOURCEFORGE=${BUILDTOOL_MIRROR_SOURCEFORGE:-${MIRROR_SOURCEFORGE}}
	export SOURCE_MIRROR_GENTOO_DISTFILES=${BUILDTOOL_MIRROR_GENTOO_DISTFILES:-${MIRROR_GENTOO_DISTFILES}}
}

# Just a separator...
SYS____________________FUNCTIONS() {
	true
}

# Clear dialog screen.
FUNC_CLEAR_SCREEN() {
	dialog --clear
	clear
}

# Create exit message screen.
FUNC_PREPARE_EXIT() {
	FUNC_CLEAR_SCREEN
	echo "${PROJECT_BACKTITLE}"
	echo "${RULER1}"
	echo ""
	echo "Exiting..."
	echo ""
	echo ""
}

# Exit by user.
FUNC_EXIT_BY_USER() {
	FUNC_PREPARE_EXIT
	exit 0
}

# Display small infobox.
FUNC_PROGRESS_BOX() {
	dialog --title "PLEASE WAIT..." --backtitle "${PROJECT_BACKTITLE}" --infobox "$1" 3 40
}

# Display large infobox.
FUNC_PROGRESS_BOX_LARGE() {
	dialog --title "PLEASE WAIT..." --backtitle "${PROJECT_BACKTITLE}" --infobox "$1" 3 60
}

# Show a custom infobox.
FUNC_INFO_BOX() {
	dialog --title "$1" --backtitle "${PROJECT_BACKTITLE}" --infobox "$2" 8 40
}

# Show a message box.
FUNC_MESSAGE_BOX() {
	dialog --title "$1" --backtitle "${PROJECT_BACKTITLE}" --msgbox "$2" 8 40
}

# Print "Please wait..." dialog.
FUNC_PLEASE_WAIT() {
	dialog --backtitle "${PROJECT_BACKTITLE}" --infobox "           Please wait..." 3 40
}

# Init progress bar.
FUNC_INIT_PROGRESS_BAR() {
	local a=0
	local char="#"
	local percent=0
	PROGRESS_BAR=""
	PROGRESS_PERCENT[0]=""
	PROGRESS_MAX_WIDTH=$(dialog --stdout --print-maxsize | sed "s,.* ,,g")
	echo -ne "\r"
	while [ ${a} -lt ${PROGRESS_MAX_WIDTH} ]; do
		let percent=(${a}+1)*100/${PROGRESS_MAX_WIDTH}
		if [ ${percent} -gt 100 ]; then
			let percent=100
		fi
		PROGRESS_PERCENT[${a}]="[${percent}%] "
		PROGRESS_BAR="${PROGRESS_BAR}${char}"
		echo -n " "
		let a=a+1
	done
}

# Draw the progress bar: $1: current count, $2: max count
FUNC_DRAW_PROGRESS_BAR() {
	local progress=0
	let progress=$1*PROGRESS_MAX_WIDTH/$2
	echo -ne "\r${PROGRESS_BAR:0:$progress}\r${PROGRESS_PERCENT[${progress}]}"
}

# Clear the progress bar.
FUNC_CLEAR_PROGRESS_BAR() {
	echo -ne "\r${PROGRESS_BAR//#/ }"
}

# Get numeric version of the package and installed package.
FUNC_GET_PACKAGE_NUMERIC_VERSION_ALL() {
	FUNC_GET_PACKAGE_NUMERIC_VERSION_INSTALLED $1
	FUNC_GET_PACKAGE_NUMERIC_VERSION $1
}

# Get numeric version of the installed package.
FUNC_GET_PACKAGE_NUMERIC_VERSION_INSTALLED() {
	if [ $1 -eq 0 ]; then
		FUNC_CLEAR_SCREEN
		echo "Error: FUNC_GET_PACKAGE_NUMERIC_VERSION_ALL() was called without package entry!"
		exit 1
	fi

	if [ x"${SYSDB_PACKAGE_INSTALLED[$1]}" != x"" ]; then
		FUNC_CALC_NUMERIC_VERSION "${SYSDB_PACKAGE_INSTALLED_VERSION[$1]}"
		let NUMPKG_INSTALLED_VERSION=RETURN_VERSION
	else
		let NUMPKG_INSTALLED_VERSION=0
	fi

	return 0
}

# Get numeric version of the package.
FUNC_GET_PACKAGE_NUMERIC_VERSION() {
	if [ $1 -eq 0 ]; then
		FUNC_CLEAR_SCREEN
		echo "Error: FUNC_GET_PACKAGE_NUMERIC_VERSION_ALL() was called without package entry!"
		exit 1
	fi

	FUNC_CALC_NUMERIC_VERSION "${SYSDB_PACKAGE_VERSION[$1]}"
	let NUMPKG_PACKAGE_VERSION=RETURN_VERSION

	return 0
}

# Calculate a numeric version.
FUNC_CALC_NUMERIC_VERSION() {
	local major=0
	local minor=0
	local micro=0
	local nano=0
	local version=""

	version="$1.0.0.0.0"
	# Fix version code for xorg-video-rendition-4.2.4+-...
	version="${version//+/}"
	major=${version%%.*}
	while [ x${major#0} != x${major} ]; do major=${major#0}; done
	major=${major:-0}

	version=${version#*.}
	minor=${version%%.*}
	while [ x${minor#0} != x${minor} ]; do minor=${minor#0}; done
	minor=${minor:-0}

	version=${version#*.}
	micro=${version%%.*}
	while [ x${micro#0} != x${micro} ]; do micro=${micro#0}; done
	micro=${micro:-0}

	version=${version#*.}
	version=${version%%.*}
	nano=${version%%.*}
	while [ x${nano#0} != x${nano} ]; do nano=${nano#0}; done
	nano=${nano:-0}

	# Check for date as part of the pakage version like
	# in 1.07.2.20120501.
	if [ ${minor:-0} -gt 19700101 ]; then
		minor=0
	fi
	if [ ${micro:-0} -gt 19700101 ]; then
		micro=0
	fi
	if [ ${nano:-0} -gt 19700101 ]; then
		nano=0
	fi

	let RETURN_VERSION=major*1000000000+minor*1000000+micro*1000+nano
	let RETURN_VERSION_MAJOR=major
	let RETURN_VERSION_MINOR=minor
	return 0
}

# Check for default video/input drivers
# $1 = package number.
FUNC_CHECK_DEFAULT_XORG_DRIVER() {
	local package=$1
	local is_default=false

	case ${SYSDB_PACKAGE_NAME[${package}]} in
		xf86-video-vesa)		is_default=true;;
		xf86-video-amdgpu)		is_default=true;;
		xf86-video-ati)			is_default=true;;
		xf86-video-nouveau)		is_default=true;;
		xf86-video-nv)			is_default=true;;
		xf86-video-intel)		is_default=true;;
		xf86-video-vboxvideo)	is_default=true;;
		xf86-video-*)			is_default=false;;
		xf86-input-mouse)		is_default=true;;
		xf86-input-keyboard)	is_default=true;;
		xf86-input-synaptics)	is_default=true;;
		xf86-input-evdev)		is_default=true;;
		xf86-input-libinput)	is_default=true;;
		xf86-input-*)			is_default=false;;
		*)						is_default=false;;
	esac

	if [ x"${is_default:-false}" == x"true" ]; then
		return 0
	else
		return 1
	fi
}

# The following functions:
#  -FUNC_CHECK_PACKAGE_INSTALLED
#  -FUNC_CHECK_FIND_PACKAGE
# will be used inside the install/remove scripts also.

## LABEL:FUNCFINDPKG

# Is package installed?
# This function checks for package names without extension.
# $1 = package name
FUNC_CHECK_PACKAGE_INSTALLED() {
	local search=$1
	local check=""
	local package_list=""
	local package_name=""

	# This defines a filter when we use find to search for a package in /var/log/packages:
	# If you make changes here also update FUNC_CHECK_FIND_PACKAGE!
	local find_filter="-[a-zA-Z0-9]*-*-[[:digit:]]*"

	# Check for regular package versions like qt-3.3.8...
	package_list=$(find /var/log/packages/ -type f -name "${search}${find_filter}" -printf "%f\n" | xargs)

	# For some package we may get more then one package name, like
	# xcb-util and xcb-util-cursor because of our search-term.
	# Find the matching package name.
	check=""
	if [ x"${search//\*/}" == x"${search}" ]; then
		for file_found in ${package_list}; do
			package_name=${file_found%-*}		# Remove build
			package_name=${package_name%-*}		# Remove arch
			package_name=${package_name%-*}		# Remove version
			if [ x"${package_name}" == x"${search}" ]; then
				check=${file_found}
				break
			fi
		done
	else
		# For some packages likes kde-l10n-* or libreoffice-l10n-*
		# we can get more then one package name. Always return
		# the complete package list.
		check=${package_list}
	fi

	RETURN_VALUE="${check}"
	RETURN_VALUE="${RETURN_VALUE# }"
	RETURN_VALUE="${RETURN_VALUE% }"
	if [ x"" != x"${RETURN_VALUE}" ]; then
		return 0
	else
		return 1
	fi
}

# Do we have a package in search path?
# This function checks for package names with extension tgz/txz.
# $1 = package name
# $2 = search path
FUNC_CHECK_FIND_PACKAGE() {
	local search=$1
	local pkgdir=$2
	local check=""
	local package_list=""
	local package_name=""

	# Do we have a directory named "$2"?
	if ! test -d "${pkgdir}"; then
		RETURN_VALUE=""
		return 1
	fi

	# This defines a filter when we use find to search for a package in search path:
	# If you make changes here also update FUNC_CHECK_PACKAGE_INSTALLED!
	local find_filter="-[a-zA-Z0-9]*-*-[[:digit:]]*"

	# Check for regular package versions like qt-3.3.8...
	package_list=$(find "${pkgdir}/" -type f -name "${search}${find_filter}.t[x|g|l|b]z" -printf "%f\n" | xargs)

	# For some package we may get more then one package name, like
	# xcb-util and xcb-util-cursor because of our search-term.
	# Find the matching package name.
	check=""
	if [ x"${search//\*/}" == x"${search}" ]; then
		for file_found in ${package_list}; do
			package_name=${file_found%-*}		# Remove build
			package_name=${package_name%-*}		# Remove arch
			package_name=${package_name%-*}		# Remove version
			if [ x"${package_name}" == x"${search}" ]; then
				check=${file_found}
				break
			fi
		done
	else
		# For some packages likes kde-l10n-* or libreoffice-l10n-*
		# we can get more then one package name. Always return
		# the complete package list.
		check=${package_list}
	fi

	RETURN_VALUE="${check}"
	RETURN_VALUE="${RETURN_VALUE# }"
	RETURN_VALUE="${RETURN_VALUE% }"
	if [ x"" != x"${RETURN_VALUE}" ]; then
		return 0
	else
		return 1
	fi
}

## LABEL:FUNCFINDPKG

# Check for source packages.
FUNC_CHECK_SOURCE_PACKAGES() {

	local count=0
	local source=""
	local check=0

	RUNTIME_MISSING_SOURCE_PACKAGES=0

	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
		let count=count+1

		if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
			for source in ${SYSDB_PACKAGE_SOURCE[${count}]//@@@/ }; do
				if [ x"${source}" != x"" ]; then
					if test ! -e "${SYSTEM_PATH_SOURCES}/${SYSDB_PACKAGE_GROUP[${count}]}/${source}"; then
						let RUNTIME_MISSING_SOURCE_PACKAGES=RUNTIME_MISSING_SOURCE_PACKAGES+1
					fi
				fi
			done
		fi

	done

}

# Check for packages that will be updated.
FUNC_CHECK_UPDATE_PACKAGES() {

	local count=0
	local package=""
	local check=""

	RUNTIME_UPDATE_PACKAGES=0

	rm -f ${DIALOG_TEMP_FILE1}
	touch ${DIALOG_TEMP_FILE1}
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
		let count=count+1
		if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
			if [ x"${SYSDB_PACKAGE_INSTALLED[${count}]}" != x"" ]; then
				for package in ${SYSDB_PACKAGE_INSTALLED[${count}]}; do
					check=$(grep "^${package}$" ${DIALOG_TEMP_FILE1})
					if [ x"${check}" == x"" ]; then
						echo "${package}" >>${DIALOG_TEMP_FILE1}
						let RUNTIME_UPDATE_PACKAGES=RUNTIME_UPDATE_PACKAGES+1
					fi
				done
			fi
		fi
	done

	count=0
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
		let count=count+1
		if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
			if [ x"${SYSDB_PACKAGE_REMOVE[${count}]}" != x"" ]; then
				for package in ${SYSDB_PACKAGE_REMOVE[${count}]}; do
					check=$(grep "^${package}$" ${DIALOG_TEMP_FILE1})
					if [ x"${check}" == x"" ]; then
						echo "${package}" >>${DIALOG_TEMP_FILE1}
						let RUNTIME_UPDATE_PACKAGES=RUNTIME_UPDATE_PACKAGES+1
					fi
				done
			fi
		fi
	done

	rm -f ${DIALOG_TEMP_FILE1}
}

# Check for packages that will be removed.
FUNC_CHECK_REMOVE_PACKAGES() {

	local count=0
	local package=""
	local check=""

	let RUNTIME_REMOVE_PACKAGES=RUNTIME_UPDATE_PACKAGES

	for package in ${RUNTIME_REMOVE_EXTRA_PACKAGES}; do
		let RUNTIME_REMOVE_PACKAGES=RUNTIME_REMOVE_PACKAGES+1
	done

}

# Check for packages that will be updated.
FUNC_CHECK_INSTALL_PACKAGES() {

	local count=0

	RUNTIME_INSTALL_PACKAGES=0

	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
		let count=count+1
		if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
			let RUNTIME_INSTALL_PACKAGES=RUNTIME_INSTALL_PACKAGES+1
		fi
	done

}

# Check if a package is enabled for single-mode.
FUNC_CHECK_SINGLE_MODE_SELECTED() {
	local check=""
	local count=$1
	# Disable a package has highest priority.
	check=$(grep "^-${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}$" ${SYSTEM_CONFIG_SINGLE_MODE})
	if [ x"${check}" != x"" ]; then
		RETURN_VALUE=off
	else
		# Is package part of a included @group of packages?
		check=$(grep "^@${SYSDB_PACKAGE_GROUP[${count}]}$" ${SYSTEM_CONFIG_SINGLE_MODE})
		if [ x"${check}" != x"" ]; then
			RETURN_VALUE=on
		else
			# Enable single package?
			check=$(grep "^+${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}$" ${SYSTEM_CONFIG_SINGLE_MODE})
			if [ x"${check}" == x"" ]; then
				RETURN_VALUE=off
			else
				RETURN_VALUE=on
			fi
		fi
	fi
	return 0
}

# Check for required and optional packages,
FUNC_CHECK_DUPLICATES() {
	local package=""
	local package_found=false

	for package in $1; do
		if [ x"${package}" == x"$2" ]; then
			package_found=true
			break
		fi
	done
	case ${package_found} in
		true)	return 1;;
	esac
	return 0
}

# Update mime/desktop database
FUNC_SYSTEM_UPDATE_DESKTOP_DATABASE() {

	## LABEL:UPDATEDDB

	# Update mime type database (if installed)
	if [ -x /usr/bin/update-mime-database ]; then
		FUNC_PROGRESS_BOX "Updating filetype handlers..."
		echo "Updating filetype handlers..." >>${SYSTEM_LOGFILE_DATA_UPDATE}
		/usr/bin/update-mime-database /usr/share/mime 1>/dev/null 2>>${SYSTEM_LOGFILE_DATA_UPDATE}
	fi

	# Update desktop database (if installed)
	if [ -x /usr/bin/update-desktop-database ]; then
		FUNC_PROGRESS_BOX "Updating menu entries..."
		echo "Updating menu entries..." >>${SYSTEM_LOGFILE_DATA_UPDATE}
		XDG_DATA_DIRS=`bash -l -c "export | grep XDG_DATA_DIRS" | sed 's,.*"\(.*\)",\1,g'` /usr/bin/update-desktop-database 1>/dev/null 2>>${SYSTEM_LOGFILE_DATA_UPDATE}
	fi

	## LABEL:UPDATEDDB

	return 0
}

# Update GConf database
FUNC_SYSTEM_UPDATE_GCONF_DATABASE() {

	## LABEL:UPDATEGDB

	# Update GConf database.
	if [ -x /usr/bin/gconftool-2 ]; then

		FUNC_PROGRESS_BOX "Updating GConf configuration..."
		echo "Updating GConf configuration..." >>${SYSTEM_LOGFILE_DATA_UPDATE}

		GCONF_CONFIG_SOURCE=$(gconftool-2 --get-default-source)
		export GCONF_CONFIG_SOURCE

		(	for schema in $(find /etc/gconf/schemas/ -type f -name "*.schemas"); do
				echo \'$schema\'
			done | xargs gconftool-2 --makefile-install-rule
		) 1>/dev/null 2>>${SYSTEM_LOGFILE_DATA_UPDATE}

		(	for entry in $(find /etc/gconf/schemas/ -type f -name "*.entries"); do
				gconftool-2 --direct --config-source=$GCONF_CONFIG_SOURCE --load $entry
			done
		) 1>/dev/null 2>>${SYSTEM_LOGFILE_DATA_UPDATE}

	fi

	## LABEL:UPDATEGDB

	return 0
}

# Update giomodules cache
FUNC_SYSTEM_UPDATE_GIOMODULES_CACHE() {

	## LABEL:UPDATEGIO

	# Update giomodules cache
	if [ -x /usr/bin/gio-querymodules ]; then

		FUNC_PROGRESS_BOX "Updating giomodules cache..."
		echo "Updating giomodules cache..." >>${SYSTEM_LOGFILE_DATA_UPDATE}

		/usr/bin/gio-querymodules ${SYSTEM_LIBDIR}/gio/modules \
			1>/dev/null 2>>${SYSTEM_LOGFILE_DATA_UPDATE}

	fi

	## LABEL:UPDATEGIO

	return 0
}

# Compile glib schema files
FUNC_SYSTEM_UPDATE_GLIB_SCHEMAS() {

	## LABEL:UPDATEGLIB

	# Compile glib schema files
	if [ -x /usr/bin/glib-compile-schemas ]; then

		FUNC_PROGRESS_BOX "Compiling glib schema files..."
		echo "Compiling glib schema files..." >>${SYSTEM_LOGFILE_DATA_UPDATE}

		/usr/bin/glib-compile-schemas /usr/share/glib-2.0/schemas >&/dev/null

	fi

	## LABEL:UPDATEGLIB

	return 0
}

# Update icon cache.
FUNC_SYSTEM_UPDATE_ICON_CACHE() {

	## LABEL:UPDATEGIC

	if ${OPTION_UPDATE_GTK_ICON_CACHE:-false}; then
		if [ -x /usr/bin/gtk-update-icon-cache ]; then

			# Update logfiles when in system mode.
			if [ x"${SYSVAR_MODE}" != x"ICONCACHE" -a x"${RUNTIME_CLI_MODE}" != x"true" ]; then
				echo "Updating GTK icon cache..." >>${SYSTEM_LOGFILE_DATA_UPDATE}
				echo "" >>${SYSTEM_LOGFILE_GTK_ICON_CACHE}
				echo "${RULER2}" >>${SYSTEM_LOGFILE_GTK_ICON_CACHE}
				echo "Rebuild all GTK icon cache files:" >>${SYSTEM_LOGFILE_GTK_ICON_CACHE}
			fi

			# Save current GTK icon cache status.
			find /usr/share/icons/ -mindepth 1 -maxdepth 2 \
				-type f \
				-name "icon-theme.cache" \
				-exec md5sum {} \; \
				| sort -k 2 \
				>${SYSTEM_PATH_TEMP}/MD5.orig

			# Count icon cache files.
			local icon_dir_count=$(find /usr/share/icons/ \
									-type d \
									-maxdepth 1 \
									-mindepth 1 \
									-exec grep -H "^Directories=" {}/index.theme 2>/dev/null \; \
									| grep "index.theme:Directories=" \
									| sed "s,:.*,,g" \
									| sed "s,/index.theme,,g" \
									| grep -c ".")

			# Update all icon cache files.
			local count=0
			(	for icondir in $(find /usr/share/icons/ \
									-type d \
									-maxdepth 1 \
									-mindepth 1 \
									-exec grep -H "^Directories=" {}/index.theme 2>/dev/null \; \
									| grep "index.theme:Directories=" \
									| sed "s,:.*,,g" \
									| sed "s,/index.theme,,g" \
									| sort); do
					let PERCENT=count*100/icon_dir_count
					echo ${PERCENT}
					echo "XXX"
					echo "Updating icon cache: $(basename $icondir)"
					echo "XXX"
					echo "  -> ${icondir#/}" 1>&2
					gtk-update-icon-cache -f ${icondir}
					let count=count+1
				done
			) 2>>${SYSTEM_LOGFILE_GTK_ICON_CACHE} \
				| dialog	--title "PLEASE WAIT..." \
							--backtitle "${PROJECT_BACKTITLE}" \
							--gauge "Updating icon cache..." 7 60 0
			sed -e "s,^gtk-update-icon-cache: ,     ,g" -i ${SYSTEM_LOGFILE_GTK_ICON_CACHE}

			# Update global icon cache.
			#FUNC_PROGRESS_BOX "Updating system icon cache..."
			#gtk-update-icon-cache -t -f /usr/share/icons &>/dev/null
			# A top-level icon-theme.cache is stupid and might be huge...
			rm -f /usr/share/icons/icon-theme.cache

			# Save new icon cache status.
			find /usr/share/icons/ -mindepth 1 -maxdepth 2 \
				-type f \
				-name "icon-theme.cache" \
				-exec md5sum {} \; \
				| sort -k 2 \
				>${SYSTEM_PATH_TEMP}/MD5.new

			# Add modified cache files to logfile.
			echo "${RULER2}" >>${SYSTEM_LOGFILE_GTK_ICON_CACHE}
			echo "Summary - Updated GTK icon cache files:" >>${SYSTEM_LOGFILE_GTK_ICON_CACHE}
			diff -U0 -d -r -N ${SYSTEM_PATH_TEMP}/MD5.orig ${SYSTEM_PATH_TEMP}/MD5.new \
				| grep "^+[0-9a-f]" \
				| sed "s,.* /,  -> ,g" \
				| sed "s,/icon-theme.cache,,g" \
				>>${SYSTEM_LOGFILE_GTK_ICON_CACHE}
			echo "${RULER2}" >>${SYSTEM_LOGFILE_GTK_ICON_CACHE}

			# Remove temporary stuff...
			rm ${SYSTEM_PATH_TEMP}/MD5.orig
			rm ${SYSTEM_PATH_TEMP}/MD5.new
		fi
	else
		if [ x"${BUILDSYS_GTK_ICON_CACHE}" == x"remove" ]; then
			find /usr/share/icons/ -type f -name "icon-theme.cache" -exec rm -f {} \; &>/dev/null
		fi
	fi

	## LABEL:UPDATEGIC

	return 0
}

# Create dummy .gtk-bookmarks file
FUNC_SYSTEM_CREATE_DUMMY_GTK_BOOKMARKS() {

	## LABEL:BOOKMARKS

	## Create empty .gtk-bookmarks file for root.
	## Without this file root may get errors like:
	## CRITICAL: Error opening file: No such file or directory.
	## For new users we add that to /etc/skel.
	if [ -e /etc/skel/.gtk-bookmarks ]; then
		touch /root/.gtk-bookmarks
	fi

	## LABEL:BOOKMARKS

	return 0
}

# Update timer using NTP.
FUNC_SYNC_SYSTEM_TIME() {
	if ${OPTION_USE_NTP}; then
		if ( ping -c1 ${OPTION_USE_NTP_SERVER} &>/dev/null ); then
			/usr/sbin/ntpdate -s -u ${OPTION_USE_NTP_SERVER} &>/dev/null
		else
			OPTION_USE_NTP=false
		fi
	fi
}

# Find package number by name.
FUNC_FIND_PACKAGE_BY_NAME() {
	local find="$1"
	local orig_package="$2"
	local found=false
	local package=""
	local count=0

	for package in ${RUNTIME_PACKAGE_CODE_CACHE}; do
		if [ x"${package//@*/}" == x"${find}" ]; then
			# Package blacklisted or removed by user?
			# Needed if we have multiple packages with same
			# package name but with different package versions.
			# Examples: mesa, xorg-server...
			# Only find packages not yet been disabled/removed.
			if ${SYSDB_PACKAGE_REMOVED_BY_USER[${package//*@/}]}; then
				continue
			fi
			found=true
			break
		fi
	done

	if ${found}; then
		RETURN_VALUE=${package//*@/}
	else
		count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
			let count=count+1
			if [ x"${SYSDB_PACKAGE_NAME[${count}]}" == x"${find}" ]; then
				# Package blacklisted or removed by user?
				if ${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}; then
					continue
				fi
				found=true
				break
			fi
		done
		if ${found}; then
			RUNTIME_PACKAGE_CODE_CACHE="${RUNTIME_PACKAGE_CODE_CACHE} ${find}@${count} "
			RETURN_VALUE=${count}
		else
			echo "${orig_package}/${find} not found" >>${SYSTEM_DEBUG_LOGFILE_MISSING_EXTRAS}
			return 1
		fi
	fi

	return 0
}

# Write the updated/removed packages to logfile.
FUNC_WRITE_UPDATED_PACKAGES_TO_LOGFILE() {

	local logfile="$1"
	local count=0
	local count_update=0
	local count_remove=0
	local package=""
	local package_lang=""
	local text=""
	local text2=""
	local first_package=false
	local check=""
	local package_data_temp=""
	local package_name_orig=""

# *** Stage1 ***
	cat <<EOF >>${logfile}

Packages that will be updated (@@2 total):
                                 [ NEW ]      => [ OLD ]
${RULER2}
EOF

	count=0
	count_update=0
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
		let count=count+1
		if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
			if [ x"${SYSDB_PACKAGE_INSTALLED[${count}]}" != x"" ]; then
				first_package=true
				for package in ${SYSDB_PACKAGE_INSTALLED[${count}]}; do
					check=$(grep " => ${package}$" ${logfile})
					if [ x"${check}" == x"" ]; then
						let count_update=count_update+1
						if ${first_package}; then
							text="${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]}${SPACEBAR}"
							text="${text:0:32} ${SYSDB_PACKAGE_VERSION[${count}]}${SPACEBAR}"
							text="${text:0:45}"
							first_package=false
						else
							text="${SPACEBAR:0:45}"
						fi
						package_data_temp=${package%-*}				# Remove build
						package_data_temp=${package_data_temp%-*}	# Remove arch
						package_lang=${package_data_temp%-*}		# Remove version
						package_lang=${package_lang##*-}			# Remove generic package name
						if ${SYSDB_PACKAGE_LANG[${count}]}; then
							echo "${text} => ${package_data_temp##${SYSDB_PACKAGE_SEARCH_NAME[${count}]}-}-${package_lang}" >>${logfile}
						else
							echo "${text} => ${package_data_temp##${SYSDB_PACKAGE_NAME[${count}]}-}" >>${logfile}
						fi
					fi
				done
			fi
		fi
	done

	sed -i  "s,@@2,${count_update}," ${logfile}

# *** Stage2 ***
	cat <<EOF >>${logfile}
${RULER2}


Extra packages that will be removed: (@@2 total):
                                 [ INSTALLED ]
${RULER2}
EOF

	count=0
	count_update=0
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
		let count=count+1
		if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
			if [ x"${SYSDB_PACKAGE_REMOVE[${count}]}" != x"" ]; then
				first_package=true
				for package in ${SYSDB_PACKAGE_REMOVE[${count}]}; do
					package_data_temp=${package%-*}				# Remove build
					package_data_temp=${package_data_temp%-*}	# Remove arch
					package_name_orig=${package_data_temp%-*}	# Remove version
					package_data_version=${package_data_temp#$package_name_orig-}
					# Continue if the name of the current package matches
					# the name of the package to removed because if enabled the
					# current package will be removed/updated anyway...
					if [ x"${package_name_orig}" == x"${SYSDB_PACKAGE_NAME[${count}]}" ]; then
						continue
					fi
					# Check if the package should be removed by a
					# previous package during stage2.
					check=`grep " => ${package_name_orig}-${package_data_version}$" ${logfile}`
					if [ x"$check" != x"" ]; then
						continue
					fi
					# Is the current package x86/x64 or de/en specific?
					if [ x"${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}" == x"${SYSDB_PACKAGE_SEARCH_NAME[${count}]}" ]; then
						# No, use package name to check if we do have
						# the package already in the list of packages to
						# be updated during stage1.
						check=`grep "^${package_name_orig} " ${logfile}`
						if [ x"$check" != x"" ]; then
							continue
						fi
					else
						# Check for packages like kde-l10n-de-* that
						# should be removed during stage1.
						check=`grep "^${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]} " ${logfile}`
						if [ x"$check" != x"" ]; then
							continue
						fi
					fi
					let count_update=count_update+1
					if ${first_package}; then
						text="${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]}${SPACEBAR}"
						text="${text:0:32}"
						first_package=false
					else
						text="${SPACEBAR:0:32}"
					fi
					echo "${text} => ${package_data_temp}" >>${logfile}
				done
			fi
		fi
	done

	sed -i  "s,@@2,${count_update}," ${logfile}

# *** Stage3 ***
	cat <<EOF >>${logfile}
${RULER2}


More packages that will be removed: (@@3 total):
                                 [ INSTALLED ]
${RULER2}
EOF

	count_update=0
	for package in ${RUNTIME_REMOVE_EXTRA_PACKAGES}; do
		# Do we have the package name already in the list?
		if [ x"${package}" == x"${package//-\*/-\\\*}" ]; then
			check=`grep "^${package//-\*/-\\\*} " ${logfile}`
			if [ x"$check" != x"" ]; then
				continue
			fi
		fi

		# Find installed packages.
		if FUNC_CHECK_PACKAGE_INSTALLED ${package}; then
			# Package installed, get package name-version-arch-build.
			remove_package="${RETURN_VALUE}"

			# Check for all files if allready in the list
			for package_data_temp in ${remove_package}; do
				text="${package_data_temp}"
				text=${text%-*}
				text=${text%-*}
				text2=${text%-*}

				# Do we have the package name already in the list?
				# Check for main package and language-packages.
				check=`grep -e "^${text2} " -e " => ${text}$" ${logfile}`
				if [ x"$check" != x"" ]; then
					continue
				fi

				let count_update=count_update+1

				# Package allready in the list?
				check=$(grep "^${package//-\*/-\\\*} " ${logfile})
				if [ x"${check}" == x"" ]; then
					# No... print name and package to be removed.
					text="${package}${SPACEBAR}"
				else
					# Yes... print package to be removed only.
					text="${SPACEBAR}"
				fi

				# Language package?
				if [ x"${package}" == x"${package//-\*/-\\\*}" ]; then
					# No... print version info only.
					text="${text:0:32} => ${package_data_temp##${text2}-}"
				else
					# Yes... print language and version info.
					text="${text:0:32} => ${package_data_temp}"
				fi

				text=${text%-*} # Remove build...
				text=${text%-*} # Remove arch...

				# Add package to logfile.
				echo "${text:0:69}" >>${logfile}
			done
		fi

	done

	sed -i  "s,@@3,${count_update}," ${logfile}

	cat <<EOF >>${logfile}
${RULER2}

EOF

}

# List installed packages.
FUNC_DISPLAY_PACKAGE_LIST() {

	local packages=$1
	local title=$2
	local entry=""
	local check=""

	cat <<EOF >${DIALOG_TEMP_FILE2}
Packages that are currently installed:
${RULER2}
EOF

	rm -f ${DIALOG_TEMP_FILE1}

	# Make sure we have a file or sort-command will fail...
	touch ${DIALOG_TEMP_FILE1}
	for entry in ${packages}; do
		# Do we have the package name already in the list?
		check=`grep "^${entry//-\*/-\\\*}$" ${DIALOG_TEMP_FILE1}`
		if [ x"$check" != x"" ]; then
			continue
		fi
		echo "${entry}" >>${DIALOG_TEMP_FILE1}
	done

	# Sort package list...
	sort ${DIALOG_TEMP_FILE1} >>${DIALOG_TEMP_FILE2}

	cat <<EOF >>${DIALOG_TEMP_FILE2}
${RULER2}
EOF

	# Display the dialog.
	dialog	--title "${PROJECT_NAME} - ${title}" \
			--backtitle "${PROJECT_BACKTITLE}" \
			--cr-wrap \
			--exit-label "${BUTTON_LABEL_OK}" \
			--textbox ${DIALOG_TEMP_FILE2} 20 75

	rm -f ${DIALOG_TEMP_FILE1}
	rm -f ${DIALOG_TEMP_FILE2}

}

# List packages in selected category.
FUNC_DISPLAY_PACKAGES_IN_GROUP() {

	FUNC_PROGRESS_BOX "Creating package list..."

	rm -f ${DIALOG_TEMP_FILE1}
	rm -f ${DIALOG_TEMP_FILE2}

	local count=0
	local tag=""
	local group=$1
	local name=""
	local version=""
	local release=""
	local check=""

	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
		let count=count+1
		found=false

		for check in ${group}; do
			if [ x"${SYSDB_PACKAGE_GROUP[${count}]}" == x"${check}" ]; then
				name="${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]}${SPACEBAR}"
				name=${name:0:40}
				version="${SYSDB_PACKAGE_VERSION[${count}]}"
				release="${SYSDB_PACKAGE_RELEASE[${count}]}"
				echo "    ${name} - ${version}${release}" >>${DIALOG_TEMP_FILE1}
			fi
		done
	done

	echo "Packages that will be installed by group(s):" >${DIALOG_TEMP_FILE2}
	for name in ${group}; do
		echo "  => ${name}" >>${DIALOG_TEMP_FILE2}
	done
	echo "${RULER2}" >>${DIALOG_TEMP_FILE2}
	sort ${DIALOG_TEMP_FILE1} >>${DIALOG_TEMP_FILE2}
	echo "${RULER2}" >>${DIALOG_TEMP_FILE2}
	echo "" >>${DIALOG_TEMP_FILE2}

	dialog	--title "${PROJECT_NAME} - LIST OF PACKAGES" \
			--backtitle "${PROJECT_BACKTITLE}" \
			--cr-wrap \
			--exit-label "${BUTTON_LABEL_OK}" \
			--textbox ${DIALOG_TEMP_FILE2} 20 75

	rm -f ${DIALOG_TEMP_FILE1}
	rm -f ${DIALOG_TEMP_FILE2}

	return 0

}

# Count packages enabled to build.
FUNC_COUNT_ENABLED_PACKAGES() {
	local count=0

	FUNC_PROGRESS_BOX "Preparing package list..."

	# Reset package counter.
	SYSDB_PACKAGE_COUNT_ENEBALED=0

	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
		let count=count+1
		if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
			let SYSDB_PACKAGE_COUNT_ENEBALED=SYSDB_PACKAGE_COUNT_ENEBALED+1
		fi
	done
}

# Calculate ETA of packages to be installed.
FUNC_CALCULATE_ETA() {

	local count=0
	SELECTED_PACKAGES_ETA=0

	FUNC_PROGRESS_BOX "Calculating ETA..."
	FUNC_INIT_PROGRESS_BAR

	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
		let count=count+1
		FUNC_DRAW_PROGRESS_BAR ${count} ${SYSDB_PACKAGE_COUNT}

		if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
			let SELECTED_PACKAGES_ETA=SELECTED_PACKAGES_ETA+SYSDB_PACKAGE_ETA[${count}]
		fi
	done

	return 0

}

# Check for 'XORGDRV' selected.
FUNC_CHECK_GROUP_XORGDRV_SELECTED() {
	local group=""
	local found=1

	for group in ${BUILDSYS_INSTALL_GROUP}; do
		case ${group} in
			xorgdrv1)	found=0;;
			xorgdrv2)	found=0;;
		esac
	done

	return ${found}
}

# Remove jobs option from MAKEOPTS.
FUNC_CLEAN_MAKEOPTS_JOBS() {
	local option=""
	RETURN_VALUE=""
	for option in $1; do
		case ${option} in
			-j*)	continue;;
			*)		RETURN_VALUE="${RETURN_VALUE} ${option}";;
		esac
	done
	RETURN_VALUE="${RETURN_VALUE} $2"
	RETURN_VALUE="${RETURN_VALUE## }"
	return 0
}

# Just a separator...
SYS____________________INITIALIZE() {
	true
}

# Let the user select the locale (german or english)
INITIALIZE_LANGUAGE() {

	# Display a language selection dialog to the user?
	case ${OPTION_BUILD_GERMAN_PACKAGES} in
		true)	true;;
		false)	true;;
		*)		while [ true ]; do
					dialog	--title "${PROJECT_NAME} - SELECT LANGUAGE" \
							--backtitle "${PROJECT_BACKTITLE}" \
							--ok-label "${BUTTON_LABEL_OK}" \
							--cancel-label "${BUTTON_LABEL_EXIT}" \
							--menu "\
A few packages can be installed as english or german package such as Mozilla Firefox, Mozilla Thunderbird and LibreOffice.\n\
\n\
Some other packages are only needed if you need support for other languages then english like the KDE desktop language packages.\n\
\n\
\n\
\n\
\n\
Please select the language you want to be used when install localized packages for applications listed above.\n" 20 75 3 \
"english" "Install english localized packages" \
"german"  "Install german localized packages" 2> ${DIALOG_RETURN_VALUE}

					DIALOG_KEYCODE=$?
					DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}")
					rm -f "${DIALOG_RETURN_VALUE}"

					# 'CANCEL' or 'ESC' ?
					if test ${DIALOG_KEYCODE} -ne 0; then
						# If menu get cancelled, set language temporary to english.
						OPTION_BUILD_GERMAN_PACKAGES=false
						return 1
					fi

					# Backup the selectd language in our build settings.
					case ${DIALOG_REPLY} in
						"german")	OPTION_BUILD_GERMAN_PACKAGES=true
									break ;;
						*)			OPTION_BUILD_GERMAN_PACKAGES=false
									break ;;
					esac

				done
				CONFIG_EDITOR_SAVE_MENU_OPTION	"OPTION_BUILD_GERMAN_PACKAGES"	"${OPTION_BUILD_GERMAN_PACKAGES}"
				;;
	esac

	return 0
}

# Initialize group 'EXTRAS' cache.
INITIALIZE_GROUP_EXTRAS_CACHE() {
	local group_list_temp=$(grep "." ${SYSTEM_PATH_BUILDTOOLS}/GROUPS | grep -v "^#" | grep -v "^<" | grep -v "^@" | sed "s,:.*,,g" | sort)
	local group_name=""

	# Prepare screen...
	FUNC_PROGRESS_BOX "Initializing configuration..."

	count=0
	maxcount=$(grep "." ${SYSTEM_PATH_BUILDTOOLS}/GROUPS | grep -v "^#" | grep -v "^<" | grep -v "^@" | grep -c ".")
	FUNC_INIT_PROGRESS_BAR
	FUNC_DRAW_PROGRESS_BAR ${count} ${maxcount}

	rm -rf ${SYSTEM_PATH_GROUP_EXTRAS_CACHE}
	mkdir -p ${SYSTEM_PATH_GROUP_EXTRAS_CACHE}

	for group_name in ${group_list_temp}; do
		# Update the progress bar.
		let count=count+1
		FUNC_DRAW_PROGRESS_BAR ${count} ${maxcount}
		if test -e ${SYSTEM_PATH_BUILDTOOLS}/${group_name}/EXTRAS; then
			rm -f "${SYSTEM_PATH_GROUP_EXTRAS_CACHE}/${group_name}"
			INITIALIZE_GROUP_EXTRAS_CACHE_ENTRIES "${SYSTEM_PATH_BUILDTOOLS}/${group_name}/EXTRAS" "${group_name}" || return 1
		fi
	done

	return 0
}

# Read entries from 'EXTRAS' file and add data to the cache.
INITIALIZE_GROUP_EXTRAS_CACHE_ENTRIES() {
	local group_extras_file="$1"
	local group_extras_entry=""
	local group_name="$2"
	local include_conf_path=""
	local include_conf_name=""
	local check=""
	local buildtool_list=""
	local buildtool_name=""
	local extras_checked=""

	# Do we have an 'EXTRAS' file?
	if test -e ${group_extras_file}; then

		# Yes, check 'EXTRAS' entries...
		for group_extras_entry in $(grep "." ${group_extras_file} | grep -v "^#" | grep -v "^-" | sed "s,^+,,g"); do

			# Remove a file?
			if [ x"${group_extras_entry#=}" != x"${group_extras_entry}" ]; then
				# Add packagename to the list.
				echo "${SPACER}${group_extras_entry}" >>${SYSTEM_PATH_GROUP_EXTRAS_CACHE}/${group_name}
				continue
			fi

			# Check for including all buildtools of a group...
			if [ x"${group_extras_entry#@}" != x"${group_extras_entry}" ]; then
				# Add group to the include list.
				# Get a list of buildtools of the requested group.
				echo "${SPACER}# INCLUDE GROUP ${group_extras_entry}" >>${SYSTEM_PATH_GROUP_EXTRAS_CACHE}/${group_name}
				SPACER="${SPACER}  "
				buildtool_list=$(grep "^${group_extras_entry#@}/" ${SYSTEM_CONFIG_BUILDTOOLS} | sed "s,.*/,,g")
				for buildtool_name in ${buildtool_list}; do
					# Is buildtool blacklisted?
					check=$(grep "^${buildtool_name##*/}$" ${SYSTEM_CONFIG_PACKAGE_BLACKLIST})
					if [ x"${check}" == x"" ]; then
						# Exclude buildtool from list?
						# This covers the following rule:
						#   -> include.conf
						#      # Inlcude all buildtools from group a1...
						#      @a1
						#      # Include all buildtools except the following packages...
						#      -avahi
						check=$(grep "^-${buildtool_name##*/}$" ${group_extras_file})
						if [ x"${check}" == x"" ]; then
							# No, get the name of the package to be installed.
							# The list of required packages includes the real package name to be installed.
							if ! test -e ${SYSTEM_PATH_BUILDTOOLS}/${group_extras_entry#@}/${buildtool_name}/${buildtool_name}.${BUILDTOOL_SUFFIX}; then
								# Not found...
								clear
								echo ""
								echo "Missing buildtool file:"
								echo "  => ${group_extras_entry#@}/${buildtool_name}/${buildtool_name}.${BUILDTOOL_SUFFIX}"
								echo ""
								echo "Groupd extras file:"
								echo "  => ${group_extras_file}"
								echo ""
								echo "Groupd extras entry:"
								echo "  => ${group_extras_entry}"
								echo ""
								echo "Exiting now!"
								echo ""
								return 1
							fi
							pkg_name=`grep "^PACKAGE_NAME=" ${SYSTEM_PATH_BUILDTOOLS}/${group_extras_entry#@}/${buildtool_name}/${buildtool_name}.${BUILDTOOL_SUFFIX} | sed "s,.*=,,g"`
							# Add buildtool to the include list.
							echo "${SPACER}+${pkg_name}" >>${SYSTEM_PATH_GROUP_EXTRAS_CACHE}/${group_name}
						fi
					fi
				done
				echo "${SPACER}# ---" >>${SYSTEM_PATH_GROUP_EXTRAS_CACHE}/${group_name}
				SPACER="${SPACER%  }"
				# Continue with next entry in EXTRAS file.
				continue
			fi

			# 'INCLUDE.CONF' file?
			if [ x"${group_extras_entry#<}" != x"${group_extras_entry}" ]; then
				# Check for the INCLUDE.CONF file in all paths.
				include_conf_name=${group_extras_entry//[<>]/}
				# Search for the INCLUDE.CONF file in the system path.
				include_conf_path="${SYSTEM_PATH_INCLUDES_CONFIG}"
				if ! test -e "${include_conf_path}/${include_conf_name}.conf"; then
					# Search for the INCLUDE.CONF file in the archive path.
					include_conf_path="${SYSTEM_PATH_INCLUDES_CONFIG}/config"
					if ! test -e "${include_conf_path}/${include_conf_name}.conf"; then
						# Not found...
						clear
						echo ""
						echo "Missing includes file:"
						echo "  => ${include_conf_path}/${include_conf_name}.conf"
						echo ""
						echo "Source file:"
						echo "  => ${group_extras_file}"
						echo ""
						echo "Exiting now!"
						echo ""
						return 1
					fi
				fi

				# Found it, add its entries to the include list.
				# Fix: Try to work around circular dependencies.
				#      Do we have check the config file already?
				if test -e "${SYSTEM_PATH_GROUP_EXTRAS_CACHE}/${group_name}"; then
					extras_checked=`grep "# INCLUDE ${include_conf_name}.conf$" ${SYSTEM_PATH_GROUP_EXTRAS_CACHE}/${group_name}`
				else
					extras_checked=""
				fi
				if [ x"${extras_checked}" == x"" ]; then
					# No, check the config file.
					echo "${SPACER}# INCLUDE ${include_conf_name}.conf" >>${SYSTEM_PATH_GROUP_EXTRAS_CACHE}/${group_name}
					SPACER="${SPACER}  "
					INITIALIZE_GROUP_EXTRAS_CACHE_ENTRIES "${include_conf_path}/${include_conf_name}.conf" "${group_name}" || return 1
				else
					# Yes, do not check the config file again, just add a comment
					# the the cache file.
					echo "${SPACER}# INCLUDE ${include_conf_name}.conf" >>${SYSTEM_PATH_GROUP_EXTRAS_CACHE}/${group_name}
					echo "${SPACER}  [SKIPPED]" >>${SYSTEM_PATH_GROUP_EXTRAS_CACHE}/${group_name}
					SPACER="${SPACER}  "
				fi
				# Any errors?
				if test $? -eq 0; then
					echo "${SPACER}# ---" >>${SYSTEM_PATH_GROUP_EXTRAS_CACHE}/${group_name}
					SPACER="${SPACER%  }"
				else
					return 1
				fi
				# Continue with next entry in EXTRAS file.
				continue
			fi

			# Is buildtool blacklisted?
			check=$(grep "^${group_extras_entry}$" ${SYSTEM_CONFIG_PACKAGE_BLACKLIST})
			if [ x"${check}" == x"" ]; then
				# Exclude buildtool from list?
				# This covers the following rule:
				#   -> include.conf
				#      # Inlcude all buildtools from group a1...
				#      @a1
				#      # Include all buildtools except the following packages...
				#      -avahi
				check=$(grep "^-${group_extras_entry}$" ${group_extras_file})
				if [ x"${check}" == x"" ]; then
					# Add buildtool to the include list.
					echo "${SPACER}+${group_extras_entry##+}" >>${SYSTEM_PATH_GROUP_EXTRAS_CACHE}/${group_name}
				fi
			fi

		done
		return 0
	else
		return 1
	fi
}

# Initialize the system database.
INITIALIZE_SYSTEM_DATABASE() {
	# Name of the package that will be installed.
	SYSDB_PACKAGE_NAME[0]=""

	# Name of the source package..
	SYSDB_PACKAGE_NAME_SOURCE[0]=""

	# Name of the package that will be used for search in /var/log/packages.
	SYSDB_PACKAGE_SEARCH_NAME[0]=""

	# Orignal name including '_de' and '_x64'
	SYSDB_PACKAGE_ORIGINAL_NAME[0]=""

	# Name of the package group.
	SYSDB_PACKAGE_GROUP[0]=""

	# Name of the buildtool as listed in the package configuration file.
	SYSDB_PACKAGE_BUILDTOOL_NAME[0]=""

	# Path of the buildtool.
	SYSDB_PACKAGE_BUILDTOOL_PATH[0]=""

	# Name of the buildtool-script including suffix.
	SYSDB_PACKAGE_BUILDTOOL_SCRIPT[0]=""

	# Package flags.
	SYSDB_PACKAGE_FLAGS[0]=""

	# Extra packages that will be installed.
	SYSDB_PACKAGE_EXTRAPKG[0]=""

	# Package is requested for certain groups via 'EXTRAS' files.
	SYSDB_PACKAGE_EXTRA_FOR_GROUPS[0]=""

	# Optional packages.
	SYSDB_PACKAGE_OPTIONAL[0]=""
	SYSDB_PACKAGE_OPTIONAL_FOR_PACKAGE[0]=""

	# Rebuild packages.
	# These package will be build after the current package.
	SYSDB_PACKAGE_REBUILD[0]=""
	SYSDB_PACKAGE_REBUILD_FOR_PACKAGE[0]=""

	# Required packages.
	# These package must be installed before the current package.
	SYSDB_PACKAGE_REQUIRED[0]=""
	SYSDB_PACKAGE_REQUIRED_FOR_PACKAGE[0]=""

	# Automatically add packages.
	SYSDB_PACKAGE_ADDPKG[0]=""

	# Packages already checked for required/optional packages?
	SYSDB_PACKAGE_DEPOPT_CHECKED[0]=""

	# Build package that depend on external sources.
	SYSDB_PACKAGE_EXTERNAL[0]=""

	# Version of the package that will be installed.
	SYSDB_PACKAGE_VERSION[0]=""

	# Version of the source package that will be installed.
	SYSDB_PACKAGE_VERSION_SOURCE[0]=""

	# Release of the package that will be installed.
	SYSDB_PACKAGE_RELEASE[0]=""

	# Release of the source package that will be installed.
	SYSDB_PACKAGE_RELEASE_SOURCE[0]=""

	# Package arch.
	SYSDB_PACKAGE_ARCH[0]=""

	# Package archive type.
	SYSDB_PACKAGE_TYPE[0]=""

	# Package tag.
	SYSDB_PACKAGE_TAG[0]=""

	# Package build.
	SYSDB_PACKAGE_BUILD[0]=1

	# Time needed to build this package.
	SYSDB_PACKAGE_ETA[0]=0

	# Package is enabled and will be installed,
	SYSDB_PACKAGE_ENABLED[0]=true

	# Package is disabled during creating the buildorder.
	SYSDB_PACKAGE_DISABLED[0]=false

	# Package was auto selected.
	SYSDB_PACKAGE_AUTOSELECT[0]=false

	# Package was selected during dependency checking.
	SYSDB_PACKAGE_DEPOPT_SELECT[0]=false

	# Package was disabled by user.
	SYSDB_PACKAGE_REMOVED_BY_USER[0]=false

	# Is the package a language package?
	SYSDB_PACKAGE_LANG[0]=""

	# Build number of an installed package.
	SYSDB_PACKAGE_INSTALLED_BUILD[0]=""

	# ARCH of an installed package.
	SYSDB_PACKAGE_INSTALLED_ARCH[0]=""

	# Version of an installed package.
	SYSDB_PACKAGE_INSTALLED_VERSION[0]=""

	# Release (cvs, beta...) of an installed package.
	SYSDB_PACKAGE_INSTALLED_RELEASE[0]=""

	# Tag of an installed package.
	SYSDB_PACKAGE_INSTALLED_TAG[0]=""

	# Name and version of an installed package.
	SYSDB_PACKAGE_INSTALLED[0]=""

	# Name and version of packages to be removed when we install this package.
	# Entries must match the name of the packages in /var/log/packages.
	# Example: "kdelibs-4.14.0-i686-1 nepomuk-core-4.13.0-i686-1"
	SYSDB_PACKAGE_REMOVE[0]=""

	# Packages that this package will replace.
	SYSDB_PACKAGE_REPLACE[0]=""

	# Description of the package.
	SYSDB_PACKAGE_DESC[0]=""

	# Info if package is installed or new.
	SYSDB_PACKAGE_STATUS[0]=""

	# 'true' if sources have been checked and are all there...
	SYSDB_PACKAGE_SOURCES_CHECKED[0]=""

	# Skip checking for MD5 checksum.
	SYSDB_PACKAGE_SKIP_MD5[0]=""

	# Source package names.
	# Multiple source packages need to be separated using @@@, example:
	# llvm-3.9.1.tar.gz@@@clang-3.9.1.tar.gz
	SYSDB_PACKAGE_SOURCE[0]=""

	# Source package URLs.
	SYSDB_PACKAGE_URL1[0]=""
	SYSDB_PACKAGE_URL2[0]=""
	SYSDB_PACKAGE_URL3[0]=""
	SYSDB_PACKAGE_URL4[0]=""

	# Dep/Opt status.
	SYSDB_PACKAGE_RUNTIME_DEPOPT_STATUS[0]=""

	# Checked for optional packages.
	SYSDB_PACKAGE_OPTIONAL_FOR_PACKAGE_CHECKED[${count}]=""

}

# Create/Update the cache file.
INITIALIZE_CACHE_FILE() {
	local count=0

	FUNC_PROGRESS_BOX "Updating package cache..."
	echo "#@:${RUNTIME_PACKAGE_CONFIG_FILE}" >${SYSTEM_CONFIG_BUILDTOOLS_CACHE}
	echo "SYSDB_PACKAGE_COUNT=${SYSDB_PACKAGE_COUNT}" >>${SYSTEM_CONFIG_BUILDTOOLS_CACHE}
	echo "SYSDB_BUILD_GERMAN_PACKAGES=${OPTION_BUILD_GERMAN_PACKAGES}" >>${SYSTEM_CONFIG_BUILDTOOLS_CACHE}

	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
		let count=count+1
		cat <<EOF >>${SYSTEM_CONFIG_BUILDTOOLS_CACHE}
SYSDB_PACKAGE_NAME[${count}]="${SYSDB_PACKAGE_NAME[${count}]}"
SYSDB_PACKAGE_NAME_SOURCE[${count}]="${SYSDB_PACKAGE_NAME_SOURCE[${count}]}"
SYSDB_PACKAGE_SEARCH_NAME[${count}]="${SYSDB_PACKAGE_SEARCH_NAME[${count}]}"
SYSDB_PACKAGE_ORIGINAL_NAME[${count}]="${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}"
SYSDB_PACKAGE_GROUP[${count}]="${SYSDB_PACKAGE_GROUP[${count}]}"
SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]="${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]}"
SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]="\${SYSTEM_PATH_BUILDTOOLS}${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]//${SYSTEM_PATH_BUILDTOOLS}/}"
SYSDB_PACKAGE_BUILDTOOL_SCRIPT[${count}]="${SYSDB_PACKAGE_BUILDTOOL_SCRIPT[${count}]}"
SYSDB_PACKAGE_FLAGS[${count}]="${SYSDB_PACKAGE_FLAGS[${count}]}"
SYSDB_PACKAGE_EXTRAPKG[${count}]="${SYSDB_PACKAGE_EXTRAPKG[${count}]}"
SYSDB_PACKAGE_OPTIONAL[${count}]="${SYSDB_PACKAGE_OPTIONAL[${count}]}"
SYSDB_PACKAGE_REBUILD[${count}]="${SYSDB_PACKAGE_REBUILD[${count}]}"
SYSDB_PACKAGE_REQUIRED[${count}]="${SYSDB_PACKAGE_REQUIRED[${count}]}"
SYSDB_PACKAGE_REPLACE[${count}]="${SYSDB_PACKAGE_REPLACE[${count}]}"
SYSDB_PACKAGE_ADDPKG[${count}]="${SYSDB_PACKAGE_ADDPKG[${count}]}"
SYSDB_PACKAGE_EXTERNAL[${count}]="${SYSDB_PACKAGE_EXTERNAL[${count}]}"
SYSDB_PACKAGE_VERSION[${count}]="${SYSDB_PACKAGE_VERSION[${count}]}"
SYSDB_PACKAGE_VERSION_SOURCE[${count}]="${SYSDB_PACKAGE_VERSION_SOURCE[${count}]}"
SYSDB_PACKAGE_RELEASE[${count}]="${SYSDB_PACKAGE_RELEASE[${count}]}"
SYSDB_PACKAGE_RELEASE_SOURCE[${count}]="${SYSDB_PACKAGE_RELEASE_SOURCE[${count}]}"
SYSDB_PACKAGE_ARCH[${count}]="${SYSDB_PACKAGE_ARCH[${count}]}"
SYSDB_PACKAGE_TYPE[${count}]="${SYSDB_PACKAGE_TYPE[${count}]}"
SYSDB_PACKAGE_TAG[${count}]="${SYSDB_PACKAGE_TAG[${count}]}"
SYSDB_PACKAGE_BUILD[${count}]="${SYSDB_PACKAGE_BUILD[${count}]}"
SYSDB_PACKAGE_LANG[${count}]="${SYSDB_PACKAGE_LANG[${count}]}"
SYSDB_PACKAGE_DESC[${count}]="${SYSDB_PACKAGE_DESC[${count}]}"
SYSDB_PACKAGE_STATUS[${count}]="${SYSDB_PACKAGE_STATUS[${count}]}"
SYSDB_PACKAGE_SKIP_MD5[${count}]="${SYSDB_PACKAGE_SKIP_MD5[${count}]}"
SYSDB_PACKAGE_SOURCE[${count}]="${SYSDB_PACKAGE_SOURCE[${count}]}"
SYSDB_PACKAGE_URL1[${count}]="${SYSDB_PACKAGE_URL1[${count}]}"
SYSDB_PACKAGE_URL2[${count}]="${SYSDB_PACKAGE_URL2[${count}]}"
SYSDB_PACKAGE_URL3[${count}]="${SYSDB_PACKAGE_URL3[${count}]}"
SYSDB_PACKAGE_URL4[${count}]="${SYSDB_PACKAGE_URL4[${count}]}"
EOF
	done
}

# Initialize filter for buildtool configuration file.
INITIALIZE_BTFILTER() {
	local check=""
	local name=""
	local entry=""
	local entry_name=""
	local count=0
	local pkgcount=0
	local maxcount=0
	local group=""
	local newconf=false
	local btname=""
	local pkgname=""

	# Prepare screen...
	FUNC_PROGRESS_BOX "Creating buildtool filter..."

	count=0
	maxcount=$(grep "." ${RUNTIME_PACKAGE_CONFIG_FILE} | grep -cv "#")
	FUNC_INIT_PROGRESS_BAR
	FUNC_DRAW_PROGRESS_BAR ${count} ${maxcount}

	rm -rf "${SYSTEM_PATH_INCLUDES_CONFIG_COPY}"
	mkdir -p "${SYSTEM_PATH_INCLUDES_CONFIG_COPY}"
	find "${SYSTEM_PATH_INCLUDES_CONFIG}/" -mindepth 1 -maxdepth 1 -name "*.conf" -exec cp {} ${SYSTEM_PATH_INCLUDES_CONFIG_COPY}/ \;
	while true; do
		newconf=false
		for entry in $(grep -H "." "${SYSTEM_PATH_INCLUDES_CONFIG_COPY}"/*.conf 2>/dev/null | grep -v "#" | grep ":<" | sort | uniq); do
			entry_name=${entry//*:/}
			if ! test -e "${SYSTEM_PATH_INCLUDES_CONFIG_COPY}/${entry_name//[<|>]/}.conf"; then
				if ! test -e "${SYSTEM_PATH_INCLUDES_CONFIG}/config/${entry_name//[<|>]/}.conf"; then
					clear
					echo ""
					echo "Missing includes file:"
					echo "  => ${entry_name}"
					echo ""
					echo "Source file:"
					echo "  => $(basename ${entry//:*/})"
					echo ""
					echo "Exiting now!"
					echo ""
					return 1
				else
					newconf=true
					cat "${SYSTEM_PATH_INCLUDES_CONFIG}/config/${entry_name//[<|>]/}.conf" >"${SYSTEM_PATH_INCLUDES_CONFIG_COPY}/${entry_name//[<|>]/}.conf"
				fi
			fi
		done
		if [ x"${newconf}" == x"false" ]; then
			break
		fi
	done

	cat ${SYSTEM_PATH_INCLUDES_CONFIG_COPY}/*.conf 2>/dev/null | grep -v "#" | grep "." | sort | uniq >${SYSTEM_CONFIG_BUILDTOOLS_FILTER}
	rm -f ${SYSTEM_CONFIG_BUILDTOOLS_TEMP}

	# Parse the buildtool configuration file.
	for entry in $(grep "." ${RUNTIME_PACKAGE_CONFIG_FILE} | grep -v "#"); do

		# Update the progress bar.
		let count=count+1
		FUNC_DRAW_PROGRESS_BAR ${count} ${maxcount}

		# Get the buildtool name.
		btname="${SYSTEM_PATH_BUILDTOOLS}/${entry}/${entry#*/}.${BUILDTOOL_SUFFIX}"
		if ! test -e "${btname}"; then
			clear
			echo ""
			echo "Cannot find buildtool for ${entry}!"
			echo "Please check ${RUNTIME_PACKAGE_CONFIG_FILE}"
			echo ""
			echo "Exiting now!"
			echo ""
			return 1
		fi

		# Get the package name.
		pkgname=`grep "^PACKAGE_NAME=" ${btname}`
		name=${pkgname#*=}

		# Exclude package from the build?
		# Check for buildtool name and package name in filter so we can
		# exclude a package or an specific package version.
		# Example: -xorg-server, -xorg-server20
		check=`grep -e "^-${entry#*/}$" -e "^-${name}$" ${SYSTEM_CONFIG_BUILDTOOLS_FILTER}`
		if [ x"${check}" != x"" ]; then
			# Yes, ignore it...
			echo "${entry}" >>${SYSTEM_DEBUG_LOGFILE_SKIPPED_PACKAGES}
			continue
		fi

		# Include package?
		# Check for buildtool name and package name in filter so we can
		# include a package or an specific package version.
		# Example: +xorg-server, +xorg-server20
		check=`grep -e "^+${entry#*/}$" -e "^+${name}$" ${SYSTEM_CONFIG_BUILDTOOLS_FILTER}`
		if [ x"${check}" != x"" ]; then
			# Yes, add package to the filter.
			echo "${entry}" >>${SYSTEM_CONFIG_BUILDTOOLS_TEMP}
			let pkgcount=pkgcount+1
		else
			# Include complete package group?
			group="${entry%%/*}"
			check=`grep "^@${group}$" ${SYSTEM_CONFIG_BUILDTOOLS_FILTER}`
			if [ x"${check}" != x"" ]; then
				# Yes, add package to the filter.
				echo "${entry}" >>${SYSTEM_CONFIG_BUILDTOOLS_TEMP}
				let pkgcount=pkgcount+1
			else
				# If not, ignore it...
				echo "${entry}" >>${SYSTEM_DEBUG_LOGFILE_SKIPPED_PACKAGES}
			fi
		fi

	done

	# Clear the progress bar.
	FUNC_CLEAR_PROGRESS_BAR

	# Did we find any packages to be installed?
	if [ ${pkgcount} -eq 0 ]; then
		# No display an error message, exit the buildsystem.
		FUNC_MESSAGE_BOX "ERROR" "No packages selected.\nNothing to build.\n\nExiting now!"
		FUNC_CLEAR_SCREEN
		return 1
	fi
	RUNTIME_PACKAGE_CONFIG_FILE="${SYSTEM_CONFIG_BUILDTOOLS_TEMP}"
	RUNTIME_SYSTEM_CUSTOM_CONFIG=true
	return 0
}

# Include contents of an include.conf file to the list
# of packages to be removed for the current buildtool.
READ_REMOVE_PACKAGES_FROM_INCLUDE() {
	local include_conf_entry="$1"
	local current_buildtool="$2"
	local replacepkg_list="$3"
	local include_conf_list="$4"
	local remove_pkg=""
	local include_conf_path=""
	local pkg_group=""
	local pkg_name=""
	local pkg_list=""
	local pkg_remove_name=""
	local pkg_list_check=""
	local pkg_found=false
	local buildtool_list=""
	local buildtool_name=""
	local check=""
	local include_conf_checked=false

	# Get the include filename from the INCLUDE:<*> entry in
	# the package configuration file '.config'.
	include_conf_entry=${include_conf_entry//[<>]/}

	# Search for the INCLUDE.CONF file in the system path.
	include_conf_path="${SYSTEM_PATH_INCLUDES_CONFIG}"
	if ! test -e "${include_conf_path}/${include_conf_entry}.conf"; then
		# Search for the INCLUDE.CONF file in the archive path.
		include_conf_path="${SYSTEM_PATH_INCLUDES_CONFIG}/config"
		if ! test -e "${include_conf_path}/${include_conf_entry}.conf"; then
			# Not found => Error.
			clear
			echo
			echo "Missing includes file: ${include_conf_entry}.conf"
			echo "Source file: ${current_buildtool}"
			echo
			echo "Exiting now!"
			echo
			return 1
		fi
	fi

	# Read all include file entries...
	for remove_pkg in $(grep "." "${include_conf_path}/${include_conf_entry}.conf" | grep -v "^#" | grep -v "^-" | grep -v "^+"); do

		# Check for remove all buildtools of a group...
		if [ x"${remove_pkg#=@}" != x"${remove_pkg}" ]; then
			# Add group to the replacepkg list.
			# Get a list of buildtools of the requested group.
			buildtool_list=$(grep "^${remove_pkg#@}/" ${SYSTEM_CONFIG_BUILDTOOLS} | sed "s,.*/,,g")
			for buildtool_name in ${buildtool_list}; do
				# Get the name of the package to be installed.
				# The list of packages to be removed includes the real package name to be removed.
				pkg_name=`grep "^PACKAGE_NAME=" ${SYSTEM_PATH_BUILDTOOLS}/${remove_pkg#@}/${buildtool_name}/${buildtool_name}.${BUILDTOOL_SUFFIX} | sed "s,.*=,,g"`
				if FUNC_CHECK_PACKAGE_INSTALLED ${pkg_name}; then
					for pkg_list in ${RETURN_VALUE}; do
						pkg_remove_name=${pkg_list%-*}			# Remove build/tag
						pkg_remove_name=${pkg_remove_name%-*}	# Remove arch
						pkg_remove_name=${pkg_remove_name%-*}	# Remove version/release
						# Do we have the package allready in the list of packages to be removed?
						pkg_found=false
						for pkg_list_check in ${replacepkg_list}; do
							if [ x"${pkg_list_check}" == x"${pkg_remove_name}" ]; then
								# Yes, continue...
								pkg_found=true
								break
							fi
						done
						if ! ${pkg_found:-false}; then
							# Not yet included, add package to list.
							replacepkg_list="${replacepkg_list} ${pkg_remove_name}"
						fi
					done
				fi
			done
			continue
		fi

		# Check for reading another include.conf file.
		if [ x"${remove_pkg#<}" != x"${remove_pkg}" ]; then
			# Fix: Try to work around circular dependencies.
			#      Do we have checked the include.conf file already?
			include_conf_checked=false
			for check in ${include_conf_list}; do
				if [ x"${check}" == x"${remove_pkg}" ]; then
					include_conf_checked=true
					break
				fi
			done
			if ! ${include_conf_checked}; then
				# Add another config file...
				include_conf_list="${include_conf_list} ${remove_pkg}"
				READ_REMOVE_PACKAGES_FROM_INCLUDE "${remove_pkg}" "${current_buildtool}" "${replacepkg_list}" "${include_conf_list}"
				# Any errors?
				if test $? -eq 0 ; then
					# No, set the new list of packages to be removed.
					replacepkg_list="${RETURN_VALUE}"
				else
					return 1
				fi
			fi
			continue
		fi

		# Get the name of the package to be installed.
		# The list of packages to be removed includes the real package name to be removed.
		pkg_group=`grep "/${remove_pkg#=}$/" ${SYSTEM_CONFIG_BUILDTOOLS} | sed "s,/.*,,g"`
		if [ x"${pkg_group}" == x"" ]; then
			pkg_name=${remove_pkg#=}
		else
			pkg_name=`grep "^PACKAGE_NAME=" ${SYSTEM_PATH_BUILDTOOLS}/${pkg_group}/${buildtool_name}/${buildtool_name}.${BUILDTOOL_SUFFIX} | sed "s,.*=,,g"`
		fi
		if FUNC_CHECK_PACKAGE_INSTALLED ${pkg_name}; then
			for pkg_list in ${RETURN_VALUE}; do
				pkg_remove_name=${pkg_list%-*}			# Remove build/tag
				pkg_remove_name=${pkg_remove_name%-*}	# Remove arch
				pkg_remove_name=${pkg_remove_name%-*}	# Remove version/release
				# Add package to the list of packages to be removed.
				# Do we have the package allready in the list?
				pkg_found=false
				for pkg_list_check in ${replacepkg_list}; do
					if [ x"${pkg_list_check}" == x"${pkg_remove_name}" ]; then
						# Yes, continue...
						pkg_found=true
						break
					fi
				done
				if ! ${pkg_found:-false}; then
					# Not yet included, add package to list.
					replacepkg_list="${replacepkg_list} ${pkg_remove_name}"
				fi
			done
		fi

	done

	# Return the updated list of required packages.
	RETURN_VALUE="${replacepkg_list}"
	return 0
}

# Include contents of an include.conf file to the list
# of required packages for the current buildtool.
READ_REQUIRED_PACKAGES_FROM_INCLUDE() {
	local include_conf_entry="$1"
	local current_buildtool="$2"
	local required_list="$3"
	local include_conf_list="$4"
	local include_pkg=""
	local include_conf_path=""
	local pkg_name=""
	local pkg_list_check=""
	local pkg_found=false
	local buildtool_list=""
	local buildtool_name=""
	local check=""
	local include_conf_checked=false

	# Get the include filename from the INCLUDE:<*> entry in
	# the package configuration file '.config'.
	include_conf_entry=${include_conf_entry//[<>]/}

	# Search for the INCLUDE.CONF file in the system path.
	include_conf_path="${SYSTEM_PATH_INCLUDES_CONFIG}"
	if ! test -e "${include_conf_path}/${include_conf_entry}.conf"; then
		# Search for the INCLUDE.CONF file in the archive path.
		include_conf_path="${SYSTEM_PATH_INCLUDES_CONFIG}/config"
		if ! test -e "${include_conf_path}/${include_conf_entry}.conf"; then
			# Not found => Error.
			clear
			echo
			echo "Missing includes file: ${include_conf_entry}.conf"
			echo "Source file: ${current_buildtool}"
			echo
			echo "Exiting now!"
			echo
			return 1
		fi
	fi

	# Read all include file entries...
	for include_pkg in $(grep "." "${include_conf_path}/${include_conf_entry}.conf" | grep -v "^#" | grep -v "^-" | grep -v "^=" | sed "s,^+,,g"); do

		# Check for including all buildtools of a group...
		if [ x"${include_pkg#@}" != x"${include_pkg}" ]; then
			# Add group to the include list.
			# Get a list of buildtools of the requested group.
			buildtool_list=$(grep "^${include_pkg#@}/" ${SYSTEM_CONFIG_BUILDTOOLS} | sed "s,.*/,,g")
			for buildtool_name in ${buildtool_list}; do
				# Is buildtool blacklisted?
				check=$(grep "^${buildtool_name##*/}$" ${SYSTEM_CONFIG_PACKAGE_BLACKLIST})
				if [ x"${check}" == x"" ]; then
					# Exclude buildtool from list?
					# This covers the following rule:
					#   -> include.conf
					#      # Inlcude all buildtools from group a1...
					#      @a1
					#      # Include all buildtools except the following packages...
					#      -avahi
					check=$(grep "^-${buildtool_name##*/}$" "${include_conf_path}/${include_conf_entry}.conf")
					if [ x"${check}" == x"" ]; then
						# No, get the name of the package to be installed.
						# The list of required packages includes the real package name to be installed.
						pkg_name=`grep "^PACKAGE_NAME=" ${SYSTEM_PATH_BUILDTOOLS}/${include_pkg#@}/${buildtool_name}/${buildtool_name}.${BUILDTOOL_SUFFIX} | sed "s,.*=,,g"`
						# Do we have the package allready in the list of required packages?
						pkg_found=false
						for pkg_list_check in ${required_list}; do
							if [ x"${pkg_list_check}" == x"${pkg_name}" ]; then
								# Yes, continue...
								pkg_found=true
								break
							fi
						done
						if ! ${pkg_found:-false}; then
							# Not yet included, add package to required list.
							required_list="${required_list} ${pkg_name}"
						fi
					fi
				fi
			done
			continue
		fi

		# Check for reading another include.conf file.
		if [ x"${include_pkg#<}" != x"${include_pkg}" ]; then
			# Fix: Try to work around circular dependencies.
			#      Do we have checked the include.conf file already?
			include_conf_checked=false
			for check in ${include_conf_list}; do
				if [ x"${check}" == x"${include_pkg}" ]; then
					include_conf_checked=true
					break
				fi
			done
			if ! ${include_conf_checked}; then
				# Add another config file...
				include_conf_list="${include_conf_list} ${include_pkg}"
				READ_REQUIRED_PACKAGES_FROM_INCLUDE "${include_pkg}" "${current_buildtool}" "${required_list}" "${include_conf_list}"
				# Any errors?
				if test $? -eq 0 ; then
					# No, set the new list of required packages.
					required_list="${RETURN_VALUE}"
				else
					return 1
				fi
			fi
			continue
		fi

		# Add package to the required list.
		# Do we have the package allready in the list of required packages?
		pkg_found=false
		for pkg_list_check in ${required_list}; do
			if [ x"${pkg_list_check}" == x"${include_pkg}" ]; then
				# Yes, continue...
				pkg_found=true
				break
			fi
		done
		if ! ${pkg_found:-false}; then
			# Not yet included, add package to required list.
			required_list="${required_list} ${include_pkg}"
		fi

	done

	# Return the updated list of required packages.
	RETURN_VALUE="${required_list}"
	return 0
}

# Load package configuration from packages.conf.
INITIALIZE_PACKAGE_CONFIGURATION() {
	local entry=0
	local count=0
	local count_bar=0
	local data=""
	local data_CATEGORY=""
	local data_FLAGS=""
	local data_REPLACEPKG=""
	local data_OPTIONAL=""
	local data_DEPS=""
	local data_REQUIRED=""
	local data_ADDPKG=""
	local data_ETA=""
	local name=""
	local group=""
	local file=""
	local buildtool=""
	local buildtool_path=""
	local buildtool_script=""
	local pkginfo_file=${SYSTEM_PATH_TEMP}/pkginfo
	local check=""
	local config_file=""
	local language=""
	local source=0

	# Get the total count of buildtools. Will be updated at the end of the function.
	SYSDB_PACKAGE_COUNT=$(grep "." ${RUNTIME_PACKAGE_CONFIG_FILE} | grep -cv "#")

	# Only use cache in debug mode.
	if ${RUNTIME_OPTION_DEBUG_MODE:-false}; then
		if ! ${RUNTIME_SYSTEM_RESTART}; then
			if ! ${RUNTIME_SYSTEM_USERMODE}; then
				if [ -e ${SYSTEM_CONFIG_BUILDTOOLS_CACHE} -a ! -e ${SYSTEM_CONFIG_BUILDTOOLS_FILTER} ]; then
					check="$(find ${SYSTEM_PATH_BUILDTOOLS}/ -type f -newer ${SYSTEM_CONFIG_BUILDTOOLS_CACHE} | grep -v \.config | grep -v TAGFILE | grep -v INCLUDE)"
					if [ x"${check}" == x"" ]; then
						if [ x"$(grep ^#@:${RUNTIME_PACKAGE_CONFIG_FILE}$ ${SYSTEM_CONFIG_BUILDTOOLS_CACHE})" != x"" ]; then
							FUNC_PROGRESS_BOX "Loading package data from cache..."
							# Load package configuration from cache.
							. ${SYSTEM_CONFIG_BUILDTOOLS_CACHE}
							if [ x"${OPTION_BUILD_GERMAN_PACKAGES}" == x"${SYSDB_BUILD_GERMAN_PACKAGES}" ]; then
								# Initialize remaining package settings.
								let count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
									let count=count+1
									# Package blacklisted?
									check=$(grep "^${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}$" ${SYSTEM_CONFIG_PACKAGE_BLACKLIST})
									if [ x"${check}" != x"" ]; then
										SYSDB_PACKAGE_REMOVED_BY_USER[${count}]=true
									else
										SYSDB_PACKAGE_REMOVED_BY_USER[${count}]=false
									fi
									# Set default values.
									SYSDB_PACKAGE_ENABLED[${count}]=true
									SYSDB_PACKAGE_DISABLED[${count}]=false
									# Auto-select all packages?
									if [ x${RUNTIME_SYSTEM_USERMODE} == xtrue -o x${RUNTIME_SYSTEM_CUSTOM_CONFIG} == xtrue ]; then
										SYSDB_PACKAGE_AUTOSELECT[${count}]=true
									else
										SYSDB_PACKAGE_AUTOSELECT[${count}]=false
									fi
									SYSDB_PACKAGE_DEPOPT_SELECT[${count}]=false
									SYSDB_PACKAGE_INSTALLED[${count}]=""
									SYSDB_PACKAGE_INSTALLED_BUILD[${count}]=0
									SYSDB_PACKAGE_INSTALLED_ARCH[${count}]=""
									SYSDB_PACKAGE_INSTALLED_VERSION[${count}]=0
									SYSDB_PACKAGE_INSTALLED_RELEASE[${count}]=""
									SYSDB_PACKAGE_INSTALLED_TAG[${count}]=""
									SYSDB_PACKAGE_REMOVE[${count}]=""
									SYSDB_PACKAGE_SOURCES_CHECKED[${count}]=false
									SYSDB_PACKAGE_OPTIONAL_FOR_PACKAGE[${count}]=""
									SYSDB_PACKAGE_REBUILD_FOR_PACKAGE[${count}]=""
									SYSDB_PACKAGE_REQUIRED_FOR_PACKAGE[${count}]=""
									SYSDB_PACKAGE_DEPOPT_CHECKED[${count}]=false
									SYSDB_PACKAGE_RUNTIME_DEPOPT_STATUS[${count}]=""
									SYSDB_PACKAGE_ETA[${count}]=$(grep -m1 "^ETA:" ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/.config)
									SYSDB_PACKAGE_ETA[${count}]=${SYSDB_PACKAGE_ETA[${count}]##*:}
								done
								return 0
							fi
						fi
					fi
				fi
			fi
		fi
	fi

	# Prepare screen...
	FUNC_PROGRESS_BOX "Loading package data..."
	FUNC_INIT_PROGRESS_BAR

	# Parse the buildtool configuration file.
	rm -rf ${SYSTEM_DEBUG_LOGFILE_BLACKLISTED}
	for entry in $(grep "." ${RUNTIME_PACKAGE_CONFIG_FILE} | grep -v "#"); do

		name="${entry##*/}"
		name="${name%%_de}"
		name="${name%%_x64}"

		group="${entry%%/*}"

		buildtool="${entry##*/}"
		buildtool_path="${SYSTEM_PATH_BUILDTOOLS}/${group}/${buildtool}"
		buildtool_script="${SYSTEM_PATH_BUILDTOOLS}/${group}/${buildtool}/${buildtool}.${BUILDTOOL_SUFFIX}"
		config_file="${SYSTEM_PATH_BUILDTOOLS}/${group}/${buildtool}/.config"

		# Check if the package does exist:
		if [ ! -e ${buildtool_script} ]; then
			FUNC_CLEAR_SCREEN
			echo "Error: Unknown package!"
			echo "       Name: ${entry}"
			return 1
		fi

		# Check if .config file does exist:
		if [ ! -e ${config_file} ]; then
			FUNC_CLEAR_SCREEN
			echo "Error: Missing .config file!"
			echo "       Name: ${entry}"
			return 1
		else
			#config_data=$(grep -v "#" ${config_file} | grep "." | sed "s,^,@@,g")
			config_data=""; while read line; do
				if [ x"${line###}" == x"${line}" ]; then
					if [ x"${line}" != x"" ]; then
						config_data="${config_data} @@${line}"
					fi
				fi
			done <${config_file}
		fi

		# Display the progress bar.
		let count_bar=count_bar+1
		FUNC_DRAW_PROGRESS_BAR ${count_bar} ${SYSDB_PACKAGE_COUNT}

		# Package blacklisted?
		if ! ${RUNTIME_SYSTEM_RESTART}; then
			check=$(grep "^${entry##*/}$" ${SYSTEM_CONFIG_PACKAGE_BLACKLIST})
			if [ x"${check}" != x"" ]; then
				if [ ! -e ${SYSTEM_DEBUG_LOGFILE_BLACKLISTED} ]; then
					echo "Blacklisted packages:" >${SYSTEM_DEBUG_LOGFILE_BLACKLISTED}
					echo "${RULER1}" >>${SYSTEM_DEBUG_LOGFILE_BLACKLISTED}
				fi
				echo "  => ${group}/${buildtool}" >>${SYSTEM_DEBUG_LOGFILE_BLACKLISTED}
				continue
			fi
		fi

		# Build the package on the detected platform?
		if [ x"${config_data//@@PLATFORM:/}" != x"${config_data}" ]; then
			case ${OS_ARCH} in
				"x86_64")	if [ x"${config_data//@@PLATFORM:SKIP_FOR_X86_64/}" != x"${config_data}" ]; then
								continue
							fi;;
				*)			if [ x"${config_data//@@PLATFORM:BUILD_ONLY_FOR_X86_64/}" != x"${config_data}" ]; then
								continue
							fi;;
			esac
			if ( grep "^PLATFORM:REQ${OS_TYPE}" ${config_file} &>/dev/null ); then
				if ! ( grep "PLATFORM:REQ${OS_TYPE}${OS_VERSION_CODE}" ${config_file} &>/dev/null ); then
					continue
				fi
			fi
			if [ x"${config_data//@@PLATFORM:NOT${OS_TYPE}${OS_VERSION_CODE}}" != x"${config_data}" ]; then
				continue
			fi
			if ( grep "^PLATFORM:REQPLATFORM" ${config_file} &>/dev/null ); then
				if ! ( grep "^PLATFORM:REQPLATFORM_${OS_PLATFORM_CODE}" ${config_file} &>/dev/null ); then
					continue
				fi
			fi
		fi

		# Build the package for the selected language?
		if [ x"${config_data//@@LANGUAGE:/}" != x"${config_data}" ]; then
			if ! ${RUNTIME_CLI_MODE:-false}; then
				# Backup current language setting.
				language="${OPTION_BUILD_GERMAN_PACKAGES}"
				# Which packages to build (english/german)?
				INITIALIZE_LANGUAGE
				# Anything changed? If so, restore screen...
				if [ x"${language}" != x"${OPTION_BUILD_GERMAN_PACKAGES}" ]; then
					FUNC_PROGRESS_BOX "Loading package data..."
				fi
				if [ x"${config_data//@@LANGUAGE:GERMAN/}" != x"${config_data}" ]; then
					if ! ${OPTION_BUILD_GERMAN_PACKAGES}; then continue; fi
				fi
				if [ x"${config_data//@@LANGUAGE:ENGLISH/}" != x"${config_data}" ]; then
					if   ${OPTION_BUILD_GERMAN_PACKAGES}; then continue; fi
				fi
			fi
		fi

		# Package is valid for the selected platform, add to database.
		let count=count+1
		SYSDB_PACKAGE_ENABLED[${count}]=true
		SYSDB_PACKAGE_DISABLED[${count}]=false
		SYSDB_PACKAGE_REMOVED_BY_USER[${count}]=false
		SYSDB_PACKAGE_DEPOPT_SELECT[${count}]=false
		SYSDB_PACKAGE_DEPOPT_CHECKED[${count}]=false
		SYSDB_PACKAGE_RUNTIME_DEPOPT_STATUS[${count}]=""
		SYSDB_PACKAGE_SOURCES_CHECKED[${count}]=false

		# When in 'RESTART' mode, mark all packages as 'auto-selected'.
		if ${RUNTIME_SYSTEM_RESTART}; then
			SYSDB_PACKAGE_AUTOSELECT[${count}]=true
		else
			# Auto-select all packages?
			if [ x${RUNTIME_SYSTEM_USERMODE} == xtrue -o x${RUNTIME_SYSTEM_CUSTOM_CONFIG} == xtrue ]; then
				SYSDB_PACKAGE_AUTOSELECT[${count}]=true
			else
				SYSDB_PACKAGE_AUTOSELECT[${count}]=false
			fi
		fi

		# Orignal name including '_de' and '_x64'
		SYSDB_PACKAGE_ORIGINAL_NAME[${count}]="${entry##*/}"

		# Other package releated information.
		SYSDB_PACKAGE_GROUP[${count}]="${group}"
		SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]="${buildtool}"
		SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]="${buildtool_path}"
		SYSDB_PACKAGE_BUILDTOOL_SCRIPT[${count}]="${buildtool_script##*/}"
		SYSDB_PACKAGE_INSTALLED[${count}]=""
		SYSDB_PACKAGE_INSTALLED_BUILD[${count}]=0
		SYSDB_PACKAGE_INSTALLED_ARCH[${count}]=""
		SYSDB_PACKAGE_INSTALLED_VERSION[${count}]=0
		SYSDB_PACKAGE_INSTALLED_RELEASE[${count}]=""
		SYSDB_PACKAGE_INSTALLED_TAG[${count}]=""

		# Clear buildtool variables.
		# (llvm currently includes 10 source packages)
		let source=0; while [ ${source} -lt 10 ]; do
			PACKAGE_URL[${source}]=""
			PACKAGE_SOURCE[${source}]=""
			let source=source+1
		done

		# Get package info from buildtool.
		grep "^PACKAGE_.*=" ${buildtool_script} >${pkginfo_file}
		. ${pkginfo_file}

		# Do some sanity checks:
		# A PACKAGE_RELEASE_TAG with a '-' might be a bad idea. Example:
		#   perl-SQlite-1.51-04-x86_64-1slp.tgz
		# In that case 'perl-SQlite-1.51' would be the package name and
		# '04' the package version if you remove arch/build/type from the
		# end of the package name.
		if test x"${PACKAGE_RELEASE_TAG}" != x"${PACKAGE_RELEASE_TAG//-/}"; then
			FUNC_CLEAR_SCREEN
			echo "Error: Bad package release tag!"
			echo "       Name: ${entry}"
			return 1
		fi

		# If we have to search for packages/sources, define the filter we have to use.
		# Set SYSDB_PACKAGE_NAME[] with real package name without '_de' and '_x64'
		case "${PACKAGE_NAME}" in
			kde-l10n )				SYSDB_PACKAGE_NAME[${count}]="${PACKAGE_NAME}"
									SYSDB_PACKAGE_SEARCH_NAME[${count}]="${PACKAGE_NAME}-*"
									SYSDB_PACKAGE_LANG[${count}]=true;;
			libreoffice-l10n )		SYSDB_PACKAGE_NAME[${count}]="${PACKAGE_NAME}"
									SYSDB_PACKAGE_SEARCH_NAME[${count}]="${PACKAGE_NAME}-*"
									SYSDB_PACKAGE_LANG[${count}]=true;;
			libreoffice-help )		SYSDB_PACKAGE_NAME[${count}]="${PACKAGE_NAME}"
									SYSDB_PACKAGE_SEARCH_NAME[${count}]="${PACKAGE_NAME}-*"
									SYSDB_PACKAGE_LANG[${count}]=true;;
			*)						SYSDB_PACKAGE_NAME[${count}]="${PACKAGE_NAME}"
									SYSDB_PACKAGE_SEARCH_NAME[${count}]="${SYSDB_PACKAGE_NAME[${count}]}"
									SYSDB_PACKAGE_LANG[${count}]=false;;
		esac

		# Copy package info to local data array.
		SYSDB_PACKAGE_NAME_SOURCE[${count}]="${PACKAGE_NAME_SOURCE}"

		SYSDB_PACKAGE_VERSION[${count}]="${PACKAGE_VERSION#r}"
		SYSDB_PACKAGE_VERSION_SOURCE[${count}]="${PACKAGE_VERSION_SOURCE#r}"

		SYSDB_PACKAGE_RELEASE[${count}]="${PACKAGE_RELEASE_TAG}"
		SYSDB_PACKAGE_RELEASE_SOURCE[${count}]="${PACKAGE_RELEASE_TAG_SOURCE}"

		SYSDB_PACKAGE_ARCH[${count}]="${PACKAGE_ARCH}"
		SYSDB_PACKAGE_TAG[${count}]="${PACKAGE_TAG}"
		SYSDB_PACKAGE_BUILD[${count}]="${PACKAGE_BUILD}"

		SYSDB_PACKAGE_TYPE[${count}]="${PACKAGE_TYPE}"

		let source=0; while [ true ]; do
			if [ x"${PACKAGE_SOURCE[${source}]}" == x"" ]; then
				break
			fi
			SYSDB_PACKAGE_SOURCE[${count}]="${SYSDB_PACKAGE_SOURCE[${count}]}@@@${PACKAGE_SOURCE[${source}]}"
			let source=source+1
		done
		SYSDB_PACKAGE_SOURCE[${count}]="${SYSDB_PACKAGE_SOURCE[${count}]#@@@}"

		SYSDB_PACKAGE_URL1[${count}]="${PACKAGE_URL[0]}"
		SYSDB_PACKAGE_URL2[${count}]="${PACKAGE_URL[1]}"
		SYSDB_PACKAGE_URL3[${count}]="${PACKAGE_URL[2]}"
		SYSDB_PACKAGE_URL4[${count}]="${PACKAGE_URL[3]}"

		# Set some default values.
		SYSDB_PACKAGE_EXTERNAL[${count}]=false
		SYSDB_PACKAGE_SKIP_MD5[${count}]=false

		# Parse the buildtool .config file.
		data_CATEGORY=""
		data_FLAGS=""
		data_EXTRAPKG=""
		data_REPLACEPKG=""
		data_OPTIONAL=""
		data_DEPS=""
		data_REQUIRED=""
		data_ADDPKG=""
		data_ETA=""

		for check in ${config_data}; do
			case ${check} in
				@@ETA:*)										data_ETA="${check##*:}";;
				@@SKIPMD5)										SYSDB_PACKAGE_SKIP_MD5[${count}]=true;;
				@@FLAGS:EXTERNAL)								SYSDB_PACKAGE_EXTERNAL[${count}]=true;;
				@@FLAGS:*)										data_FLAGS="${data_FLAGS} ${check##*:}";;
				@@CATEGORY:*)									data_CATEGORY="${data_CATEGORY} ${check##*:}";;
				@@EXTRAPKG:*)									data_EXTRAPKG="${data_EXTRAPKG} ${check##*:}";;
				@@OPTIONAL.${OS_TYPE}${OS_VERSION_CODE}:*)		data_OPTIONAL="${data_OPTIONAL} ${check##*:}";;
				@@OPTIONAL.${OS_PLATFORM_CODE}:*)				data_OPTIONAL="${data_OPTIONAL} ${check##*:}";;
				@@OPTIONAL:*)									data_OPTIONAL="${data_OPTIONAL} ${check##*:}";;
				@@REPLACEPKG.${OS_TYPE}${OS_VERSION_CODE}:*)	data_REPLACEPKG="${data_REPLACEPKG} ${check##*:}";;
				@@REPLACEPKG.${OS_PLATFORM_CODE}:*)				data_REPLACEPKG="${data_REPLACEPKG} ${check##*:}";;
				@@REPLACEPKG:*)									data_REPLACEPKG="${data_REPLACEPKG} ${check##*:}";;
				@@REBUILD.${OS_TYPE}${OS_VERSION_CODE}:*)		data_DEPS="${data_DEPS} ${check##*:}";;
				@@REBUILD.${OS_PLATFORM_CODE}:*)				data_DEPS="${data_DEPS} ${check##*:}";;
				@@REBUILD:*)									data_DEPS="${data_DEPS} ${check##*:}";;
				@@REQUIRED.${OS_TYPE}${OS_VERSION_CODE}:*)		data_REQUIRED="${data_REQUIRED} ${check##*:}";;
				@@REQUIRED.${OS_PLATFORM_CODE}:*)				data_REQUIRED="${data_REQUIRED} ${check##*:}";;
				@@REQUIRED:*)									data_REQUIRED="${data_REQUIRED} ${check##*:}";;
				@@INCLUDE:*)									READ_REQUIRED_PACKAGES_FROM_INCLUDE "${check##*:}" "${group}/${buildtool}/.config" "${data_REQUIRED}" "";
																if test $? -eq 0 ; then
																	data_REQUIRED="${RETURN_VALUE}"
																else
																	return 1
																fi
																READ_REMOVE_PACKAGES_FROM_INCLUDE "${check##*:}" "${group}/${buildtool}/.config" "${data_REPLACEPKG}" "";
																if test $? -eq 0 ; then
																	data_REPLACEPKG="${RETURN_VALUE}"
																else
																	return 1
																fi;;
				@@ADDPKG.${OS_TYPE}${OS_VERSION_CODE}:*)		data_ADDPKG="${data_ADDPKG} ${check##*:}";;
				@@ADDPKG.${OS_PLATFORM_CODE}:*)					data_ADDPKG="${data_ADDPKG} ${check##*:}";;
				@@ADDPKG:*)										data_ADDPKG="${data_ADDPKG} ${check##*:}";;
				@@ADDCONFIG:*)									READ_REQUIRED_PACKAGES_FROM_INCLUDE "${check##*:}" "${group}/${buildtool}/.config" "${data_ADDPKG}" "";
																if test $? -eq 0 ; then
																	data_ADDPKG="${RETURN_VALUE}"
																else
																	return 1
																fi

			esac
		done

		SYSDB_PACKAGE_FLAGS[${count}]="${data_FLAGS}"
		SYSDB_PACKAGE_REPLACE[${count}]="${data_REPLACEPKG}"
		SYSDB_PACKAGE_EXTRAPKG[${count}]="${data_EXTRAPKG}"
		SYSDB_PACKAGE_OPTIONAL[${count}]="${data_OPTIONAL}"
		SYSDB_PACKAGE_REBUILD[${count}]="${data_DEPS}"
		SYSDB_PACKAGE_REQUIRED[${count}]="${data_REQUIRED}"
		SYSDB_PACKAGE_ADDPKG[${count}]="${data_ADDPKG}"
		SYSDB_PACKAGE_ETA[${count}]="${data_ETA}"

		# Get description for the package.
		# SYSDB_PACKAGE_DESC[${count}]=$(grep -m1 "^${name}: .*(.*)" ${buildtool_path}/slack-desc | sed "s,^${name}: \(.*\),\1,g" | sed "s,.*(\(.*\)),\1,g")
		while read line; do
			if [ x"${line##${PACKAGE_NAME}:}" != x"${line}" ]; then
				SYSDB_PACKAGE_DESC[${count}]="${line##${PACKAGE_NAME}:*(}"
				SYSDB_PACKAGE_DESC[${count}]="${SYSDB_PACKAGE_DESC[${count}]%%)*}"
				break
			fi
		done <${buildtool_path}/slack-desc

	done

	# Clear the progress bar.
	FUNC_CLEAR_PROGRESS_BAR

	# Remove temporary package info gile.
	rm -f ${pkginfo_file}

	# Remember count of packages.
	let SYSDB_PACKAGE_COUNT=count

	# Only use cache in debug mode.
	if ${RUNTIME_OPTION_DEBUG_MODE}; then
		if ! ${RUNTIME_SYSTEM_RESTART}; then
			if ! ${RUNTIME_SYSTEM_USERMODE}; then
				if ${RUNTIME_CLI_MODE:-false}; then
					INITIALIZE_CACHE_FILE
				fi
			fi
		fi
	fi

	# Display debug info.
	if ${RUNTIME_OPTION_DEBUG_MODE}; then
		if [ -e ${SYSTEM_DEBUG_LOGFILE_BLACKLISTED} ]; then
			if ! ${RUNTIME_SYSTEM_RESTART}; then
				dialog	--title "${PROJECT_NAME} - DEBUGGING INFORMATION" \
						--backtitle "${PROJECT_BACKTITLE}" \
						--cr-wrap \
						--exit-label "${BUTTON_LABEL_OK}" \
						--textbox ${SYSTEM_DEBUG_LOGFILE_BLACKLISTED} 20 75
			fi
		fi
	fi

	return 0
}

# Create package status text.
INITIALIZE_PACKAGE_STATUS() {
	local check=""
	local package_installed_release_arch_and_build=""
	local package_release_arch_and_build=""
	local package_diff=""
	local package_buildtool_info=""
	local package_status=""
	local arch_installed_info=""
	local arch_package_info=""
	local release_installed_info=""
	local release_package_info=""
	local build_installed_info=""
	local build_package_info=""
	local info_installed=""
	local info_package=""
	local info_text=""
	local tag_info=""

	if [ x"${SYSDB_PACKAGE_STATUS[$1]}" == x"" ]; then
		if [ x"${SYSDB_PACKAGE_INSTALLED[$1]}" != x"" ]; then

			# Is installed package a buildtool?
			if [ x"${SYSDB_PACKAGE_INSTALLED_TAG[$1]}" == x"${SYSDB_PACKAGE_TAG[$1]}" ]; then
				# yes
				package_buildtool_info="; SLP"
			else
				# No
				package_buildtool_info="; NoSLP"
			fi

			# Get the numeric version of the package and the installed package.
			FUNC_GET_PACKAGE_NUMERIC_VERSION_ALL $1

			if [ ${NUMPKG_PACKAGE_VERSION} -gt ${NUMPKG_INSTALLED_VERSION} ]; then
				info_text="Update      ${SYSDB_PACKAGE_INSTALLED_VERSION[$1]:-??} < ${SYSDB_PACKAGE_VERSION[$1]}${SYSDB_PACKAGE_RELEASE[$1]}${package_buildtool_info}"
			else
				if [ ${NUMPKG_PACKAGE_VERSION} -lt ${NUMPKG_INSTALLED_VERSION} ]; then
					info_text="Installed   ${SYSDB_PACKAGE_INSTALLED_VERSION[$1]:-??}${package_buildtool_info}"
				else
					# Package version of the installed package and the package to be build are equal.
					package_installed_release_arch_and_build=${SYSDB_PACKAGE_INSTALLED_RELEASE[$1]}-${SYSDB_PACKAGE_INSTALLED_ARCH[$1]}-${SYSDB_PACKAGE_INSTALLED_BUILD[$1]}
					package_release_arch_and_build=${SYSDB_PACKAGE_RELEASE[$1]}-${SYSDB_PACKAGE_ARCH[$1]}-${SYSDB_PACKAGE_BUILD[$1]}

					# Is package and installed package of the same build/arch?
					if [ x"${package_installed_release_arch_and_build}" != x"${package_release_arch_and_build}" ]; then

						# Package and installed package do have different release/arch/build.
						arch_installed_info=""
						arch_package_info=""
						build_installed_info=""
						build_package_info=""
						release_installed_info="${SYSDB_PACKAGE_INSTALLED_RELEASE[$1]}"
						release_package_info="${SYSDB_PACKAGE_RELEASE[$1]}"
						#release_installed_info=""
						#release_package_info=""
						tag_info=""

						# Is package and installed package of different arch?
						if [ x"${SYSDB_PACKAGE_INSTALLED_ARCH[$1]}" != x"${SYSDB_PACKAGE_ARCH[$1]}" ]; then
							# yes
							arch_installed_info="-${SYSDB_PACKAGE_INSTALLED_ARCH[$1]}"
							arch_package_info="-${SYSDB_PACKAGE_ARCH[$1]}"
						fi

						# Is package and installed package of different build?
						if [ x"${SYSDB_PACKAGE_INSTALLED_BUILD[$1]}" != x"${SYSDB_PACKAGE_BUILD[$1]}" ]; then
							# yes
							build_installed_info="-${SYSDB_PACKAGE_INSTALLED_BUILD[$1]}"
							build_package_info="-${SYSDB_PACKAGE_BUILD[$1]}"
						fi

						# Is package and installed package of different tag?
						if [ x"${SYSDB_PACKAGE_INSTALLED_TAG[$1]}" != x"${SYSDB_PACKAGE_TAG[$1]}" ]; then
							# yes
							tag_info="${PACKAGE_TAG}"
						fi

						# Is package and installed package of different release?
						if [ x"${SYSDB_PACKAGE_INSTALLED_RELEASE[$1]}" != x"${SYSDB_PACKAGE_RELEASE[$1]}" ]; then
							# yes
							package_status="Release  "
							package_diff="!="
						else
							# no, newer build available?
							if [ ${SYSDB_PACKAGE_INSTALLED_BUILD[$1]:-0} -lt ${SYSDB_PACKAGE_BUILD[$1]:-1} ]; then
								# yes
								package_status="New build"
								package_diff="<"
							else
								# no, ARCH changed?
								if [ x"${SYSDB_PACKAGE_INSTALLED_ARCH[$1]}" != x"${SYSDB_PACKAGE_ARCH[$1]}" ]; then
									package_status="New ARCH "
									package_diff="!="
								else
									package_status="Installed"
								fi
							fi
						fi

						if [ x"${package_status}" == x"Installed" ]; then
							info_installed="${SYSDB_PACKAGE_INSTALLED_VERSION[$1]:-??}${release_installed_info}${arch_installed_info}${build_installed_info}"
							info_package="${SYSDB_PACKAGE_VERSION[$1]}${release_package_info}${arch_package_info}${build_package_info}${tag_info}"
							if [ x"${info_installed}" != x"${info_package}" ]; then
								info_text="${package_status}   ${info_installed} != ${info_package}${package_buildtool_info}"
							else
								info_text="${package_status}   ${SYSDB_PACKAGE_VERSION[$1]}${SYSDB_PACKAGE_RELEASE[$1]}${arch_installed_info}${build_installed_info}${tag_info}${package_buildtool_info}"
							fi
						else
							info_installed="${SYSDB_PACKAGE_INSTALLED_VERSION[$1]:-??}${release_installed_info}${arch_installed_info}${build_installed_info}"
							info_package="${SYSDB_PACKAGE_VERSION[$1]}${release_package_info}${arch_package_info}${build_package_info}${tag_info}"
							info_text="${package_status}   ${info_installed} ${package_diff} ${info_package}${package_buildtool_info}"
						fi
					else
						info_text="Installed   ${SYSDB_PACKAGE_VERSION[$1]}${SYSDB_PACKAGE_RELEASE[$1]}${package_buildtool_info}"
					fi
				fi
			fi

			SYSDB_PACKAGE_STATUS[$1]="${info_text}"
		else
			SYSDB_PACKAGE_STATUS[$1]="New package ${SYSDB_PACKAGE_VERSION[$1]}${SYSDB_PACKAGE_RELEASE[$1]}"
		fi
	fi


	return 0
}

# Check for package configuration options.
# This will make it possible to modify the packages.conf during setup.
# You need to add some options to the packages.conf file:
# #@PKGOPT,tag1,tag2,...:Name:Description
# 'Name' and 'Description' will be used by the menu to let the user
# choose the preferred build option.
# 'tag1', 'tag2',... ist a list of tags that specifiy a section in the
# packages.conf file to be removed. Example:
# @PKGOPT,stable:Test:Testing packages
# @PKGOPT,testing:Stable:Stable packages
#   ##<stable>##
#   group1/package1
#   ##</stable>##
#   ##<testing>##
#   group2/package1
#   group2/package2
#   ##</testing>##
#   group3/package1
# In this example SETUP will delete all buildtools between <testing>
# </testing> if the 'Stable' build is selected from the menu.
INITIALIZE_PACKAGE_CONFIG_OPTIONS() {
	local check=""
	local pkgconfopt[0]=""
	local pkgconftitle[0]=""
	local pkgconfinfo[0]=""
	local pkgconfcount=0
	local pkgconf=""
	local check=""
	local check_pkgopts=""
	local check_opt_used=""
	local check_opt_found=false
	local count=0
	local config_option_name=""
	local help_text1=""
	local default_item=""

	# Print some info message...
	FUNC_PROGRESS_BOX "Checking for buildtool options..."

	# Do we have any packages.conf options?
	if ! test -e ${SYSTEM_CONFIG_BUILDTOOLS_OPTIONS}; then
		return 0
	fi

	# Any enabled options in the configuration file?
	check=$(grep "^#@PKGOPT," ${SYSTEM_CONFIG_BUILDTOOLS_OPTIONS})
	if [ x"${check}" != x"" ]; then

		# Yes, get a list of available options.
		grep "^#@PKGOPT," ${SYSTEM_CONFIG_BUILDTOOLS_OPTIONS} >${SYSTEM_CONFIG_PACKAGE_OPTIONS}
		cat ${RUNTIME_PACKAGE_CONFIG_FILE} >${SYSTEM_CONFIG_PACKAGE_OPTDATA}

		# Read the options into our database.
		while read pkgconf; do
			check_pkgopts=${pkgconf#\#@PKGOPT,}
			check_pkgopts=${check_pkgopts%%:*}
			check_opt_found=false
			for check_opt_used in ${check_pkgopts//,/ }; do
				check=$(grep "^##<${check_opt_used}>##$" ${SYSTEM_CONFIG_PACKAGE_OPTDATA})
				if [ x"${check}" != x"" ]; then
					check=$(grep "^##<\/${check_opt_used}>##$" ${SYSTEM_CONFIG_PACKAGE_OPTDATA})
					if [ x"${check}" != x"" ]; then
						local check_opt_found=true
						break
					fi
				fi
			done
			if ${check_opt_found:-false}; then
				let pkgconfcount=pkgconfcount+1
				pkgconftitle[${pkgconfcount}]=${pkgconf#*:}
				pkgconftitle[${pkgconfcount}]=${pkgconftitle[${pkgconfcount}]%%:*}
				pkgconfhelp[${pkgconfcount}]=${pkgconf##*:}
				pkgconfopt[${pkgconfcount}]=${pkgconf#\#@PKGOPT,}
				pkgconfopt[${pkgconfcount}]=${pkgconfopt[${pkgconfcount}]%%:*}
			fi
		done <${SYSTEM_CONFIG_PACKAGE_OPTIONS}

		# More then one option found?
		if [ ${pkgconfcount} -gt 1 ]; then

			default_item=${pkgconftitle[1]}
			while [ true ]; do

				# Yes, display a menu to choose the build.
				cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "${PROJECT_NAME} - SELECT LANGUAGE" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--ok-label "${BUTTON_LABEL_OK}" \\
		--cancel-label "${BUTTON_LABEL_EXIT}" \\
		--help-button \\
		--help-label "${BUTTON_LABEL_HELP}" \\
		--default-item "${default_item}" \\
		--menu "\\
The package configuration file includes some optional package settings.\n\\
\n\\
Depending on your choice the list of available buildtools will be updated. Be careful, some options might be experimental!\n\\
Even if you do not want to build any of these package options you have to make a choice here.\n\\
\n\\
Please choose which kind of package list to use:\n" 20 75 6 \\
EOF

				# Add the available options to the menu.
				let count=0
				while [ ${count} -lt ${pkgconfcount} ]; do
					let count=count+1
					cat <<EOF >>${DIALOG_SCRIPT}
"${pkgconftitle[${count}]}" "${pkgconfhelp[${count}]}" \\
EOF
				done

			cat <<EOF >>${DIALOG_SCRIPT}
 2> "${DIALOG_RETURN_VALUE}"
EOF

				# Start the script and offer the options select dialog.
				. "${DIALOG_SCRIPT}"

				DIALOG_KEYCODE=$?
				DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
				rm -f "${DIALOG_SCRIPT}"
				rm -f "${DIALOG_RETURN_VALUE}"

				# 'HELP' ?
				if test ${DIALOG_KEYCODE} -eq 2; then
					# Get the option we have to display the helppage for.
					config_option_name=${DIALOG_REPLY// >> */}
					config_option_name=${config_option_name//* /}

					# Create the help page.
					cat "${SYSTEM_CONFIG_BUILDTOOLS_OPTIONS}" | sed -n "/HELP:${config_option_name}$/,/HELP:${config_option_name}$/p" | grep -v "# HELP:" >${DIALOG_TEMP_FILE1}
					help_text1=$(cat ${DIALOG_TEMP_FILE1} | sed "s,# ,,g" | sed "s,^#$,,g" | sed "s,$,\\\n,g")

					# Display help.
					dialog	--title "${PROJECT_NAME} - INFORMATION" \
							--backtitle "${PROJECT_BACKTITLE}" \
							--cr-wrap \
							--msgbox "\n${config_option_name}:\n\n${help_text1}\n" 20 75

					# Back to menu loop.
					default_item="${config_option_name}"
					continue
				fi

				# 'CANCEL' or 'ESC' ?
				if test ${DIALOG_KEYCODE} -ne 0; then
					return 1
				else
					# Get the taglist from our database for the selected option.
					#cat ${RUNTIME_PACKAGE_CONFIG_FILE} >${SYSTEM_CONFIG_PACKAGE_OPTDATA}
					let count=0
					while [ ${count} -lt ${pkgconfcount} ]; do
						let count=count+1
						if [ x"${DIALOG_REPLY}" == x"${pkgconftitle[${count}]}" ]; then
							pkgconfcount=${count}
							break
						fi
					done
					break
				fi
			done
		fi
	fi
	if [ ${pkgconfcount} -gt 0 ]; then
		# Delete the sections from the packages.conf specified
		# by the taglist.
		#cat ${RUNTIME_PACKAGE_CONFIG_FILE} >${SYSTEM_CONFIG_PACKAGE_OPTDATA}
		for pkgconf in ${pkgconfopt[${pkgconfcount}]//,/ }; do
			sed -i "/^##<${pkgconf}>##$/,/^##<\/${pkgconf}>##$/ d" ${SYSTEM_CONFIG_PACKAGE_OPTDATA}
		done
		RUNTIME_PACKAGE_CONFIG_FILE="${SYSTEM_CONFIG_PACKAGE_OPTDATA}"
	fi
	return 0
}

# Initialize Buildtools...
INITIALIZE_BUILDTOOLS() {
	# Load package configuration.
	INITIALIZE_SYSTEM_DATABASE
	if ! ${RUNTIME_SYSTEM_RESTART}; then
		if ! ${OPTION_USE_INCLUDE_CONF_FILES:-false}; then
			# Check for configuration file options.
			INITIALIZE_PACKAGE_CONFIG_OPTIONS
			if test $? -ne 0 ; then
				FUNC_CLEAR_SCREEN
				return 1
			fi
		fi
		# Initialize group/EXTRAS chache file
		INITIALIZE_GROUP_EXTRAS_CACHE || return 1
		# Create buildtool filter
		if ${OPTION_USE_INCLUDE_CONF_FILES:-false}; then
			# Yes, do we have a includes directory?
			if test -d "${SYSTEM_PATH_INCLUDES_CONFIG}"; then
				INITIALIZE_BTFILTER || return 1
			fi
		fi
	fi
	INITIALIZE_PACKAGE_CONFIGURATION || return 1
	return 0
}

# Check for renamed packages and correct kde version.
INITIALIZE_CHECK_INSTALLED_PACKAGES() {
	local name=""
	local version=""
	local release=""
	local arch=""
	local build=""
	local tag=""
	local temp=""

	local version_installed=0
	local search=""
	local file=""
	local found_list=""
	local found_file=""
	local count_installed=0
	local count_installed_max=$(find /var/log/packages/ -type f | grep -c "^/var/log/packages/*" )
	local original_name=""
	local compare_name=""
	local first=true
	local replace=""

	# Create a database with installed packages.
	FUNC_PROGRESS_BOX "Analyzing installed packages..."

	# Check installed packages.
	FUNC_INIT_PROGRESS_BAR

	count=0
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

		# Update the progress bar.
		let count=count+1
		FUNC_DRAW_PROGRESS_BAR ${count} ${SYSDB_PACKAGE_COUNT}

		for package in $(find /var/log/packages/ -type f -name "${SYSDB_PACKAGE_NAME[${count}]}*" -printf "%f\n" | sort); do
			name=${package%-*-*-*}

			# For some package we need a different name.
			case ${name} in
				kde-l10n* )			name="${name%-*}";;
				libreoffice-l10n* )	name="${name%-*}";;
				libreoffice-help* )	name="${name%-*}";;
			esac

			if [ x"${name}" == x"${SYSDB_PACKAGE_NAME[${count}]}" ]; then
				if [ x"${SYSDB_PACKAGE_INSTALLED[${count}]}" == x"" ]; then
					# Add info from the first package to the current package.
					name=${package%-*-*-*}
					temp=${package#$name-}
					release=${temp%%-*}
					version=${release#r}
					version=${version//[a-zA-Z_]*/}
					release=${release#*$version}
					temp=${temp#*$version$release-}
					arch=${temp%-*}
					tag=${temp#$arch-}
					build=${tag//[a-zA-Z_]*/}
					tag=${tag#$build}
					build=${build:-0}
					SYSDB_PACKAGE_INSTALLED_TAG[${count}]="${tag}"
					SYSDB_PACKAGE_INSTALLED_BUILD[${count}]="${build}"
					SYSDB_PACKAGE_INSTALLED_ARCH[${count}]="${arch}"
					SYSDB_PACKAGE_INSTALLED_RELEASE[${count}]="${release}"
					SYSDB_PACKAGE_INSTALLED_VERSION[${count}]="${version}"
				fi
				SYSDB_PACKAGE_INSTALLED[${count}]="${SYSDB_PACKAGE_INSTALLED[${count}]} ${package}"
				SYSDB_PACKAGE_INSTALLED[${count}]="${SYSDB_PACKAGE_INSTALLED[${count}]# }"
				SYSDB_PACKAGE_REMOVE[${count}]="${SYSDB_PACKAGE_REMOVE[${count}]} ${package}"
				SYSDB_PACKAGE_REMOVE[${count}]="${SYSDB_PACKAGE_REMOVE[${count}]# }"
			fi
		done

		# Package not installed, set some default values.
		if [ x"${SYSDB_PACKAGE_INSTALLED[${count}]}" == x"" ]; then
			SYSDB_PACKAGE_INSTALLED_BUILD[${count}]="0"
			SYSDB_PACKAGE_INSTALLED_ARCH[${count}]=""
			SYSDB_PACKAGE_INSTALLED_VERSION[${count}]="0"
			SYSDB_PACKAGE_INSTALLED_RELEASE[${count}]=""
			SYSDB_PACKAGE_INSTALLED_TAG[${count}]=""
		fi

		# Check for replacement packages.
		for replacepkg in ${SYSDB_PACKAGE_REPLACE[${count}]}; do
			if FUNC_CHECK_PACKAGE_INSTALLED "${replacepkg}"; then
				SYSDB_PACKAGE_REMOVE[${count}]="${SYSDB_PACKAGE_REMOVE[${count}]} ${RETURN_VALUE}"
			fi
		done

	done

	# Clear the progress bar.
	FUNC_CLEAR_PROGRESS_BAR




	# Debug mode?
	if ${RUNTIME_OPTION_DEBUG_MODE}; then

		# Are we in restart mode?
		if ! ${RUNTIME_SYSTEM_RESTART}; then
			# No, delete existing logfiles.
			rm -f ${SYSTEM_DEBUG_LOGFILE_INSTALLED}
		fi

		# Clean up the database.
		FUNC_PROGRESS_BOX "Creating logfile..."

		count=0
		while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
			let count=count+1

			# Add 'Package installed' status to logfile.
			if ! ${RUNTIME_SYSTEM_RESTART}; then
				let count_custom=count_custom+1
				text="0000${count}"
				text="[${text:${#text}-4:4}]"
				text1="${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}${SPACEBAR}"
				text1="${text1:0:30}"
				if [ x"${SYSDB_PACKAGE_INSTALLED[${count}]}" != x"" ]; then
					first=true
					for file in ${SYSDB_PACKAGE_INSTALLED[${count}]}; do
						#text2="   >> ${SYSDB_PACKAGE_NAME[${count}]}-${SYSDB_PACKAGE_INSTALLED_VERSION[${count}]}${SYSDB_PACKAGE_INSTALLED_RELEASE[${count}]}-${SYSDB_PACKAGE_INSTALLED_ARCH[${count}]}-${SYSDB_PACKAGE_INSTALLED_BUILD[${count}]}${SYSDB_PACKAGE_INSTALLED_TAG[${count}]}"
						#echo "${text} ${text1}${text2}" >>"${SYSTEM_DEBUG_LOGFILE_INSTALLED}"
						if ${first}; then
							echo "${text} ${text1}   >> ${file}" >>"${SYSTEM_DEBUG_LOGFILE_INSTALLED}"
							first=false
						else
							echo "${SPACEBAR:0:37}   >> ${file}" >>"${SYSTEM_DEBUG_LOGFILE_INSTALLED}"
						fi
					done
				else
					# Try to find alternative package names.
					found_list=""
					for found_file in $(find /var/log/packages/ -name ${SYSDB_PACKAGE_NAME[${count}]}-* -printf '%f '); do
						found=${found_file%-*-*-*}	# Remove version-arch-build_tag
						if [ x"${found}" == x"${SYSDB_PACKAGE_NAME[${count}]}" ]; then
							found_list="${found_list}${found_file} "
						fi
					done
					if [ x"${found_list}" != x"" ]; then
						text2=" **** ${found_list}"
					else
						text2=" --  "
					fi
					echo "${text} ${text1}${text2}" >>"${SYSTEM_DEBUG_LOGFILE_INSTALLED}"
				fi
			fi
		done

		# Display debug info.
		if test -e ${SYSTEM_DEBUG_LOGFILE_INSTALLED}; then
			dialog	--title "${PROJECT_NAME} - DEBUGGING INFORMATION" \
					--backtitle "${PROJECT_BACKTITLE}" \
					--cr-wrap \
					--exit-label "${BUTTON_LABEL_OK}" \
					--textbox ${SYSTEM_DEBUG_LOGFILE_INSTALLED} 20 75
		fi
	fi

	return 0

}

# Just a separator...
SYS____________________MODIFY_BUILDORDER() {
	true
}

# Create entry for the edit buildorder package list.
BUILDSYS_MODIFY_BUILDORDER_CREATE_ENTRY() {
	INITIALIZE_PACKAGE_STATUS $1
	if [ x"$3" == x"" ]; then
		echo "\"${SYSDB_PACKAGE_ORIGINAL_NAME[$1]}\" \"${SYSDB_PACKAGE_STATUS[$1]}\"  \"$2\" \"${SYSDB_PACKAGE_DESC[$1]}\" \\" >>${DIALOG_TEMP_FILE1}
	else
		echo "\"${SYSDB_PACKAGE_ORIGINAL_NAME[$1]}\" \"${SYSDB_PACKAGE_STATUS[$1]}\"  \"$2\" \"${SYSDB_PACKAGE_DESC[$1]} (${SYSDB_PACKAGE_EXTRA_FOR_GROUPS[$1]## })\" \\" >>${DIALOG_TEMP_FILE1}
	fi
}

# Create the package list.
BUILDSYS_MODIFY_BUILDORDER_CREATE_LIST() {

	local count=0
	local entries=0
	local mode=""
	local dependency=0
	local package_installed=false
	local failed=false
	local select_type=$1
	local select_mode=$2
	local pkgbuild=""
	local pkginstalled=""

	FUNC_PROGRESS_BOX "Analyzing packages..."
	FUNC_INIT_PROGRESS_BAR

	# Create checklist
	rm -f ${DIALOG_TEMP_FILE1}
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

		# Draw the progress bar
		let count=count+1
		FUNC_DRAW_PROGRESS_BAR ${count} ${SYSDB_PACKAGE_COUNT}

		# Add packages to the list.
		case ${select_type} in
			REMOVE)			if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
								case ${select_mode} in
									off)	mode="off";;
									*)		mode="on";;
								esac
								BUILDSYS_MODIFY_BUILDORDER_CREATE_ENTRY ${count} ${mode}
								let entries=entries+1
							fi;;
			REMDPKG)		if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
								for dependency in ${SYSDB_PACKAGE_REBUILD[${count}]} ${SYSDB_PACKAGE_REQUIRED[${count}]}; do
									if FUNC_FIND_PACKAGE_BY_NAME ${dependency} ${SYSDB_PACKAGE_NAME[${count}]}; then
										if ! ${SYSDB_PACKAGE_ENABLED[${RETURN_VALUE}]}; then
											case ${select_mode} in
												off)	mode="off";;
												*)		mode="on";;
											esac
											BUILDSYS_MODIFY_BUILDORDER_CREATE_ENTRY ${count} ${mode}
											let entries=entries+1
											break
										fi
									fi
								done
							fi;;
			RESTART)		case ${select_mode} in
								off)	mode="off";;
								*)		mode="on";;
							esac
							BUILDSYS_MODIFY_BUILDORDER_CREATE_ENTRY ${count} ${mode}
							let entries=entries+1
							;;
			ADD)			failed=false
							if ! ${failed:-false}; then
								if ! ${SYSDB_PACKAGE_ENABLED[${count}]}; then
									case ${select_mode} in
										on)		mode="on";;
										*)		mode="off";;
									esac
									BUILDSYS_MODIFY_BUILDORDER_CREATE_ENTRY ${count} ${mode}
									let entries=entries+1
								fi
							fi;;
			UPDATE)			failed=false
							if ! ${failed:-false}; then
								if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
									if [ x"${SYSDB_PACKAGE_INSTALLED[${count}]}" != x"" ]; then
										case ${select_mode} in
											off)	mode="off";;
											*)		mode="on";;
										esac
										BUILDSYS_MODIFY_BUILDORDER_CREATE_ENTRY ${count} ${mode}
										let entries=entries+1
									fi
								fi
							fi;;
			FULL|SORT)		failed=false
							if ! ${failed:-false}; then
								if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
									mode="on"
								else
									mode="off"
								fi
								BUILDSYS_MODIFY_BUILDORDER_CREATE_ENTRY ${count} ${mode}
								let entries=entries+1
							fi;;
			SINGLE)			if [ x${RUNTIME_SYSTEM_USERMODE} == xtrue -o x${RUNTIME_SYSTEM_CUSTOM_CONFIG} == xtrue ]; then
								case ${select_mode} in
									off)	mode="off";;
									*)		mode="on";;
								esac
								BUILDSYS_MODIFY_BUILDORDER_CREATE_ENTRY ${count} ${mode}
								let entries=entries+1
							else
								failed=false
								if ! ${failed:-false}; then
									case ${select_mode} in
										default)	FUNC_CHECK_SINGLE_MODE_SELECTED ${count}
													mode="${RETURN_VALUE}";;
										on)			mode="on";;
										off)		mode="off";;
									esac
									BUILDSYS_MODIFY_BUILDORDER_CREATE_ENTRY ${count} ${mode}
									let entries=entries+1
								fi
							fi;;
			SYSUPDATE)		failed=false
							if ! ${failed:-false}; then
								if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
									case ${select_mode} in
										on)		mode="on";;
										*)		mode="off";;
									esac
									BUILDSYS_MODIFY_BUILDORDER_CREATE_ENTRY ${count} ${mode}
									let entries=entries+1
								fi
							fi;;
			EXTRAS)			if ! ${SYSDB_PACKAGE_ENABLED[${count}]}; then
								if [ x"${SYSDB_PACKAGE_EXTRA_FOR_GROUPS[${count}]}" != x"" ]; then
									if [ x"${SYSDB_PACKAGE_INSTALLED[${count}]}" != x"" ]; then
										pkginstalled=${SYSDB_PACKAGE_INSTALLED_VERSION[${count}]}${SYSDB_PACKAGE_INSTALLED_BUILD[${count}]}-${SYSDB_PACKAGE_INSTALLED_ARCH[${count}]}-${SYSDB_PACKAGE_INSTALLED_BUILD[${count}]}${SYSDB_PACKAGE_INSTALLED_TAG[${count}]}
										pkgbuild=${SYSDB_PACKAGE_VERSION[${count}]}${SYSDB_PACKAGE_BUILD[${count}]}-${SYSDB_PACKAGE_ARCH[${count}]}-${SYSDB_PACKAGE_BUILD[${count}]}${SYSDB_PACKAGE_TAG[${count}]}
										if [ x"${pkgbuild}" == x"${pkginstalled}" ]; then
											mode="off"
										else
											mode="on"
										fi
									else
										mode="on"
									fi
									BUILDSYS_MODIFY_BUILDORDER_CREATE_ENTRY ${count} ${mode} "EXTRA_GROUPS"
									let entries=entries+1
								fi
							fi;;
		esac

	done

	# Set number of buildtools found.
	let SYSDB_PACKAGE_LIST_COUNT=entries

	# Clear the progress bar.
	FUNC_CLEAR_PROGRESS_BAR

}

# Update the package list.
BUILDSYS_MODIFY_BUILDORDER_UPDATE_LIST() {

	local count=0
	local selected=false
	local selected_package_list="$2"

	FUNC_PROGRESS_BOX "Analyzing packages..."
	FUNC_INIT_PROGRESS_BAR

	# Create checklist
	rm -f ${DIALOG_TEMP_FILE1}
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

		# Draw the progress bar
		let count=count+1
		FUNC_DRAW_PROGRESS_BAR ${count} ${SYSDB_PACKAGE_COUNT}

		# Check if the current package was selected.
		selected=false
		for package in ${selected_package_list}; do
			if [ x"${package}" == x"${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}" ]; then
				selected=true
				break
			fi
		done

		# Add packages to the list.
		case $1 in
			REMOVE|\
			RESTART|\
			SYSUPDATE)				if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
										if ! ${selected}; then
											SYSDB_PACKAGE_ENABLED[${count}]=false
											SYSDB_PACKAGE_REMOVED_BY_USER[${count}]=true
										fi
									fi;;
			UPDATE)					if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
										if [ x"${SYSDB_PACKAGE_INSTALLED[${count}]}" != x"" ]; then
											if ! ${selected}; then
												SYSDB_PACKAGE_ENABLED[${count}]=false
												SYSDB_PACKAGE_REMOVED_BY_USER[${count}]=true
											fi
										fi
									fi;;
			REMDPKG)				if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
										for dependency in ${SYSDB_PACKAGE_REBUILD[${count}]} ${SYSDB_PACKAGE_REQUIRED[${count}]}; do
											if FUNC_FIND_PACKAGE_BY_NAME ${dependency} ${SYSDB_PACKAGE_NAME[${count}]}; then
												if ! ${SYSDB_PACKAGE_ENABLED[${RETURN_VALUE}]}; then
													if ! ${selected}; then
														SYSDB_PACKAGE_ENABLED[${count}]=false
														SYSDB_PACKAGE_REMOVED_BY_USER[${count}]=true
													fi
													break
												fi
											fi
										done
									fi;;
			ADD)					if ! ${SYSDB_PACKAGE_ENABLED[${count}]}; then
										if ${selected}; then
											SYSDB_PACKAGE_ENABLED[${count}]=true
											SYSDB_PACKAGE_REMOVED_BY_USER[${count}]=false
										fi
									fi;;
			FULL|SORT)				if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
										if ! ${selected}; then
											SYSDB_PACKAGE_ENABLED[${count}]=false
											SYSDB_PACKAGE_REMOVED_BY_USER[${count}]=true
										fi
									else
										if ${selected}; then
											SYSDB_PACKAGE_ENABLED[${count}]=true
											SYSDB_PACKAGE_REMOVED_BY_USER[${count}]=false
										fi
									fi;;
			SINGLE)					if ${selected}; then
										SYSDB_PACKAGE_ENABLED[${count}]=true
										SYSDB_PACKAGE_REMOVED_BY_USER[${count}]=false
									else
										SYSDB_PACKAGE_ENABLED[${count}]=false
										SYSDB_PACKAGE_REMOVED_BY_USER[${count}]=false
									fi;;
			EXTRAS)					if ${selected}; then
										SYSDB_PACKAGE_ENABLED[${count}]=true
										SYSDB_PACKAGE_REMOVED_BY_USER[${count}]=false
										SYSDB_PACKAGE_AUTOSELECT[${count}]=true
									fi;;
		esac

	done

	# Clear the progress bar.
	FUNC_CLEAR_PROGRESS_BAR

}

# Modify the buildorder.
BUILDSYS_MODIFY_BUILDORDER_EDIT_LIST() {

	local title=""
	local select_mode=$1

	BUILDSYS_MODIFY_BUILDORDER_CREATE_LIST ${select_mode} "default"

	# Do we have any packages not yet enabled?
	if [ ${SYSDB_PACKAGE_LIST_COUNT} -gt 0 ]; then

		# Endless menu loop...
		while true; do

			case ${select_mode} in
				ADD)				title="ADD EXTRA PACKAGES";
									text="Add extra packages to be installed. Some packages may have extra dependencies which can be resolved later.";;
				REMOVE|RESTART)		title="REMOVE PACKAGES";
									text="Remove packages from the list. Removing packages may break the build! Automatically selected dependencies will not be removed automatically.";;
				REMDPKG)			title="REMOVE PACKAGES WITH DEPENDENCIES";
									text="Remove packages that have extra dependencies. Removing packages may break the build!";;
				FULL)				title="ADD/REMOVE PACKAGES";
									text="This is a complete list of all packages in the current configuration, be carefull about which packages you add or remove!";;
				SINGLE)				title="SELECT PACKAGES";
									text="This is a list of all packages included for the build. \n ";;
				SYSUPDATE)			title="UPDATE PACKAGES";
									text="This is a list of packages to be updated. \n ";;
				UPDATE)				title="AVAILABLE UPDATES";
									text="This is a list of packages which can be updated. \n ";;
				EXTRAS)				title="ADD RECOMMENDED PACKAGES";
									text="This is a list of recommended packages to be installed. Unselecting packages may break the build!";;
				SORT)				title="EDIT PACKAGE LIST";
									text="This is a sorted list of all packages included in the current configuration, be carefull about which packages you add or remove!";;
				*)					title="EDIT PACKAGE LIST";
									text="Add/Remove packages. Use at pown risk! \n ";;
			esac

			# Yes, allow the user to enable additional packages.
			cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "${PROJECT_NAME} - ${title}" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--cr-wrap \\
		--ok-label "${BUTTON_LABEL_OK}" \\
		--cancel-label "$2" \\
		--extra-button \\
		--extra-label "${BUTTON_LABEL_EVERYTHING}" \\
		--help-button \\
		--help-label "${BUTTON_LABEL_NOTHING}" \\
		--item-help \\
		--checklist "\\
${text}\n\
Use spacebar to select/unselect and UP/DOWN arrow keys to scroll through the list. See bottom of the screen for package description.\n\
Note: SLP=Buildtool installed, NoSLP=Slackware/3rd party package.\n" 20 75 9 \\
EOF

			case $1 in
				SORT)	cat ${DIALOG_TEMP_FILE1} | sort >>${DIALOG_SCRIPT};;
				FULL)	cat ${DIALOG_TEMP_FILE1} >>${DIALOG_SCRIPT};;
				*)		cat ${DIALOG_TEMP_FILE1} | sort >>${DIALOG_SCRIPT};;
			esac

			cat <<EOF >>${DIALOG_SCRIPT}
 2> "${DIALOG_RETURN_VALUE}"
EOF

			# Start the script and offer the version select dialog.
			. "${DIALOG_SCRIPT}"

			DIALOG_KEYCODE=$?
			DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
			rm -f "${DIALOG_SCRIPT}"
			rm -f "${DIALOG_TEMP_FILE1}"
			rm -f "${DIALOG_RETURN_VALUE}"

			# Extra button?
			if test ${DIALOG_KEYCODE} -eq 3; then
				BUILDSYS_MODIFY_BUILDORDER_CREATE_LIST ${select_mode} "on"
				continue
			else
				if test ${DIALOG_KEYCODE} -eq 2; then
					BUILDSYS_MODIFY_BUILDORDER_CREATE_LIST ${select_mode} "off"
					continue
				fi
			fi
			break
		done

		# 'CANCEL' or 'ESC' ?
		if test ${DIALOG_KEYCODE} -ne 0; then
			return 1
		fi
	else
		FUNC_MESSAGE_BOX "INFORMATION" "\nNo packages left to ADD/REMOVE!\n "
		return 0
	fi

	BUILDSYS_MODIFY_BUILDORDER_UPDATE_LIST "${select_mode}" "${DIALOG_REPLY}"

	return 0

}

# Display edit buildorder dialog.
BUILDSYS_MODIFY_BUILDORDER_MENU() {
	local cancel=false
	local buildtime_minutes=0
	local buildtime_hours=0
	local buildtime_minutes_total=0
	local text1=""
	local text2=""
	local count=0

	while [ true ]; do

		# We need to count the packages after we maybe added/removed extra packages.
		FUNC_COUNT_ENABLED_PACKAGES

		dialog	--title "${PROJECT_NAME} - CUSTOMIZE PACKAGE LIST" \
				--backtitle "${PROJECT_BACKTITLE}" \
				--default-item "DONE" \
				--ok-label "${BUTTON_LABEL_OK}" \
				--cancel-label "${BUTTON_LABEL_EXIT}" \
				--menu "\
Packages to be installed: ${SYSDB_PACKAGE_COUNT_ENEBALED}\n\n\
You can customize the list of packages that will be installed now. \
To modify the automatically created buildorder is not recommended.\n\n \
If you have done select 'DONE' to continue with the setup.\n\n" 20 75 8 \
      "DONE"     "Accept the package list and continue..." \
      "ADD"      "Select additional packages to the buildorder" \
      "REMOVE"   "Remove packages from the buildorder" \
      "AUTO"     "Mark all installed packages as 'to be updated'" \
      "UPDATE"   "Display a list of packages that will be updated" \
      "FULL"     "List buildorder with all availables packages" \
      "SORT"     "Sorted list with all available packages" \
      "INFO"     "Display info about packages to be installed" 2> ${DIALOG_RETURN_VALUE}

		DIALOG_KEYCODE=$?
		DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
		rm -f "${DIALOG_RETURN_VALUE}"

		# 'CANCEL' or 'ESC' ?
		if test ${DIALOG_KEYCODE} -ne 0; then
			cancel=true
			break
		fi

		# Accept the package list?
		case ${DIALOG_REPLY} in
			DONE)	cancel=false
					break;;
			ADD)	BUILDSYS_MODIFY_BUILDORDER_EDIT_LIST "ADD" "BACK"
					continue;;
			REMOVE)	BUILDSYS_MODIFY_BUILDORDER_EDIT_LIST "REMOVE" "BACK"
					continue;;
			UPDATE)	BUILDSYS_MODIFY_BUILDORDER_EDIT_LIST "UPDATE" "BACK"
					continue;;
			FULL)	BUILDSYS_MODIFY_BUILDORDER_EDIT_LIST "FULL" "BACK"
					continue;;
			SORT)	BUILDSYS_MODIFY_BUILDORDER_EDIT_LIST "SORT" "BACK"
					continue;;
			AUTO)	FUNC_PROGRESS_BOX "Analyzing packages..."
					FUNC_INIT_PROGRESS_BAR
					count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
						# Draw the progress bar
						let count=count+1
						FUNC_DRAW_PROGRESS_BAR ${count} ${SYSDB_PACKAGE_COUNT}
						if [ x"${SYSDB_PACKAGE_INSTALLED[${count}]}" != x"" ]; then
							if [ x"${SYSDB_PACKAGE_INSTALLED[${count}]}" != x"${SYSDB_PACKAGE_INSTALLED[${count}]%%${PACKAGE_TAG}}" ]; then
								SYSDB_PACKAGE_ENABLED[${count}]="true"
							fi
						fi
					done
					# Clear the progress bar.
					FUNC_CLEAR_PROGRESS_BAR
					continue;;
			INFO)	FUNC_CALCULATE_ETA
					let buildtime_minutes_total=SELECTED_PACKAGES_ETA/60
					let buildtime_hours=buildtime_minutes_total/60
					let buildtime_minutes=buildtime_minutes_total-buildtime_hours*60
					text1="Packages to be installed : ${SYSDB_PACKAGE_COUNT_ENEBALED}"
					text2="ETA (for a 3.0GHz CPU) : ${buildtime_minutes_total}min. (${buildtime_hours}h${buildtime_minutes}m)"
					dialog	--title "${PROJECT_NAME} - INFORMATION" \
							--backtitle "${PROJECT_BACKTITLE}" \
							--cr-wrap \
							--msgbox "\n${text1}\n${text2}\n" 8 75
					BUILDSYS_LAST_WARNING_DISPLAY_PACKAGES
					continue;;
		esac

	done

	if ${cancel}; then
		return 1
	else
		return 0
	fi

}

# Select the MESA version if we have multiple versions available.
BUILDSYS_MODIFY_SELECT_MESA() {

	local count=0
	local entries=0
	local default="mesa"
	local mesa_selected=0
	local mesa_package=0

	# Quick check if we have multiple MESA buildtools in the queue...
	count=0
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

		let count=count+1

		# MESA package found?
		if [ x"${SYSDB_PACKAGE_NAME[${count}]}" == x"mesa" ]; then
			if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
				let entries=entries+1
			fi
		fi

	done

	# Did we find any MESA packages?
	if [ ${entries} -lt 2 ]; then
		return 0
	fi

	# In debug-mode use 'mesa-git' as default.
	if ${RUNTIME_OPTION_DEBUG_MODE}; then
		default="mesa-git"
	fi

	# More then one package found, choose it.
	FUNC_PROGRESS_BOX "Analyzing MESA buildtools..."

	# Create selection list.
	rm -f ${DIALOG_TEMP_FILE1}
	count=0
	entries=0
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

		let count=count+1

		# Package blacklisted or removed by user?
		if ${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}; then
			continue
		fi

		# MESA package found?
		if [ x"${SYSDB_PACKAGE_NAME[${count}]}" == x"mesa" ]; then
			if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
				# Add the available MESA package to the menu.
				cat <<EOF >>${DIALOG_TEMP_FILE1}
"${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]}" "Install version ${SYSDB_PACKAGE_VERSION[${count}]}${SYSDB_PACKAGE_RELEASE[${count}]}" \\
EOF
			fi
			let entries=entries+1
		fi

	done

	# Did we find any enabled MESA packages?
	if [ ${entries} -gt 1 ]; then

		# Yes, create a menu to select the one to build/install.
		cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "${PROJECT_NAME} - SELECT MESA VERSION" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--cr-wrap \\
		--ok-label "${BUTTON_LABEL_SELECT}" \\
		--cancel-label "${BUTTON_LABEL_CANCEL}" \\
		--default-item "$default" \\
		--menu "\\
This is a list of available MESA packages. Select the one you want to install.\n\
\n\
\n\
Use UP/DOWN arrow keys to scroll up and down through the entire list.\n" 20 75 9 \\
EOF

		cat ${DIALOG_TEMP_FILE1} | sort >>${DIALOG_SCRIPT}

		cat <<EOF >>${DIALOG_SCRIPT}
2> ${DIALOG_RETURN_VALUE}\\
EOF

		# Start the script and offer the options select dialog.
		. "${DIALOG_SCRIPT}"

		DIALOG_KEYCODE=$?
		DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
		rm -f "${DIALOG_SCRIPT}"
		rm -f "${DIALOG_TEMP_FILE1}"
		rm -f "${DIALOG_RETURN_VALUE}"

		# 'SELECT' ?
		if test ${DIALOG_KEYCODE} -eq 0; then

			# Yes, search for MESA packages.
			count=0
			while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

				let count=count+1

				# MESA package found?
				if [ x"${SYSDB_PACKAGE_NAME[${count}]}" == x"mesa" ]; then
					# Found the requested MESA package?
					if [ x"${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]}" == x"${DIALOG_REPLY}" ]; then
						# Yes, make sure it is enabled.
						SYSDB_PACKAGE_ENABLED[${count}]=true
						mesa_selected=${count}
					else
						# No, disable it.
						SYSDB_PACKAGE_ENABLED[${count}]=false
						SYSDB_PACKAGE_REMOVED_BY_USER[${count}]=true
					fi
				fi

			done


		fi

	fi

	# Remove temporary files.
	rm -f "${DIALOG_TEMP_FILE1}"

	# 'CANCEL' or 'ESC' ?
	if test ${DIALOG_KEYCODE} -ne 0; then
		return 1
	else
		return 0
	fi

}

# Select the XOrg X server if we have multiple versions available.
BUILDSYS_MODIFY_SELECT_XORG_SERVER() {

	local count=0
	local entries=0

	# Quick check if we have multiple XOrg X server buildtools in the queue...
	count=0
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

		let count=count+1

		# XOrg-Server package found?
		if [ x"${SYSDB_PACKAGE_NAME[${count}]}" == x"xorg-server" ]; then
			if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
				let entries=entries+1
			fi
		fi

	done

	# Did we find any XOrg X server packages?
	if [ ${entries} -lt 2 ]; then
		return 0
	fi

	# More then one package found, choose it.
	FUNC_PROGRESS_BOX "Analyzing XOrg X servers..."

	# Create selection list.
	rm -f ${DIALOG_TEMP_FILE1}
	count=0
	entries=0
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

		let count=count+1

		# Package blacklisted or removed by user?
		if ${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}; then
			continue
		fi

		# XOrg-Server package found?
		if [ x"${SYSDB_PACKAGE_NAME[${count}]}" == x"xorg-server" ]; then
			if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
				# Add the available XORG-server to the menu.
				cat <<EOF >>${DIALOG_TEMP_FILE1}
"${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]}" "Install version ${SYSDB_PACKAGE_VERSION[${count}]}${SYSDB_PACKAGE_RELEASE[${count}]}" \\
EOF
			fi
			let entries=entries+1
		fi

	done

	# Did we find any enabled XOrg X server packages?
	if [ ${entries} -gt 1 ]; then

		# Yes, create a menu to select the one to build/install.
		cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "${PROJECT_NAME} - SELECT XORG SERVER" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--cr-wrap \\
		--ok-label "${BUTTON_LABEL_SELECT}" \\
		--cancel-label "${BUTTON_LABEL_CANCEL}" \\
		--default-item "xorg-server20" \\
		--menu "\\
This is a list of available XOrg X server packages. Select the one you want to install.\n\
\n\
\n\
Use UP/DOWN arrow keys to scroll up and down through the entire list.\n" 20 75 9 \\
EOF

		cat ${DIALOG_TEMP_FILE1} | sort >>${DIALOG_SCRIPT}

		cat <<EOF >>${DIALOG_SCRIPT}
2> ${DIALOG_RETURN_VALUE}\\
EOF

		# Start the script and offer the options select dialog.
		. "${DIALOG_SCRIPT}"

		DIALOG_KEYCODE=$?
		DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
		rm -f "${DIALOG_SCRIPT}"
		rm -f "${DIALOG_TEMP_FILE1}"
		rm -f "${DIALOG_RETURN_VALUE}"

		# 'SELECT' ?
		if test ${DIALOG_KEYCODE} -eq 0; then

			# Yes, search for XOrg X server packages.
			count=0
			while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

				let count=count+1

				# XOrg-Server package found?
				if [ x"${SYSDB_PACKAGE_NAME[${count}]}" == x"xorg-server" ]; then
					# Found the requested XOrg X server package?
					if [ x"${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]}" == x"${DIALOG_REPLY}" ]; then
						# Yes, make sure it is enabled.
						SYSDB_PACKAGE_ENABLED[${count}]=true
					else
						# No, disable it.
						SYSDB_PACKAGE_ENABLED[${count}]=false
						SYSDB_PACKAGE_REMOVED_BY_USER[${count}]=true
					fi
				fi

			done
		fi

	fi

	# Remove temporary files.
	rm -f "${DIALOG_TEMP_FILE1}"

	# 'CANCEL' or 'ESC' ?
	if test ${DIALOG_KEYCODE} -ne 0; then
		return 1
	else
		return 0
	fi

}

# Select input/display drivers.
BUILDSYS_MODIFY_SELECT_XORG_DRIVER() {

	local count=0
	local entries=0
	local mode=""
	local driver_text=""
	local driver_type=""
	local found=false
	local driver=""
	local select_everything=false

	case $1 in
		video)	driver_type="display"
				driver_text="video";;
		input)	driver_type="input"
				driver_text="input";;
		all)	driver_type="display/input"
				driver_text="all";;
	esac

	FUNC_PROGRESS_BOX "Analyzing XOrg drivers..."
	FUNC_INIT_PROGRESS_BAR

	# Create checklist
	rm -f ${DIALOG_TEMP_FILE1}
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

		# Draw the progress bar
		let count=count+1
		FUNC_DRAW_PROGRESS_BAR ${count} ${SYSDB_PACKAGE_COUNT}

		# Package blacklisted or removed by user?
		if ${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}; then
			continue
		fi

		# Is package enabled?
		case ${driver_text} in
			video|input)	if [ x"${SYSDB_PACKAGE_NAME[${count}]}" != x"${SYSDB_PACKAGE_NAME[${count}]##xf86-${driver_text}-}" ]; then
								if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
									mode="on"
								else
									mode="off"
								fi
								BUILDSYS_MODIFY_BUILDORDER_CREATE_ENTRY ${count} ${mode}
								let entries=entries+1
							fi;;
			all)			# Select default video/display drivers:
							if [ x"${SYSDB_PACKAGE_NAME[${count}]}" != x"${SYSDB_PACKAGE_NAME[${count}]##xf86-video-}" ]; then
								if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
									mode="on"
								else
									mode="off"
								fi
								BUILDSYS_MODIFY_BUILDORDER_CREATE_ENTRY ${count} ${mode}
								let entries=entries+1
							fi;
							# Select default input drivers:
							if [ x"${SYSDB_PACKAGE_NAME[${count}]}" != x"${SYSDB_PACKAGE_NAME[${count}]##xf86-input-}" ]; then
								if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
									mode="on"
								else
									mode="off"
								fi
								BUILDSYS_MODIFY_BUILDORDER_CREATE_ENTRY ${count} ${mode}
								let entries=entries+1
							fi;;
		esac

	done

	# Clear the progress bar.
	FUNC_CLEAR_PROGRESS_BAR

	# Did we find any enabled packages?
	if [ ${entries} -gt 0 ]; then
		cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "${PROJECT_NAME} - SELECT XORG DRIVERS" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--cr-wrap \\
		--ok-label "${BUTTON_LABEL_OK}" \\
		--cancel-label "${BUTTON_LABEL_GOBACK}" \\
		--extra-button \\
		--extra-label "${BUTTON_LABEL_EVERYTHING}" \\
		--item-help \\
		--checklist "\\
This is a list of ${driver_type} drivers for the XOrg X server. Select additional drivers you want to install.\n\
Use the spacebar to select/unselect a package, and the UP/DOWN arrow keys to scroll up and down through the entire list.\n" 20 75 10 \\
EOF

		cat ${DIALOG_TEMP_FILE1} | sort >>${DIALOG_SCRIPT}

		cat <<EOF >>${DIALOG_SCRIPT}
2> ${DIALOG_RETURN_VALUE}\\
EOF

		# Start the script and offer the version select dialog.
		. "${DIALOG_SCRIPT}"

		DIALOG_KEYCODE=$?
		DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
		rm -f "${DIALOG_SCRIPT}"
		rm -f "${DIALOG_TEMP_FILE1}"
		rm -f "${DIALOG_RETURN_VALUE}"

		# 'EVERYTHING' ?
		if test ${DIALOG_KEYCODE} -eq 3; then
			select_everything=true
		else
			# 'CANCEL' or 'ESC' ?
			if test ${DIALOG_KEYCODE} -ne 0; then
				return 1
			fi
		fi

		FUNC_PROGRESS_BOX "Reloading package list..."

		count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
			let count=count+1

			# Package blacklisted or removed by user?
			if ${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}; then
				continue
			fi

			if [ x"${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}" != x"${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]##xf86-${driver_text}-}" ]; then
				if ${select_everything}; then
					SYSDB_PACKAGE_ENABLED[${count}]=true
				else
					found=false
					for driver in ${DIALOG_REPLY}; do
						if [ x"${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}" == x"${driver}" ]; then
							found=true
							break
						fi
					done
					if ${found}; then
						SYSDB_PACKAGE_ENABLED[${count}]=true
					else
						SYSDB_PACKAGE_ENABLED[${count}]=false
					fi
				fi
			fi
		done

	fi

	return 0
}

# Menu to select input/display drivers.
BUILDSYS_MODIFY_SELECT_XORG_DRIVER_MENU() {

	local count=0
	local count_video=0
	local count_video_max=0
	local count_input=0
	local count_input_max=0
	local count_all=0
	local count_all_max=0

	# Reset driver selection to the system defaults.
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
		let count=count+1

		# # Package blacklisted or removed by user?
		if ${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}; then
			continue
		fi

		if [ x"${SYSDB_PACKAGE_NAME[${count}]}" != x"${SYSDB_PACKAGE_NAME[${count}]##xf86-}" ]; then
			if [ x"${SYSDB_PACKAGE_NAME[${count}]}" != x"${SYSDB_PACKAGE_NAME[${count}]##xf86-video-}" ]; then
				if ! ${BUILDSYS_INSTALL_EVERYTHING}; then
					if FUNC_CHECK_DEFAULT_XORG_DRIVER ${count}; then
						SYSDB_PACKAGE_ENABLED[${count}]=true
					else
						SYSDB_PACKAGE_ENABLED[${count}]=false
					fi
				else
					SYSDB_PACKAGE_ENABLED[${count}]=true
				fi
			else
				if [ x"${SYSDB_PACKAGE_NAME[${count}]}" != x"${SYSDB_PACKAGE_NAME[${count}]##xf86-input-}" ]; then
					if ! ${BUILDSYS_INSTALL_EVERYTHING}; then
						if FUNC_CHECK_DEFAULT_XORG_DRIVER ${count}; then
							SYSDB_PACKAGE_ENABLED[${count}]=true
						else
							SYSDB_PACKAGE_ENABLED[${count}]=false
						fi
					else
						SYSDB_PACKAGE_ENABLED[${count}]=true
					fi
				fi
			fi
		fi

	done

	# Display the menu and let the user decide what to compile...
	while [ true ]; do

		# Reset counters.
		count_video=0
		count_video_max=0
		count_input=0
		count_input_max=0
		count_all=0
		count_all_max=0

		# Count available input and video drivers..
		count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
			let count=count+1
			# Package blacklisted or removed by user?
			if ${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}; then
				continue
			fi
			case ${SYSDB_PACKAGE_NAME[${count}]} in
				xf86-video-*)	let count_video_max=count_video_max+1
								let count_all_max=count_all_max+1
								case ${SYSDB_PACKAGE_ENABLED[${count}]} in
									true)	let count_video=count_video+1;
											let count_all=count_all+1;;
								esac
								;;
				xf86-input-*)	let count_input_max=count_input_max+1
								let count_all_max=count_all_max+1
								case ${SYSDB_PACKAGE_ENABLED[${count}]} in
									true)	let count_input=count_input+1;
											let count_all=count_all+1;;
								esac
								;;
			esac
		done

		dialog	--title "${PROJECT_NAME} - SELECT XORG DRIVERS" \
				--backtitle "${PROJECT_BACKTITLE}" \
				--default-item "DONE" \
				--ok-label "${BUTTON_LABEL_OK}" \
				--cancel-label "${BUTTON_LABEL_EXIT}" \
				--menu "\
${PROJECT_NAME} automatically selected a few display and input drivers from ATI, NVidia and Intel. \
If you have a different hardware then you maybe want to select additional drivers.\n\
Additional drivers are available within the 'xorgdrv' directory and can \
be compiled manually after you have installed latest XORG server.\n\
\n\
Select 'VIDEO' to add additional display drivers and select 'INPUT' to \
select additional input drivers. Any driver not selected will also be \
removed from your system if already installed. \
" 20 75 5 \
"DONE"       "All drivers selected, continue..." \
"EVERYTHING" "Select all display and input drivers" \
"STANDARD"   "Select standard display/input drivers" \
"VIDEO"      "Select display drivers                 [Selected: ${count_video}/${count_video_max}]" \
"INPUT"      "Select input drivers                   [Selected: ${count_input}/${count_input_max}]" \
2> ${DIALOG_RETURN_VALUE}

		DIALOG_KEYCODE=$?
		DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
		rm -f "${DIALOG_RETURN_VALUE}"

		# 'CANCEL' or 'ESC' ?
		if test ${DIALOG_KEYCODE} -ne 0; then
			return 1
		fi

		# User actions...
		case ${DIALOG_REPLY} in
			"EVERYTHING")	# Select all drivers.
							count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
								let count=count+1
								# Package blacklisted or removed by user?
								if ${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}; then
									continue
								fi
								case ${SYSDB_PACKAGE_NAME[${count}]} in
									xf86-video-*)	SYSDB_PACKAGE_ENABLED[${count}]=true;;
									xf86-input-*)	SYSDB_PACKAGE_ENABLED[${count}]=true;;
								esac
							done
							break
							;;
			"STANDARD")		# Select standard drivers.
							count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
								let count=count+1
								# Package blacklisted or removed by user?
								if ${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}; then
									continue
								fi
								if [ x"${SYSDB_PACKAGE_NAME[${count}]}" != x"${SYSDB_PACKAGE_NAME[${count}]##xf86-video-}" ]; then
									if FUNC_CHECK_DEFAULT_XORG_DRIVER ${count}; then
										SYSDB_PACKAGE_ENABLED[${count}]=true
									else
										SYSDB_PACKAGE_ENABLED[${count}]=false
									fi
								else
									if [ x"${SYSDB_PACKAGE_NAME[${count}]}" != x"${SYSDB_PACKAGE_NAME[${count}]##xf86-input-}" ]; then
										if FUNC_CHECK_DEFAULT_XORG_DRIVER ${count}; then
											SYSDB_PACKAGE_ENABLED[${count}]=true
										else
											SYSDB_PACKAGE_ENABLED[${count}]=false
										fi
									fi
								fi
							done
							BUILDSYS_MODIFY_SELECT_XORG_DRIVER all || continue
							;;
			"VIDEO")		BUILDSYS_MODIFY_SELECT_XORG_DRIVER video || continue
							;;
			"INPUT")		BUILDSYS_MODIFY_SELECT_XORG_DRIVER input || continue
							;;
			*)				break
							;;
		esac

	done

	# Check for remaining XOrg packages.
	FUNC_PROGRESS_BOX "Analyzing remaining XORG packages..."

	# Count available input and video drivers..
	count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
		let count=count+1
		# Package blacklisted or removed by user?
		if ${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}; then
			continue
		fi
		case ${SYSDB_PACKAGE_NAME[${count}]} in
			xf86-video-*)	case ${SYSDB_PACKAGE_ENABLED[${count}]} in
								false)	SYSDB_PACKAGE_REMOVED_BY_USER[${count}]=true;;
							esac
							;;
			xf86-input-*)	case ${SYSDB_PACKAGE_ENABLED[${count}]} in
								false)	SYSDB_PACKAGE_REMOVED_BY_USER[${count}]=true;;
							esac
							;;
		esac
	done

	return 0
}

# Check for remaining/installed xorg driver packages.
BUILDSYS_CHECK_XORG_DRIVERS_REMAINING() {

	local count=0
	local found=false
	local package=""
	local package_name=""
	local package_list=""
	local package_list_input=""
	local package_list_video=""
	local package_list_remove=""
	local check=""
	local first_package=true
	local skip_count=0
	local remove_count=0
	local text=""
	local text_package=""

	count=0
	found=false
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
		let count=count+1
		if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
			case ${SYSDB_PACKAGE_NAME[${count}]} in
				xf86-video-*|\
				xf86-input-*)	found=true; break;;
			esac
		fi
	done
	if ! ${found}; then
		return 0
	fi

	# Search for installed/not enabled xorg drivers...
	FUNC_PROGRESS_BOX "Analyzing remaining XOrg drivers..."

	# Add driver list to logfile.
	cat <<EOF >>${SYSTEM_DEBUG_LOGFILE_REMOVED_XORG_DRIVERS}

Available drivers that will not be installed:
${RULER1}
EOF

	# Add drivers to logfile that will not be installed...
	count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
		let count=count+1
		case ${SYSDB_PACKAGE_NAME[${count}]} in
			xf86-video-*|\
			xf86-input-*)	case ${SYSDB_PACKAGE_ENABLED[${count}]} in
								false)	let skip_count=skip_count+1
										text="000${skip_count}"
										text="[${text:${#text}-4:4}] ${SYSDB_PACKAGE_NAME[${count}]}${SPACEBAR}"
										text="${text:0:30}"
										echo "${text} - ${SYSDB_PACKAGE_INSTALLED[${count}]// */}" >>${SYSTEM_DEBUG_LOGFILE_REMOVED_XORG_DRIVERS}
										package_list_remove="${package_list_remove} ${SYSDB_PACKAGE_NAME[${count}]}"
										;;
							esac
							;;
		esac
	done

	# Add driver list to logfile.
	cat <<EOF >>${SYSTEM_DEBUG_LOGFILE_REMOVED_XORG_DRIVERS}

Remaining XOrg drivers that will be removed:
${RULER1}
EOF

	# Driver selected, add remaining drivers to the remove list.
	package_list_input=$(find /var/log/packages/ -type f -name "xf86-input-*" -printf "%f\n")
	package_list_video=$(find /var/log/packages/ -type f -name "xf86-video-*" -printf "%f\n")

	# Check installed drivers. Add remaining drivers to remove list.
	remove_count=0
	for package in ${package_list_input} ${package_list_video}; do
		package_name=${package%-*}			# Remove build
		package_name=${package_name%-*}		# Remove arch
		package_name=${package_name%-*}		# Remove version
		check=`grep -e "] ${package_name} " -e "] ${package}$" ${SYSTEM_DEBUG_LOGFILE_REMOVED_XORG_DRIVERS}`
		if [ x"${check}" == x"" ]; then
			count=0
			found=false
			while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
				let count=count+1
				if [ x"${SYSDB_PACKAGE_NAME[${count}]}" == x"${package_name}" ]; then
					if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
						found=true
						break
					fi
				fi
			done
			if ! ${found}; then
				package_list_remove="${package_list_remove} ${package_name}"
				let remove_count=remove_count+1
				text="000${remove_count}"
				text="[${text:${#text}-4:4}] ${package}"
				echo "${text}" >>${SYSTEM_DEBUG_LOGFILE_REMOVED_XORG_DRIVERS}
			fi
		fi
	done

	# If we are in debug mode show some extra info.
	if ${RUNTIME_OPTION_DEBUG_MODE}; then
		dialog	--title "${PROJECT_NAME} - DEBUGGING INFORMATION" \
				--backtitle "${PROJECT_BACKTITLE}" \
				--cr-wrap \
				--exit-label "${BUTTON_LABEL_OK}" \
				--textbox ${SYSTEM_DEBUG_LOGFILE_REMOVED_XORG_DRIVERS} 20 75
	fi

	# Add removed XOrg drivers to the list of extra packages to remove if installed.
	RUNTIME_REMOVE_EXTRA_PACKAGES="${RUNTIME_REMOVE_EXTRA_PACKAGES} ${package_list_remove}"

	return 0
}

# Check for remaining/installed xorg packages.
BUILDSYS_CHECK_XORG_PACKAGES_REMAINING() {

	local count=0
	local found=false
	local package=""
	local package_name=""
	local package_xorg_server=0
	local package_list=""
	local package_list_fixed=""
	local package_list_remove=""
	local check=""
	local addpkg=""
	local first_package=true
	local default_item=""
	local skip_count=0
	local text=""
	local text_package=""

	count=0
	found=false
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
		let count=count+1

		# Package blacklisted or removed by user?
		if ${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}; then
			continue
		fi

		# XOrg-Server package found?
		if [ x"${SYSDB_PACKAGE_NAME[${count}]}" == x"xorg-server" ]; then
			if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
				found=true
				break
			fi
		fi
	done
	if ! ${found}; then
		return 0
	fi

	# Search for installed/not enabled xorg drivers...
	FUNC_PROGRESS_BOX "Analyzing remaining XOrg packages..."

	# Find remaing XORG packages.
	BUILDSYS_CHECK_XORG_FIND_PACKAGES

	# Add XORG packages to the list of packages to be removed.
	# Check if we have enabled packages with the name of
	# an XORG package to be removed.
	for replace in ${RUNTIME_XORG_PACKAGES}; do
		count=0
		found=false
		while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
			let count=count+1
			for package in ${SYSDB_PACKAGE_REMOVE[${count}]}; do
				if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
					if [ x"${package}" == x"${replace}" ]; then
						found=true
						break
					fi
				fi
			done
			# Enabled package found?
			if ${found}; then
				# Requested package will be replaced.
				break
			fi
		done
		# No enabled package found?
		if ! ${found}; then
			# Check if we have the requested package installed.
			check=$(find /var/log/packages/ -type f -name "${replace}*" -printf "%f\n")
			if [ x"${check}" != x"" ]; then
				for package in ${check}; do
					text_package=${package%-*}			# Remove build
					text_package=${text_package%-*}		# Remove arch
					text_package=${text_package%-*}		# Remove version
					if [ x"${text_package}" == x"${replace}" ]; then
						package_list_remove="${package_list_remove} ${text_package}"
					fi
				done
			fi
		fi
	done

	# Add XORG package names to the list of replaced packages for installtgz.sh.
	for replace in ${RUNTIME_XORG_PACKAGE_INFO}; do
		count=0
		found=false
		while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
			let count=count+1
			if [ x"${SYSDB_PACKAGE_NAME[${count}]}" == x"${replace}" ]; then
				if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
					found=true
					break
				fi
			fi
		done
		if ! ${found}; then
			for package in ${package_list_remove}; do
				if [ x"${package}" == x"${replace}" ]; then
					found=true
					break
				fi
			done
			if ! ${found}; then
				package_list_remove="${package_list_remove} ${package}"
			fi
		fi
	done

	# Add removed XOrg packages to logfile.
	cat <<EOF >>${SYSTEM_DEBUG_LOGFILE_REMAINING_XORG_PACKAGES}

Remaining XOrg packages that will be removed:
${RULER1}
EOF

	# Find the XOrg server package.
	count=0
	package_xorg_server=0
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

		let count=count+1

		# Package blacklisted or removed by user?
		if ${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}; then
			continue
		fi

		# XOrg-Server package found?
		if [ x"${SYSDB_PACKAGE_NAME[${count}]}" == x"xorg-server" ]; then
			if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
				let package_xorg_server=count
				break
			fi
		fi

	done
 
	# Add additional replaced XOrg packages to the list.
	if [ ${package_xorg_server:-0} -gt 0 ]; then
		package_list_xserver="${SYSDB_PACKAGE_REPLACE[${package_xorg_server}]//,/ }"
	fi

	count=0
	package_list="${package_list_remove} ${package_list_xserver}"
	package_list_remove=""
	for package in ${package_list}; do
		let count=count+1
		check=$(find /var/log/packages/ -name ${package}-* -printf '%f\n')
		if [ x"${check}" != x"" ]; then
			text="000${count}"
			text="[${text:${#text}-4:4}] ${package}${SPACEBAR}"
			text="${text:0:30}"
			echo "${text}${package}" >>${SYSTEM_DEBUG_LOGFILE_REMAINING_XORG_PACKAGES}
			package_list_remove="${package_list_remove}${package} "
		else
			package_list_fixed=""
			for addpkg in ${package_list_remove}; do
				case ${addpkg} in
					${package})	true;;
					*)			package_list_fixed="${package_list_fixed}${addpkg} ";;
				esac
			done
			package_list_remove="${package_list_fixed}"
		fi
	done

	# If we are in debug mode show some extra info.
	if ${RUNTIME_OPTION_DEBUG_MODE}; then
		FUNC_DISPLAY_PACKAGE_LIST "${package_list_remove}" "REPLACED XORG PACKAGES"
	fi

	# Add removed XOrg drivers to the list of extra packages to remove if installed.
	RUNTIME_REMOVE_EXTRA_PACKAGES="${RUNTIME_REMOVE_EXTRA_PACKAGES} ${package_list_remove}"

	return 0
}

# Check if we have multiple versions of a package available.
BUILDSYS_MODIFY_SELECT_DUPLICATE_PACKAGES() {

	local count=0
	local duplicates=0
	local select=0
	local first=false
	local entries=0

	# More then one package found, choose it.
	FUNC_PROGRESS_BOX "Checking for duplicate packages..."
	FUNC_INIT_PROGRESS_BAR

	# Remove existing menu file.
	rm -f ${DIALOG_TEMP_FILE1}

	# Quick check if we have multiple buildtools with
	# the same package name in the queue...
	count=0
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

		# Draw the progress bar
		let count=count+1
		FUNC_DRAW_PROGRESS_BAR ${count} ${SYSDB_PACKAGE_COUNT}

		# Package blacklisted or removed by user?
		if ${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}; then
			continue
		fi
		if ! ${SYSDB_PACKAGE_ENABLED[${count}]}; then
			continue
		fi

		first=true
		let entries=0
		let duplicates=count
		while [ ${duplicates} -lt ${SYSDB_PACKAGE_COUNT} ]; do

			let duplicates=duplicates+1

			# Package blacklisted or removed by user?
			if ${SYSDB_PACKAGE_REMOVED_BY_USER[${duplicates}]}; then
				continue
			fi
			if ! ${SYSDB_PACKAGE_ENABLED[${duplicates}]}; then
				continue
			fi

			# Duplicate package found?
			if [ x"${SYSDB_PACKAGE_NAME[${count}]}" == x"${SYSDB_PACKAGE_NAME[${duplicates}]}" ]; then
				if ${first:-false}; then
					# Add the original package to the menu.
					cat <<EOF >>${DIALOG_TEMP_FILE1}
"${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]}" "Install ${SYSDB_PACKAGE_NAME[${count}]}-${SYSDB_PACKAGE_VERSION[${count}]}${SYSDB_PACKAGE_RELEASE[${count}]}" \\
EOF
					let entries=entries+1
					first=false
				fi
				# Add the duplicate packages to the menu.
				cat <<EOF >>${DIALOG_TEMP_FILE1}
"${SYSDB_PACKAGE_BUILDTOOL_NAME[${duplicates}]}" "Install ${SYSDB_PACKAGE_NAME[${duplicates}]}-${SYSDB_PACKAGE_VERSION[${duplicates}]}${SYSDB_PACKAGE_RELEASE[${duplicates}]}" \\
EOF
				let entries=entries+1
			fi

		done

		# Did we find any duplicate packages?
		if [ ${entries} -lt 2 ]; then
			continue
		fi

		# Yes, create a menu to select the one to build/install.
		cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "${PROJECT_NAME} - SELECT DUPLICATE PACKAGES" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--cr-wrap \\
		--ok-label "${BUTTON_LABEL_SELECT}" \\
		--cancel-label "${BUTTON_LABEL_CANCEL}" \\
		--menu "\\
There are currently multiple packages with different versions enabled but only one of these packages can be installed.\n\
\n\
This is a list of packages with the same package name. Select the one you want to install.\n\
\n\
\n\
Use UP/DOWN arrow keys to scroll up and down through the entire list.\n" 20 75 6 \\
EOF

		cat ${DIALOG_TEMP_FILE1} | sort >>${DIALOG_SCRIPT}

		cat <<EOF >>${DIALOG_SCRIPT}
2> ${DIALOG_RETURN_VALUE}\\
EOF

		# Start the script and offer the options select dialog.
		. "${DIALOG_SCRIPT}"

		DIALOG_KEYCODE=$?
		DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
		rm -f "${DIALOG_SCRIPT}"
		rm -f "${DIALOG_TEMP_FILE1}"
		rm -f "${DIALOG_RETURN_VALUE}"

		# 'SELECT' ?
		if test ${DIALOG_KEYCODE} -eq 0; then

			# Yes, search for selected package.
			select=0
			while [ ${select} -lt ${SYSDB_PACKAGE_COUNT} ]; do

				let select=select+1

				# package found?
				if [ x"${SYSDB_PACKAGE_NAME[${select}]}" == x"${SYSDB_PACKAGE_NAME[${count}]}" ]; then
					# Found the requested package?
					if [ x"${SYSDB_PACKAGE_BUILDTOOL_NAME[${select}]}" == x"${DIALOG_REPLY}" ]; then
						# Yes, make sure it is enabled.
						SYSDB_PACKAGE_ENABLED[${select}]=true
					else
						# No, disable it.
						SYSDB_PACKAGE_ENABLED[${select}]=false
						SYSDB_PACKAGE_REMOVED_BY_USER[${select}]=true
					fi
				fi

			done
		else
			break
		fi

	done

	# Remove temporary files.
	rm -f "${DIALOG_TEMP_FILE1}"

	# 'CANCEL' or 'ESC' ?
	if test ${DIALOG_KEYCODE} -ne 0; then
		return 1
	else
		return 0
	fi

}

# Just a separator...
SYS____________________BUILDTOOL_SETUP() {
	true
}

# Let user select language for MPlayer-GUI.
BUILDSYS_CHECK_MPLAYER_GUI_LANG() {

	local install_mplayer=false
	local count=0
	local man_pages=""
	local lang=""
	local extraline=""

	FUNC_PROGRESS_BOX "Checking for MPlayer GUI..."

	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

		let count=count+1
		if [ x"${SYSDB_PACKAGE_NAME[${count}]}" == x"MPlayer" ]; then
			if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
				install_mplayer=true
				break
			fi
		fi

	done

	# MPlayer selected to be installed?
	if ! ${install_mplayer}; then
		return 0
	fi

	# Language set through buildsystem configuration file?
	if ! test -z ${OPTION_BUILD_MPLAYER_GUI_LANG}; then
		# No, do we have a environment variable set for the language?
		if test -z ${MPLAYER_GUI_MANPAGE_LANGUAGE}; then
			# Yes, check languages and add them to the list.
			for lang in ${OPTION_BUILD_MPLAYER_GUI_LANG//,/ }; do
				case ${lang} in
					bg|cs|da|de|el|en|es|fr|hu|it|ja|ko|mk|nb|nl|pl|ro|ru|sk|sv|tr|uk|pt_BR|zh_CN|zh_TW) \
						MPLAYER_GUI_MANPAGE_LANGUAGE="${MPLAYER_GUI_MANPAGE_LANGUAGE},${lang}";;
				esac
			done
			# Any valid languages found?
			if [ x"${MPLAYER_GUI_MANPAGE_LANGUAGE}" != x"" ]; then
				# Yes, remove blurb and continue...
				MPLAYER_GUI_MANPAGE_LANGUAGE=${MPLAYER_GUI_MANPAGE_LANGUAGE#,}
				# English manpages will always be installed, remove it here.
				export MPLAYER_GUI_MANPAGE_LANGUAGE="${MPLAYER_GUI_MANPAGE_LANGUAGE//,en/}"
				return 0
			fi
		fi
	fi

	# Menu loop.
	while [ true ]; do

		# Build german packages?
		case ${OPTION_BUILD_GERMAN_PACKAGES} in
			true)	local default_item="de";;
			*)		local default_item="en";;
		esac

		# Select language for the GUI.
		BUILDSYS_CHECK_MPLAYER_GET_LANG "\
You have selected the MPlayer package to be installed. \
The MPlayer GUI supports different languages. \
If you skip this step the english GUI will be installed.\n\
\n\
Please select the language you want to use for the GUI:\n\
 Suggested => ${default_item}\n " \
 			"SELECT GUI LANGUAGE" \
 			"${default_item}" \
			"MPlayer GUI" \
			"${BUTTON_LABEL_SKIP}"
 
		# Skip? Yes, use the buildtool defaults.
		if test ${DIALOG_KEYCODE} -ne 0; then
			unset MPLAYER_GUI_MANPAGE_LANGUAGE
			break
		fi

		# Separator selected... repeat language selection.
		if [ x"${DIALOG_REPLY}" == x"---" ]; then
			continue
		fi

		# "All done" selected... select default item.
		if [ x"${DIALOG_REPLY}" == x"All done" ]; then
			export MPLAYER_GUI_MANPAGE_LANGUAGE="${default_item}"
		else
			export MPLAYER_GUI_MANPAGE_LANGUAGE="${DIALOG_REPLY}"
		fi

		if [ x"${MPLAYER_GUI_MANPAGE_LANGUAGE}" != x"en" ]; then
			man_pages=" ${MPLAYER_GUI_MANPAGE_LANGUAGE}"
		fi

		# Defaults for manpage language selection.
		default_item=""
		default_button="--defaultno"

		# Menu loop.
		while [ true ]; do

			if test ${#man_pages} -le 58; then
				extraline="\n "
			else
				extraline=""
			fi

			# Select additional languages for manpages.
			BUILDSYS_CHECK_MPLAYER_GET_LANG "\
You have selected the MPlayer package to be installed. \
MPlayer supports help files in different languages. \
If nothing select then english help files will be installed only. \
Multiple selections are possible.\n\
\n\
Please select the languages you want to use for the help files:\n\
 Selected: en${man_pages}${extraline}" \
 				"SELECT HELP FILES" \
				"${default_item}" \
				"MPlayer help files" \
				"${BUTTON_LABEL_RESTART}"
 
			# 'EXTRA' (Reset)? Yes, Reset everything and start again.
			if test ${DIALOG_KEYCODE} -eq 1; then
				unset MPLAYER_GUI_MANPAGE_LANGUAGE
				man_pages=""
				break
			fi

			# 'ESC'?
			if test ${DIALOG_KEYCODE} -eq 255; then
				break
			fi

			# "All done" selected... select default item.
			if [ x"${DIALOG_REPLY}" == x"All done" ]; then
				break
			fi

			# Separator selected... repeat language selection.
			if [ x"${DIALOG_REPLY}" == x"---" ]; then
				continue
			fi

			# Add the language to the list of selected manpages languages.
			if [ x"${MPLAYER_GUI_MANPAGE_LANGUAGE//${DIALOG_REPLY}/}" == x"${MPLAYER_GUI_MANPAGE_LANGUAGE}" ]; then
				if [ x"${man_pages//${DIALOG_REPLY}/}" == x"${man_pages}" ]; then
					man_pages="${man_pages} ${DIALOG_REPLY}"
				fi
			fi

			# Set new default item.
			default_item=${DIALOG_REPLY}
			default_button=""

		done

		# Restart?
		if test ${DIALOG_KEYCODE} -eq 1; then
			continue
		else
			break
		fi

	done

	# English manpages will always be installed.
	man_pages="${man_pages// en/}"
	# GUI manpages will also always be installed.
	man_pages="${man_pages// ${MPLAYER_GUI_MANPAGE_LANGUAGE}/}"
	MPLAYER_GUI_MANPAGE_LANGUAGE="${MPLAYER_GUI_MANPAGE_LANGUAGE}${man_pages// /,}"

	# Make the selection the new default so we do
	# not have to select the language again.
	OPTION_BUILD_MPLAYER_GUI_LANG=${MPLAYER_GUI_MANPAGE_LANGUAGE}

	return 0
}

# Select a mplayer language.
BUILDSYS_CHECK_MPLAYER_GET_LANG() {
	local text="$1"
	local title="$2"
	local default_item="$3"
	local type="$4"
	local button="$5"

	dialog	--title "${PROJECT_NAME} - ${title}" \
			--backtitle "${PROJECT_BACKTITLE}" \
			--yes-label "${BUTTON_LABEL_SELECT}" \
			--no-label "${button}" \
			--ok-label "${BUTTON_LABEL_SELECT}" \
			--cancel-label "${button}" \
			--default-item "${default_item}" \
			--menu "${text}" 18 75 5 \
"All done"  "Accept current selection and continue" \
"---"       "---" \
"bg"        "Install Bulgarian ${type}" \
"cs"        "Install Czech ${type}" \
"da"        "Install Danish ${type}" \
"de"        "Install German ${type}" \
"el"        "Install Greek ${type}" \
"en"        "Install English ${type}" \
"es"        "Install Spanish ${type}" \
"fr"        "Install French ${type}" \
"hu"        "Install Hungarian ${type}" \
"it"        "Install italian ${type}" \
"ja"        "Install Japanese ${type}" \
"ko"        "Install Korean ${type}" \
"mk"        "Install Macedonian ${type}" \
"nb"        "Install Norwegian ${type}" \
"nl"        "Install Dutch ${type}" \
"pl"        "Install Polish ${type}" \
"ro"        "Install Romanian ${type}" \
"ru"        "Install Russian ${type}" \
"sk"        "Install Slovak ${type}" \
"sv"        "Install Swedish ${type}" \
"tr"        "Install Turkish ${type}" \
"uk"        "Install Ukrainian ${type}" \
"pt_BR"     "Install Portuguese ${type}" \
"zh_CN"     "Install Chinese Simplified ${type}" \
"zh_TW"     "Install Chinese Traditional ${type}" \
2> ${DIALOG_RETURN_VALUE}

	DIALOG_KEYCODE=$?
	DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
	rm -f "${DIALOG_RETURN_VALUE}"

	return 0

}

# Find packages to be replaced/removed.
BUILDSYS_CHECK_FIND_PACKAGES() {
	local buildtool_dir=${SYSTEM_PATH_BUILDTOOLS}/$1
	local package=""
	local package_name=""
	local extra_package=""

	RETURN_VALUE=""

	for package in $(find ${buildtool_dir}/ -maxdepth 2 -mindepth 2 -type f -name "*.${BUILDTOOL_SUFFIX}" | grep -v "\.svn"); do
		package_name=$(grep "^PACKAGE_NAME=" ${package} | sed "s,.*=,,g")
		case ${package_name} in
			kde-l10n* )			package_name="${package_name}-*";;
			libreoffice-l10n* )	package_name="${package_name}-*";;
			libreoffice-help* )	package_name="${package_name}-*";;
		esac
		RETURN_VALUE="${RETURN_VALUE} ${package_name}"
		for extra_package in $(grep "EXTRAPKG:" $(dirname ${package})/.config); do
			RETURN_VALUE="${RETURN_VALUE} ${extra_package##*:}"
		done
		for replace_package in $(grep "REPLACEPKG:" $(dirname ${package})/.config); do
			RETURN_VALUE="${RETURN_VALUE} ${replace_package##*:}"
		done
	done
}

# Find XOrg packages.
BUILDSYS_CHECK_XORG_FIND_PACKAGES() {
	local package=""
	local category=""

	# Find XORG packages and package names...
	RUNTIME_XORG_PACKAGES=""
	RUNTIME_XORG_PACKAGE_INFO=""

	for category in xorgdrv1 xorgdrv2; do
		BUILDSYS_CHECK_FIND_PACKAGES ${category}
		for package in ${RETURN_VALUE}; do
			if FUNC_CHECK_PACKAGE_INSTALLED "${package}"; then
				RUNTIME_XORG_PACKAGES="${RUNTIME_XORG_PACKAGES}${RETURN_VALUE} "
				RUNTIME_XORG_PACKAGE_INFO="${RUNTIME_XORG_PACKAGE_INFO}${package} "
			fi
		done
	done

	return 0

}

# Get the package type of an alternative source package file.
BUILDSYS_CHECK_SOURCE_TYPE() {
	local source=$1

	case ${source} in
		*.tar.gz)		RETURN_VALUE=tar.gz;;
		*.tar.bz2)		RETURN_VALUE=tar.bz2;;
		*.tgz)			RETURN_VALUE=tgz;;
		*.tbz2)			RETURN_VALUE=tbz2;;
		*.tbz)			RETURN_VALUE=tbz;;
		*.tar.xz)		RETURN_VALUE=tar.xz;;
		*.xz)			RETURN_VALUE=xz;;
		*.tar.lzma)		RETURN_VALUE=tar.lzma;;
		*.tar.lz)		RETURN_VALUE=tar.lz;;
		*.tlz)			RETURN_VALUE=tlz;;
		*.tar)			RETURN_VALUE=tar;;
		*.zip)			RETURN_VALUE=zip;;
		*.rpm)			RETURN_VALUE=rpm;;
		*.bin)			RETURN_VALUE=bin;;
	esac

	return 0
}

# Check if we have sources in download dir or newer source packages.
BUILDSYS_CHECK_SOURCES_FIND_OTHER() {
	local count=$1
	local source=$2
	local source_path=${SYSTEM_PATH_SOURCES}/${SYSDB_PACKAGE_GROUP[${count}]}
	local source_other=""
	local source_other_count=0
	local source_other_version=""
	local source_other_release=""
	local source_other_release_for_package=""
	local source_other_temp1=""
	local source_other_temp2=""
	local source_other_type=""
	local source_other_search=""
	local check_version=""
	local package=""
	local check=""

	# No not check for newer/other sources if this package requires a specific version.
	if ( echo ${SYSDB_PACKAGE_FLAGS[${count}]} | grep "FORCE_VERSION" &>/dev/null ); then
		if [ -r ${SYSTEM_PATH_DOWNLOADS}/${source} ]; then
			if ( cd ${SYSTEM_PATH_DOWNLOADS} && cat ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/MD5SUM | grep "${source}" | md5sum -c &>/dev/null ); then
				mv ${SYSTEM_PATH_DOWNLOADS}/${source} ${source_path}/${source}
			fi
		fi
		return 0
	fi

	# If the buildtool requires multiple source packages then check if
	# we have a copy of the source package in the download directory.
	# Do not check for the main source package since that can be
	# updated with a newer source package version later.
	if [ x"${source}" != x"${SYSDB_PACKAGE_SOURCE[${count}]//@@@*/}" ]; then
		if [ -r ${SYSTEM_PATH_DOWNLOADS}/${source} ]; then
			if ( cd ${SYSTEM_PATH_DOWNLOADS} && cat ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/MD5SUM | grep "${source}" | md5sum -c &>/dev/null ); then
				mv ${SYSTEM_PATH_DOWNLOADS}/${source} ${source_path}/
			fi
		fi
		return 0
	fi

	# Search for the main source package with a different version
	# Create a more generic search string like brasero-[[:digit:]]*.tar.gz
	#source_other_search=$(echo ${source} | sed "s,-${SYSDB_PACKAGE_VERSION_SOURCE[${count}]}${SYSDB_PACKAGE_RELEASE_SOURCE[${count}]},-[a-zA-Z0-9]*-*-[[:digit:]]*," | sed "s,[.]${SYSDB_PACKAGE_TYPE[${count}]},\.*,")
	source_other_search=${source%.${SYSDB_PACKAGE_TYPE[${count}]}}
	source_other_search=${source_other_search//-${SYSDB_PACKAGE_VERSION_SOURCE[${count}]}${SYSDB_PACKAGE_RELEASE_SOURCE[${count}]}/-[[:digit:]]*}
	# Find source packages that match the more generic search string.
	source_other_count=$(find ${source_path}/ -type f -name "${source_other_search}" -printf "%f\n" | grep -c ".")
	source_other=$(find ${source_path}/ -type f -name "${source_other_search}" -printf "%f\n")
	# Any other main source package found?
	if [ x"" == x"${source_other}" ]; then
		# No other main source package found...
		# Check if we have a copy of the main source package in the download directory.
		if [ -r ${SYSTEM_PATH_DOWNLOADS}/${source} ]; then
			if ( cd ${SYSTEM_PATH_DOWNLOADS} && cat ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/MD5SUM | grep "${source}" | md5sum -c &>/dev/null ); then
				mv ${SYSTEM_PATH_DOWNLOADS}/${source} ${source_path}/
			fi
		fi
		return 0
	fi

	# Multiple source packages found?
	if [ ${source_other_count:-0} -gt 1 ]; then

		# We have multiple source packages found (for example opera and opera_x64).
		dialog	--title "${PROJECT_NAME} - VERSION MISMATCH" \
				--backtitle "${PROJECT_BACKTITLE}" \
				--cr-wrap \
				--msgbox "\
\n\
The source package for ${SYSDB_PACKAGE_GROUP[${count}]}/${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]} could not be found.\n\
\n\
However, there are multiple other source packages available in the \
source package directory but the auto detection for the best package \
has failed. The existing source packages will now be moved to the \
temporary download directory:\n\
\n\
${SYSTEM_PATH_DOWNLOADS}\n\
\n\
and the source package required for the buildtool will be downloaded \
automatically. The missing source package is:\n\
\n\
${source}\
		" 20 75

		# Remove newer source package.
		DIALOG_REPLY="SCRIPT"

	else

		# Does the package has a standard version like a.b.c.d?
		check_version=$(grep "^PACKAGE_VERSION_SOURCE=.PACKAGE_VERSION$" "${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/${SYSDB_PACKAGE_BUILDTOOL_SCRIPT[${count}]}")
		if [ x"" == x"${check_version}" ]; then

			# We don't want to mess with non-standard version schemes.
			dialog	--title "${PROJECT_NAME} - VERSION MISMATCH" \
					--backtitle "${PROJECT_BACKTITLE}" \
					--cr-wrap \
					--msgbox "\
Source package for ${SYSDB_PACKAGE_GROUP[${count}]}/${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]} could not be found.\n\
\n\
Source package version: ${source_other}\n\
Buildtool version     : ${source}\n\
\n\
The buildtool script for this package can not be updated automatically. \
That means the existing source package will be moved to the temporary \
download directory:\n\
\n\
${SYSTEM_PATH_DOWNLOADS}\n\
\n\
and the source package required for the buildtool will be downloaded \
automatically. The missing source package is:\n\
\n\
${source}\
		" 20 75

			# Remove newer source package.
			DIALOG_REPLY="SCRIPT"

		else

			# Offer a dialog window and let the user decide.
			if ${SYSDB_PACKAGE_LANG[${count}]}; then
				source_other_temp1=$(echo ${source_other} | sed "s,.*/,," | sed "s,${SYSDB_PACKAGE_NAME_SOURCE[${count}]}-[[:alpha:]]*-,," | sed "s,[.]${SYSDB_PACKAGE_TYPE[${count}]},,")
				source_other_temp2=""
			else
				source_other_temp1=$(echo ${source_other} | sed "s,.*/,," | sed "s,${SYSDB_PACKAGE_NAME_SOURCE[${count}]}[-|_],," | sed "s,[.]${SYSDB_PACKAGE_TYPE[${count}]},,")
				## Remove some blurb like for gtklp-1.2.5.src.tar.gz. Without this the version would look like "1.2.5.src" instead of "1.2.5".
				source_other_temp2=$(echo ${source} | sed "s,${SYSDB_PACKAGE_NAME_SOURCE[${count}]}[-|_]${SYSDB_PACKAGE_VERSION_SOURCE[${count}]}${SYSDB_PACKAGE_RELEASE_SOURCE[${count}]},," | sed "s,[.]${SYSDB_PACKAGE_TYPE[${count}]},,")
				source_other_temp1=${source_other_temp1%%${source_other_temp2}}
			fi

			BUILDSYS_CHECK_SOURCE_TYPE ${source_other}
			source_other_type=${RETURN_VALUE}

			source_other_version=$(echo "${source_other_temp1}" | sed "s,[-_\.]*[[:alpha:]].*,,")
			source_other_release=$(echo ${source_other_temp1} | sed "s,${source_other_version},," | sed "s,[.]${source_other_type}$,,")
			source_other_release_for_package=$(echo ${source_other_release} | sed "s,[-_\.]*,,g")

			# Always use source/script?
			if [ x"${RUNTIME_SELECT_SOURCE_PACKAGE_MODE}" != x"ASK" ]; then

				# Yes, set the default and continue...
				DIALOG_REPLY=${RUNTIME_SELECT_SOURCE_PACKAGE_MODE}

			else

				# Ignore the other source package version?
				source_other_temp1=${source_other_version}${source_other_release}
				source_other_temp2=$(echo ${SYSDB_PACKAGE_FLAGS[${count}]} | grep "IGNORE_VERSION" | sed "s,.*IGNORE_VERSION___,,g")
				if [ x"${source_other_temp1}" != x"${source_other_temp2}" ]; then

					while [ true ] ; do
						dialog	--title "${PROJECT_NAME} - VERSION-CHECKING" \
								--backtitle "${PROJECT_BACKTITLE}" \
								--cr-wrap \
								--default-item "SCRIPT" \
								--no-cancel \
								--menu "\
Version mismatch for source package ${SYSDB_PACKAGE_GROUP[${count}]}/${SYSDB_PACKAGE_NAME[${count}]}.\n\
  Source version   :${source_other_version}${source_other_release}.${source_other_type}\n\
  Buildtool version:${SYSDB_PACKAGE_VERSION_SOURCE[${count}]}${SYSDB_PACKAGE_RELEASE_SOURCE[${count}]}.${SYSDB_PACKAGE_TYPE[${count}]}\n\
\n\
Select 'SOURCE/ASOURCE' to use version ${source_other_version}${source_other_release}\n\
of the source package and edit the buildtool\n\
Select 'SCRIPT/ASCRIPT' to use version ${SYSDB_PACKAGE_VERSION_SOURCE[${count}]}${SYSDB_PACKAGE_RELEASE_SOURCE[${count}]}\n\
of the buildtool and download the missing source package\n " 20 75 5 \
"SOURCE"  "Build version ${source_other_version}${source_other_release} and edit buildtool" \
"SCRIPT"  "Build version ${SYSDB_PACKAGE_VERSION_SOURCE[${count}]}${SYSDB_PACKAGE_RELEASE_SOURCE[${count}]} and download the source package" \
"ASOURCE" "Always edit the buildtool" \
"ASCRIPT" "Always download the missing source package" \
"EXIT"    "Exit ${PROJECT_NAME}" \
 2> ${DIALOG_RETURN_VALUE}

						DIALOG_KEYCODE=$?
						DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
						rm -f "${DIALOG_RETURN_VALUE}"

						# 'CANCEL' or 'ESC' ?
						if test ${DIALOG_KEYCODE} -ne 0; then
							DIALOG_REPLY="SCRIPT"
							break
						fi

						case ${DIALOG_REPLY} in
							"EXIT"    )	break;;
							"SOURCE"  )	break;;
							"SCRIPT"  )	break;;
							"ASOURCE" )	DIALOG_REPLY="SOURCE"
										RUNTIME_SELECT_SOURCE_PACKAGE_MODE="SOURCE"
										break;;
							"ASCRIPT" )	DIALOG_REPLY="SCRIPT"
										RUNTIME_SELECT_SOURCE_PACKAGE_MODE="SCRIPT"
										break;;
						esac

					done
				fi
			fi
		fi
	fi

	if [ x"${DIALOG_REPLY}" == x"EXIT" ]; then
		FUNC_CLEAR_SCREEN
		return 1
	fi


	# User wants to make use of the newer source package.
	if [ x"${DIALOG_REPLY}" == x"SOURCE" ]; then
		sed -i "/^PACKAGE_VERSION=/ c\PACKAGE_VERSION=${source_other_version}" "${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/${SYSDB_PACKAGE_BUILDTOOL_SCRIPT[${count}]}"
		sed -i "/^PACKAGE_RELEASE_TAG_SOURCE=/ c\PACKAGE_RELEASE_TAG_SOURCE=${source_other_release}" "${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/${SYSDB_PACKAGE_BUILDTOOL_SCRIPT[${count}]}"
		sed -i "/^PACKAGE_RELEASE_TAG=/ c\PACKAGE_RELEASE_TAG=${source_other_release_for_package}" "${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/${SYSDB_PACKAGE_BUILDTOOL_SCRIPT[${count}]}"
		sed -i "/^PACKAGE_BUILD=/ c\PACKAGE_BUILD=1" "${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/${SYSDB_PACKAGE_BUILDTOOL_SCRIPT[${count}]}"
		sed -i "/^PACKAGE_TYPE=/ c\PACKAGE_TYPE=${source_other_type}" "${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/${SYSDB_PACKAGE_BUILDTOOL_SCRIPT[${count}]}"

		# Update package database.
		SYSDB_PACKAGE_SOURCE[${count}]="${SYSDB_PACKAGE_SOURCE[${count}]}@@@"
		SYSDB_PACKAGE_SOURCE[${count}]="${source_other}@@@${SYSDB_PACKAGE_SOURCE[${count}]#*@@@}"
		SYSDB_PACKAGE_SOURCE[${count}]="${SYSDB_PACKAGE_SOURCE[${count}]%@@@}"
		SYSDB_PACKAGE_VERSION[${count}]=${source_other_version}
		SYSDB_PACKAGE_VERSION_SOURCE[${count}]=${source_other_version}
		SYSDB_PACKAGE_RELEASE[${count}]=${source_other_release_for_package}
		SYSDB_PACKAGE_RELEASE_SOURCE[${count}]=${source_other_release}
		SYSDB_PACKAGE_BUILD[${count}]=1
		SYSDB_PACKAGE_TYPE[${count}]=${source_other_type}

		# Update the MD5SUM for the new source package.
		(	sed -i "/${source}/ d" ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/MD5SUM
			cd ${source_path} && md5sum ${SYSDB_PACKAGE_SOURCE[${count}]//@@@*/} >>${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/MD5SUM
		)

		dialog	--title "${PROJECT_NAME} - INFORMATION" \
				--backtitle "${PROJECT_BACKTITLE}" \
				--cr-wrap \
				--infobox "\
${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]} updated... New version is:\n\
  => ${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]}-${SYSDB_PACKAGE_VERSION[${count}]}${SYSDB_PACKAGE_RELEASE[${count}]}" 4 60
		SLEEPMODE 2
	fi

	# User wants to make use of the buildtool:
	if [ x"${DIALOG_REPLY}" == x"SCRIPT" ]; then
		for package in ${source_other}; do
			mv ${source_path}/${package} ${SYSTEM_PATH_DOWNLOADS}/
			if ( grep "${package}" ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/MD5SUM &>/dev/null ); then
				sed -i "/${package}/ d" ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/MD5SUM
			fi
		done
		if ! ( grep "${source}" ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/MD5SUM &>/dev/null ); then
			echo "00000000000000000000000000000000  ${source}" >>${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/MD5SUM
		else
			if [ -r ${SYSTEM_PATH_DOWNLOADS}/${source} ]; then
				if ( cd ${SYSTEM_PATH_DOWNLOADS} && cat ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/MD5SUM | grep "${source}" | md5sum -c &>/dev/null ); then
					mv ${SYSTEM_PATH_DOWNLOADS}/${source} ${source_path}/${source}
				fi
			fi
		fi
	fi

	FUNC_PROGRESS_BOX "Checking source packages..."
	return 0

}

# Check for missing source packages.
BUILDSYS_CHECK_SOURCES() {

	local count=0
	local count_missing_sources=0
	local count_missing_external_sources=0
	local count_source=0
	local source_path=""
	local verify_mode=""
	local source=""
	local missing_sources_text=""
	local check=false
	local findpkg=""
	local text=""

	rm -f ${SYSTEM_LOGFILE_SOURCES}

	FUNC_PROGRESS_BOX "Checking source packages..."
	FUNC_INIT_PROGRESS_BAR

	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

		# Draw the progress bar.
		let count=count+1
		FUNC_DRAW_PROGRESS_BAR ${count} ${SYSDB_PACKAGE_COUNT}

		# Check-1: Is the package enabled/selected?
		if ${SYSDB_PACKAGE_ENABLED[${count}]}; then

			# Check-2: Did we check for the sources already?
			if ${SYSDB_PACKAGE_SOURCES_CHECKED[${count}]}; then

				# Add package name to logfile...
				text="0000${count}"
				text="[${text:${#text}-4:4}] ${SYSDB_PACKAGE_NAME[${count}]}${SPACEBAR}"
				echo -n "${text:0:50}" >> ${SYSTEM_LOGFILE_SOURCES}
				echo "OK!" >> ${SYSTEM_LOGFILE_SOURCES}

			else

				# Make sure we have the sources directory.
				source_path=${SYSTEM_PATH_SOURCES}/${SYSDB_PACKAGE_GROUP[${count}]}
				mkdir -p ${source_path}

				# Set location for source packages for the buildtool functions file.
				export SOURCE_PATH=${source_path}
				export SOURCE_CACHE=${SYSTEM_PATH_DOWNLOADS}

				# Add package name to logfile...
				text="0000${count}"
				text="[${text:${#text}-4:4}] ${SYSDB_PACKAGE_NAME[${count}]}${SPACEBAR}"
				echo -n "${text:0:50}" >> ${SYSTEM_LOGFILE_SOURCES}

				# By default, do not check for MD5 since this will slow down checking.
				check=true
				for source in ${SYSDB_PACKAGE_SOURCE[${count}]//@@@/ }; do
					findpkg=`find "${source_path}/" -type f -name "${source}"`
					if [ x"${findpkg}" == x"" ]; then
						check=false
					fi
				done

				# All source packages found?
				if ${check}; then
					echo "OK!" >> ${SYSTEM_LOGFILE_SOURCES}
					SYSDB_PACKAGE_SOURCES_CHECKED[${count}]=true
					continue
				else
					# Some sources do not exist...
					check=true
					count_source=1
					while [ true ]; do
						source=`echo ${SYSDB_PACKAGE_SOURCE[${count}]} | awk -F@@@ -v FIELD=$count_source '{printf("%s\n",$FIELD)}'`
						# Does the source package exist?
						if [ x"${source}" != x"" ]; then
							# Check-2: Does the source package exist? If not check downloads directory
							findpkg=`find "${source_path}/" -type f -name "${source}"`
							if [ x"${findpkg}" != x"" ]; then
								# Skip checking MD5SUM?
								if ${SYSDB_PACKAGE_SKIP_MD5[${count}]}; then
									# yes
									true
								else
									# no, check MD5SUM...
									if ( cd ${source_path} && cat ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/MD5SUM | grep "${source}" | md5sum -c &>/dev/null ); then
										true
									else
										# No, move the source package out of the sources directory.
										mv ${source_path}/${source} ${SYSTEM_PATH_DOWNLOADS}/${source}
									fi
								fi
							else
								# Source not there.. find other source package, i.e. with different version number.
								# This only works for the first package.
								if [ ${count_source:-1} -eq 1 ]; then
									BUILDSYS_CHECK_SOURCES_FIND_OTHER ${count} ${source} || return 1
									source=${SYSDB_PACKAGE_SOURCE[${count}]//@@@*/}
								fi
							fi
							findpkg=`find "${source_path}/" -type f -name "${source}"`
							if [ x"${findpkg}" == x"" ]; then
								check=false
							fi
						else
							break
						fi
						let count_source=count_source+1
					done

					if ${check}; then
						# All individual sources checked, all sources there now?
						if ${SYSDB_PACKAGE_SKIP_MD5[${count}]}; then
							verify_mode="-C"
						else
							verify_mode="-V"
						fi

						if ( cd ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]} && sh ${SYSDB_PACKAGE_BUILDTOOL_SCRIPT[${count}]} ${verify_mode} --quiet &>/dev/null ); then
							echo "OK!" >> ${SYSTEM_LOGFILE_SOURCES}
							SYSDB_PACKAGE_SOURCES_CHECKED[${count}]=true
							continue
						else
							echo "Not found!" >> ${SYSTEM_LOGFILE_SOURCES}
							SYSDB_PACKAGE_SOURCES_CHECKED[${count}]=false
							let count_missing_sources=count_missing_sources+1
							if ${SYSDB_PACKAGE_EXTERNAL[${count}]}; then
								let count_missing_external_sources=count_missing_external_sources+1
							fi
							continue
						fi
					else
						echo "Not found!" >> ${SYSTEM_LOGFILE_SOURCES}
						SYSDB_PACKAGE_SOURCES_CHECKED[${count}]=false
						let count_missing_sources=count_missing_sources+1
						if ${SYSDB_PACKAGE_EXTERNAL[${count}]}; then
							let count_missing_external_sources=count_missing_external_sources+1
						fi
						continue
					fi

				fi
			fi
		fi
	done

	# Clear the progress bar.
	FUNC_CLEAR_PROGRESS_BAR

	sed -i  "1 iMissing source packages (${count_missing_sources} total):" ${SYSTEM_LOGFILE_SOURCES}
	sed -i  "1 a${RULER2}" ${SYSTEM_LOGFILE_SOURCES}

	echo "${RULER2}" >>${SYSTEM_LOGFILE_SOURCES}
	echo "" >>${SYSTEM_LOGFILE_SOURCES}

	# Display debug info.
	if ${RUNTIME_OPTION_DEBUG_MODE}; then
		dialog	--title "${PROJECT_NAME} - DEBUGGING INFORMATION" \
				--backtitle "${PROJECT_BACKTITLE}" \
				--cr-wrap \
				--exit-label "${BUTTON_LABEL_OK}" \
				--textbox ${SYSTEM_LOGFILE_SOURCES} 20 75
	fi

	# Any external source packages like jre, jdk, realplayer missing?
	if [ ${count_missing_external_sources} -gt 0 ]; then

		# Yes, create a list of missing packages.
		count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
			let count=count+1
			if ${SYSDB_PACKAGE_EXTERNAL[${count}]}; then
				if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
					if ! ${SYSDB_PACKAGE_SOURCES_CHECKED[${count}]}; then
						source_path=${SYSTEM_PATH_SOURCES}/${SYSDB_PACKAGE_GROUP[${count}]}
						for source in ${SYSDB_PACKAGE_SOURCE[${count}]//@@@/ }; do
							findpkg=`find "${source_path}/" -type f -name "${source}"`
							if [ x"${findpkg}" == x"" ]; then
								missing_sources_text="${missing_sources_text}  ${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]}\n"
								missing_sources_text="${missing_sources_text}    Download-URL : ${SYSDB_PACKAGE_URL1[${count}]}\n"
								missing_sources_text="${missing_sources_text}    Download-Name: ${source}\n"
								missing_sources_text="${missing_sources_text}    Move the sources to:\n"
								missing_sources_text="${missing_sources_text}      ${SYSTEM_PATH_DOWNLOADS}\n"
								missing_sources_text="${missing_sources_text}\n"
							fi
						done
					fi
				fi
			fi
		done

		# Display the dialog.
		dialog	--title "${PROJECT_NAME} - EXTERNAL SOURCES" \
				--backtitle "${PROJECT_BACKTITLE}" \
				--cr-wrap \
				--msgbox "\
WARNING:\n\
\n\
You have selected packages based on external sources but you do not have all required sources for those packages.\n\
Please download the missing source packages and move them manually to the sources directory or disable 'EXTERNAL' from options menu.\n\
\n\
${RULER2}\n\
${missing_sources_text}\n\
${RULER2}\n\
" 20 75

		FUNC_CLEAR_SCREEN
		return 1

	fi

	return 0
}

# Configuration file for sudo updated?
BUILDSYS_CHECK_ETC_SUDOERS() {

	## This code will be reused in the installtgz.sh script...

	## LABEL:ETC_SUDOERS

	# has MD5 of /etc/sudoers changed?
	if [ x"$(grep sudoers ${SYSTEM_PATH_LOGFILES_SYSTEM}/etc_sudoers.md5)" != x"" ]; then
		if ! ( md5sum -c ${SYSTEM_PATH_LOGFILES_SYSTEM}/etc_sudoers.md5 &>/dev/null ); then

			dialog	--title "${PROJECT_NAME} - INFORMATION" \
					--backtitle "${PROJECT_BACKTITLE}" \
					--cr-wrap \
					--no-label "${BUTTON_LABEL_INFO}" \
					--yes-label "${BUTTON_LABEL_OK}" \
					--ok-label "${BUTTON_LABEL_OK}" \
					--yesno "\n\
The configuration file for SUDO (User authentification for specified commands and services) has been updated. \n\
\n\
A few packages may add data to this file to allow specific user/groups to execute specific commands. \
The default for most applications like 'gksu', 'gparted', 'gdmsetup' or 'system-config-printer' is to allow the users in group 'adm' to execute these applications on the local client only. \n\
\n\
Select '${BUTTON_LABEL_INFO}' for more information.
" 20 75

			# Display changes to /etc/sudoers ?
			if test $? -eq 1 ; then
				cat <<EOF >${DIALOG_TEMP_FILE1}
Changed lines in /etc/sudoers. Some notes:
     +: Added to /etc/sudoers
     -: Removed from /etc/sudoers
${RULER2}
EOF

				diff -U0 -d -r -N ${SYSTEM_PATH_LOGFILES_SYSTEM}/etc_sudoers.orig /etc/sudoers |\
					grep -v "^---" |\
					grep -v "^+++" |\
					grep -v "^@@" |\
					sed "s,^\([+|-]\),\1:  ,g" \
					>>${DIALOG_TEMP_FILE1}
				cat <<EOF >>${DIALOG_TEMP_FILE1}
${RULER2}
If you want to change the configuration then you have to use an editor
to edit the /etc/sudoers file. Here are some hints:

Replace group '%adm' with either '%groupname' or 'username' (when using
username make sure there is no % on top of the username) to give groups
or users access to these commands.

To allow access from various computers please replace
  '${HOSTNAME//.*/}'
with 'ALL' (all clients) or by an aliasname (selected clients) to
specify computers that have access to these commands.

Replace 'PASSWD:' (always ask) or 'NOPASSWD:' (do not ask) to ask for a
user password (not the root password!).
EOF
				dialog	--title "${PROJECT_NAME} - SUDO CONFIGURATION" \
						--backtitle "${PROJECT_BACKTITLE}" \
						--cr-wrap \
						--exit-label "${BUTTON_LABEL_OK}" \
						--textbox ${DIALOG_TEMP_FILE1} 20 75
				# Delete the temp datafile.
				rm -f ${DIALOG_TEMP_FILE1}

			fi
		fi
	fi

	## LABEL:ETC_SUDOERS

	## This code will be reused in the installtgz.sh script.

}

# Check for *.new files like /etc/rc.d/rc.hald.new
BUILDSYS_CHECK_ETC_NEW_FILES() {

	## This code will be reused in the installtgz.sh script.
	## LABEL:CHECKETC

	local default_item=""
	local etc_new_config_files=""
	local etc_new_config_files_by_buildtools=0
	local etc_new_config_file_package_list=""
	local package=""
	local package_found=false
	local packages_with_new_files=""
	local etc_new_files_dialog_hint=""
	local config_file=""
	local etc_new_file=""
	local etc_new_file_path=""
	local etc_new_file_name_default=""
	local etc_new_file_name_new=""
	local etc_new_file_name_backup=""
	local install_mode="ASK"
	local installed_by=""
	local check=""
	local select_mode="Off"
	local selected_config_files=""
	local select_file=""
	local config_installed=false
	local dialog_text1=""
	local dialog_text2=""

	# Get the *.new files
	FUNC_PROGRESS_BOX "Searching for new config files..."
	etc_new_config_files=$( ( find /etc/ -type f -name "*.new" && find /opt/ -type f -name "*.new" ) | sort | grep -v "rc.font.new" | grep -v "gtkrc.new" | sed "s,^/,,g" )
	if test x"${etc_new_config_files}" == x""; then
		# Only display a message box when started from
		# command line using the -n/--newconfig switch.
		if ${RUNTIME_CLI_MODE:-false}; then
			FUNC_MESSAGE_BOX "INFORMATION" "No new configuration files found, nothing to install\n\nEverything done. Exiting now!"
		fi
	else
		# Create a list of packages including *.new files.
		packages_with_new_files=$( grep -l ".*[.]new$" /var/log/packages/* | sed "s,.*/,,g" | xargs )

		# Find packages that have installed a specific *.new file.
		etc_new_config_file_package_list=""
		etc_new_config_files_by_buildtools=0
		for config_file in ${etc_new_config_files}; do
			etc_new_file=${config_file}
			package_found=false
			for package in ${packages_with_new_files}; do
				if [ x"$(grep ${etc_new_file} /var/log/packages/${package} 2>/dev/null)" != x"" ]; then
					if [ x"${package}" != x"${package%${PACKAGE_TAG}}" ]; then
						etc_new_config_file_package_list="$etc_new_config_file_package_list ${etc_new_file}@@@$(basename ${package})"
						package_found=true
						let etc_new_config_files_by_buildtools=etc_new_config_files_by_buildtools+1
						break
					fi
				fi
			done

			# File was not installed by a buildtool.
			if ! ${package_found}; then
				etc_new_config_file_package_list="$etc_new_config_file_package_list ${etc_new_file}@@@NoSLP"
			fi
		done

		# Any *.new files installed by the buildsystem found?
		if [ x"${etc_new_config_file_package_list}" != x"" ]; then

			if [ ${etc_new_config_files_by_buildtools} -eq 0 ]; then
				etc_new_files_dialog_hint="but none of these files were installed by ${PROJECT_NAME}."
				default_item="CONTINUE"
			else
				etc_new_files_dialog_hint="and some of these files were installed by ${PROJECT_NAME}."
				default_item="LIST"
			fi

			while [ true ]; do
				if ${RUNTIME_CLI_MODE:-false}; then
					dialog_text1="EXIT"
					dialog_text2="Leave these files as they are and exit"
				else
					dialog_text1="CONTINUE"
					dialog_text2="Leave these files as they are and continue"
				fi
				dialog	--title "${PROJECT_NAME} - NEW CONFIGURATION FILES" \
						--backtitle "${PROJECT_BACKTITLE}" \
						--default-item "${default_item}" \
						--no-cancel \
						--menu "\
WARNING!\n\
\n\
${PROJECT_NAME} has found some new configuration files. \
These files are named like 'filename.<new>' and can be found in /etc ${etc_new_files_dialog_hint}\n\n\
To activate these new configuration files you must replace the old files by the new files (${PROJECT_NAME} will backup the old files).\n\
\n\
Select one of the following options:\n" 20 75 4 \
"LIST"      "Display list of new configuration files" \
"ASK"       "Ask me for each configuration file what to do" \
"SELECT"    "Select/Unselect configuration files from a list" \
"${dialog_text1}" "${dialog_text2}" 2> ${DIALOG_RETURN_VALUE}

				DIALOG_KEYCODE=$?
				DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
				rm -f "${DIALOG_RETURN_VALUE}"

				# 'CANCEL' or 'ESC' ?
				if test ${DIALOG_KEYCODE} -ne 0; then
					break
				fi

				# Leave all files unchanged?
				if [ x"${DIALOG_REPLY}" == x"${dialog_text1}" ]; then
					break
				fi

				# Show a list of *.new files and by which package this file was installed.
				if [ x"${DIALOG_REPLY}" == x"LIST" ]; then
					cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "${PROJECT_NAME} - NEW CONFIGURATION FILES" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--cr-wrap \\
		--msgbox "List of new configuration files:\n\\
${RULER2}\n\\
EOF
					for etc_new_file in ${etc_new_config_file_package_list} ; do
						config_file="/${etc_new_file%%@@@*}"
						package=${etc_new_file##*@@@}
						echo "    ${config_file}\n" >>${DIALOG_SCRIPT}
						case ${package} in
							"NoSLP")	echo "    => Warning: Not installed by ${PROJECT_NAME}!\n\n" >>${DIALOG_SCRIPT};;
							*)			echo "    => Installed by: ${package}\n\n" >>${DIALOG_SCRIPT};;
						esac
					done
					echo "${RULER2}\n\" 20 75" >>${DIALOG_SCRIPT}

					# Start the script and offer the version select dialog.
					. "${DIALOG_SCRIPT}"

					# Remove the temp dialog script.
					rm -f "${DIALOG_SCRIPT}"

					default_item="ASK"
					continue
				fi

				# Let the user device if the *.new file should be installed or not.
				if [ x"${DIALOG_REPLY}" == x"SELECT" ]; then

					# If not specified autoselect only config
					# files installed by the buildsystem.
					selected_config_files=""

					# Bring up the dialog and let the user decide.
					while [ true ]; do

						cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "${PROJECT_NAME} - SELECT NEW CONFIG FILES" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--ok-label "${BUTTON_LABEL_OK}" \\
		--cancel-label "${BUTTON_LABEL_GOBACK}" \\
		--extra-button \\
		--extra-label "${BUTTON_LABEL_INFO}" \\
		--item-help \\
		--checklist "\\
Please select the new configuration files to be installed. Existing configuration files will be renamed. Example:\n\
\n\
  /etc/cups/cups-files.conf.old.160628060708\n\
\n\
Use the UP/DOWN arrow keys to scroll up and down through the list and select 'OK' to install the selected configuration files." 20 75 7 \\
EOF

						# Add the new configuration files to the dialog message box.
						# On first run only new configuration files installed by the
						# buildsystem will be selected.
						# If you select "Info" you will get a list of differences
						# between all selected configuration files and after this
						# you will be back to the checklist with the previously
						# selected packages again.
						for etc_new_file in ${etc_new_config_file_package_list} ; do
							config_file="/${etc_new_file%%@@@*}"
							if test x"${etc_new_file%@@@NoSLP}" != x"${etc_new_file}"; then
								installed_by="NoSLP"
							else
								installed_by="SLP"
							fi
							package_name=${etc_new_file#*@@@}
							package_name=${package_name%@@@*}
							package=${package_name%-*}	# Remove build
							package=${package%-*}		# Remove arch
							package=${package%-*}		# Remove version
							# Except for the first run re-select all
							# previously selected configuration files.
							if test x"${selected_config_files}" != x""; then
								select_mode="off"
								for select_file in ${selected_config_files}; do
									if test x"${config_file}" == x"${select_file}"; then
										select_mode="on"
										break
									fi
								done
							else
								# Only select new configuration files
								# installed by the buildsystem.
								case ${installed_by} in
									"NoSLP")	select_mode="off";;
									*)			select_mode="on";;
								esac
							fi
							# Create the checklist entry.
							echo -n "\"${config_file}\" \"${package}\" \"${select_mode}\" \"${package_name}" >>${DIALOG_SCRIPT}
							# Add some comment about which package installed this new configuration file.
							case ${installed_by} in
									"NoSLP")	echo " (Not installed by ${PROJECT_NAME})\" \\" >>${DIALOG_SCRIPT};;
									*)			echo " (Installed by ${PROJECT_NAME})\" \\" >>${DIALOG_SCRIPT};;
							esac
						done

						echo " 2> ${DIALOG_RETURN_VALUE}" >>${DIALOG_SCRIPT}

						# Start the script and offer the version select dialog.
						. "${DIALOG_SCRIPT}"

						DIALOG_KEYCODE=$?
						DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")

						rm -f "${DIALOG_SCRIPT}"
						rm -f "${DIALOG_RETURN_VALUE}"

						# Backup the selected configuration file names.
						selected_config_files=""
						if test x"${DIALOG_REPLY// /}" != x""; then
							for select_file in ${DIALOG_REPLY}; do
								selected_config_files="${selected_config_files} ${select_file}"
							done
						fi

						# Extra button?
						if test ${DIALOG_KEYCODE} -eq 3; then
							# Display differences between configuration files...
							cat <<EOF >${DIALOG_TEMP_FILE1}
Differences between configuration files. Some notes:
     +: Line added to new configuration file
     -: Line removed from new configuration file
${RULER2}
EOF
							if test x"${selected_config_files// /}" != x""; then
								for config_file in ${etc_new_config_file_package_list}; do
									etc_new_file="/${config_file%%@@@*}"
									for select_file in ${selected_config_files}; do
										if test x"${etc_new_file}" == x"${select_file}"; then

											package=${config_file##*@@@}
											etc_new_file_path=$(dirname ${etc_new_file})
											etc_new_file_name_default=$(basename ${etc_new_file} .new)
											etc_new_file_name_new=$(basename ${etc_new_file})

											cat <<EOF >>${DIALOG_TEMP_FILE1}
${RULER2}
${package}:
 => ${etc_new_file}
${RULER2}
EOF

											diff -U0 -d -r -N \
												${etc_new_file_path}/${etc_new_file_name_default} \
												${etc_new_file_path}/${etc_new_file_name_new} |\
											grep -v "^---" |\
											grep -v "^+++" |\
											grep -v "^@@" |\
											sed "s,^\([+|-]\),\1:  ,g" \
												>>${DIALOG_TEMP_FILE1}

											cat <<EOF >>${DIALOG_TEMP_FILE1}
${RULER2}


EOF
											break
										fi
									done
								done
							fi

							dialog	--title "${PROJECT_NAME} - NEW CONFIGURATION FILES" \
									--backtitle "${PROJECT_BACKTITLE}" \
									--cr-wrap \
									--exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${DIALOG_TEMP_FILE1} 20 75

							# Delete the temp datafile.
							rm -f ${DIALOG_TEMP_FILE1}

							# Display dialog again...
							continue
						fi

						# Back button?
						if test ${DIALOG_KEYCODE} -ne 0; then
							break
						fi

						# Show a list of *.new files and by which package this file was installed.
						cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "${PROJECT_NAME} - NEW CONFIGURATION FILES" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--cr-wrap \\
		--msgbox "\\
List of installed configuration files:\n\\
${RULER2}\n\\
@@1\\
${RULER2}\n\\
\n\\
List of remaining configuration files:\n\\
${RULER2}\n\\
@@2\\
${RULER2}\n\\
EOF

						# Install selected new config files...
						if test x"${selected_config_files// /}" != x""; then
							for config_file in ${etc_new_config_file_package_list}; do
								etc_new_file="/${config_file%%@@@*}"
								config_installed=false
								for select_file in ${selected_config_files}; do
									if test x"${etc_new_file}" == x"${select_file}"; then
										package=${config_file##*@@@}

										etc_new_file_path=$(dirname ${etc_new_file})
										etc_new_file_name_default=$(basename ${etc_new_file} .new)
										etc_new_file_name_new=$(basename ${etc_new_file})
										etc_new_file_name_backup=${etc_new_file_name_default}.old.$(date +%y%m%d%H%m%S)

										if [ -e ${etc_new_file_path}/${etc_new_file_name_default} ]; then
											mv ${etc_new_file_path}/${etc_new_file_name_default} ${etc_new_file_path}/${etc_new_file_name_backup}
										fi
										mv ${etc_new_file_path}/${etc_new_file_name_new} ${etc_new_file_path}/${etc_new_file_name_default}
										config_installed=true
										break
									fi
								done
								if ${config_installed:-false}; then
									sed -i "/@@1/i=> ${etc_new_file}@EOL@" ${DIALOG_SCRIPT}
								else
									sed -i "/@@2/i=> ${etc_new_file}@EOL@" ${DIALOG_SCRIPT}
								fi
							done
						fi

						# Finalize logfile.
						echo "\n\" 20 75" >>${DIALOG_SCRIPT}
						sed -i -e "s,@EOL@,\\\n\\\,g" -e "/@@1/d" -e "/@@2/d" ${DIALOG_SCRIPT}

						# Start the script and offer the version select dialog.
						. "${DIALOG_SCRIPT}"
						# Remove the temp dialog script.
						rm -f "${DIALOG_SCRIPT}"

						# All new configuration files checked...
						break

					# End of "Select new configuration files" loop
					done

					# Configuration files installed or back to main menu?
					if test ${DIALOG_KEYCODE} -ne 0; then
						continue
					else
						# Only display a message box when started from
						# command line using the -n/--newconfig switch.
						if ${RUNTIME_CLI_MODE:-false}; then
							FUNC_MESSAGE_BOX "INFORMATION" "Selected Configuration files have been installed.\n\nEverything done. Exiting now!"
						fi
						break
					fi

				fi

				# Let the user decide if the *.new file should be installed or not.
				if [ x"${DIALOG_REPLY}" == x"ASK" ]; then

					for config_file in ${etc_new_config_file_package_list}; do

						etc_new_file="/${config_file%%@@@*}"
						package=${config_file##*@@@}

						etc_new_file_path=$(dirname ${etc_new_file})
						etc_new_file_name_default=$(basename ${etc_new_file} .new)
						etc_new_file_name_new=$(basename ${etc_new_file})
						etc_new_file_name_backup=${etc_new_file_name_default}.old.$(date +%y%m%d%H%m%S)

						# Was this package installed by the buildsystem?
						case ${package} in
							"NoSLP")	installed_by="=> Warning: Not installed by ${PROJECT_NAME}!"
										default_item="KEEP"
										;;
							*)			installed_by="=> Installed by: ${package}"
										default_item="INSTALL"
										;;
						esac

						# Always install new configuration files?
						if [ x"${install_mode}" == x"BUILDSYS" ]; then
							if [ x"${default_item}" == x"INSTALL" ]; then
								if [ -e ${etc_new_file_path}/${etc_new_file_name_default} ]; then
									mv ${etc_new_file_path}/${etc_new_file_name_default} ${etc_new_file_path}/${etc_new_file_name_backup}
								fi
								mv ${etc_new_file_path}/${etc_new_file_name_new} ${etc_new_file_path}/${etc_new_file_name_default}
							fi
							continue
						fi

						# Bring up the dialog and let the user decide.
						while [ true ]; do

							cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "${PROJECT_NAME} - NEW CONFIGURATION FILES" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--default-item ${default_item} \\
		--no-cancel \\
		--menu "\\
Install this new configuration file?\n\\
\n\\
${etc_new_file}\n\\
${installed_by}\n\\
\n\\
If you select 'INSTALL' then ${PROJECT_NAME} will backup the old file:\n\\
${etc_new_file_name_backup}\n\\
\n\\
Select one of the following options:\n" 20 75 5 \\
"SHOWDIFF"  "Show differences between configuration files"         \\
"KEEP"      "Keep the old configuration file"                      \\
"INSTALL"   "Install the new configuration file"                   \\
"BUILDSYS"  "Automatically install if created by ${PROJECT_NAME}"  \\
"DONE"      "Leave new configuration files as they are"            \\
2> ${DIALOG_RETURN_VALUE}
EOF

							# Start the script and offer the version select dialog.
							. "${DIALOG_SCRIPT}"

							DIALOG_KEYCODE=$?
							DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
							rm -f "${DIALOG_SCRIPT}"
							rm -f "${DIALOG_RETURN_VALUE}"

							# 'CANCEL' or 'ESC' ?
							if test ${DIALOG_KEYCODE} -ne 0; then
								break
							fi

							# Display differences between configuration files...
							if [ x"${DIALOG_REPLY}" == x"SHOWDIFF" ]; then
								cat <<EOF >${DIALOG_TEMP_FILE1}
Differences between configuration files. Some notes:
     +: Line added to new configuration file
     -: Line removed from new configuration file
${RULER2}
EOF

								diff -U0 -d -r -N \
									${etc_new_file_path}/${etc_new_file_name_default} \
									${etc_new_file_path}/${etc_new_file_name_new} |\
								grep -v "^---" |\
								grep -v "^+++" |\
								grep -v "^@@" |\
								sed "s,^\([+|-]\),\1:  ,g" \
									>>${DIALOG_TEMP_FILE1}

								cat <<EOF >>${DIALOG_TEMP_FILE1}
${RULER2}
EOF

								dialog	--title "${PROJECT_NAME} - NEW CONFIGURATION FILES" \
										--backtitle "${PROJECT_BACKTITLE}" \
										--cr-wrap \
										--exit-label "${BUTTON_LABEL_OK}" \
										--textbox ${DIALOG_TEMP_FILE1} 20 75

								# Delete the temp datafile.
								rm -f ${DIALOG_TEMP_FILE1}

								# Display dialog again...
								continue
							fi

							# Check other menu options...
							break

						done

						# 'CANCEL' or 'ESC' ?
						if test ${DIALOG_KEYCODE} -ne 0; then
							break
						fi

						# Skip checking for new configuration files...
						if [ x"${DIALOG_REPLY}" == x"DONE" ]; then
							break
						fi

						# Keep the new configuration file?
						if [ x"${DIALOG_REPLY}" == x"KEEP" ]; then
							continue
						fi

						# Does the user want to install the new config file? Yes...
						if [ x"${DIALOG_REPLY}" == x"INSTALL" -o x"${DIALOG_REPLY}" == x"BUILDSYS" ]; then
							if [ -e ${etc_new_file_path}/${etc_new_file_name_default} ]; then
								mv ${etc_new_file_path}/${etc_new_file_name_default} ${etc_new_file_path}/${etc_new_file_name_backup}
							fi
							mv ${etc_new_file_path}/${etc_new_file_name_new} ${etc_new_file_path}/${etc_new_file_name_default}
							if [ x"${DIALOG_REPLY}" == x"BUILDSYS" ]; then
								install_mode="BUILDSYS"
							fi
							continue
						fi

					# End of "Ask about new configuration files" loop
					done

					# All new configuration files checked...
					break

				# End of: "Ask the user what to do for each file..."
				fi

			# End of main menu loop...
			done

		fi
	fi

	## LABEL:CHECKETC
	## This code will be reused in the installtgz.sh script.

	return 0
}

# Just a separator...
SYS____________________SCRIPTS() {
	true
}

# Create a script to reinstall all previously build packages.
UTILITY_CREATE_SCRIPT_INSTALLTGZ() {
	local count=0

	local replace_list=""
	local replace=""
	local extra_list=""
	local extra=""

	FUNC_PROGRESS_BOX "Creating script: '$( basename ${SCRIPT_NAME_INSTALLPKG} )'"

	mkdir -p $( dirname ${SCRIPT_NAME_INSTALLPKG} )

	cat <<EOF  >"${SCRIPT_NAME_INSTALLPKG}"
#!/bin/sh

# Turn of screensaver for console because we want to see what happens...
setterm -blank 0

# Define some variables that will be used in this script.
CWD=\$(pwd)
RULER1="${RULER1}"
RULER2="${RULER2}"
SPACEBAR="${SPACEBAR}"
PACKAGE_TAG="${PACKAGE_TAG}"

PROJECT_EMAIL="${PROJECT_EMAIL}"
PROJECT_VERSION="${PROJECT_VERSION}"
PROJECT_RELEASE="${PROJECT_RELEASE}"
PROJECT_NAME="${PROJECT_NAME}"
PROJECT_BACKTITLE="${PROJECT_BACKTITLE}"

SYSTEM_PATH="\${CWD}"
SYSTEM_PATH_TEMP="/tmp"
SYSTEM_PATH_PACKAGES="\${SYSTEM_PATH}"
SYSTEM_PATH_PACKAGES_DATA="\${SYSTEM_PATH}/$(basename ${SYSTEM_PATH_PACKAGES_DATA})"

# Libraries are installed in /usr/lib or /usr/lib64
SYSTEM_LIBDIR=${SYSTEM_LIBDIR}

# Do we have write access to current path? If not place logfiles in /tmp.
if [ -w \${SYSTEM_PATH} ]; then
  SYSTEM_PATH_LOGFILES=\${SYSTEM_PATH}
else
  SYSTEM_PATH_LOGFILES=\${SYSTEM_PATH_TEMP}
fi

# Name of the remove.conf file.
SYSTEM_CONFIG_REMOVE="\${CWD}/$(basename ${SYSTEM_CONFIG_REMOVE} .orig)"

# Used in code taken from the buildsystem.
SYSTEM_PATH_LOGFILES_SYSTEM=\${SYSTEM_PATH_LOGFILES}

# Define logfile names.
SYSTEM_LOGFILE=\${SYSTEM_PATH_LOGFILES}/log.installed
SYSTEM_LOGFILE_FAILED=\${SYSTEM_PATH_LOGFILES}/log.not_installed
SYSTEM_LOGFILE_PACKAGES=\${SYSTEM_PATH_LOGFILES}/log.new_packages
SYSTEM_LOGFILE_REMOVED=\${SYSTEM_PATH_LOGFILES}/${SYSTEM_LOGFILE_REMOVED##*/}
SYSTEM_LOGFILE_REPLACED=\${SYSTEM_PATH_LOGFILES}/log.replaced
SYSTEM_LOGFILE_DATA_UPDATE=\${SYSTEM_PATH_LOGFILES}/${SYSTEM_LOGFILE_DATA_UPDATE##*/}
SYSTEM_LOGFILE_GTK_ICON_CACHE=\${SYSTEM_PATH_LOGFILES}/${SYSTEM_LOGFILE_GTK_ICON_CACHE##*/}

# Remove existing logiles.
rm -f \${SYSTEM_LOGFILE}				1>/dev/null 2>/dev/null
rm -f \${SYSTEM_LOGFILE_FAILED}			1>/dev/null 2>/dev/null
rm -f \${SYSTEM_LOGFILE_PACKAGES}		1>/dev/null 2>/dev/null
rm -f \${SYSTEM_LOGFILE_REMOVED}		1>/dev/null 2>/dev/null
rm -f \${SYSTEM_LOGFILE_REPLACED}		1>/dev/null 2>/dev/null
rm -f \${SYSTEM_LOGFILE_DATA_UPDATE}	1>/dev/null 2>/dev/null

# Dialogbox variables.
DIALOG_RETURN_VALUE="\${SYSTEM_PATH_TEMP}/${DIALOG_RETURN_VALUE##*/}"
DIALOG_SCRIPT="\${SYSTEM_PATH_TEMP}/${DIALOG_SCRIPT##*/}"
DIALOG_KEYCODE=0
DIALOG_REPLY=""
DIALOG_TEMP_FILE1="\${SYSTEM_PATH_TEMP}/${DIALOG_TEMP_FILE1##*/}"
DIALOG_TEMP_FILE2="\${SYSTEM_PATH_TEMP}/${DIALOG_TEMP_FILE2##*/}"

# Overwrite button labels.
BUTTON_LABEL_YES="${BUTTON_LABEL_YES}"
BUTTON_LABEL_NO="${BUTTON_LABEL_NO}"
BUTTON_LABEL_OK="${BUTTON_LABEL_OK}"
BUTTON_LABEL_CANCEL="${BUTTON_LABEL_CANCEL}"
BUTTON_LABEL_UPDATE="${BUTTON_LABEL_UPDATE}"
BUTTON_LABEL_EXIT="${BUTTON_LABEL_EXIT}"
BUTTON_LABEL_SKIP="${BUTTON_LABEL_SKIP}"
BUTTON_LABEL_REMOVE="${BUTTON_LABEL_REMOVE}"
BUTTON_LABEL_CONTINUE="${BUTTON_LABEL_CONTINUE}"
BUTTON_LABEL_INFO="${BUTTON_LABEL_INFO}"
BUTTON_LABEL_GOBACK="${BUTTON_LABEL_GOBACK}"

# Packages to install.
SYSTEM_PACKAGE_LIST=""
SYSTEM_PACKAGE_NAME_LIST=""
SYSTEM_PACKAGE_COUNT=0

# Max count of packages for progress bar.
SYSDB_PACKAGE_COUNT=${SYSDB_PACKAGE_COUNT}

# Build/update gtk icon cache?
OPTION_UPDATE_GTK_ICON_CACHE=${OPTION_UPDATE_GTK_ICON_CACHE:-false}
BUILDSYS_GTK_ICON_CACHE=${BUILDSYS_GTK_ICON_CACHE}

# Enable/disable verbose mode.
OPTION_VERBOSE_MODE=${OPTION_VERBOSE_MODE:-false}

# Display info message.
FUNC_PROGRESS_BOX() {
  dialog --title "IN PROGRESS:" --backtitle "\${PROJECT_BACKTITLE}" --cr-wrap --infobox "\$1" 3 40
}

# Init progress bar.
FUNC_INIT_PROGRESS_BAR() {
	local a=0
	local char="#"
	local percent=0
	PROGRESS_BAR=""
	PROGRESS_PERCENT[0]=""
	PROGRESS_MAX_WIDTH=\$(dialog --stdout --print-maxsize | sed "s,.* ,,g")
	echo -ne "\r"
	while [ \${a} -lt \${PROGRESS_MAX_WIDTH} ]; do
		let percent=(\${a}+1)*100/\${PROGRESS_MAX_WIDTH}
		if [ \${percent} -gt 100 ]; then
			let percent=100
		fi
		PROGRESS_PERCENT[\${a}]="[\${percent}%] "
		PROGRESS_BAR="\${PROGRESS_BAR}\${char}"
		echo -n " "
		let a=a+1
	done
}

# Draw the progress bar: $1: current count, $2: max count
FUNC_DRAW_PROGRESS_BAR() {
	local progress=0
	let progress=\$1*PROGRESS_MAX_WIDTH/\$2
	echo -ne "\r\${PROGRESS_BAR:0:\$progress}\r\${PROGRESS_PERCENT[\${progress}]}"
}
EOF
	sed -n "/^## LABEL:FUNCFINDPKG/,/^## LABEL:FUNCFINDPKG/p" "${SYSTEM_ROOT}/${PROJECT_SCRIPT_NAME}"	|\
		grep -v "## LABEL:FUNCFINDPKG" >>"${SCRIPT_NAME_INSTALLPKG}"
cat <<EOF  >>"${SCRIPT_NAME_INSTALLPKG}"
# Last warning.
UTILITY_INSTALLTGZ_LAST_WARNING() {
	dialog	--title "INFORMATION" \\
			--backtitle "\${PROJECT_BACKTITLE}" \\
			--defaultno \\
			--yes-label "\${BUTTON_LABEL_YES}" \\
			--no-label "\${BUTTON_LABEL_NO}" \\
			--ok-label "\${BUTTON_LABEL_YES}" \\
			--cancel-label "\${BUTTON_LABEL_NO}" \\
			--yesno "\
\n\
Install all previously build packages found in the system/packages directory?\n " 0 0 2>/dev/null

	DIALOG_KEYCODE=\$?

	if [ \${DIALOG_KEYCODE} -ne 0 ]; then
		dialog --clear
		clear
		return 1
	else
		return 0
	fi
}

# Create a list of packages to install.
UTILITY_INSTALLTGZ_CREATE_LIST() {
	local count=0
	local package_count=0
	local package_list=""
	local package_name_list=""
	local package=""
	local group=""
	local name=""
	local file_list=""
	local update=false
	local replace_list=""
	local replace=""
	local remove=""
	local installed_package_list=""
	local shell=""
	local backup_date=""

	# Print some info on screen...
	FUNC_PROGRESS_BOX "Searching for packages to install..."
	FUNC_INIT_PROGRESS_BAR

	for package in \\
EOF

	# Header is created, add a list of all packages included.
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

		let count=count+1

		# Skip mesa/xorg-server if removed by user.
		# See: BUILDSYS_MODIFY_SELECT_XORG_SERVER() and
		#      BUILDSYS_MODIFY_SELECT_MESA
		case ${SYSDB_PACKAGE_NAME[${count}]} in
			mesa)			if [ x"${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}" == x"true" ]; then
								continue
							fi;;
			xorg-server)	if [ x"${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}" == x"true" ]; then
								continue
							fi;;
			# If we have other duplicate packages it might be better to ignore
			# all packages that have been removed by user.
			*)				if [ x"${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}" == x"true" ]; then
								continue
							fi;;
		esac

		# Does the current package install/replace any other packages?
		# Example: gutenprint will replace gimp-print package.
		replace_list=""
		for replace in ${SYSDB_PACKAGE_REPLACE[${count}]}; do
			replace_list="${replace_list},-:${replace}"
		done
		extra_list=""
		for extra in ${SYSDB_PACKAGE_EXTRAPKG[${count}]}; do
			extra_list="${extra_list},+:${extra}"
		done
		if [ x"${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}" == x"${SYSDB_PACKAGE_SEARCH_NAME[${count}]}" ]; then
			echo "\"${SYSDB_PACKAGE_GROUP[${count}]}/${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}[]${replace_list}${extra_list}\" \\" >>${SCRIPT_NAME_INSTALLPKG}
		else
			echo "\"${SYSDB_PACKAGE_GROUP[${count}]}/${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}[${SYSDB_PACKAGE_SEARCH_NAME[${count}]}]${replace_list}${extra_list}\" \\" >>${SCRIPT_NAME_INSTALLPKG}
		fi

	done

cat <<EOF  >>"${SCRIPT_NAME_INSTALLPKG}"
; do


		# Draw the progress bar.
		let count=count+1
		FUNC_DRAW_PROGRESS_BAR \${count} \${SYSDB_PACKAGE_COUNT}

		entry="\${package//\"/}"

		# Extract package name and group of the current package.
		name="\${entry##*/}"			#Remove group name
		name="\${name//,*/}"			#Remove extra package info
		name="\${name//\[*/}"			#Remove search name info
		search="\${entry//*\[/}"		#Remove group name and original name
		search="\${search//\]*/}"		#Remove extra package info
		search="\${search:-\${name}}"	#If no search name specified use original name
		group="\${entry%%/*}"			#Extract group name

		# Search for a slackware package for the current entry in package list.
		FUNC_CHECK_FIND_PACKAGE "\${search}" "\${group}"
		file_list="\${RETURN_VALUE}"

		# Did we find a package?
		if [ x"" != x"\${file_list}" ]; then
			if [ "\${SYSTEM_PACKAGE_NAME_LIST}" == "\${SYSTEM_PACKAGE_NAME_LIST// \${name} /}" ]; then
				let SYSTEM_PACKAGE_COUNT=SYSTEM_PACKAGE_COUNT+1
				SYSTEM_PACKAGE_LIST="\${SYSTEM_PACKAGE_LIST} \\"\${entry}\\""
				SYSTEM_PACKAGE_NAME_LIST="\${SYSTEM_PACKAGE_NAME_LIST} \${name} "
				if [ x"\${entry}" != x"\${entry//,*/}" ]; then
					file_info=\${entry#*,}
					for file in \${file_info//,/ }; do
						case \${file} in
							+:*) let SYSTEM_PACKAGE_COUNT=SYSTEM_PACKAGE_COUNT+1;;
						esac
					done
				fi
			fi
		fi

	done

	if test \${SYSTEM_PACKAGE_COUNT} -eq 0 ; then
		dialog --clear
		clear
		return 1
	else
		return 0
	fi
}

# Start to install the packages
UTILITY_INSTALLTGZ_INSTALL_PACKAGES() {
	local count=0
	local entry=""
	local package=""
	local package_name=""
	local group=""
	local name=""
	local file_list=""
	local update=false
	local replace_list=""
	local replace=""
	local remove=""
	local install=""
	local installed_package_list=""
	local removed_packages_list=""
	local replaced_packages_list=""
	local percent_removed=0
	local count_max_remove=0
	local line=""
	local count_removed=0

	local screen_width=\`dialog --stdout --print-maxsize | sed "s,.* ,,g"\`
	if test -f "\${CWD}/SCREEN_WIDTH"; then
		local screen_width=\`grep -m1 "." \${CWD}/SCREEN_WIDTH\`
	fi

	clear

	# Remove installed packages first because some package may not
	# install fine without doing that: Example
	# xkbdata installs to /usr/share/X11/xkb but xkeyboard-config
	# does replace this directory by a symlink to /etc/X11/xkb.
	# If xkbdata gets upgraded now then it will install it's files
	# into the symlink which points to /etc/X11/xkb now. Even if
	# you reinstall xkeyboard-config there will be some extra
	# files in /etc/X11/xkb which will cause the xserver to ignore
	# some keyboard shortcuts like CTRL+ALT+F1 (switch console).
	count=0; for package in \${SYSTEM_PACKAGE_LIST}; do

		let percent_removed=count*100/SYSTEM_PACKAGE_COUNT
		let count=count+1
		let count_removed=0

		entry="\${package//\"/}"

		# Extract package name and group of the current package.
		name="\${entry##*/}"			#Remove group name
		name="\${name//,*/}"			#Remove extra package info
		name="\${name//\[*/}"			#Remove search name info
		search="\${entry//*\[/}"		#Remove group name and original name
		search="\${search//\]*/}"		#Remove extra package info
		search="\${search:-\${name}}"	#If no search name specified use original name
		group="\${entry%%/*}"			#Extract group name

		# Search for a slackware package for the current entry in package list.
		FUNC_CHECK_FIND_PACKAGE "\${search}" "\${group}"
		file_list="\${RETURN_VALUE}"

		# Did we find a package?
		if [ x"" != x"\${file_list}" ]; then
			# For each package to be removed show some info and use removepkg to uninstall the package.
			FUNC_CHECK_PACKAGE_INSTALLED "\${search}"
			installed_package_list="\${RETURN_VALUE}"
			if [ x"\${installed_package_list}" != x"" ]; then
				for remove in \${installed_package_list}; do
					echo \${percent_removed}
					echo "XXX"
					echo "Package: \${name}"
					echo "Remove : \${remove:0:46}"
					echo "XXX"
					removed_package_list="\${removed_package_list} \${remove}"
					removepkg \${remove} &>/dev/null
					let count_removed=count_removed+1
				done
			fi

			# Does this package replace or installs any other packages?
			replace_list=""
			if [ x"\${entry}" != x"\${entry//,*/}" ]; then
				file_info=\${entry#*,}
				for file in \${file_info//,/ }; do
					case \${file} in
						-:*) replace_list="\${replace_list},\${file#-:}";;
					esac
				done
				replace_list="\${replace_list#,}"
			fi

			for replace in \${replace_list//,/ }; do
				FUNC_CHECK_PACKAGE_INSTALLED "\${replace}"
				installed_package_list="\${RETURN_VALUE}"
				if [ x"\${installed_package_list}" != x"" ]; then
					for remove in \${installed_package_list}; do
						echo \${percent_removed}
						echo "XXX"
						echo "Package: \${name}"
						echo "Remove : \${remove:0:46}"
						echo "XXX"
						replaced_package_list="\${replaced_package_list} \${remove}"
						removepkg \${remove} &>/dev/null
						let count_removed=count_removed+1
					done
				fi
			done
		fi

		if [ \${count_removed} -eq 0 ]; then
			# Update progress here if no packages got removed.
			echo \${percent_removed}
			echo "XXX"
			echo "Package: \${name}"
			echo "XXX"
		fi

	done | dialog	--title "\${PROJECT_NAME} - REMOVING PACKAGES TO BE UPDATED" \
							--backtitle "\${PROJECT_BACKTITLE}" \
							--gauge "Removing packages..." 8 60 0

	# Add removed/replaced packages to logfile.
	for file in \${removed_package_list}; do
		echo "\${file}" >>\${SYSTEM_LOGFILE_REMOVED}
	done

	for file in \${replaced_package_list}; do
		echo "\${file}" >>\${SYSTEM_LOGFILE_REPLACED}
	done

	# Remove extra packages from 'remove.conf' ?
	if [ x"\${SYSTEM_CONFIG_REMOVE}" != x"" ]; then
		if test -e "\${SYSTEM_CONFIG_REMOVE}"; then

			# Configuration file with extra packages to be removed found.
			while read line; do
				if FUNC_CHECK_PACKAGE_INSTALLED \$line; then
					let count_max_remove=count_max_remove+1
				fi
			done < "\${SYSTEM_CONFIG_REMOVE}"

			if test \${count_max_remove} -gt 0 ; then
				removed_packages_list=""
				(	let count=0
					while read line; do
						let count=count+1

						if FUNC_CHECK_PACKAGE_INSTALLED \${line}; then
							remove_package="\${RETURN_VALUE}"
						else
							remove_package=""
						fi
						if [ x"\${remove_package}" != x"" ]; then
							let percent_removed=count*100/count_max_remove
							for installed_package in \${remove_package}; do
								echo \${percent_removed}
								echo "XXX"
								echo "Package: \${line}"
								echo "Remove : \${installed_package:0:46}"
								echo "XXX"
								removed_package_list="\${removed_package_list} \${remove}"
								removepkg "\${installed_package}" &>/dev/null
							done
						fi
					done < "\${SYSTEM_CONFIG_REMOVE}"
				) | dialog	--title "\${PROJECT_NAME} - REMOVING EXTRA PACKAGES" \
							--backtitle "\${PROJECT_BACKTITLE}" \
							--gauge "Removing packages..." 8 60 0
			fi
		fi
	fi

	# Add extra removed packages to logfile.
	for file in \${removed_package_list}; do
		echo "\${file}" >>\${SYSTEM_LOGFILE_REMOVED}
	done

	clear

	# Install the packages included in the package directory.
	count=0; for package in \${SYSTEM_PACKAGE_LIST}; do

		let count=count+1
		entry="\${package//\"/}"

		# Extract package name and group of the current package.
		name="\${entry##*/}"			#Remove group name
		name="\${name//,*/}"			#Remove extra package info
		name="\${name//\[*/}"			#Remove search name info
		search="\${entry//*\[/}"		#Remove group name and original name
		search="\${search//\]*/}"		#Remove extra package info
		search="\${search:-\${name}}"	#If no search name specified use original name
		group="\${entry%%/*}"			#Extract group name

		# Reset update flag.
		update=false

		# Cleanup extra packages list.
		replace_list=""
		extra_list=""

		# Search for a slackware package for the current entry in package list.
		FUNC_CHECK_FIND_PACKAGE "\${search}" "\${group}"
		file_list="\${RETURN_VALUE}"

		# Did we find a package?
		if [ x"" != x"\${file_list}" ]; then
			# Verbose mode?
			if \${OPTION_VERBOSE_MODE:-true}; then
				# Package found, prepare for install.
				echo "${RULER1}"
				echo "Installing new package [\$count/\${SYSTEM_PACKAGE_COUNT}]: \${name}" | tee -a \${SYSTEM_LOGFILE}

				# Now use upgradepkg anyway even if no older package might be installed, just to be on the save side.
				package_name=\${file_list##*/}
				package_name=\${package_name%-*}		# Remove build
				package_name=\${package_name%-*}		# Remove arch
				package_name=\${package_name%-*}		# Remove version
				for install in \${file_list}; do
					echo "\${install}" >> \${SYSTEM_LOGFILE_PACKAGES}
					upgradepkg --reinstall --install-new \${group}/\${install} | sed "s,^\${package_name}:,>>,g" | tee -a \${SYSTEM_LOGFILE}
				done

				# For each extra package to be installed show some info.
				for extra in \${extra_list//,/ }; do
					FUNC_CHECK_FIND_PACKAGE "\${extra}" "\${group}"
					file_info="\${RETURN_VALUE}"
					if [ x"\${file_info}" != x"" ]; then
						package_name=\${file_list##*/}
						package_name=\${package_name%-*}		# Remove build
						package_name=\${package_name%-*}		# Remove arch
						package_name=\${package_name%-*}		# Remove version
						echo "  => \${name} installs extra package \${file_info}" >> \${SYSTEM_LOGFILE_PACKAGES}
						upgradepkg --reinstall --install-new \${file_info} | sed "s,^\${package_name}:,>>," | tee -a \${SYSTEM_LOGFILE}
					fi
				done
			else
				# Package found, prepare for install.
				for install in \${file_list}; do
					package=\${file_list%-*}	# Remove build and archive
					package=\${package%-*}		# Remove package arch
					let text_count3=SYSTEM_PACKAGE_COUNT-count+1
					text_count3="0000\${text_count3}"
					# On low resolution screens reduce package information.
					# >60 : 123456789012345678901234567890123456789012345678901234567890
					#       hh:mm | 9999 | package-version: description....... [0000 K]
					# <60 : 123456789012345678901234567890123456789012345678901234567890
					#       9999 | package-version............................ [0000 K]
					if [ \${screen_width:-0} -gt 60 ]; then
						# hh:mm [zzzz] | remaning...
						pkginfo="\$(date +%H:%M) | \${text_count3:${#text_count3}-4:4}"
						pkginfo="\${pkginfo} | \${group}/\${package}: "
						# Create short package description.
						case \${install} in
							*txz)	package_desc=\`xz -dc \${group}/\${install} | tar -x --to-stdout  \\
										"install/slack-desc" \\
										| grep -m1 "^\${name}:" \\
										| sed -e "s,^\${name}: ,," -e "s,.* (,," -e "s,)$,,"\` \\
										;;
							*tgz)	package_desc=\`gunzip -dc \${group}/\${install} | tar -x --to-stdout  \\
										"install/slack-desc" \\
										| grep -m1 "^\${name}:" \\
										| sed -e "s,^\${name}: ,," -e "s,.* (,," -e "s,)$,,"\` \\
										;;
						esac
						pkginfo="\${pkginfo}\${package_desc}"
					else
						# [zzzz] | remaning...
						package=\${package%-*}		# Remove version
						pkginfo="\${text_count3:\${#text_count3}-4:4}"
						pkginfo="\${pkginfo} | \${package}"
					fi
					# Print short package info while install the package.
					let package_info_width=screen_width-13
					pkginfo="\${pkginfo}................................................................................"
					pkginfo="\${pkginfo}................................................................................"
					echo -n "\${pkginfo:0:\$package_info_width}... "
					# Now use upgradepkg anyway even if no older package might be installed, just to be on the save side.
					upgradepkg --reinstall --install-new \${group}/\${install} 2>&1 1>/dev/null
					# Calculate the uncompressed size of the package in bytes/kbytes.
					case \${install} in
						*txz)	uncompressedb="\$(xz -dc \${group}/\${install} | dd 2>&1 | tail -n 1 | cut -f 1 -d ' ')"
								;;
						*tgz)	uncompressedb="\$(gunzip -dc \${group}/\${install} | dd 2>&1 | tail -n 1 | cut -f 1 -d ' ')"
								;;
					esac
					# If size is > 1024Bytes calculate KBytes/MBytes.
					if [ \${uncompressedb:-0} -gt 1024 ]; then
						if [ \${uncompressedb:-0} -gt 1048576 ]; then
							uncompressed="    \$(expr \${uncompressedb} / 1024 / 1024)"
							uncompressed="\${uncompressed:\${#uncompressed}-4:4} M"
						else
							uncompressed="    \$(expr \${uncompressedb} / 1024)"
							uncompressed="\${uncompressed:\${#uncompressed}-4:4} K"
						fi
					else
						uncompressed="    \$(expr ${uncompressedb})"
						uncompressed="\${uncompressed:\${#uncompressed}-4:4} B"
					fi
					# Print package size.
					echo "[\$uncompressed]"
				done
			fi
		else
			# We did not find a slackware package in our build archive. Nothing to install.
			echo "\${package}" | tee -a \${SYSTEM_LOGFILE_FAILED}
		fi

	done
}

UTILITY_INSTALLTGZ_CHECK_ETC_SUDOERS() {
EOF
	sed -n "/^	## LABEL:ETC_SUDOERS/,/^	## LABEL:ETC_SUDOERS/p" "${SYSTEM_ROOT}/${PROJECT_SCRIPT_NAME}"	|\
		grep -v "## LABEL:ETC_SUDOERS" >>"${SCRIPT_NAME_INSTALLPKG}"
cat <<EOF  >>"${SCRIPT_NAME_INSTALLPKG}"
}

UTILITY_INSTALLTGZ_CHECK_ETC_NEW() {
EOF
	sed -n "/^	## LABEL:CHECKETC/,/^	## LABEL:CHECKETC/p"       "${SYSTEM_ROOT}/${PROJECT_SCRIPT_NAME}"	|\
		grep -v "## LABEL:CHECKETC"    >>"${SCRIPT_NAME_INSTALLPKG}"
cat <<EOF  >>"${SCRIPT_NAME_INSTALLPKG}"
}

UTILITY_INSTALLTGZ_UPDATE_DESKTOP_DATABASE() {
EOF
	sed -n "/^	## LABEL:UPDATEDDB/,/^	## LABEL:UPDATEDDB/p" "${SYSTEM_ROOT}/${PROJECT_SCRIPT_NAME}" |\
		grep -v "## LABEL:UPDATEDDB" >>"${SCRIPT_NAME_INSTALLPKG}"
cat <<EOF  >>"${SCRIPT_NAME_INSTALLPKG}"
}

UTILITY_INSTALLTGZ_UPDATE_GCONF_DATABASE() {
EOF
	sed -n "/^	## LABEL:UPDATEGDB/,/^	## LABEL:UPDATEGDB/p" "${SYSTEM_ROOT}/${PROJECT_SCRIPT_NAME}" |\
		grep -v "## LABEL:UPDATEGDB" >>"${SCRIPT_NAME_INSTALLPKG}"
cat <<EOF  >>"${SCRIPT_NAME_INSTALLPKG}"
}

UTILITY_INSTALLTGZ_UPDATE_GIOMODULES_CACHE() {
EOF
	sed -n "/^	## LABEL:UPDATEGIO/,/^	## LABEL:UPDATEGIO/p" "${SYSTEM_ROOT}/${PROJECT_SCRIPT_NAME}" |\
		grep -v "## LABEL:UPDATEGIO" >>"${SCRIPT_NAME_INSTALLPKG}"
cat <<EOF  >>"${SCRIPT_NAME_INSTALLPKG}"
}

UTILITY_INSTALLTGZ_UPDATE_GLIB_SCHEMAS() {
EOF
	sed -n "/^	## LABEL:UPDATEGLIB/,/^	## LABEL:UPDATEGLIB/p" "${SYSTEM_ROOT}/${PROJECT_SCRIPT_NAME}" |\
		grep -v "## LABEL:UPDATEGLIB" >>"${SCRIPT_NAME_INSTALLPKG}"
cat <<EOF  >>"${SCRIPT_NAME_INSTALLPKG}"
}

UTILITY_INSTALLTGZ_UPDATE_GTK_ICON_CACHE() {
EOF
	sed -n "/^	## LABEL:WARNGIC/,/^	## LABEL:WARNGIC/p" "${SYSTEM_ROOT}/${PROJECT_SCRIPT_NAME}" |\
		grep -v "## LABEL:WARNGIC" >>"${SCRIPT_NAME_INSTALLPKG}"
	sed -n "/^	## LABEL:UPDATEGIC/,/^	## LABEL:UPDATEGIC/p" "${SYSTEM_ROOT}/${PROJECT_SCRIPT_NAME}" |\
		grep -v "## LABEL:UPDATEGIC" >>"${SCRIPT_NAME_INSTALLPKG}"
cat <<EOF  >>"${SCRIPT_NAME_INSTALLPKG}"
}

UTILITY_INSTALLTGZ_UPDATE_BOOKMARKS_FILE() {
EOF
	sed -n "/^	## LABEL:BOOKMARKS/,/^	## LABEL:BOOKMARKS/p" "${SYSTEM_ROOT}/${PROJECT_SCRIPT_NAME}" |\
		grep -v "## LABEL:BOOKMARKS" >>"${SCRIPT_NAME_INSTALLPKG}"
cat <<EOF  >>"${SCRIPT_NAME_INSTALLPKG}"
}




# Main.
UTILITY_INSTALLTGZ_LAST_WARNING || exit 1

# Backup /etc/sudoers file...
md5sum /etc/sudoers >\${SYSTEM_PATH_LOGFILES_SYSTEM}/etc_sudoers.md5
cat /etc/sudoers >\${SYSTEM_PATH_LOGFILES_SYSTEM}/etc_sudoers.orig

# Create list of packages to be installed.
UTILITY_INSTALLTGZ_CREATE_LIST || exit 1

# Setup environment.
export DISABLE_SCHEMAS_UPDATE=true
export DISABLE_DESKTOPFILES_UPDATE=true
export DISABLE_MIMEINFO_UPDATE=true
export DISABLE_ICON_CACHE_UPDATE=true
export DISABLE_GIOMODULES_UPDATE=true
export DISABLE_GLIBSCHEMAS_UPDATE=true

UTILITY_INSTALLTGZ_INSTALL_PACKAGES

# Reset environment.
unset DISABLE_SCHEMAS_UPDATE
unset DISABLE_DESKTOPFILES_UPDATE
unset DISABLE_MIMEINFO_UPDATE
unset DISABLE_ICON_CACHE_UPDATE
unset DISABLE_GIOMODULES_UPDATE
unset DISABLE_GLIBSCHEMAS_UPDATE

# Do some maintenance
UTILITY_INSTALLTGZ_UPDATE_DESKTOP_DATABASE
UTILITY_INSTALLTGZ_UPDATE_GCONF_DATABASE
UTILITY_INSTALLTGZ_UPDATE_GIOMODULES_CACHE
UTILITY_INSTALLTGZ_UPDATE_GLIB_SCHEMAS
UTILITY_INSTALLTGZ_UPDATE_GTK_ICON_CACHE
UTILITY_INSTALLTGZ_UPDATE_BOOKMARKS_FILE

UTILITY_INSTALLTGZ_CHECK_ETC_NEW
UTILITY_INSTALLTGZ_CHECK_ETC_SUDOERS

# Check if all packages from the original build got installed.
if test -f \${SYSTEM_PATH_PACKAGES_DATA}; then
  ls /var/log/packages | sort >\${SYSTEM_PATH_PACKAGES_DATA//.orig./.new.}
  diff -U0 -d -r -N \${SYSTEM_PATH_PACKAGES_DATA} \${SYSTEM_PATH_PACKAGES_DATA//.orig./.new.} >\${SYSTEM_PATH}/install.diff
fi




# Remove some temp files.
rm -f \${DIALOG_RETURN_VALUE}
rm -f \${DIALOG_SCRIPT}

# Clear the screen.
dialog --clear
clear

echo
echo "All done!"
echo




# Display note about logfiles
echo
echo "All packages installed. Check these logfiles for more info:"
echo -e "\\n => For a list of installed packages:\\n    \${SYSTEM_LOGFILE}"
echo -e "\\n => For a list of removed packages:\\n    \${SYSTEM_LOGFILE_REMOVED}"
echo -e "\\n => For a list of replaced packages:\\n    \${SYSTEM_LOGFILE_REPLACED}"
echo

# Do we have a diff file for failed packages?
if test -f \${SYSTEM_PATH}/install.diff; then
  echo
  echo "Check 'install.diff' for not matching packages..."
fi

echo -e "\\n\\nYou should reboot your system now!"
echo
EOF

	# Set permissions.
	chown root.root "${SCRIPT_NAME_INSTALLPKG}"
	chmod 744 "${SCRIPT_NAME_INSTALLPKG}"

}

# Create a script to remove all previously build packages.
UTILITY_CREATE_SCRIPT_REMOVETGZ() {
	local count=0

	local extra_list=""
	local extra=""

	FUNC_PROGRESS_BOX "Creating script: '$( basename ${SCRIPT_NAME_REMOVEPKG} )'"

	mkdir -p $( dirname ${SCRIPT_NAME_REMOVEPKG} )

	cat <<EOF  >"${SCRIPT_NAME_REMOVEPKG}"
#!/bin/sh

# Define some variables that will be used in this script.
CWD=\$(pwd)
RULER1="${RULER1}"
RULER2="${RULER2}"
SPACEBAR="${SPACEBAR}"

PROJECT_EMAIL="${PROJECT_EMAIL}"
PROJECT_VERSION="${PROJECT_VERSION}"
PROJECT_RELEASE="${PROJECT_RELEASE}"
PROJECT_NAME="${PROJECT_NAME}"
PROJECT_BACKTITLE="${PROJECT_BACKTITLE}"

SYSTEM_PATH=\${CWD}
SYSTEM_PATH_TEMP="/tmp"
SYSTEM_PATH_PACKAGES="\$(pwd)"


# Do we have write access to current path? If not place logfiles in /tmp.
if [ -w \${SYSTEM_PATH} ]; then
  SYSTEM_PATH_LOGFILES=\${SYSTEM_PATH}
else
  SYSTEM_PATH_LOGFILES=\${SYSTEM_PATH_TEMP}
fi

# Used in code taken from the buildsystem.
SYSTEM_PATH_LOGFILES_SYSTEM=\${SYSTEM_PATH_LOGFILES}

# Define logfile names.
SYSTEM_LOGFILE=\${SYSTEM_PATH_LOGFILES}/log.installed
SYSTEM_LOGFILE_REMOVED=\${SYSTEM_PATH_LOGFILES}/${SYSTEM_LOGFILE_REMOVED##*/}
SYSTEM_LOGFILE_DATA_UPDATE=\${SYSTEM_PATH_LOGFILES}/${SYSTEM_LOGFILE_DATA_UPDATE##*/}
SYSTEM_LOGFILE_GTK_ICON_CACHE=\${SYSTEM_PATH_LOGFILES}/${SYSTEM_LOGFILE_GTK_ICON_CACHE##*/}

# Remove existing logiles.
rm -f \${SYSTEM_LOGFILE}				1>/dev/null 2>/dev/null
rm -f \${SYSTEM_LOGFILE_REMOVED}		1>/dev/null 2>/dev/null
rm -f \${SYSTEM_LOGFILE_DATA_UPDATE}	1>/dev/null 2>/dev/null

# Dialogbox variables.
DIALOG_KEYCODE=0

# Overwrite button labels.
BUTTON_LABEL_YES="Yes"
BUTTON_LABEL_NO="No"

# Packages to install.
SYSTEM_PACKAGE_LIST=""
SYSTEM_PACKAGE_NAME_LIST=""
SYSTEM_PACKAGE_COUNT=0

# Max count of packages for progress bar.
SYSDB_PACKAGE_COUNT=${SYSDB_PACKAGE_COUNT}




# Show a infobox.
FUNC_PROGRESS_BOX() {
  dialog --title "IN PROGRESS:" --backtitle "\${PROJECT_BACKTITLE}" --cr-wrap --infobox "\$1" 3 40
}

# Init progress bar.
FUNC_INIT_PROGRESS_BAR() {
	local a=0
	local char="#"
	local percent=0
	PROGRESS_BAR=""
	PROGRESS_PERCENT[0]=""
	PROGRESS_MAX_WIDTH=\$(dialog --stdout --print-maxsize | sed "s,.* ,,g")
	echo -ne "\r"
	while [ \${a} -lt \${PROGRESS_MAX_WIDTH} ]; do
		let percent=(\${a}+1)*100/\${PROGRESS_MAX_WIDTH}
		if [ \${percent} -gt 100 ]; then
			let percent=100
		fi
		PROGRESS_PERCENT[\${a}]="[\${percent}%] "
		PROGRESS_BAR="\${PROGRESS_BAR}\${char}"
		echo -n " "
		let a=a+1
	done
}

# Draw the progress bar: $1: current count, $2: max count
FUNC_DRAW_PROGRESS_BAR() {
	local progress=0
	let progress=\$1*PROGRESS_MAX_WIDTH/\$2
	echo -ne "\r\${PROGRESS_BAR:0:\$progress}\r\${PROGRESS_PERCENT[\${progress}]}"
}
EOF
	sed -n "/^## LABEL:FUNCFINDPKG/,/^## LABEL:FUNCFINDPKG/p" "${SYSTEM_ROOT}/${PROJECT_SCRIPT_NAME}"	|\
		grep -v "## LABEL:FUNCFINDPKG" >>"${SCRIPT_NAME_REMOVEPKG}"
cat <<EOF  >>"${SCRIPT_NAME_REMOVEPKG}"
UTILITY_REMOVETGZ_UPDATE_DESKTOP_DATABASE() {
EOF
	sed -n "/^	## LABEL:UPDATEDDB/,/^	## LABEL:UPDATEDDB/p" "${SYSTEM_ROOT}/${PROJECT_SCRIPT_NAME}" |\
		grep -v "## LABEL:UPDATEDDB" >>"${SCRIPT_NAME_REMOVEPKG}"
cat <<EOF  >>"${SCRIPT_NAME_REMOVEPKG}"
}

UTILITY_REMOVETGZ_UPDATE_GCONF_DATABASE() {
EOF
	sed -n "/^	## LABEL:UPDATEGDB/,/^	## LABEL:UPDATEGDB/p" "${SYSTEM_ROOT}/${PROJECT_SCRIPT_NAME}" |\
		grep -v "## LABEL:UPDATEGDB" >>"${SCRIPT_NAME_REMOVEPKG}"
cat <<EOF  >>"${SCRIPT_NAME_REMOVEPKG}"
}

UTILITY_REMOVETGZ_UPDATE_GIOMODULES_CACHE() {
EOF
	sed -n "/^	## LABEL:UPDATEGIO/,/^	## LABEL:UPDATEGIO/p" "${SYSTEM_ROOT}/${PROJECT_SCRIPT_NAME}" |\
		grep -v "## LABEL:UPDATEGIO" >>"${SCRIPT_NAME_REMOVEPKG}"
cat <<EOF  >>"${SCRIPT_NAME_REMOVEPKG}"
}

UTILITY_REMOVETGZ_UPDATE_GLIB_SCHEMAS() {
EOF
	sed -n "/^	## LABEL:UPDATEGLIB/,/^	## LABEL:UPDATEGLIB/p" "${SYSTEM_ROOT}/${PROJECT_SCRIPT_NAME}" |\
		grep -v "## LABEL:UPDATEGLIB" >>"${SCRIPT_NAME_REMOVEPKG}"
cat <<EOF  >>"${SCRIPT_NAME_REMOVEPKG}"
}

UTILITY_REMOVETGZ_UPDATE_GTK_ICON_CACHE() {
EOF
	sed -n "/^	## LABEL:UPDATEGIC/,/^	## LABEL:UPDATEGIC/p" "${SYSTEM_ROOT}/${PROJECT_SCRIPT_NAME}" |\
		grep -v "## LABEL:UPDATEGIC" >>"${SCRIPT_NAME_REMOVEPKG}"
cat <<EOF  >>"${SCRIPT_NAME_REMOVEPKG}"
}

# Last warning...
UTILITY_REMOVETGZ_LAST_WARNING() {
	dialog	--title "INFORMATION" \\
			--backtitle "\${PROJECT_BACKTITLE}" \\
			--yes-label "${BUTTON_LABEL_YES}" \\
			--no-label "${BUTTON_LABEL_NO}" \\
			--ok-label "${BUTTON_LABEL_YES}" \\
			--cancel-label "${BUTTON_LABEL_NO}" \\
			--defaultno \\
			--yesno "\
\n\
Remove all previously installed packages found in the system/packages directory?\n " 0 0 2>/dev/null

	DIALOG_KEYCODE=\$?

	if [ \${DIALOG_KEYCODE} -ne 0 ]; then
		dialog --clear
		clear
		return 1
	else
		return 0
	fi
}

# Create a list of packages to remove.
UTILITY_REMOVETGZ_CREATE_LIST() {
	local count=0
	local entry=""
	local package=""
	local group=""
	local name=""
	local file_list=""

	# Print some info on screen...
	FUNC_PROGRESS_BOX "Searching for packages to install..."
	FUNC_INIT_PROGRESS_BAR

	for package in \\
EOF

	# Header is created, add a list of all packages included.
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

		let count=count+1

		# XOrg-Server package found?
		if [ x"${SYSDB_PACKAGE_NAME[${count}]}" == x"xorg-server" ]; then
			# Skip the entry for the disabled XOrg server package.
			# See: BUILDSYS_MODIFY_SELECT_XORG_SERVER()
			if [ x"${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}" == x"true" ]; then
				continue
			fi
		fi

		# Does the current package install any other packages? Example: gutenprint will replace gimp-print package.
		extra_list=""
		for extra in ${SYSDB_PACKAGE_EXTRAPKG[${count}]}; do
			extra_list="${extra_list},+:${extra}"
		done
		if [ x"${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}" == x"${SYSDB_PACKAGE_SEARCH_NAME[${count}]}" ]; then
			echo "\"${SYSDB_PACKAGE_GROUP[${count}]}/${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}[]${extra_list}\" \\" >>${SCRIPT_NAME_REMOVEPKG}
		else
			echo "\"${SYSDB_PACKAGE_GROUP[${count}]}/${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}[${SYSDB_PACKAGE_SEARCH_NAME[${count}]}]${extra_list}\" \\" >>${SCRIPT_NAME_REMOVEPKG}
		fi

	done

cat <<EOF  >>"${SCRIPT_NAME_REMOVEPKG}"
; do

		# Draw the progress bar.
		let count=count+1
		FUNC_DRAW_PROGRESS_BAR \${count} \${SYSDB_PACKAGE_COUNT}

		entry="\${package//\"/}"

		# Extract package name and group of the current package.
		name="\${entry##*/}"			#Remove group name
		name="\${name//,*/}"			#Remove extra package info
		name="\${name//\[*/}"			#Remove search name info
		search="\${entry//*\[/}"		#Remove group name and original name
		search="\${search//\]*/}"		#Remove extra package info
		search="\${search:-\${name}}"	#If no search name specified use original name
		group="\${entry%%/*}"			#Extract group name

		# Search for a slackware package for the current entry in package list.
		FUNC_CHECK_FIND_PACKAGE "\${search}" "\${group}"
		file_list="\${RETURN_VALUE}"

		# Did we find a package?
		if [ x"" != x"\${file_list}" ]; then
			if [ "\${SYSTEM_PACKAGE_NAME_LIST}" == "\${SYSTEM_PACKAGE_NAME_LIST// \${name} /}" ]; then
				let SYSTEM_PACKAGE_COUNT=SYSTEM_PACKAGE_COUNT+1
				SYSTEM_PACKAGE_LIST="\${SYSTEM_PACKAGE_LIST} \\"\${package}\\""
				SYSTEM_PACKAGE_NAME_LIST="\${SYSTEM_PACKAGE_NAME_LIST} \${name} "
			fi
		fi

	done

	if [ \${SYSTEM_PACKAGE_COUNT} -eq 0 ]; then
		dialog --clear
		clear
		return 1
	else
		return 0
	fi
}

# Start to remove the packages
UTILITY_REMOVETGZ_REMOVE_PACKAGES() {
	local count=0
	local entry=""
	local package=""
	local group=""
	local name=""
	local file_list=""
	local remove_package=""
	local percent_removed=0
	local count_removed=

	for package in \${SYSTEM_PACKAGE_LIST}; do

		let count=count+1
		entry="\${package//\"/}"

		# Extract package name and group of the current package.
		name="\${entry##*/}"			#Remove group name
		name="\${name//,*/}"			#Remove extra package info
		name="\${name//\[*/}"			#Remove search name info
		search="\${entry//*\[/}"		#Remove group name and original name
		search="\${search//\]*/}"		#Remove extra package info
		search="\${search:-\${name}}"	#If no search name specified use original name
		group="\${entry%%/*}"			#Extract group name

		# Search for a slackware package for the current entry in package list.
		FUNC_CHECK_FIND_PACKAGE "\${search}" "\${group}"
		file_list="\${RETURN_VALUE}"

		# Did we find a package?
		if [ x"" != x"\${file_list}" ]; then

			remove_package=\$(basename \${file_list})
			remove_package=\${remove_package%.t[x|g|l|b]z}

			if [ -e /var/log/packages/\${remove_package} ]; then

				# Is this a package that contain any services?
				# If so, we better shut down the service first.
				case \${name} in
					avahi)			if [ -x /etc/rc.d/rc.avahidaemon ]; then
										sh /etc/rc.d/rc.avahidaemon stop 1>/dev/null 2>/dev/null
									fi
									if [ -x /etc/rc.d/rc.avahidnsconfd ]; then
										sh /etc/rc.d/rc.avahidnsconfd stop 1>/dev/null 2>/dev/null
									fi
									;;
				esac

				let percent_removed=count_removed*100/SYSTEM_PACKAGE_COUNT
				let count_removed=count_removed+1

				# Display some info and remove the package.
				echo \${percent_removed}
				echo "XXX"
				echo "Currently removing: \${name}"
				echo "Package: \${remove_package}"
				echo "XXX"

				echo "Removing package[\$count] \${remove_package} ..." >>\${SYSTEM_LOGFILE}
				removepkg \${remove_package} 1>/dev/null 2>>\${SYSTEM_LOGFILE}
				echo "\${remove_package}" >>\${SYSTEM_LOGFILE_REMOVED}

				# Does this package install any other packages?
				if [ x"\${entry}" != x"\${entry//,*/}" ]; then

					# Cleanup extra packages list.
					extra_list=""

					file_info=\${entry#*,}
					for file in \${file_info//,/ }; do
						case \${file} in
							+:*) extra_list="\${extra_list},\${file#+:}";;
						esac
					done
					extra_list="\${extra_list#,}"

					# For each extra package to be installed by the parent package show some info.
					for extra in \${extra_list//,/ }; do
						FUNC_CHECK_FIND_PACKAGE "\${extra}" "\${group}"
						file_list="\${RETURN_VALUE}"
						if [ x"\${file_info}" != x"" ]; then
							echo "  => \${name} removes extra package \${file_info}" >> \${SYSTEM_LOGFILE}
							removepkg \${file_info} 1>/dev/null 2>>\${SYSTEM_LOGFILE}
						fi
					done

				fi

			fi
		fi
	done | dialog	--title "${PROJECT_NAME} - REMOVING PACKAGES TO BE UPDATED" \
					--backtitle "${PROJECT_BACKTITLE}" \
					--gauge "Removing packages..." 8 60 0
}




# Main.
UTILITY_REMOVETGZ_LAST_WARNING || exit 1
UTILITY_REMOVETGZ_CREATE_LIST || exit 1

# Setup environment.
export DISABLE_SCHEMAS_UPDATE=true
export DISABLE_DESKTOPFILES_UPDATE=true
export DISABLE_MIMEINFO_UPDATE=true
export DISABLE_ICON_CACHE_UPDATE=true
export DISABLE_GIOMODULES_UPDATE=true
export DISABLE_GLIBSCHEMAS_UPDATE=true

UTILITY_REMOVETGZ_REMOVE_PACKAGES

# Reset environment.
unset DISABLE_SCHEMAS_UPDATE
unset DISABLE_DESKTOPFILES_UPDATE
unset DISABLE_MIMEINFO_UPDATE
unset DISABLE_ICON_CACHE_UPDATE
unset DISABLE_GIOMODULES_UPDATE
unset DISABLE_GLIBSCHEMAS_UPDATE

# Do some maintenance
UTILITY_REMOVETGZ_UPDATE_DESKTOP_DATABASE
UTILITY_REMOVETGZ_UPDATE_GCONF_DATABASE
UTILITY_REMOVETGZ_UPDATE_GIOMODULES_CACHE
UTILITY_REMOVETGZ_UPDATE_GLIB_SCHEMAS
UTILITY_REMOVETGZ_UPDATE_GTK_ICON_CACHE





# Clear the screen.
dialog --clear
clear

echo
echo "All done!"
echo




# Display note about logfiles
echo
echo "All packages installed. Check these logfiles for more info:"
echo -e "\\n => For error messages:\\n    \${SYSTEM_LOGFILE}"
echo -e "\\n => For a list of removed packages:\\n    \${SYSTEM_LOGFILE_REMOVED}"
echo

if [ -e logfile.replaced ]; then
	echo -e "\\n\\nDon't forget to re-install slackware packages listed in\\n    logfile.replaced"
fi

echo
EOF

	# Set permissions.
	chown root.root "${SCRIPT_NAME_REMOVEPKG}"
	chmod 744 "${SCRIPT_NAME_REMOVEPKG}"

}

# Create the script to generate CHECKSUM.md5 and PACKAGES.txt
UTILITY_CREATE_SCRIPT_MAKEINFO() {

  FUNC_PROGRESS_BOX "Creating script: 'makeinfo.sh'"

  # Copy script to file.
  cat <<EOF >"${SCRIPT_NAME_MAKEINFO}"
#!/bin/sh
## This script will create a PACKAGES.txt and CHECKSUM.md5 file from
## als Slackware packages found in this subdirectory.
##

PROJECT_EMAIL="${PROJECT_EMAIL}"
PROJECT_VERSION="${PROJECT_VERSION}"
PROJECT_RELEASE="${PROJECT_RELEASE}"
PROJECT_NAME="${PROJECT_NAME}"
PROJECT_BACKTITLE="${PROJECT_BACKTITLE}"

# Show a infobox.
FUNC_PROGRESS_BOX() {
	dialog --title "IN PROGRESS:" --backtitle "\${PROJECT_BACKTITLE}" --cr-wrap --infobox "\$1" 3 40
}

# Create checksum and package file.
UTILITY_CREATE_INFO_FILES() {
	local package=""
	local entry=""
	local name=""
	local group=""
	compressedb=0
	uncompressedb=0
	compressed=""
	uncompressed=""
	required=""
	conflicts=""
	suggests=""
	total_compressed=0
	total_uncompressed=0

	# Delete existing files.
	rm -f PACKAGES.txt
	rm -f CHECKSUMS.md5
	rm -f PACKAGES.txt.gz
	rm -f CHECKSUMS.md5.gz

EOF

	# We cannot make use of cat/EOF here so use echo and add this command to our script.
	echo "cat <<EOF >CHECKSUMS.md5" >>"${SCRIPT_NAME_MAKEINFO}"

	# Add some info to the script, taken from Pats MD5 files.
	cat <<EOF >>"${SCRIPT_NAME_MAKEINFO}"
These are the MD5 message digests for the files in this directory.
If you want to test your files, use 'md5sum' and compare the values to
the ones listed here.

To test all these files, use this command:

md5sum -c CHECKSUMS.md5 | less

'md5sum' can be found in the GNU coreutils package on ftp.gnu.org in
/pub/gnu, or at any GNU mirror site.

MD5 message digest                Filename
EOF
	echo "EOF" >>"${SCRIPT_NAME_MAKEINFO}"
	echo "" >>"${SCRIPT_NAME_MAKEINFO}"


	# We cannot make use of cat/EOF here so use echo and add this command to our script.
	echo "cat <<EOF >PACKAGES.txt" >>"${SCRIPT_NAME_MAKEINFO}"

	# Add some info to the script, taken from Pats text files.
	cat <<EOF >>"${SCRIPT_NAME_MAKEINFO}"
PACKAGES.txt;  \$(date)

This file provides details on the Slackware packages found
in the this directory.

Total size of all packages (compressed):  @@@1
Total size of all packages (uncompressed):  @@@2


EOF
	echo "EOF" >>"${SCRIPT_NAME_MAKEINFO}"


	# Now add the code to create the MD5 and textfiles to our script.
	cat <<EOF >>"${SCRIPT_NAME_MAKEINFO}"




	# Now search for packages and extract the data.
	for package in \$(find ./ -type f -name "*t[x|g|l|b]z" | sort); do

		# Extract package name, version and group from
		# the current slackware package being proccessed.
		entry=\${package##*/}

		name=\${entry%-*}	# Remove build and extension
		name=\${name%-*}	# Remove arch
		name=\${name%-*}	# Remove version
		group=\${package#./}
		group=\${group%%/*}

		# Display name of the package without version info.
		# This is just to let the user know which package is
		# in progress right now.
		echo -n "\${name}... "

		# Support for MD5SUMs for each created package.
		md5sum \${package} >>CHECKSUMS.md5

		# Extract the install files and create package description file.
		case \${package} in
			*txz)	xz -dc \${package} | tar xf - \\
						"install/slack-desc" \\
						"install/slack-required" \\
						"install/slack-conflicts" \\
						"install/slack-suggests" \\
						&>/dev/null
					cat "install/slack-desc" | grep "^\${name}:" \\
						>\${package//.txz/.txt} ;;
			*tgz)	gunzip -dcf \${package} | tar xf - \\
						"install/slack-desc" \\
						"install/slack-required" \\
						"install/slack-conflicts" \\
						"install/slack-suggests" \\
						&>/dev/null
					cat "install/slack-desc" | grep "^\${name}:" \\
						>\${package//.tgz/.txt} ;;
		esac

		# Calculate the un-/compressed size of the package in bytes/kbytes.
		compressedb="\$(du -sb \${package} | cut -f 1)"
		case \${package} in
			*txz)	uncompressedb="\$(xz -dc \${package} | dd 2>&1 | tail -n 1 | cut -f 1 -d ' ')"
					;;
			*tgz)	uncompressedb="\$(gunzip -dc \${package} | dd 2>&1 | tail -n 1 | cut -f 1 -d ' ')"
					;;
		esac
		compressed="\$(expr \${compressedb} / 1024) K"
		if [ x"\${compressed}" == x"0" ]; then
			compressed="1"
		fi
		uncompressed="\$(expr \${uncompressedb} / 1024) K"
		if [ x"\${uncompressed}" == x"0" ]; then
			uncompressed="1"
		fi

		# Get additional package information.
		required=\$(cat  install/slack-required  2>/dev/null | xargs -r -iZ echo -n "Z," | sed -e "s/,$//")
		conflicts=\$(cat install/slack-conflicts 2>/dev/null | xargs -r -iZ echo -n "Z," | sed -e "s/,$//")
		suggests=\$(cat  install/slack-suggests  2>/dev/null | xargs -r )

EOF

	# Add package info to the repository file.
	echo "		cat <<EOF >>PACKAGES.txt" >>"${SCRIPT_NAME_MAKEINFO}"
	cat <<EOF >>"${SCRIPT_NAME_MAKEINFO}"
PACKAGE NAME: \${entry}
PACKAGE LOCATION:  ./\${group}
PACKAGE SIZE (compressed):  \${compressed}
PACKAGE SIZE (uncompressed):  \${uncompressed}
PACKAGE REQUIRED:  \${required}
PACKAGE CONFLICTS:  \${conflicts}
PACKAGE SUGGESTS:  \${suggests}
PACKAGE DESCRIPTION:
EOF
	echo "EOF" >>"${SCRIPT_NAME_MAKEINFO}"

	# Add code to include the slack-desc file of the package into the repository file.
	cat <<EOF >>"${SCRIPT_NAME_MAKEINFO}"

		# Add the package description
		cat "install/slack-desc" | grep "^\${name}" >>PACKAGES.txt
		echo "" >>PACKAGES.txt

		# Remove the temporary install files.
		if [ -d install ]; then rm -rf install; fi

		# Do some math to calculate overall bytes.
		let total_compressed=total_compressed+compressedb
		let total_uncompressed=total_uncompressed+uncompressedb

	done




	# Replace overall bytes in the PACKAGES.txt.
	total_compressed="\$(expr \${total_compressed} / 1024) K"
	total_uncompressed="\$(expr \${total_uncompressed} / 1024) K"

	sed -i "s,@@@1,\${total_compressed}," PACKAGES.txt
	sed -i "s,@@@2,\${total_uncompressed}," PACKAGES.txt

	# Create gzipped versions of the files.
	gzip CHECKSUMS.md5
	gunzip -c CHECKSUMS.md5.gz >CHECKSUMS.md5
	gzip PACKAGES.txt
	gunzip -c PACKAGES.txt.gz  >PACKAGES.txt

}




# Main.
UTILITY_CREATE_INFO_FILES
echo ""
echo ""
echo "${RULER1}"
echo "All done!"
echo "${RULER1}"
echo ""

EOF

	# Set permissions.
	chown root.root "${SCRIPT_NAME_MAKEINFO}"
	chmod 744 "${SCRIPT_NAME_MAKEINFO}"

	return 0
}

# Just a separator...
SYS____________________PROFILE_EDITOR() {
	true
}

# Edit profile script in /etc/profile.d/buildtools.sh
PROFILE_EDITOR_MAIN_MENU() {
	local default_item="LIST"

	# Do we have write access to the profile script?
	if [ ! -w $(dirname ${SYSTEM_CONFIG_PROFILE}) ]; then
		dialog	--title "${PROJECT_NAME} - INFORMATION" \
				--backtitle "${PROJECT_BACKTITLE}" \
				--cr-wrap \
				--msgbox "\nYou do not have write access to the profile script directory:\n => $(dirname  ${SYSTEM_CONFIG_PROFILE})" 8 75
		return 0
	fi

	# This is the main menu loop.
	while [ true ]; do

		# The 'Edit profile' menu.
		dialog	--title "${PROJECT_NAME} - PROFILE SETUP" \
				--backtitle "${PROJECT_BACKTITLE}" \
				--no-cancel \
				--default-item "${default_item}" \
				--menu "\n\
You can use this menu to create or edit a profile. The profile script will be placed in '${SYSTEM_CONFIG_PROFILE}' and will be started automatically whenever you login to a shell.\n\n\
This script will set some default options that can be used when you want to compile buildtools manually.\n\n\
Select 'DONE' to return to the main menu.\n\n" 20 75 5 \
"LIST"     "Display the current profile" \
"EDIT"     "Edit the current profile" \
"DELETE"   "Delete the current profile" \
"DONE"     "Back to main menu" \
2> ${DIALOG_RETURN_VALUE}

		DIALOG_KEYCODE=$?
		DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")

		rm -f "${DIALOG_RETURN_VALUE}"

		# 'CANCEL' or 'ESC' ?
		if test ${DIALOG_KEYCODE} -ne 0; then
			break
		fi

		# Exit profile script editor?
		if [ "${DIALOG_REPLY}" == "DONE" ]; then
			break
		fi

		# Does the user already have a profile script?
		if [ ! -e "${SYSTEM_CONFIG_PROFILE}" ]; then

			# No, ask the User if we should create an empty script.
			dialog	--title "${PROJECT_NAME} - INFORMATION" \
					--backtitle "${PROJECT_BACKTITLE}" \
					--cr-wrap \
					--yes-label "${BUTTON_LABEL_YES}" \
					--no-label "${BUTTON_LABEL_NO}" \
					--ok-label "${BUTTON_LABEL_YES}" \
					--cancel-label "${BUTTON_LABEL_NO}" \
					--yesno "\nYou do not have a profile yet, create an empty profile script?" 8 40

			# Should we create a script?
			if test $? -eq 0 ; then
				# Yes, create an empty dummy script.
				echo "#!/bin/sh" >"${SYSTEM_CONFIG_PROFILE}"
				echo "" >>"${SYSTEM_CONFIG_PROFILE}"
				chmod +x "${SYSTEM_CONFIG_PROFILE}"
			else
				# No, then there is nothing to edit ;-)
				continue
			fi

		fi

		# Display the profile script to the user.
		if [ "${DIALOG_REPLY}" == "LIST" ]; then
			PROFILE_EDITOR_LIST_VALUES
			default_item="EDIT"
			continue
		fi

		# Edit the profile script.
		if [ "${DIALOG_REPLY}" == "EDIT" ]; then
			PROFILE_EDITOR_SELECT_OPTION
			default_item="DONE"
			continue
		fi

		# Delete the profile script?
		if [ "${DIALOG_REPLY}" == "DELETE" ]; then

			# Ask the User if we should delete the profile script.
			dialog	--title "${PROJECT_NAME} - INFORMATION" \
					--backtitle "${PROJECT_BACKTITLE}" \
					--cr-wrap \
					--yes-label "${BUTTON_LABEL_YES}" \
					--no-label "${BUTTON_LABEL_NO}" \
					--ok-label "${BUTTON_LABEL_YES}" \
					--cancel-label "${BUTTON_LABEL_NO}" \
					--defaultno \
					--yesno "\nAre you sure that you wand to delete your current profile script?" 8 40

			# Should we delete the script?
			if [ $? = 0 ]; then
				# Yes, delete it.
				rm -f "${SYSTEM_CONFIG_PROFILE}"
			fi

			continue

		fi

	done

	# Leave the profile menu.
	return 0
}

# List profile script.
PROFILE_EDITOR_LIST_VALUES() {

	# Remove additional comments from the config file and redirect the output to a temp datafile.
	cat <<EOF >${DIALOG_TEMP_FILE1}
This is a list of options set by your profile script:
=> ${SYSTEM_CONFIG_PROFILE}

Note: It is recommended not to set 'SLACK_ARCH' or 'SLACK_OPT' since
these options can break the build process.
EOF

	grep -v "#" ${SYSTEM_CONFIG_PROFILE} | sed "s,^export ,,g" >>${DIALOG_TEMP_FILE1}
	dialog	--title "${PROJECT_NAME} - PROFILE SETUP" \
			--backtitle "${PROJECT_BACKTITLE}" \
			--cr-wrap \
			--exit-label "${BUTTON_LABEL_OK}" \
			--textbox ${DIALOG_TEMP_FILE1} 20 75

	# Delete the temp datafile.
	rm -f ${DIALOG_TEMP_FILE1}

	return 0
}

# Edit profile script.
PROFILE_EDITOR_SELECT_OPTION() {
	local default_item=""
	local option_name=""
	local option_value=""
	local option_text=""

	# The main menu loop.
	while [ true ]; do

		# Display the menu.
		dialog	--title "${PROJECT_NAME} - PROFILE SETUP" \
				--backtitle "${PROJECT_BACKTITLE}" \
				--item-help \
				--default-item "${default_item}" \
				--ok-label "${BUTTON_LABEL_OK}" \
				--cancel-label "${BUTTON_LABEL_DONE}" \
				--menu "\n\
Here is a list with options that can be added to the profile script.\n\n\
Move the cursor to one of the options below and select 'OK' to add, remove or edit the option." 20 75 8 \
"FUNCTIONS_PATH"     "Specify the path to the 'functions' file"          "Specify a path to a custom 'functions' file to be used for all buildtools" \
"PACKAGE_TAG"        "Set a custom package tag"                          "Specify a custom package tag to sign your packages" \
"PACKAGE_EXT"        "Select package compression method"                 "Select package extension and compression method like tgz or txz" \
"MIRROR_GNOME"       "Select the default GNOME download mirror"          "Specify a custom download mirror for the GNOME desktop source files" \
"MIRROR_KDE"         "Select the default KDE download mirror"            "Specify a custom download mirror for the KDE desktop source files" \
"MIRROR_XORG"        "Select the default X.org download mirror"          "Specify a custom download mirror for the X.org source files" \
"MIRROR_SOURCEFORGE" "Select the default SOURCEFORGE download mirror"    "Specify a custom download mirror for projects hosted by SourceForge" \
"MIRROR_GENTOO_DIST" "Select a mirror for GENTOO's distfiles"            "The gentoo project keep copies of many sources packages on their servers" \
"REQUIREDBUILDER"    "Create 'slack-required' files"                     "Create 'slack-required' files with ${PROJECT_NAME}, sbbdep or requiredbuilder" \
"RBUILDER_FLAGS"     "Options for creating 'slack-required' files"       "Use extra flags/options when creating 'slsack-required' files." \
"RBUILDER_DBASE"     "Always use 'slack-required' files found here"      "Use customized '<packagename>.slack-required' files found in this directory." \
"RBUILDER_CONFIG"    "Add/Remove packages to 'slack-required' files"     "Secify a path to '<package>.ADD/EXCLUDE' files to finetune 'slack-required'" \
"RBUILDER_COPY"      "Specify a path for copies of 'slack-required'"     "Specify a pth to place a copy of the 'slack-required' file." \
"RBUILDER_FILTER"    "Filter 'slack-required' files"                     "Filter the 'slack-required' file against a custom package list." \
"RBUILDER_FPATH"     "Specify a path for the filter files"               "Specify a path to a filter list for the 'slack-required' file" \
"DEBUG"              "Remove debug data from executables and libraries"  "Create smaller packages by removing debug information from binary files" \
"BUILDFILES"         "Include build files to package"                    "Add the buildfiles to the package so anyone can easily rebuild the package" \
"SLACK_ARCH"         "Overwrite system architecture"                     "Specify a CPU architecture to use (DANGEROUS! Can break the build process)" \
"SLACK_OPT"          "Overwrite gcc optimization modes"                  "Specify custom GCC compiler options (DANGEROUS! Can break the build process)" \
"MAKEOPTS"           "Bypass extra options to the make command"          "Bypass extra options to the 'make' command when building packages from source" \
2> ${DIALOG_RETURN_VALUE}

		DIALOG_KEYCODE=$?
		DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")

		rm -f "${DIALOG_RETURN_VALUE}"

		# 'CANCEL' or 'ESC' ?
		if test ${DIALOG_KEYCODE} -ne 0; then
			break
		fi

		# Remember selection.
		default_item="${DIALOG_REPLY}"

		# Get the option alias name.
		option_name=${DIALOG_REPLY}
		PROFILE_EDITOR_SELECT_OPTION_NAME ${option_name}

		# Display a menu with ADD/REMOVE/EDIT options.
		option_value=$(grep "^export ${RUNTIME_EDITOR_OPTION_NAME}=" ${SYSTEM_CONFIG_PROFILE})
		option_value="${option_value//\"/}"
		# Is this option already in the profile script?
		if [ x"${option_value}" == x"" ]; then
			option_text="Option is not set"
		else
			option_value=${option_value##*=}
			if [ x"${option_value}" == x"" ]; then
				option_text="Empty/No values specified"
			else
				option_text="${option_value}"
			fi
		fi

		# Create the dialog script.
		cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "${PROJECT_NAME} - PROFILE SETUP" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--ok-label "${BUTTON_LABEL_OK}" \\
		--cancel-label "${BUTTON_LABEL_CANCEL}" \\
		--default-item "EDIT" \\
		--menu "\n\\
Option       : ${RUNTIME_EDITOR_OPTION_NAME}\n
Current value: ${option_value}\n\n\n\\
Select one of the following options:" 14 75 2 \\
EOF

		if [ x"${option_value}" == x"" ]; then
			# If the option does not yet exist, only option will be "ADD"
			cat <<EOF >>${DIALOG_SCRIPT}
"ADD"    "Add this option to your profile script" \\
EOF
		else
			# If the option is already in the profile script allow to "REMOVE" or "EDIT" the option.
			cat <<EOF >>${DIALOG_SCRIPT}
"REMOVE" "Remove this option from your profile script" \\
"EDIT"   "Edit this option" \\
EOF
		fi

		cat <<EOF >>${DIALOG_SCRIPT}
2> ${DIALOG_RETURN_VALUE}
EOF

		# Start the script and offer the dialog.
		. "${DIALOG_SCRIPT}"

		DIALOG_KEYCODE=$?
		DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")

		rm -f "${DIALOG_SCRIPT}"
		rm -f "${DIALOG_RETURN_VALUE}"

		if test ${DIALOG_KEYCODE} -ne 0; then
			continue
		fi

		# Call the function to add, remove or edit the option.
		PROFILE_EDITOR_GET_OPTION_TYPE "${RUNTIME_EDITOR_OPTION_NAME}"

		case ${DIALOG_REPLY} in
			"ADD")     PROFILE_EDITOR_OPTION_ADD    "${option_value}"; continue;;
			"REMOVE")  PROFILE_EDITOR_OPTION_REMOVE "${option_value}"; continue;;
			"EDIT")    PROFILE_EDITOR_OPTION_MODIFY "${option_value}"; continue;;
		esac

	done

	# Leave the profile menu.
	return 0
}

# Add the option to the profile script.
PROFILE_EDITOR_OPTION_ADD() {

	# Delete the old option (if existing).
	sed -i "/^export ${RUNTIME_EDITOR_OPTION_NAME}/d" ${SYSTEM_CONFIG_PROFILE}

	# Add a new dummy value to the script.
	case ${RUNTIME_EDITOR_OPTION_TYPE} in
		"MENU")   echo "export ${RUNTIME_EDITOR_OPTION_NAME}="     >>${SYSTEM_CONFIG_PROFILE} ;;
		"INPUT")  echo "export ${RUNTIME_EDITOR_OPTION_NAME}=\"\"" >>${SYSTEM_CONFIG_PROFILE} ;;
	esac

	# Let the user edit the new option.
	PROFILE_EDITOR_OPTION_MODIFY "$1"

	return 0
}

# Delete the option from the profile script.
PROFILE_EDITOR_OPTION_REMOVE() {

	# Delete the old option (if existing).
	sed -i "/^export ${RUNTIME_EDITOR_OPTION_NAME}/d" ${SYSTEM_CONFIG_PROFILE}

	return 0
}

# Edit the option.
PROFILE_EDITOR_OPTION_MODIFY() {

	# Call the function to edit the value.
	case ${RUNTIME_EDITOR_OPTION_TYPE} in
		"MENU")   PROFILE_EDITOR_SET_MENU_OPTION   "$1";;
		"INPUT")  PROFILE_EDITOR_SET_INPUT_OPTION  "$1";;
	esac

	return 0
}

# For each profile option define the option type and help text.
# These are the option names usind in the buildtools.
PROFILE_EDITOR_GET_OPTION_TYPE() {

	case $1 in
		"BUILDTOOL_FUNCTIONS_PATH")				RUNTIME_EDITOR_OPTION_TYPE="INPUT";
												RUNTIME_EDITOR_OPTION_MODES="NOSPACE";
												RUNTIME_EDITOR_HELP_TEXT="Specify the path to the 'functions' file. This file is required if you want to write your own buildtools. Instead of place a copy of this file into each buildtool directory you can put it elsewhere and let the variable 'BUILDTOOL_FUNCTIONS_PATH' point to it.";;
		"PACKAGE_TAG")							RUNTIME_EDITOR_OPTION_TYPE="INPUT";
												RUNTIME_EDITOR_OPTION_MODES="NOSPACE";
												RUNTIME_EDITOR_HELP_TEXT="You can use this option to tag your packages with your name or project.\nExample: package-version-arch-build<mytag>.${SYSTEM_PACKAGE_EXTENSION}\n\n  => Default: slp";;
		"BUILDTOOL_PACKAGE_EXTENSION")			RUNTIME_EDITOR_OPTION_TYPE="MENU";
												RUNTIME_EDITOR_OPTION_MODES="tgz txz";
												RUNTIME_EDITOR_HELP_TEXT="The default package extension for Slackware past 13.0 ist txz. The compression method used by txz is 'xz' which is more effective but also slower.\n  tgz => Faster but larger packages\n  txz => Slower but smaller packages.";;
		"MIRROR_GNOME")							RUNTIME_EDITOR_OPTION_TYPE="INPUT";
												RUNTIME_EDITOR_OPTION_MODES="NOSPACE";
												RUNTIME_EDITOR_HELP_TEXT="Select the GNOME download mirror. The selected mirror directory must include the 'sources' subdirectory.\n\n  => Default: http://ftp.gnome.org/pub/GNOME";;
		"MIRROR_KDE")							RUNTIME_EDITOR_OPTION_TYPE="INPUT";
												RUNTIME_EDITOR_OPTION_MODES="NOSPACE";
												RUNTIME_EDITOR_HELP_TEXT="Select the KDE download mirror. The selected mirror directory must include the 'stable' subdirectory.\n\n  => Default: ftp://ftp.solnet.ch/mirror/KDE";;
		"MIRROR_XORG")							RUNTIME_EDITOR_OPTION_TYPE="INPUT";
												RUNTIME_EDITOR_OPTION_MODES="NOSPACE";
												RUNTIME_EDITOR_HELP_TEXT="Select the X.org download mirror. The selected mirror directory must include the 'pub' subdirectory.\n\n  => Default: ftp://ftp.x.org";;
		"MIRROR_SOURCEFORGE")					RUNTIME_EDITOR_OPTION_TYPE="INPUT";
												RUNTIME_EDITOR_OPTION_MODES="NOSPACE";
												RUNTIME_EDITOR_HELP_TEXT="Select the SOURCEFORGE download mirror. The selected mirror directory must include the 'package name' subdirectory.\n\n  => Default: http://switch.dl.sourceforge.net/sourceforge";;
		"MIRROR_GENTOO_DISTFILES")				RUNTIME_EDITOR_OPTION_TYPE="INPUT";
												RUNTIME_EDITOR_OPTION_MODES="NOSPACE";
												RUNTIME_EDITOR_HELP_TEXT="Select a download mirror for GENTOO's distfiles directory. The selected mirror directory must include the 'distfiles' subdirectory.\n\n  => Default: http://gentoo.osuosl.org/distfiles";;
		"BUILDTOOL_RBUILDER_CREATE")			RUNTIME_EDITOR_OPTION_TYPE="MENU";
												RUNTIME_EDITOR_OPTION_MODES="true false";
												RUNTIME_EDITOR_HELP_TEXT="Add support for creating 'slack-required' files to your self created packages. Make sure to re-check these files before you share your packages!\n\n  => Default: false";;
		"BUILDTOOL_RBUILDER_FLAGS")				RUNTIME_EDITOR_OPTION_TYPE="INPUT";
												RUNTIME_EDITOR_OPTION_MODES="";
												RUNTIME_EDITOR_HELP_TEXT="If creating 'slack-required' files is enabled you can bypass some command line options to finetune the output like -v/--version to add package version info.\n\n  => Default: '-y'";;
		"BUILDTOOL_RBUILDER_DATABASE_PATH")		RUNTIME_EDITOR_OPTION_TYPE="INPUT";
												RUNTIME_EDITOR_OPTION_MODES="";
												RUNTIME_EDITOR_HELP_TEXT="Don't build create new'slack-required' files. This option defines a directory with existing 'slack-required' files to use. Files must be named like this:\n  => <packagename>.slack-required      or\n  => <packagename>.slack-suggested     or\n  => <packagename>.slack-conflicts\n\nThese files will use the same syntax as 'slack-required'.";;
		"BUILDTOOL_RBUILDER_CONFIG_PATH")		RUNTIME_EDITOR_OPTION_TYPE="INPUT";
												RUNTIME_EDITOR_OPTION_MODES="";
												RUNTIME_EDITOR_HELP_TEXT="Bypass extra packages to ADD or to EXCLUDE from the 'slack-required' file. This will define a directory containing files named like this:\n  => <packagename>.ADD      or\n  => <packagename>.EXCLUDE\n\nThese files will use the same syntax as 'slack-required'.";;
		"BUILDTOOL_RBUILDER_COPY_PATH")			RUNTIME_EDITOR_OPTION_TYPE="INPUT";
												RUNTIME_EDITOR_OPTION_MODES="";
												RUNTIME_EDITOR_HELP_TEXT="Specify a directory to place a copy of the 'slack-required' file for the compiled packages. The files will be tagged with the package name like this:\n\n  => slack-required.<packagename>\n\nIf there are no dependencies a blank file will be created.";;
		"BUILDTOOL_RBUILDER_FILTER")			RUNTIME_EDITOR_OPTION_TYPE="MENU";
												RUNTIME_EDITOR_OPTION_MODES="true false";
												RUNTIME_EDITOR_HELP_TEXT="Filter 'slack-required' files by a customized package list. Packages not included in that list will be excluded from the 'slack-required' file.\n\n  => Default: false";;
		"BUILDTOOL_RBUILDER_FILTER_PATH")		RUNTIME_EDITOR_OPTION_TYPE="INPUT";
												RUNTIME_EDITOR_OPTION_MODES="";
												RUNTIME_EDITOR_HELP_TEXT="Specify a directory to look for the filter files. The filter file must be named like:\n\n  => slack_required_filter.user\n\nIf not specified the buildsystem directory will be used.";;
		"BUILDTOOL_STRIP_BINARIES")				RUNTIME_EDITOR_OPTION_TYPE="MENU";
												RUNTIME_EDITOR_OPTION_MODES="false true";
												RUNTIME_EDITOR_HELP_TEXT="Remove debugging information from binaries and libraries. Set this to 'false' for debugging.\n\n  => Default: true";;
		"BUILDTOOL_INCLUDE_BUILDFILES")			RUNTIME_EDITOR_OPTION_TYPE="MENU";
												RUNTIME_EDITOR_OPTION_MODES="false true";
												RUNTIME_EDITOR_HELP_TEXT="Include the build files to the package.\n\n  => Default: true";;
		"SLACK_ARCH")							RUNTIME_EDITOR_OPTION_TYPE="INPUT";
												RUNTIME_EDITOR_OPTION_MODES="NOSPACE";
												RUNTIME_EDITOR_HELP_TEXT="Overwrite the default system architecture when compiling Buildtools. This will not work for binary distributed applications and packages that are architecture independent. If left blank the system architecture will be configured inside the buildtools (RECOMMENDED!).\n\n  Valid options: i486,i586,i686,x86_64\n  => Default= ''";;
		"SLACK_OPT")							RUNTIME_EDITOR_OPTION_TYPE="INPUT";
												RUNTIME_EDITOR_OPTION_MODES="";
												RUNTIME_EDITOR_HELP_TEXT="Overwrite the default gcc optimization modes that will be used when compiling Buildtools. This will have no effect to binary distributed applications and packages that only include data files. If left blank the gcc optimizations will be configured inside the buildtools depending on the system architecture (RECOMMENDED!).\n\n  => Default= ''";;
		"MAKEOPTS")								RUNTIME_EDITOR_OPTION_TYPE="INPUT";
												RUNTIME_EDITOR_OPTION_MODES="";
												RUNTIME_EDITOR_HELP_TEXT="Bypass extra options to the make command, for example '-j5' to specify the count of jobs that can be compiled at the same time. This can speed up the build on multiple core CPUs or when using distcc.\n\n  => Default=''";;
		*)										echo "Unknown 'PROFILE_EDITOR_GET_OPTION_TYPE': $1";
												exit 1;;
	esac

	return 0
}

# Get the correct option name.
# This will translate the internal (shorter) option name on the left
# to the correct option name on the right used in buildtools.
PROFILE_EDITOR_SELECT_OPTION_NAME() {

	case $1 in
		"FUNCTIONS_PATH")		RUNTIME_EDITOR_OPTION_NAME="BUILDTOOL_FUNCTIONS_PATH";;
		"PACKAGE_TAG")			RUNTIME_EDITOR_OPTION_NAME="PACKAGE_TAG";;
		"PACKAGE_EXT")			RUNTIME_EDITOR_OPTION_NAME="BUILDTOOL_PACKAGE_EXTENSION";;
		"MIRROR_GNOME")			RUNTIME_EDITOR_OPTION_NAME="MIRROR_GNOME";;
		"MIRROR_KDE")			RUNTIME_EDITOR_OPTION_NAME="MIRROR_KDE";;
		"MIRROR_XORG")			RUNTIME_EDITOR_OPTION_NAME="MIRROR_XORG";;
		"MIRROR_SOURCEFORGE")	RUNTIME_EDITOR_OPTION_NAME="MIRROR_SOURCEFORGE";;
		"MIRROR_GENTOO_DIST")	RUNTIME_EDITOR_OPTION_NAME="MIRROR_GENTOO_DISTFILES";;
		"REQUIREDBUILDER")		RUNTIME_EDITOR_OPTION_NAME="BUILDTOOL_RBUILDER_CREATE";;
		"RBUILDER_DBASE")		RUNTIME_EDITOR_OPTION_NAME="BUILDTOOL_RBUILDER_DATABASE_PATH";;
		"RBUILDER_CONFIG")		RUNTIME_EDITOR_OPTION_NAME="BUILDTOOL_RBUILDER_CONFIG_PATH";;
		"RBUILDER_FLAGS")		RUNTIME_EDITOR_OPTION_NAME="BUILDTOOL_RBUILDER_FLAGS";;
		"RBUILDER_COPY")		RUNTIME_EDITOR_OPTION_NAME="BUILDTOOL_RBUILDER_COPY_PATH";;
		"RBUILDER_FILTER")		RUNTIME_EDITOR_OPTION_NAME="BUILDTOOL_RBUILDER_FILTER";;
		"RBUILDER_FPATH")		RUNTIME_EDITOR_OPTION_NAME="BUILDTOOL_RBUILDER_FILTER_PATH";;
		"DEBUG")				RUNTIME_EDITOR_OPTION_NAME="BUILDTOOL_STRIP_BINARIES";;
		"BUILDFILES")			RUNTIME_EDITOR_OPTION_NAME="BUILDTOOL_INCLUDE_BUILDFILES";;
		"SLACK_ARCH")			RUNTIME_EDITOR_OPTION_NAME="SLACK_ARCH";;
		"SLACK_OPT")			RUNTIME_EDITOR_OPTION_NAME="SLACK_OPT";;
		"MAKEOPTS")				RUNTIME_EDITOR_OPTION_NAME="MAKEOPTS";;
		*)						echo "Unknown 'PROFILE_EDITOR_SELECT_OPTION_NAME': $1"; exit 1;;
	esac

	return 0
}

# Edit profile options that require a text string.
PROFILE_EDITOR_SET_INPUT_OPTION() {
	local option_value="$1"

	# Create the dialog script.
	dialog	--title "${PROJECT_NAME} - PROFILE SETUP" \
			--backtitle "${PROJECT_BACKTITLE}" \
			--inputbox "\n\
${RUNTIME_EDITOR_HELP_TEXT}\n\
\nCurrent value for this option is:\n\
  => ${RUNTIME_EDITOR_OPTION_NAME}=\"${option_value}\"\n\
\nEnter the new value for this option:" 21 75 "${option_value}" \
  2> ${DIALOG_RETURN_VALUE}

	DIALOG_KEYCODE=$?
	DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")

	rm -f "${DIALOG_RETURN_VALUE}"

	# 'CANCEL' or 'ESC' ?
	if test ${DIALOG_KEYCODE} -ne 0; then
		if [ x"${option_value}" == x"" ]; then
			PROFILE_EDITOR_OPTION_REMOVE
		fi
		return 0
	fi

	# Does the value allow blanks in the string?
	if [ x"${RUNTIME_EDITOR_OPTION_MODES}" == x"NOSPACE" ]; then
		DIALOG_REPLY=${DIALOG_REPLY// /}
	fi

	# Empty input field?
	if [ x"${DIALOG_REPLY}" == x"" ]; then
		PROFILE_EDITOR_OPTION_REMOVE
		return 0
	fi

	# Do some extra tests for some options.
	case "${RUNTIME_EDITOR_OPTION_NAME}" in
		SLACK_ARCH)									case ${DIALOG_REPLY} in
														i486|\
														i586|\
														i686|\
														x86_64)			true;;
														*)				DIALOG_REPLY=""
													esac;;
		RBUILDER_DATABASE_PATH|\
		RBUILDER_CONFIG_PATH|\
		RBUILDER_COPY_PATH|\
		RBUILDER_FILTER_PATH|\
		OPTION_MOVE_INSTALLED_PACKAGES_TO_PATH)		if [ ! -d "${DIALOG_REPLY}" ]; then
														DIALOG_REPLY="${option_value}"
													fi
													if [ x"${DIALOG_REPLY}" == x"" ]; then
														PROFILE_EDITOR_OPTION_REMOVE
													fi;;
		OPTION_MAX_COMPILE_JOBS)					if [ x"${OPTION_MAX_COMPILE_JOBS:-0}" != x"auto" ]; then
														# Make sure OPTION_MAX_COMPILE_JOBS is a number.
														let OPTION_MAX_COMPILE_JOBS=OPTION_MAX_COMPILE_JOBS
														if test ${OPTION_MAX_COMPILE_JOBS:-0} -gt ${MAX_COMPILE_JOBS}; then
															OPTION_MAX_COMPILE_JOBS=auto
														fi
														if test ${OPTION_MAX_COMPILE_JOBS:-0} -lt 1; then
															OPTION_MAX_COMPILE_JOBS=auto
														fi
													fi;;
	esac

	# Configuration changed?
	if [ x"${option_value}" != x"${DIALOG_REPLY}" ]; then

		# Change the option in profile script.
		PROFILE_EDITOR_OPTION_REMOVE
		echo "export ${RUNTIME_EDITOR_OPTION_NAME}=\"${DIALOG_REPLY}\"" >>${SYSTEM_CONFIG_PROFILE}

	fi

	# Configuration updated.
	return 0

}

# Edit profile options that require bolean operators.
PROFILE_EDITOR_SET_MENU_OPTION() {
	local option_value="$1"
	local option_settings=""
	local count_options=0

	# Create the dialog script.
	cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "${PROJECT_NAME} - PROFILE SETUP" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--default-item "${VALUE}" \\
		--menu "\n\\
${RUNTIME_EDITOR_HELP_TEXT}\n\
\nCurrent value for this option is:\n\\
  => ${RUNTIME_EDITOR_OPTION_NAME}=${option_value}\n\\
\nSelect one of the following options:" 20 75 @@@ \\
EOF

	# Add the possible values to the dialog script.
	count_options=0
	for option_settings in ${RUNTIME_EDITOR_OPTION_MODES}; do

		echo "\"${option_settings}\" \"Set ${RUNTIME_EDITOR_OPTION_NAME} to '${option_settings}'\" \\" >>${DIALOG_SCRIPT}
		let count_options=count_options+1

	done

	sed -i "s,@@@,${count_options}," ${DIALOG_SCRIPT}

	cat <<EOF >>${DIALOG_SCRIPT}
2> ${DIALOG_RETURN_VALUE}
EOF

	# Start the script and offer the dialog.
	. "${DIALOG_SCRIPT}"

	DIALOG_KEYCODE=$?
	DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")

	rm -f "${DIALOG_SCRIPT}"
	rm -f "${DIALOG_RETURN_VALUE}"

	# 'CANCEL' or 'ESC' ?
	if test ${DIALOG_KEYCODE} -ne 0; then
		PROFILE_EDITOR_OPTION_REMOVE
		return 0
	fi

	# Do some extra tests for some options.
	case "${RUNTIME_EDITOR_OPTION_NAME}" in
		OPTION_BUILD_MPLAYER_GUI_LANG)	if [ x"${DIALOG_REPLY}" == x"DEFAULT" ]; then
											DIALOG_REPLY=""
										fi;;
	esac


	# Configuration changed?
	if [ x"${option_value}" != x"${DIALOG_REPLY}" ]; then

		# Change the option in profile script.
		PROFILE_EDITOR_OPTION_REMOVE
		echo "export ${RUNTIME_EDITOR_OPTION_NAME}=${DIALOG_REPLY}" >>${SYSTEM_CONFIG_PROFILE}

	fi

	# Configuration updated.
	return 0
}

# Just a separator...
SYS____________________CONFIG_EDITOR() {
	true
}

# Job: Edit system configuration file.
CONFIG_EDITOR_MAIN_MENU() {
	local default_item="show"

	# Set some default settings.
	RUNTIME_EDITOR_CONFIG_UPDATED=false

	# This is the main menu loop.
	while [ true ]; do

		# Edit config dialog.
		dialog	--title "${PROJECT_NAME} - CONFIGURATION" \
				--backtitle "${PROJECT_BACKTITLE}" \
				--no-cancel \
				--default-item "${default_item}" \
				--menu "\n\
You can use this menu to edit the '$(basename ${SYSTEM_CONFIG_SETTINGS})' file which will help you to customize the ${PROJECT_NAME}.\n\n\
Some of those features can also be changed during ${PROJECT_NAME} setup. To edit this file is only recommended if you want to use ${PROJECT_NAME} to build multiple packages more then once.\n\n\
Select 'DONE' to return to the main menu.\n\n" 20 75 5 \
"LIST"       "Display the ${PROJECT_NAME} configuration file" \
"SHORT"      "Display a compressed ${PROJECT_NAME} configuration file" \
"EDIT"       "Edit ${PROJECT_NAME} configuration settings" \
"DONE"       "Back to main menu" 2> ${DIALOG_RETURN_VALUE}

		DIALOG_KEYCODE=$?
		DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")

		rm -f "${DIALOG_RETURN_VALUE}"

		# 'CANCEL' or 'ESC' ?
		if test ${DIALOG_KEYCODE} -ne 0; then
			return 0
		fi

		case ${DIALOG_REPLY} in
			DONE)	break;;
			LIST)	CONFIG_EDITOR_LIST_VALUES
					default_item="EDIT"
					continue;;
			SHORT)	CONFIG_EDITOR_SHORTLIST_VALUES
					default_item="EDIT"
					continue;;
			EDIT)	CONFIG_EDITOR_SELECT_OPTION
					default_item="DONE"
					continue;;
		esac

	done

	# Did we change some options?
	if ! ${RUNTIME_CLI_MODE}; then
		if ${RUNTIME_EDITOR_CONFIG_UPDATED}; then

			# Display a warning about system configuration file has changed.
			dialog	--title "${PROJECT_NAME} - INFORMATION" \
					--backtitle "${PROJECT_BACKTITLE}" \
					--cr-wrap \
					--yes-label "${BUTTON_LABEL_YES}" \
					--no-label "${BUTTON_LABEL_NO}" \
					--ok-label "${BUTTON_LABEL_YES}" \
					--cancel-label "${BUTTON_LABEL_NO}" \
					--yesno "\n${PROJECT_NAME} configuration file has been modified. Reload configuration?" 8 40

			DIALOG_KEYCODE=$?
			if test ${DIALOG_KEYCODE} -eq 0; then
				# Reload system configuration.
				CONFIGURE_LOAD_SYSTEM_SETTINGS || return 1
			fi

		fi
	fi

	return 0

}

# Display the system configuration file.
CONFIG_EDITOR_LIST_VALUES() {

	grep -v "##" ${SYSTEM_CONFIG_SETTINGS} >${DIALOG_TEMP_FILE1}
	dialog	--title "${PROJECT_NAME} - CONFIGURATION" \
			--backtitle "${PROJECT_BACKTITLE}" \
			--cr-wrap \
			--exit-label "${BUTTON_LABEL_OK}" \
			--textbox ${DIALOG_TEMP_FILE1} 20 75

	# Delete the temp datafile.
	rm -f ${DIALOG_TEMP_FILE1}

	return 0
}

# Display the short system configuration file.
CONFIG_EDITOR_SHORTLIST_VALUES() {

	grep -v "^#" ${SYSTEM_CONFIG_SETTINGS} | grep "=" >${DIALOG_TEMP_FILE1}
	dialog	--title "${PROJECT_NAME} - CONFIGURATION" \
			--backtitle "${PROJECT_BACKTITLE}" \
			--cr-wrap \
			--exit-label "${BUTTON_LABEL_OK}" \
			--textbox ${DIALOG_TEMP_FILE1} 20 75

	# Delete the temporary datafile.
	rm -f ${DIALOG_TEMP_FILE1}

	return 0
}

# Edit the system configuration file.
CONFIG_EDITOR_SELECT_OPTION() {
	local default_item=""
	local config_line=""
	local config_option_name=""
	local config_option_value=""
	local help_text1=""
	local help_text2=""

	# The main menu loop.
	while [ true ]; do

		# Get a list of all options included in system configuration file.
		grep -v "#" ${SYSTEM_CONFIG_SETTINGS} | grep "=" >${DIALOG_TEMP_FILE1}

		# Create the dialog script.
		cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "${PROJECT_NAME} - CONFIGURATION" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--ok-label "${BUTTON_LABEL_EDIT}" \\
		--cancel-label "${BUTTON_LABEL_DONE}" \\
		--default-item "${default_item}" \\
		--extra-button \\
		--extra-label "${BUTTON_LABEL_HELP}" \\
		--item-help \\
		--menu "\n\\
Here is a list with options included in the '$(basename ${SYSTEM_CONFIG_SETTINGS})' configuration file.\n\n\\
Move the cursor to one of the options below and select 'EDIT' to change the value. Select 'HELP' to get more info." 20 75 8 \\
EOF

		# For each option, get name and value.
		while read config_line; do

			config_option_name=${config_line%%=*}
			config_option_value=${config_line##*=}
			config_option_value=${config_option_value//\"/}

			echo "\"${config_option_name}\" \"${config_option_value}\" \"${config_option_name} >> ${config_option_value}\" \\" >>${DIALOG_SCRIPT}

		done <${DIALOG_TEMP_FILE1}

		cat <<EOF >>${DIALOG_SCRIPT}
2> ${DIALOG_RETURN_VALUE}
EOF

		# Start the script and offer the dialog.
		. "${DIALOG_SCRIPT}"

		DIALOG_KEYCODE=$?
		DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")

		rm -f "${DIALOG_SCRIPT}"
		rm -f "${DIALOG_RETURN_VALUE}"
		rm -f "${DIALOG_TEMP_FILE1}"

		if test ${DIALOG_KEYCODE} -eq 3; then

			# Get the option we have to display the helppage for.
			config_option_name=${DIALOG_REPLY// >> */}
			config_option_name=${config_option_name//* /}
			CONFIG_EDITOR_GET_OPTION_TYPE "${config_option_name}"

			# Create the help page.
			cat ${SYSTEM_CONFIG_SETTINGS} | sed -n "/LABEL:${RUNTIME_EDITOR_OPTION_LABEL}/,/LABEL:${RUNTIME_EDITOR_OPTION_LABEL}/p" | grep -v "##" >${DIALOG_TEMP_FILE1}
			config_option_value=$(cat ${DIALOG_TEMP_FILE1} | grep "^${RUNTIME_EDITOR_OPTION_NAME}=" | sed "s,.*=,,")
			config_option_value=${config_option_value//\"/}

			help_text1=$(cat ${DIALOG_TEMP_FILE1} | grep -v "^[[:alpha:]].*=" | sed "s,# ,,g" | sed "s,^#$,,g" | sed "s,$,\\\n,g")

			case ${RUNTIME_EDITOR_OPTION_TYPE} in
				"MENU")		help_text2="  => ${config_option_name}=${config_option_value}";;
				"INPUT")	case ${config_option_name} in
								MIRROR_*)	help_text2="  => ${config_option_name}\n     \"${config_option_value}\"";;
								*)			help_text2="  => ${config_option_name}=\"${config_option_value}\"";;
							esac;;
			esac

			# Display help.
			dialog	--title "${PROJECT_NAME} - INFORMATION" \
					--backtitle "${PROJECT_BACKTITLE}" \
					--cr-wrap \
					--msgbox "\n${help_text1}\n\nCurrent value for this option is:\n\n${help_text2}\n" 20 75

			# Back to menu loop.
			default_item="${config_option_name}"
			continue
		fi

		# 'Option selected?
		if test ${DIALOG_KEYCODE} -eq 0; then
			default_item="${DIALOG_REPLY}"
			CONFIG_EDITOR_GET_OPTION_TYPE "${DIALOG_REPLY}"
			case ${RUNTIME_EDITOR_OPTION_TYPE} in
				"MENU")   CONFIG_EDITOR_SET_MENU_OPTION		; continue;;
				"INPUT")  CONFIG_EDITOR_SET_INPUT_OPTION 	; continue;;
			esac
			continue
		fi

		# 'CANCEL' or ESC... exit editor.
		break

	done

	return 0
}

# For each system configuration file option define LABEL and option type.
CONFIG_EDITOR_GET_OPTION_TYPE() {

	RUNTIME_EDITOR_OPTION_NAME=$1
	case $1 in
		"OPTION_BUILDSYS_FIRSTTIME_USER")			RUNTIME_EDITOR_OPTION_LABEL=README;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_BUILD_GERMAN_PACKAGES")				RUNTIME_EDITOR_OPTION_LABEL=LOCALIZATION;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false ask";;
		"OPTION_BUILD_MPLAYER_GUI_LANG")			RUNTIME_EDITOR_OPTION_LABEL=MPLAYERGUI;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="DEFAULT bg cs de dk el en es fr hu it ja ko mk nb nl pl ro ru sk sv tr uk pt_BR zh_CN zh_TW";;
		"OPTION_BUILD_EXTERNAL_SOURCES")			RUNTIME_EDITOR_OPTION_LABEL=EXTERNAL;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_RBUILDER_CREATE")					RUNTIME_EDITOR_OPTION_LABEL=RBUILDER;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_RBUILDER_FLAGS")					RUNTIME_EDITOR_OPTION_LABEL=RBUILDER;
													RUNTIME_EDITOR_OPTION_TYPE="INPUT";
													RUNTIME_EDITOR_OPTION_MODES="";;
		"OPTION_RBUILDER_DATABASE_PATH")			RUNTIME_EDITOR_OPTION_LABEL=RBDBASEPATH;
													RUNTIME_EDITOR_OPTION_TYPE="INPUT";
													RUNTIME_EDITOR_OPTION_MODES="NOSPACE";;
		"OPTION_RBUILDER_CONFIG_PATH")				RUNTIME_EDITOR_OPTION_LABEL=RBCONFIGPATH;
													RUNTIME_EDITOR_OPTION_TYPE="INPUT";
													RUNTIME_EDITOR_OPTION_MODES="NOSPACE";;
		"OPTION_RBUILDER_COPY_PATH")				RUNTIME_EDITOR_OPTION_LABEL=RBCOPYPATH;
													RUNTIME_EDITOR_OPTION_TYPE="INPUT";
													RUNTIME_EDITOR_OPTION_MODES="NOSPACE";;
		"OPTION_RBUILDER_FILTER")					RUNTIME_EDITOR_OPTION_LABEL=RBFILTER;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_RBUILDER_FILTER_PATH")				RUNTIME_EDITOR_OPTION_LABEL=RBFPATH;
													RUNTIME_EDITOR_OPTION_TYPE="INPUT";
													RUNTIME_EDITOR_OPTION_MODES="NOSPACE";;
		"OPTION_PACKAGE_TAG")						RUNTIME_EDITOR_OPTION_LABEL=TAG;
													RUNTIME_EDITOR_OPTION_TYPE="INPUT";
													RUNTIME_EDITOR_OPTION_MODES="NOSPACE";;
		"OPTION_PACKAGE_EXT")						RUNTIME_EDITOR_OPTION_LABEL=EXTENSION;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="txz tgz tbz tlz";;
		"OPTION_USE_NTP")							RUNTIME_EDITOR_OPTION_LABEL=NTP;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_USE_NTP_SERVER")					RUNTIME_EDITOR_OPTION_LABEL=NTP;
													RUNTIME_EDITOR_OPTION_TYPE="INPUT";
													RUNTIME_EDITOR_OPTION_MODES="";;
		"OPTION_CREATE_PKGMD5")						RUNTIME_EDITOR_OPTION_LABEL=PKGMD5;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_CREATE_PKGTXT")						RUNTIME_EDITOR_OPTION_LABEL=PKGTXT;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_FIND_OPTIONAL_PACKAGES")			RUNTIME_EDITOR_OPTION_LABEL=OPTPKG;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_USE_INCLUDE_CONF_FILES")			RUNTIME_EDITOR_OPTION_LABEL=BTINCLUDE;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_USE_INCLUDE_CONF_FILES")			RUNTIME_EDITOR_OPTION_LABEL=BTINCLUDE;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_VERBOSE_MODE")						RUNTIME_EDITOR_OPTION_LABEL=VERBOSE;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_SET_MAX_COMPILE_JOBS")				RUNTIME_EDITOR_OPTION_LABEL=SELECTCC;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_MAX_COMPILE_JOBS")					RUNTIME_EDITOR_OPTION_LABEL=CPUCORES;
													RUNTIME_EDITOR_OPTION_TYPE="INPUT";
													RUNTIME_EDITOR_OPTION_MODES="";;
		"OPTION_MULTIPLE_CPU_RETRIES")				RUNTIME_EDITOR_OPTION_LABEL=RETRY;
													RUNTIME_EDITOR_OPTION_TYPE="INPUT";
													RUNTIME_EDITOR_OPTION_MODES="";;
		"OPTION_GCC_RETRIES")						RUNTIME_EDITOR_OPTION_LABEL=RETRY;
													RUNTIME_EDITOR_OPTION_TYPE="INPUT";
													RUNTIME_EDITOR_OPTION_MODES="";;
		"OPTION_REMOVE_ALL_PKG_BEFORE_INSTALL")		RUNTIME_EDITOR_OPTION_LABEL=REMOVE;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_AUTO_REMOVE_PACKAGES")				RUNTIME_EDITOR_OPTION_LABEL=AUTOREMOVE;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_REMOVE_EXTRA_PACKAGES")				RUNTIME_EDITOR_OPTION_LABEL=RMEXTRA;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_INCLUDE_BUILDFILES")				RUNTIME_EDITOR_OPTION_LABEL=BUILDFILES;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_STRIP_BINARIES")					RUNTIME_EDITOR_OPTION_LABEL=STRIPBINARIES;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_UPDATE_GTK_ICON_CACHE")				RUNTIME_EDITOR_OPTION_LABEL=ICONCACHE;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_DELETE_INSTALLED_SOURCES")			RUNTIME_EDITOR_OPTION_LABEL=DELETESOURCE;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_DEBUG_OVERWRITE_CHECK")				RUNTIME_EDITOR_OPTION_LABEL=DBOVRCHECK;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_DEBUG_OVERWRITE_HALT")				RUNTIME_EDITOR_OPTION_LABEL=DBOVRHALT;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_MOVE_INSTALLED_PACKAGES")			RUNTIME_EDITOR_OPTION_LABEL=BTCACHEMOVE;
													RUNTIME_EDITOR_OPTION_TYPE="MENU";
													RUNTIME_EDITOR_OPTION_MODES="true false";;
		"OPTION_MOVE_INSTALLED_PACKAGES_TO_PATH")	RUNTIME_EDITOR_OPTION_LABEL=BTCACHEPATH;
													RUNTIME_EDITOR_OPTION_TYPE="INPUT";
													RUNTIME_EDITOR_OPTION_MODES="NOSPACE";;
		"MIRROR_GNOME")								RUNTIME_EDITOR_OPTION_LABEL=DOWNLOADMIRRORS;
													RUNTIME_EDITOR_OPTION_TYPE="INPUT";
													RUNTIME_EDITOR_OPTION_MODES="NOSPACE";;
		"MIRROR_KDE")								RUNTIME_EDITOR_OPTION_LABEL=DOWNLOADMIRRORS;
													RUNTIME_EDITOR_OPTION_TYPE="INPUT";
													RUNTIME_EDITOR_OPTION_MODES="NOSPACE";;
		"MIRROR_XORG")								RUNTIME_EDITOR_OPTION_LABEL=DOWNLOADMIRRORS;
													RUNTIME_EDITOR_OPTION_TYPE="INPUT";
													RUNTIME_EDITOR_OPTION_MODES="NOSPACE";;
		"MIRROR_SOURCEFORGE")						RUNTIME_EDITOR_OPTION_LABEL=DOWNLOADMIRRORS;
													RUNTIME_EDITOR_OPTION_TYPE="INPUT";
													RUNTIME_EDITOR_OPTION_MODES="NOSPACE";;
		"MIRROR_GENTOO_DISTFILES")					RUNTIME_EDITOR_OPTION_LABEL=DOWNLOADMIRRORS;
													RUNTIME_EDITOR_OPTION_TYPE="INPUT";
													RUNTIME_EDITOR_OPTION_MODES="NOSPACE";;
	esac

}

# Edit options that require a text string.
CONFIG_EDITOR_SET_INPUT_OPTION() {
	local option_value=""
	local string_len=0

	# Get the comments for the selected option.
	cat ${SYSTEM_CONFIG_SETTINGS} | sed -n "/LABEL:${RUNTIME_EDITOR_OPTION_LABEL}/,/LABEL:${RUNTIME_EDITOR_OPTION_LABEL}/p" | grep -v "##" >${DIALOG_TEMP_FILE1}

	# Get the current value for the selected option.
	option_value=$(cat ${DIALOG_TEMP_FILE1} | grep "^${RUNTIME_EDITOR_OPTION_NAME}=" | sed "s,.*=,,")
	option_value=${option_value//\"/}

	# Create the dialog script.
	cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "${PROJECT_NAME} - CONFIGURATION" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--default-item "${option_value}" \\
		--inputbox "\n\\
EOF

	cat ${DIALOG_TEMP_FILE1} | grep -v "^[[:alpha:]].*=" | sed "s,# ,,g" | sed "s,^#$,,g" | sed "s,$,\\\n\\\\,g" >>${DIALOG_SCRIPT}

	let string_len=${#RUNTIME_EDITOR_OPTION_NAME}+${#option_value}
	if test ${string_len} -lt 62; then
		cat <<EOF >>${DIALOG_SCRIPT}
\nCurrent value for this option is:\n \\
  => ${RUNTIME_EDITOR_OPTION_NAME}=\"${option_value}\"\n \\
\nEnter the new value for this option:" 20 75 "${option_value}" \\
  2> ${DIALOG_RETURN_VALUE}
EOF
	else
		cat <<EOF >>${DIALOG_SCRIPT}
\nCurrent value for this option is:\n \\
  => ${RUNTIME_EDITOR_OPTION_NAME}=\n \\
     \"${option_value}\"\n \\
\nEnter the new value for this option:" 20 75 "${option_value}" \\
  2> ${DIALOG_RETURN_VALUE}
EOF
	fi

	# Start the script and offer the dialog.
	. "${DIALOG_SCRIPT}"

	DIALOG_KEYCODE=$?
	DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")

	rm -f "${DIALOG_SCRIPT}"
	rm -f "${DIALOG_RETURN_VALUE}"
	rm -f "${DIALOG_TEMP_FILE1}"

	# 'CANCEL' or 'ESC' ?
	if test ${DIALOG_KEYCODE} -eq 0; then

		# Does the value allow blanks in the string?
		if [ x"${RUNTIME_EDITOR_OPTION_MODES}" == x"NOSPACE" ]; then
			DIALOG_REPLY=${DIALOG_REPLY// /}
		fi

		# Do some extra tests for some options.
		case "${RUNTIME_EDITOR_OPTION_NAME}" in
			OPTION_MAX_COMPILE_JOBS)	if [ x"${DIALOG_REPLY}" != x"auto" ]; then
											# Make sure OPTION_MAX_COMPILE_JOBS is a number.
											let DIALOG_REPLY=DIALOG_REPLY
											if test ${DIALOG_REPLY:-0} -gt ${MAX_COMPILE_JOBS}; then
												DIALOG_REPLY=auto
											fi
											if test ${DIALOG_REPLY:-0} -lt 1; then
												DIALOG_REPLY=auto
											fi
										fi;;
	esac

		# Configuration changed?
		if [ x"${option_value}" != x"${DIALOG_REPLY}" ]; then

			# Change the option in the system configuration file.
			CONFIG_EDITOR_SAVE_INPUT_OPTION "${RUNTIME_EDITOR_OPTION_NAME}" "${DIALOG_REPLY}"
			RUNTIME_EDITOR_CONFIG_UPDATED=true

		fi

	fi

	# Configuration updated.
	return 0

}

# Save new input option to configuration file.
CONFIG_EDITOR_SAVE_INPUT_OPTION() {
	sed -i "s,^$1=.*,$1=\"$2\"," ${SYSTEM_CONFIG_SETTINGS}
	return 0
}

# Edit options that require bolean operators.
CONFIG_EDITOR_SET_MENU_OPTION() {
	local option_value=""
	local option_settings=""

	# Get the comments for the selected option.
	cat ${SYSTEM_CONFIG_SETTINGS} | sed -n "/LABEL:${RUNTIME_EDITOR_OPTION_LABEL}/,/LABEL:${RUNTIME_EDITOR_OPTION_LABEL}/p" | grep -v "##" >${DIALOG_TEMP_FILE1}

	# Get the current value for the selected option.
	option_value=$(cat ${DIALOG_TEMP_FILE1} | grep "^${RUNTIME_EDITOR_OPTION_NAME}=" | sed "s,.*=,,")

	# Create the dialog script.
	cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "${PROJECT_NAME} - CONFIGURATION" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--default-item "${option_value}" \\
		--menu "\n\\
EOF

	cat ${DIALOG_TEMP_FILE1} | grep -v "^${RUNTIME_EDITOR_OPTION_NAME}" | sed "s,# ,,g" | sed "s,$,\\\n\\\\,g" >>${DIALOG_SCRIPT}

	cat <<EOF >>${DIALOG_SCRIPT}
\nCurrent value for this option is:\n \\
  => ${RUNTIME_EDITOR_OPTION_NAME}=${option_value}\n \\
\nSelect one of the following options:" 20 75 3 \\
EOF

	# Add possible values to the dialog script.
	for option_settings in ${RUNTIME_EDITOR_OPTION_MODES}; do
		echo "\"${option_settings}\" \"Set ${RUNTIME_EDITOR_OPTION_NAME} to '${option_settings}'\" \\" >>${DIALOG_SCRIPT}
	done

	cat <<EOF >>${DIALOG_SCRIPT}
2> ${DIALOG_RETURN_VALUE}
EOF

	# Start the script and offer the dialog.
	. "${DIALOG_SCRIPT}"

	DIALOG_KEYCODE=$?
	DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")

	rm -f "${DIALOG_SCRIPT}"
	rm -f "${DIALOG_RETURN_VALUE}"
	rm -f "${DIALOG_TEMP_FILE1}"

	# 'CANCEL' or 'ESC' ?
	if test ${DIALOG_KEYCODE} -eq 0; then

		# Do some extra tests for some options.
		case "${RUNTIME_EDITOR_OPTION_NAME}" in
			OPTION_BUILD_MPLAYER_GUI_LANG)	if [ x"${DIALOG_REPLY}" == x"DEFAULT" ]; then
												DIALOG_REPLY=""
											fi;;
		esac

		# Configuration changed?
		if [ x"${option_value}" != x"${DIALOG_REPLY}" ]; then

			# Change the option in the system configuration file.
			CONFIG_EDITOR_SAVE_MENU_OPTION "${RUNTIME_EDITOR_OPTION_NAME}" "${DIALOG_REPLY}"
			RUNTIME_EDITOR_CONFIG_UPDATED=true

		fi
	fi

	# Configuration updated.
	return 0

}

# Save new menu option to configuration file.
CONFIG_EDITOR_SAVE_MENU_OPTION() {
	case $2 in
		true|false)	sed -i "s,^$1=.*,$1=$2," ${SYSTEM_CONFIG_SETTINGS};;
		*)			sed -i "s,^$1=.*,$1=\"$2\"," ${SYSTEM_CONFIG_SETTINGS};;
	esac
	return 0
}

# Just a separator...
SYS____________________SOURCE_BUILD() {
	true
}

# Let the user select the build-settings.
BUILDSYS_SELECT_BUILDOPTIONS() {

	local check=""
	local tmp_keycode=""
	local tmp_dialog_reply=""
	local default_pkgdir=""
	local moveto_pkgdir=""
	local default_pkgdir_input=""
	local moveto_pkgdir_input=""

	local select_stripbin="off"
	local select_includebt="off"
	local select_rbuilder="off"
	local select_rbfilter="off"
	local select_rbcopy="off"

	# Pre-Select BuildtoolSettings
	if ${OPTION_STRIP_BINARIES} 			; then select_stripbin="on"		; fi
	if ${OPTION_INCLUDE_BUILDFILES} 		; then select_includebt="on"	; fi
	if ${OPTION_RBUILDER_CREATE} 			; then select_rbuilder="on"		; fi
	if ${OPTION_RBUILDER_FILTER}; then
		if ${OPTION_RBUILDER_CREATE} 		; then select_rbfilter="on"		; fi
	fi
	if [ x"${OPTION_RBUILDER_COPY_PATH}" != x"" ]; then
		if ${OPTION_RBUILDER_CREATE}; then
			select_rbcopy="on"
		else
			select_rbcopy="off"
		fi
	fi

	local select_usentp="off"
	local select_pkgmd5="off"
	local select_pkgtxt="off"
	local select_autormpkg="off"
	local select_rmextra="off"
	local select_deletesrc="off"
	local select_external="off"
	local select_overwrite="off"
	local select_slkupdate="off"
	local select_optpkg="off"
	local select_iconcache="off"
	local select_multicpu="off"
	local select_cpucores="off"
	local select_move="off"
	local select_verbose="off"
	local select_remove_off=false

	# Pre-Select SystemBuildSettings
	if ${OPTION_USE_NTP}					; then	select_usentp="on"		; fi
	if ${OPTION_CREATE_PKGMD5}				; then	select_pkgmd5="on"		; fi
	if ${OPTION_CREATE_PKGTXT}				; then	select_pkgtxt="on"		; fi
	if ${OPTION_AUTO_REMOVE_PACKAGES}		; then	select_autormpkg="on"	; fi
	if ${OPTION_REMOVE_EXTRA_PACKAGES}		; then	select_rmextra="on"		; fi
	if ${OPTION_DELETE_INSTALLED_SOURCES}	; then	select_deletesrc="on"	; fi
	if ${OPTION_BUILD_EXTERNAL_SOURCES}		; then	select_external="on"	; fi
	if ${OPTION_DEBUG_OVERWRITE_CHECK}		; then	select_overwrite="on"	; fi
	if ${OPTION_FIND_OPTIONAL_PACKAGES}		; then	select_optpkg="on"		; fi
	if ${OPTION_UPDATE_GTK_ICON_CACHE}		; then	select_iconcache="on"	; fi
	if ${OPTION_VERBOSE_MODE}				; then	select_verbose="on"		; fi
	if ${OPTION_MULTIPLE_CPU_COMPILE}		; then	select_multicpu="on"	; fi
	if ${OPTION_SET_MAX_COMPILE_JOBS}		; then	select_cpucores="on"	; fi
	# Check directory for moving installed packages...
	if [ x"${OPTION_MOVE_INSTALLED_PACKAGES_TO_PATH}" != x"" ]; then
		select_move="false"
		if [ -d "${OPTION_MOVE_INSTALLED_PACKAGES_TO_PATH}" ]; then
			if ${OPTION_MOVE_INSTALLED_PACKAGES}	; then
				select_move="on"
			else
				select_move="off"
			fi
		fi
	else
		select_move="false"
	fi

	# Create the dialog.
	cat <<EOF >${DIALOG_SCRIPT}
	dialog	--title "CONFIGURE THE ${PROJECT_NAME}" \\
			--backtitle "${PROJECT_BACKTITLE}" \\
			--ok-label "${BUTTON_LABEL_OK}" \\
			--cancel-label "${BUTTON_LABEL_EXIT}" \\
			--help-button \\
			--help-label "${BUTTON_LABEL_INFO}" \\
			--cr-wrap \\
			--item-help \\
			--checklist "\
Please select the options you want to use for this build.\n\
\n\
Use the spacebar to select options, and the UP/DOWN arrow keys to scroll up and down through the entire list. \
Have a look to bottom of the screen for more details about the selected option." 20 75 9 \\
EOF

	# Some core build options...
	cat <<EOF >>${DIALOG_SCRIPT}
 "NTP"         "Use NTP-TimeServer to sync system time ($OPTION_USE_NTP_SERVER)"           "${select_usentp}"     "Set system time using NTP timeserver to calculate buildtime/ETA"                      \\
 "OPTPKG"      "Search for optional packages"                                              "${select_optpkg}"     "Search for optional packages (recommended)"                                           \\
 "EXTERNAL"    "Build packages based on external sources"                                  "${select_external}"   "Install packages based on external sources like Java JRE, JDK"                        \\
 "NODEBUG"     "Remove debug information from binary files"                                "${select_stripbin}"   "Strip down binaries and libraries (helpful for debugging applications only)"          \\
EOF

	# Do we have valid path to move installed packages?
	if [ x"${select_move}" != x"false" ]; then
	cat <<EOF >>${DIALOG_SCRIPT}
 "MOVEINST"    "Move installed packages to an external directory"                          "${select_move}"       "Move installed packages to external directory to save disk space on local drive"      \\
EOF
	fi

	# When upgrade/update/sync packages do not remove installed
	# packages or installing packages may fail because of missing
	# libraries of optional/required dependencies.
	case ${RUNTIME_SETUP_MODE} in
		UPDATE|UPGRADE|SYNC)	select_remove_off="true";;
		*)						select_remove_off="false";;
	esac

	if ! ${select_remove_off:-false}; then
		# Remove packages...
		cat <<EOF >>${DIALOG_SCRIPT}
 "REMOVE"      "Automatically remove already installed packages"                           "${select_autormpkg}"  "Automatically remove already installed packages without any warning"                  \\
 "RMEXTRA"     "Remove extra packages"                                                     "${select_rmextra}"    "Remove extra packages specified in 'remove.conf'"                                     \\
EOF
	fi

	# Verbose output..
	cat <<EOF >>${DIALOG_SCRIPT}
 "VERBOSE"     "Verbose output, print detailed package description"                        "${select_verbose}"    "Print detailed package description for installed packages"                            \\
EOF

	# Cross-compiling options...
	cat <<EOF >>${DIALOG_SCRIPT}
 "MULTICPU"    "Use multiple CPUs ($OPTION_MAX_COMPILE_JOBS) or computers to compile"      "${select_multicpu}"   "Using multiple CPUs or computers to compile (some packages may fail to build)"        \\
 "SELECTCC"    "Set max. count of compile jobs ($OPTION_MAX_COMPILE_JOBS)"                 "${select_cpucores}"   "Set max. count of compile jobs ($OPTION_MAX_COMPILE_JOBS)"                            \\
EOF

	# Some helpful debugging options...
	cat <<EOF >>${DIALOG_SCRIPT}
 "OVERWRITE"   "Check for files to be replaced from other packages"                        "${select_overwrite}"  "Check for files to be replaced from other packages"                                   \\
EOF

	# Free some disk space after package got installed...
	cat <<EOF >>${DIALOG_SCRIPT}
 "DELETESRC"   "Delete sources after package has been installed"                           "${select_deletesrc}"  "Delete sources after package has been installed to save some disk space"              \\
EOF

	# Create 'slack-required' files??
	cat <<EOF >>${DIALOG_SCRIPT}
 "RBUILDER"    "Create 'slack-required' files"                                             "${select_rbuilder}"   "Enable this option to create 'slack-required' files"                                  \\
 "RBFILTER"    "Filter 'slack-required' by package lists"                                  "${select_rbfilter}"   "Enable this option to filter 'slack-required' files by a package list."               \\
 "RBCOPY"      "Collect copies of 'slack-required' files"                                  "${select_rbcopy}"     "Enable this option to set a path to collect copies of slack-required files"           \\
EOF

	# Create optional files?
	cat <<EOF >>${DIALOG_SCRIPT}
 "ICONCACHE"   "Build/update the gtk icon cache"                                           "${select_iconcache}"  "Build/update the gtk icon cache to reduce disk access when searching for menu icons"  \\
 "BUILDTOOL"   "Include build files to the package"                                        "${select_includebt}"  "Include the buildscript source files to the package (to rebuild a package later)"     \\
 "PKGMD5"      "Create 'package.${SYSTEM_PACKAGE_EXTENSION}.md5' for all created packages" "${select_pkgmd5}"     "Create 'package.${SYSTEM_PACKAGE_EXTENSION}.md5' for all created packages"            \\
 "PKGTXT"      "Create 'package-version-arch.txt' files"                                   "${select_pkgtxt}"     "Create 'package-version-arch.txt' files that can be used for repositories"            \\
EOF

	cat <<EOF >>${DIALOG_SCRIPT}
 2> "${DIALOG_RETURN_VALUE}"
EOF

	while [ true ]; do
		# Select build options .
		. "${DIALOG_SCRIPT}"
		DIALOG_KEYCODE=$?

		# 'INFO' ?
		if test ${DIALOG_KEYCODE} -eq 2; then
			dialog	--title "BUILD OPTIONS" \
					--backtitle "${PROJECT_BACKTITLE}" \
					--cr-wrap \
					--msgbox "\
Here is a short description of the build options:\n\
\n\
NTP\n\
${RULER2}\n\
Set system time using NTP timeserver to calculate buildtime/ETA. \
Only usefull if you want to get correct buildtime for each package.\n\
\n\
OPTPKG\n\
${RULER2}\n\
Search for optional packages. This is recommended since optional packages may enable additional features. \
However, optional packages will not be enabled by default.\n\
\n\
ICONCACHE\n\
${RULER2}\n\
Build/update the gtk icon cache to reduce disk access when searching for menu icons.\n\
\n\
NODEBUG\n\
${RULER2}\n\
Strip down binaries and libraries to reduce filesize. \
Debug info is only helpful for debugging applications.\n\
\n\
MOVEINST\n\
${RULER2}\n\
Move installed packages to external directory to save disk space on local drive.\n\
\n\
BUILDFILES\n\
${RULER2}\n\
Include the buildtool source files to the package. \
Only useful if you want to rebuild the package later.\n\
\n\
VERBOSE\n\
${RULER2}\n\
Print package description for installed packages. \
If disabled only a short description will be printed. \n\
\n\
MULTICPU\n\
${RULER2}\n\
Using multiple core CPUs or computers to compile the package. \
WARNING! Some packages may fail to build! \
By default ${PROJECT_NAME} will retry building the package using only one CPU on a single computer.\n\
\n\
SELECTCC\n\
${RULER2}\n\
Set max. count of compile jobs to be computed at the same time. \
If set to 'auto' ${PROJECT_NAME} will use the count of available CPUs+3 as default.\n\
\n\
OVERWRITE\n\
${RULER2}\n\
Check for files to be replaced from other packages. Only useful if you are paranoid.\n\
\n\
MOVEINST\n\
${RULER2}\n\
Move installed packages to external directory to save disk space on local drive.\n\
\n\
DELETESRC\n\
${RULER2}\n\
Delete source packages after package has been installed to save some disk space.\n\
\n\
EXTERNAL\n\
${RULER2}\n\
Install packages based on external sources like Java JRE or JDK. \
These packages are normally not required to install most packages so this option should be disabled- \
If you enable this option make sure you have the required source packages.\n\
\n\
RBUILDER\n\
${RULER2}\n\
Enable this option to create 'slack-required' files. Supported externel dependency checking tools are \
sbbdep and requiredbuilder. If not installed ${PROJECT_NAME} will use its own tools. \n\
Only useful if you want to share your packages with other users via 'slapt-get' based repositories.\n\
\n\
RBFILTER\n\
${RULER2}\n\
Enable this option to filter 'slack-required' files by a package list. \
Only useful if you want to share your packages with other users via 'slapt-get' based repositories.\n\
\n\
RBCOPY\n\
${RULER2}\n\
Enable this option to specify a path to collect copies of 'slack-required' files from all. \
compiled packages. Useful if you want to check dependencies manually.\n\
\n\
REMOVE\n\
${RULER2}\n\
Automatically remove already installed packages without any warning.\n\
\n\
RMEXTRA\n\
${RULER2}\n\
Remove extra packages specified in 'remove.conf'.\n\
\n\
PKGMD5\n\
${RULER2}\n\
Create 'package.${SYSTEM_PACKAGE_EXTENSION}.md5' for all created packages\n\
\n\
PKGTXT\n\
${RULER2}\n\
Create 'package-version-arch.txt' files that can be used for repositories.
\n" 20 75
			continue
		fi
		break
	done

	DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
	rm -f "${DIALOG_SCRIPT}"
	rm -f "${DIALOG_RETURN_VALUE}"

	# 'CANCEL' or 'ESC' ?
	if test ${DIALOG_KEYCODE} -ne 0; then
		return 1
	fi




	# Create 'slack-required' files?
	check=$(echo "${DIALOG_REPLY}" | grep "RBUILDER")
	if [ x"${check}" == x"" ]; then
		OPTION_RBUILDER_CREATE=false
	else
		OPTION_RBUILDER_CREATE=true
	fi

	# Filter 'slack-required' files?
	check=$(echo "${DIALOG_REPLY}" | grep "RBFILTER")
	if [ x"${check}" == x"" ]; then
		OPTION_RBUILDER_FILTER=false
	else
		OPTION_RBUILDER_FILTER=true
	fi

	# Create copies of 'slack-required' files?
	if ${OPTION_RBUILDER_CREATE}; then
		check=$(echo "${DIALOG_REPLY}" | grep "RBCOPY")
		if [ x"${check}" != x"" ]; then
			while [ true ]; do
				dialog	--title "${PROJECT_NAME} - SET COPY PATH" \
						--backtitle "${PROJECT_BACKTITLE}" \
						--cr-wrap \
						--ok-label "${BUTTON_LABEL_OK}" \
						--cancel-label "${BUTTON_LABEL_SKIP}" \
						--inputbox "\n\
Specify the path where copies of the 'slack-required' files should\n\
be copied to (leave empty to use a temporary internal path):\n\
 \n\
" 12 75 "${OPTION_RBUILDER_COPY_PATH}" 2> ${DIALOG_RETURN_VALUE}

				tmp_keycode=$?
				tmp_dialog_reply=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
				rm -f "${DIALOG_RETURN_VALUE}"
				# 'CANCEL' or 'ESC' ?
				if test ${tmp_keycode} -eq 0; then
					if [ x"${tmp_dialog_reply}" == x"" ]; then
						tmp_dialog_reply="${SYSTEM_PATH_RBUILDER}"
					fi
					# Make sure path does exist.
					if test -d "${tmp_dialog_reply}"; then
						OPTION_RBUILDER_COPY_PATH="${tmp_dialog_reply}"
						break
					else
						# Display a infobox and warning.
						dialog	--title "${PROJECT_NAME} - WARNING" \
								--backtitle "${PROJECT_BACKTITLE}" \
								--cr-wrap \
								--yes-label "${BUTTON_LABEL_YES}" \
								--no-label "${BUTTON_LABEL_GOBACK}" \
								--ok-label "${BUTTON_LABEL_YES}" \
								--cancel-label "${BUTTON_LABEL_GOBACK}" \
								--defaultno \
								--yesno "\n\
Create a copy of all 'slack-required' files to the following path?\n\
\n\
${tmp_dialog_reply:0:70}\n\
${tmp_dialog_reply:70:70}\n\
\n\
Path does not exist, create it now? \n\
 " 12 75
						DIALOG_KEYCODE=$?

						# Continue?
						if test ${DIALOG_KEYCODE} -eq 0; then
							# Yes...
							OPTION_RBUILDER_COPY_PATH="${tmp_dialog_reply}"
							mkdir -p "${tmp_dialog_reply}" && break
						fi
						continue
					fi
				else
					OPTION_RBUILDER_COPY_PATH=""
					break
				fi
			done
		fi
	fi

	# Include build files files to package?
	check=$(echo "${DIALOG_REPLY}" | grep "BUILDTOOL")
	if [ x"${check}" != x""  ]; then
		OPTION_INCLUDE_BUILDFILES=true
	else
		OPTION_INCLUDE_BUILDFILES=false
	fi

	# Strip binaries and libraries?
	check=$(echo "${DIALOG_REPLY}" | grep "NODEBUG")
	if [ x"${check}" != x""  ]; then
		OPTION_STRIP_BINARIES=true
	else
		OPTION_STRIP_BINARIES=false
	fi

	# Verbose mode.
	check=$(echo "${DIALOG_REPLY}" | grep "VERBOSE")
	if [ x"${check}" != x""  ]; then
		OPTION_VERBOSE_MODE=true
	else
		OPTION_VERBOSE_MODE=false
	fi

	# Use multiple cpu cores/distcc.
	check=$(echo "${DIALOG_REPLY}" | grep "MULTICPU")
	if [ x"${check}" != x""  ]; then
		OPTION_MULTIPLE_CPU_COMPILE=true
	else
		OPTION_MULTIPLE_CPU_COMPILE=false
	fi

	# Set max. compile jobs.
	check=$(echo "${DIALOG_REPLY}" | grep "SELECTCC")
	if [ x"${check}" != x"" ]; then
		if [ x"${OPTION_MULTIPLE_CPU_COMPILE}" == x"true" ]; then
			while [ true ]; do
				dialog	--title "${PROJECT_NAME} - SELECT MAX THREADS" \
						--backtitle "${PROJECT_BACKTITLE}" \
						--cr-wrap \
						--ok-label "${BUTTON_LABEL_CONTINUE}" \
						--cancel-label "${BUTTON_LABEL_CANCEL}" \
						--inputbox "\n\
Enter maximum count of parallel compile jobs.\n\
This is only helpful on systems with more then one CPU.\n\
 \n\
" 12 75 "${OPTION_MAX_COMPILE_JOBS}" 2> ${DIALOG_RETURN_VALUE}

				tmp_keycode=$?
				tmp_dialog_reply=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
				rm -f "${DIALOG_RETURN_VALUE}"
				# 'CANCEL' or 'ESC' ?
				if test ${tmp_keycode} -eq 0; then
					if [ x"${tmp_dialog_reply:-auto}" == x"auto"  ]; then
						OPTION_MAX_COMPILE_JOBS="${tmp_dialog_reply}"
						OPTION_SET_MAX_COMPILE_JOBS=false
						break
					else
						# Make sure OPTION_MAX_COMPILE_JOBS is a number.
						let tmp_dialog_reply=tmp_dialog_reply
						if [ ${tmp_dialog_reply:-0} -lt 1  ]; then
							OPTION_MAX_COMPILE_JOBS="auto"
							continue
						fi
						if [ ${tmp_dialog_reply:-0} -gt ${MAX_COMPILE_JOBS} ]; then
							OPTION_MAX_COMPILE_JOBS="auto"
							continue
						fi
						OPTION_MAX_COMPILE_JOBS="${tmp_dialog_reply}"
						OPTION_SET_MAX_COMPILE_JOBS=true
						FUNC_PLEASE_WAIT
						break
					fi
				else
					OPTION_MULTIPLE_CPU_COMPILE=false
					OPTION_MAX_COMPILE_JOBS="1"
					OPTION_SET_MAX_COMPILE_JOBS=false
					FUNC_PLEASE_WAIT
					break
				fi
			done
		else
			OPTION_MAX_COMPILE_JOBS="1"
			OPTION_SET_MAX_COMPILE_JOBS=false
		fi
	fi

	# 'UseNTP' selected?
	check=$(echo "${DIALOG_REPLY}" | grep "NTP")
	if [ x"${check}" != x""  ]; then
		OPTION_USE_NTP=true
	else
		OPTION_USE_NTP=false
	fi

	# 'CreateMD5' selected?
	check=$(echo "${DIALOG_REPLY}" | grep "PKGMD5")
	if [ x"${check}" != x""  ]; then
		OPTION_CREATE_PKGMD5=true
	else
		OPTION_CREATE_PKGMD5=false
	fi

	# 'CreateTXT' selected?
	check=$(echo "${DIALOG_REPLY}" | grep "PKGTXT")
	if [ x"${check}" != x""  ]; then
		OPTION_CREATE_PKGTXT=true
	else
		OPTION_CREATE_PKGTXT=false
	fi

	# 'AutoRemove' selected?
	check=$(echo "${DIALOG_REPLY}" | grep "REMOVE")
	if [ x"${check}" != x""  ]; then
		OPTION_AUTO_REMOVE_PACKAGES=true
	else
		OPTION_AUTO_REMOVE_PACKAGES=false
	fi

	# 'RemoveExtra' selected?
	check=$(echo "${DIALOG_REPLY}" | grep "RMEXTRA")
	if [ x"${check}" != x""  ]; then
		OPTION_REMOVE_EXTRA_PACKAGES=true
	else
		OPTION_REMOVE_EXTRA_PACKAGES=false
	fi

	# Delete source packages after package has been installed?
	check=$(echo "${DIALOG_REPLY}" | grep "DELETESRC")
	if [ x"${check}" != x""  ]; then
		OPTION_DELETE_INSTALLED_SOURCES=true
	else
		OPTION_DELETE_INSTALLED_SOURCES=false
	fi

	# Build packages based on external sources.
	check=$(echo "${DIALOG_REPLY}" | grep "EXTERNAL")
	if [ x"${check}" != x""  ]; then
		OPTION_BUILD_EXTERNAL_SOURCES=true
	else
		OPTION_BUILD_EXTERNAL_SOURCES=false
	fi

	# Check for files to be replaced.
	check=$(echo "${DIALOG_REPLY}" | grep "OVERWRITE")
	if [ x"${check}" != x""  ]; then
		OPTION_DEBUG_OVERWRITE_CHECK=true
	else
		OPTION_DEBUG_OVERWRITE_CHECK=false
	fi

	# Search for optional packages?
	check=$(echo "${DIALOG_REPLY}" | grep "OPTPKG")
	if [ x"${check}" != x""  ]; then
		OPTION_FIND_OPTIONAL_PACKAGES=true
	else
		OPTION_FIND_OPTIONAL_PACKAGES=false
	fi

	# Build gtk icon cache?
	check=$(echo "${DIALOG_REPLY}" | grep "ICONCACHE")
	if [ x"${check}" != x""  ]; then
		OPTION_UPDATE_GTK_ICON_CACHE=true
	else
		OPTION_UPDATE_GTK_ICON_CACHE=false
	fi

	# Move installed packages to another directory?
	check=$(echo "${DIALOG_REPLY}" | grep "MOVEINST")
	if [ x"${check}" != x""  ]; then

		# Set the default target path.
		default_pkgdir="${OPTION_MOVE_INSTALLED_PACKAGES_TO_PATH}"
		default_pkgdir_input=${default_pkgdir}

		# Define package directory based on build mode.
		if [ x${RUNTIME_SYSTEM_USERMODE} == xtrue -o x${RUNTIME_SYSTEM_CUSTOM_CONFIG} == xtrue ]; then
			moveto_pkgdir="/${OS_PLATFORM_CODE}/${PROJECT_VERSION}-$(date +%s)-CUSTOM"
		else
			case ${RUNTIME_SETUP_MODE} in
				UPDATE|UPGRADE)		moveto_pkgdir="/${OS_PLATFORM_CODE}/${PROJECT_VERSION}-$(date +%s)-UPDATES";;
				*)					moveto_pkgdir="/${OS_PLATFORM_CODE}/${PROJECT_VERSION}-$(date +%s)";;
			esac
		fi
		moveto_pkgdir_input=${moveto_pkgdir}

		# Enter/Modify the target path.
		while [ true ]; do

			dialog	--title "${PROJECT_NAME} - SET TARGET PATH" \
					--backtitle "${PROJECT_BACKTITLE}" \
					--cr-wrap \
					--ok-label "${BUTTON_LABEL_CONTINUE}" \
					--cancel-label "${BUTTON_LABEL_SKIP}" \
					--extra-button \
					--extra-label "${BUTTON_LABEL_DEFAULT}" \
					--form "\
Move installed packages to some other directory like a NFS share to \
save disk space on the local drive. \n\
Please specify a root path and a package directory. The root path is \
taken from the system configuration file. The package directory is \
suggested by platform/build-date and should be changed for each build.\
 \n\
 \n\
The complete path will be: /root-path/package-directory/packages\n\
" 18 75 0 \
"Root path:" 1 2 \
	"${default_pkgdir_input}" 2 2 67 250 \
"Package directory for this build (leave empty for root-path only):" 4 2 \
	"${moveto_pkgdir_input}" 5 2 67 250 \
2> ${DIALOG_RETURN_VALUE}

			tmp_keycode=$?
			let count=0; while read line; do
				let count=count+1
				case ${count} in
					1)	default_pkgdir_input=$line;;
					2)	moveto_pkgdir_input=$line;;
					3)	break;;
				esac
			done < ${DIALOG_RETURN_VALUE}

			rm -f "${DIALOG_RETURN_VALUE}"
			# 'CANCEL' or 'ESC' ?
			if test ${tmp_keycode} -eq 0; then
				# Path empty?
				if [ x"${default_pkgdir_input:-x}" == x"x" ]; then
					# Yes, disable the function.
					OPTION_MOVE_INSTALLED_PACKAGES=false
					SYSTEM_PATH_PKGDIR=""
					break
				else
					# Try to create the directory.
					if $( mkdir -p "${default_pkgdir_input}${moveto_pkgdir_input}/packages" &>/dev/null ); then
						OPTION_MOVE_INSTALLED_PACKAGES=true
						SYSTEM_PATH_PKGDIR="${default_pkgdir_input}${moveto_pkgdir_input}/packages"
						OPTION_MOVE_INSTALLED_PACKAGES_TO_PATH="${default_pkgdir_input}"
						FUNC_PLEASE_WAIT
						break
					else
						# This should not happen...
						FUNC_MESSAGE_BOX "INFORMATION" "Failed to create directory!\n\nPlease enter a valid path!"
					fi
					continue
				fi
			else
				# Extra button?
				if test ${tmp_keycode} -eq 3; then
					# Reset path to default.
					default_pkgdir_input=${default_pkgdir}
					moveto_pkgdir_input=${moveto_pkgdir}
					continue
				else
					# 'CANCEL' or ESC...
					OPTION_MOVE_INSTALLED_PACKAGES=false
					SYSTEM_PATH_PKGDIR=""
					FUNC_PLEASE_WAIT
					break
				fi
			fi
		done

	else
		OPTION_MOVE_INSTALLED_PACKAGES=false
		SYSTEM_PATH_PKGDIR=""
	fi

	return 0
}

# Create entry for the group dialog.
BUILDSYS_SELECT_BUILDTOOL_GROUPS_CREATE_ENTRY() {
	local category=$1
	local group_selected=$2
	local check=""

	if [ x"${category#-}" != x"${category}" ]; then
		check=$(grep "^<SORTED:true>$" ${SYSTEM_PATH_BUILDTOOLS}/GROUPS)
		if [ x"${check}" == x"" ]; then
			cat <<EOF >>${DIALOG_SCRIPT}
"---" "-" "off" \\
EOF
		fi
	else
		if [ ! -e ${SYSTEM_PATH_BUILDTOOLS}/${category}/DESCRIPTION ]; then
			cat <<EOF

ERROR!
The category '${category}' is missing the 'DESCRIPTION' file.
Exiting now!

EOF
			SLEEPMODE 3
			exit 1
		else
			mode="off"
			for selected in ${group_selected}; do
				if [ x"${selected}" == x"${category}" ]; then
					mode="on"
					break
				fi
			done
			read category_desc <${SYSTEM_PATH_BUILDTOOLS}/${category}/DESCRIPTION
			cat <<EOF >>${DIALOG_SCRIPT}
"${category}" "${category_desc}" "${mode}" \\
EOF
		fi
	fi
}

# Select group of packages to be installed.
BUILDSYS_SELECT_BUILDTOOL_GROUPS() {
	local group=""
	local group_list=""
	local group_list_temp=""
	local category=""
	local main_category=""
	local blacklist=""
	local skip_category=false
	local category_desc=""
	local group_selected=""
	local mode="off"
	local selected=""
	local error=false
	local group_available=""
	local default_item=""
	local everything=false
	local found_group=false
	local count=0
	local check=""
	local buildmode_name=""
	local buildmode_desc=""
	local buildmode_groups=""
	local buildtool_mode=""

	# Create list of categories (desktops first).
	FUNC_PROGRESS_BOX "Analyzing buildtool categories..."

	# Check available groups.
	check=$(grep "^<SORTED:true>$" ${SYSTEM_PATH_BUILDTOOLS}/GROUPS)
	if [ x"${check}" == x"" ]; then
		group_list_temp=$(grep "." ${SYSTEM_PATH_BUILDTOOLS}/GROUPS | grep -v "^#" | grep -v "^<" | grep -v "^@" | sed "s,:.*,,g")
	else
		group_list_temp=$(grep "." ${SYSTEM_PATH_BUILDTOOLS}/GROUPS | grep -v "^#" | grep -v "^<" | grep -v "^@" | grep -v "^-" | sed "s,:.*,,g" | sort)
	fi
	for group in ${group_list_temp}; do
		# Add separator to group list.
		if [ x"${group#-}" != x"${group}" ]; then
			group_list="${group_list} ---"
			continue
		fi
		found_group=false
		# Current category already in the list?
		for check in ${group_list}; do
			if [ x"${group}" == x"${check}" ]; then
				found_group=true
				break
			fi
		done
		# Yes, continue...
		if ${found_group}; then continue; fi
		# Do we have a package in the packages.conf
		# from the current category?
		found_group=false
		count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
			let count=count+1
			if [ x"${group}" == x"${SYSDB_PACKAGE_GROUP[${count}]}" ]; then
				found_group=true
				break
			fi
		done
		# Yes, add current category to the list.
		if ${found_group}; then
			group_list="${group_list} ${group}"
		fi
	done

	# Create list of valid categories.
	# If the category is blacklisted, skip it...
	for category in ${group_list}; do

		if [ x"${category#-}" == x"${category}" ]; then
			skip_category=false
			for blacklist in ${SYSTEM_CATEGORY_BLACKLIST}; do
				if [ x"${category}" == x"${blacklist}" ]; then
					skip_category=true
					break
				fi
			done

			# Category blacklisted?
			if ${skip_category}; then
				continue
			fi
		fi

		group_available="${group_available} ${category}"

	done

	# Create the list of enabled categories.
	group_selected=""
	for category in ${group_available}; do
		if [ x"${category#-}" == x"${category}" ]; then
			check=$(grep "^${category}:ADD" ${SYSTEM_PATH_BUILDTOOLS}/GROUPS)
			if [ x"${check}" != x"" ]; then
				group_selected="${group_selected} ${category}"
			fi
		fi
	done

	# Select categories to install...
	while [ true ]; do

		# Normally we do not want to build everything included.
		BUILDSYS_INSTALL_EVERYTHING=false

		cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "PACKAGE SERIES SELECTION" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--cancel-label "${BUTTON_LABEL_EXIT}" \\
		--extra-button \
		--extra-label "${BUTTON_LABEL_HELP}" \
		--help-button \\
		--help-label "${BUTTON_LABEL_EVERYTHING}" \\
		--checklist "\
Now it is time to select the main categories of software you want to install on your system. \
Use the spacebar to select or unselect the software you wish to install. \
You can use the up and down arrows to see all the possible choices. \
Categories needed to install the KDE desktop have been preselected. \
Press ENTER key when you are finished." 20 75 9 \\
EOF

		# Create menu list.
		FUNC_PROGRESS_BOX "Analyzing buildtool categories..."

		# Do we have any GROUPS?
		check=$(grep "^@" ${SYSTEM_PATH_BUILDTOOLS}/GROUPS)
		if [ x"${check}" != x"" ]; then
			while read category; do
				if [ x"${category#@}" != x"${category}" ]; then
					buildmode_groups=${category#*:}			# Remove name
					buildmode_groups=${buildmode_groups#*:}	# Remove mode
					buildmode_groups=${buildmode_groups%:*}	# Remove description
					found=false
					# Enable buildmode if at least one category was found.
					# If config.options are used some categories may be
					# excluded (for example @PLASMA5 with no Qt4).
					# To check if one category was not found may not include
					# the buildmode to the menu.
					for selected in ${buildmode_groups//,/ }; do
						check=$(grep "^${selected}/" ${RUNTIME_PACKAGE_CONFIG_FILE})
						if [ x"${check}" != x"" ]; then
							found=true
						fi
					done
					if ${found}; then
						buildmode_name=${category%:*}		# Remove description
						buildmode_name=${buildmode_name%:*}	# Remove groups
						buildmode_mode=${buildmode_name#*:}	# Remove name
						buildmode_name=${buildmode_name%:*}	# Remove mode
						buildmode_desc=${category##*:}		# Remove anything but description
						if [ x"${group_selected}" != x"" ]; then
							if [ x"${group_selected//${buildmode_name}/}" != x"${group_selected}" ]; then
								mode="on"
							else
								mode="off"
							fi
						else
							case ${buildmode_mode} in
								ADD)	mode="on";;
								*)		mode="off";;
							esac
						fi
						cat <<EOF >>${DIALOG_SCRIPT}
"${buildmode_name}" "${buildmode_desc}" "${mode}" \\
EOF
					fi
				fi
			done < ${SYSTEM_PATH_BUILDTOOLS}/GROUPS
			cat <<EOF >>${DIALOG_SCRIPT}
"---" "-" "off" \\
EOF
		fi

		check=$(grep "^<SORTED:true>$" ${SYSTEM_PATH_BUILDTOOLS}/GROUPS)
		if [ x"${check}" == x"" ]; then
			for category in ${group_available}; do
				BUILDSYS_SELECT_BUILDTOOL_GROUPS_CREATE_ENTRY ${category} "${group_selected}"
			done
		else
			# KDE5 desktop categories.
			count=0
			for main_category in	k5frameworks k5port k5plasma \
									k5apps-core k5apps-base k5apps-a11y k5apps-admin \
									k5apps-dev k5apps-edu k5apps-games \
									k5apps-gfx k5apps-media k5apps-net k5apps-pim \
									k5apps-toys k5apps-utils; do
				for category in ${group_available}; do
					case ${category} in
						${main_category})	let count=count+1; \
											BUILDSYS_SELECT_BUILDTOOL_GROUPS_CREATE_ENTRY ${category} "${group_selected}";;
						*)					true;;
					esac
				done
			done
			if test ${count} -gt 0; then
				cat <<EOF >>${DIALOG_SCRIPT}
"---" "-" "off" \\
EOF
			fi

			# KDE desktop applications.
			count=0
			for main_category in kapps kapps1; do
				for category in ${group_available}; do
					case ${category} in
						${main_category})	let count=count+1; \
											BUILDSYS_SELECT_BUILDTOOL_GROUPS_CREATE_ENTRY ${category} "${group_selected}";;
						*)					true;;
					esac
				done
			done
			if test ${count} -gt 0; then
				cat <<EOF >>${DIALOG_SCRIPT}
"---" "-" "off" \\
EOF
			fi

			# KDE dependencies.
			count=0
			for main_category in k4deps k5deps k5deps-apps k5deps-kde4; do
				for category in ${group_available}; do
					case ${category} in
						${main_category})	let count=count+1; \
											BUILDSYS_SELECT_BUILDTOOL_GROUPS_CREATE_ENTRY ${category} "${group_selected}";;
						*)					true;;
					esac
				done
			done
			if test ${count} -gt 0; then
				cat <<EOF >>${DIALOG_SCRIPT}
"---" "-" "off" \\
EOF
			fi

			# XFCE desktop categories.
			count=0
			for category in ${group_available}; do
				case ${category} in
					xfce|\
					xfce1|\
					xfcedeps)		let count=count+1; \
								BUILDSYS_SELECT_BUILDTOOL_GROUPS_CREATE_ENTRY ${category} "${group_selected}";;
					*)			true;;
				esac
			done
			if test ${count} -gt 0; then
				cat <<EOF >>${DIALOG_SCRIPT}
"---" "-" "off" \\
EOF
			fi

			# Application categories.
			count=0
			for category in ${group_available}; do
				case ${category} in
					xap|\
					xap1|\
					xqt4|\
					xqt5|\
					xbin|\
					xbin32|\
					xbin64|\
					xgames)		let count=count+1; \
								BUILDSYS_SELECT_BUILDTOOL_GROUPS_CREATE_ENTRY ${category} "${group_selected}";;
					*)			true;;
				esac
			done
			if test ${count} -gt 0; then
				cat <<EOF >>${DIALOG_SCRIPT}
"---" "-" "off" \\
EOF
			fi

			# KDE dependencies.
			count=0
			for main_category in k4deps k5deps; do
				for category in ${group_available}; do
					case ${category} in
						${main_category})	let count=count+1; \
											BUILDSYS_SELECT_BUILDTOOL_GROUPS_CREATE_ENTRY ${category} "${group_selected}";;
						*)					true;;
					esac
				done
			done
			if test ${count} -gt 0; then
				cat <<EOF >>${DIALOG_SCRIPT}
"---" "-" "off" \\
EOF
			fi

			# Add remaing categories.
			for category in ${group_available}; do
				case ${category} in
					k4deps|\
					k5deps|\
					k5deps-apps|\
					k5deps-kde4|\
					k5frameworks|\
					k5port|\
					k5plasma|\
					k5apps-core|\
					k5apps-base|\
					k5apps-a11y|\
					k5apps-admin|\
					k5apps-dev|\
					k5apps-edu|\
					k5apps-games|\
					k5apps-gfx|\
					k5apps-media|\
					k5apps-net|\
					k5apps-pim|\
					k5apps-toys|\
					k5apps-utils|\
					kapps|\
					kapps1|\
					xorg|\
					xorg1|\
					xorg2|\
					xorgfonts|\
					xorgfonts1|\
					xorgdrv1|\
					xorgdrv2|\
					xfce|\
					xfce1|\
					xfcedeps|\
					xap|\
					xap1|\
					xqt4|\
					xqt5|\
					xbin|\
					xbin32|\
					xbin64|\
					xgames)		true;;
					*)			let count=count+1; \
								BUILDSYS_SELECT_BUILDTOOL_GROUPS_CREATE_ENTRY ${category} "${group_selected}";;
				esac
			done
			if test ${count} -gt 0; then
				cat <<EOF >>${DIALOG_SCRIPT}
"---" "-" "off" \\
EOF
			fi

			# Xorg server categories.
			#######################################################################################
			# Note: If groups are renamed we need to fix these functions:                         #
			#       -> BUILDSYS_CHECK_XORG_FIND_PACKAGES / BUILDSYS_CHECK_FIND_PACKAGES "xorgdrv" #
			#       -> FUNC_CHECK_GROUP_XORGDRV_SELECTED                                          #
			#######################################################################################
			count=0
			for category in ${group_available}; do
				case ${category} in
					xorg|\
					xorg1|\
					xorg2|\
					xorgfonts|\
					xorgfonts1|\
					xorgdrv1|\
					xorgdrv2)	BUILDSYS_SELECT_BUILDTOOL_GROUPS_CREATE_ENTRY ${category} "${group_selected}";;
					*)			true;;
				esac
			done
		fi

		cat <<EOF >>${DIALOG_SCRIPT}
 2> "${DIALOG_RETURN_VALUE}"
EOF

		# Start the script and offer the select categories dialog.
		. "${DIALOG_SCRIPT}"

		DIALOG_KEYCODE=$?
		group_selected=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g" | sed "s,---,,g")
		rm -f "${DIALOG_SCRIPT}"
		rm -f "${DIALOG_RETURN_VALUE}"

		# 'CANCEL' ?
		if test ${DIALOG_KEYCODE} -eq 1; then
			error=true
			break
		fi

		# 'ESC' ?
		if test ${DIALOG_KEYCODE} -eq 255; then
			error=true
			break
		fi

		# Check for Buildmodes like @PLASMA or @XFCE which
		# can be specified in the buildtools/GROUPS files to automatically
		# selected multiple categories needed to build all required
		# packages for the buildmode.
		group_list=""
		group_modes=""
		# Replace Buildmodes with groups needed to build the packages.
		for mode in ${group_selected}; do
			check=$(grep "^@${mode#@}:" ${SYSTEM_PATH_BUILDTOOLS}/GROUPS)
			if [ x"${check}" != x"" ]; then
				group_modes="${group_modes}${mode} "
				buildmode_groups=${check#*:}			# Remove name
				buildmode_groups=${buildmode_groups#*:}	# Remove mode
				buildmode_groups=${buildmode_groups%:*}	# Remove description
				for selected in ${buildmode_groups//,/ }; do
					found_group=false
						for category in ${group_list}; do
						if [ x"${category}" == x"${selected}" ]; then
							found_group=true
							break
						fi
					done
					if ! ${found_group}; then
						group_list="${group_list}${selected} "
					fi
				done
			else
				found_group=false
					for category in ${group_list}; do
					if [ x"${category}" == x"${mode}" ]; then
						found_group=true
						break
					fi
				done
				if ! ${found_group}; then
					group_list="${group_list}${mode} "
				fi
			fi
		done

		# 'HELP' ?
		if test ${DIALOG_KEYCODE} -eq 3; then
			FUNC_DISPLAY_PACKAGES_IN_GROUP "${group_list}"
			continue
		fi

		# 'EVERYTHING' ?
		if test ${DIALOG_KEYCODE} -eq 2; then
			group_selected="${group_available}"
			BUILDSYS_INSTALL_EVERYTHING=true
			DIALOG_KEYCODE=0
		else
			group_selected="${group_list}"
			# All categories selected?
			everything=true
			for selected in ${group_available}; do
				found_group=false
				for category in ${group_selected}; do
					if [ x"${category}" == x"${selected}" ]; then
						found_group=true
						break
					fi
				done
				if ! ${found_group}; then
					everything=false
					break
				fi
			done
			if ${everything}; then
				BUILDSYS_INSTALL_EVERYTHING=true
			fi
		fi

		# Anything selected?
		if [ x"${group_selected}" == x"" ]; then
			FUNC_MESSAGE_BOX "INFORMATION" "No categories selected!\n\nYou must select at least one group of buildtools to be installed!"
			# Reset to the default.
			group_selected="@PLASMA5"
			continue
		fi

		# Backup selected groups.
		BUILDSYS_INSTALL_GROUP=${group_selected}

		while [ true ]; do

			# Set the install method. It is recommended to build
			# and install all packages of selected groups.
			#default_item="base"
			default_item="full"

			# Ask for install method.
			cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "SELECT INSTALL MODE" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--ok-label "${BUTTON_LABEL_OK}" \\
		--cancel-label "${BUTTON_LABEL_GOBACK}" \\
		--default-item "${default_item}" \\
		--menu "\\
Now you must select the type of install method you want to use. \
If you have the drive space and the cpu power, the 'full' option is the best choice. \
The 'base' mode will preselect a few packages needed to build/install the software series you have selected. \
Otherwise you can pick packages from a menu using 'user' mode.\n
\n\n\n
EOF

			cat <<EOF >>${DIALOG_SCRIPT}
\n\
Which type of install mode you would like to use?" 20 75 4 \\
EOF

			cat <<EOF >>${DIALOG_SCRIPT}
 "base"     "Build base packages in selected groups only"                  \\
 "full"     "Build all packages included in selected groups (Recommended)" \\
 "user"     "Choose packages from selected groups to be installed"         \\
 2> "${DIALOG_RETURN_VALUE}"
EOF

			# Start the script and offer the version select dialog.
			. "${DIALOG_SCRIPT}"

			DIALOG_KEYCODE=$?
			BUILDSYS_INSTALL_MODE=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
			rm -f "${DIALOG_SCRIPT}"
			rm -f "${DIALOG_RETURN_VALUE}"

			# 'CANCEL' or 'ESC' ?
			if test ${DIALOG_KEYCODE} -ne 0; then
				BUILDSYS_INSTALL_MODE=""
				break
			fi

			# Do we want to build 'EVERYTHING'?
			if [ x"${BUILDSYS_INSTALL_MODE}" == x"base" ]; then
				# No...
				BUILDSYS_INSTALL_EVERYTHING=false
			fi

			# Manual package selection by user requested?
			if [ x"${BUILDSYS_INSTALL_MODE}" == x"user" ]; then

				FUNC_PROGRESS_BOX "Analyzing packages..."

				rm -f ${DIALOG_TEMP_FILE1}
				cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "SELECT PACKAGES TO BE INSTALLED" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--cancel-label "${BUTTON_LABEL_GOBACK}" \\
		--extra-button \
		--extra-label "${BUTTON_LABEL_EVERYTHING}" \
		--checklist "\
Choose the packages from the selected software series you want to be \
compiled/installed. This is for experienced users only!\n\
\n\
Use the spacebar to select or unselect a package, and the up and down \
arrows to scroll through the entire list.\n" 20 75 8 \\
EOF

				count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
					let count=count+1

					mode="off"
					for local_build_mode in ${BUILDSYS_INSTALL_GROUP}; do
						if [ x"${local_build_mode}" == x"${SYSDB_PACKAGE_GROUP[${count}]}" ]; then
							if [ x"$(grep ${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}:ADD ${SYSTEM_PATH_BUILDTOOLS}/${local_build_mode}/TAGFILE)" != x"" ]; then
								mode="on"
							fi

							cat <<EOF >>${DIALOG_TEMP_FILE1}
"${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}" "${SYSDB_PACKAGE_DESC[${count}]}" "${mode}" \\
EOF
							break
						fi
					done

				done

				cat ${DIALOG_TEMP_FILE1} | sort >>${DIALOG_SCRIPT}
				rm -f ${DIALOG_TEMP_FILE1}

				cat <<EOF >>${DIALOG_SCRIPT}
 2> "${DIALOG_RETURN_VALUE}"
EOF

				# Start the script and offer the package select dialog.
				. "${DIALOG_SCRIPT}"

				DIALOG_KEYCODE=$?
				DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
				rm -f "${DIALOG_SCRIPT}"
				rm -f "${DIALOG_RETURN_VALUE}"

				# 'CANCEL' ?
				if test ${DIALOG_KEYCODE} -eq 1; then
					continue
				fi

				# 'ESC' ?
				if test ${DIALOG_KEYCODE} -eq 255; then
					continue
				fi

				FUNC_PROGRESS_BOX "Analyzing packages..."

				# 'EVERYTHING' ?
				if test ${DIALOG_KEYCODE} -eq 3; then

					DIALOG_REPLY=""
					count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
						let count=count+1

						for local_build_mode in ${BUILDSYS_INSTALL_GROUP}; do
							if [ x"${local_build_mode}" == x"${SYSDB_PACKAGE_GROUP[${count}]}" ]; then
								DIALOG_REPLY="${DIALOG_REPLY} ${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}"
							fi
						done

					done

					DIALOG_KEYCODE=0

				fi

				# Remember the selected packages.
				BUILDSYS_INSTALL_USER="${DIALOG_REPLY}"
				break

			fi

			# Install mode selected.
			break

		done

		# Install mode selected?
		if [ x"${BUILDSYS_INSTALL_MODE}" == x"" ]; then
			# No, back to menu
			continue
		fi

		error=false
		break

	done

	case ${error} in
		true)	return 1;;
		*)		return 0;;
	esac

}

# Create the buildorder.
BUILDSYS_CREATE_BUILDORDER() {

	local count=0
	local name=""
	local add_reason=""
	local skip_reason=""
	local dep_check_passed=""
	local package_selected=""
	local package_selected_by_tag=""
	local package_selected_by_group=""
	local local_build_mode=""
	local local_tag=""
	local local_count_extras=0
	local package=""
	local package_installed=false
	local package_included=false
	local dependency=""
	local count_selected_by_extras=0
	local check=""

	SYSDB_PACKAGE_COUNT_ENEBALED=0

	# When in debug mode initialize logfile.
	if ${RUNTIME_OPTION_DEBUG_MODE}; then
		echo "Automatically selected packages:" >${SYSTEM_DEBUG_LOGFILE_BUILDORDER}
		echo "${RULER1}" >>${SYSTEM_DEBUG_LOGFILE_BUILDORDER}
	fi

	FUNC_PROGRESS_BOX "Creating package buildorder..."
	FUNC_INIT_PROGRESS_BAR

	let count=0
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

		# Update the progress bar.
		let count=count+1
		FUNC_DRAW_PROGRESS_BAR ${count} ${SYSDB_PACKAGE_COUNT}

		while [ true ]; do

			add_reason=""
			skip_reason=""
			dep_check_passed=true
			package_selected=false
			package_selected_by_tag=false

			# Does this package require external sources?
			if ${SYSDB_PACKAGE_EXTERNAL[${count}]}; then
				if ! ${OPTION_BUILD_EXTERNAL_SOURCES}; then
					skip_reason="(EXTERNAL SOURCES)"
					dep_check_passed=false
					# Do not list this package anymore...
					SYSDB_PACKAGE_DISABLED[${count}]=true
					break
				fi
			fi

			# Do we want to build 'EVERYTHING' ?
			for local_build_mode in ${BUILDSYS_INSTALL_GROUP}; do
				if [ x"${local_build_mode}" == x"${SYSDB_PACKAGE_GROUP[${count}]}" ]; then
					case ${BUILDSYS_INSTALL_MODE} in
						full)		package_selected_by_tag=true
									add_reason="(GROUPSELECTED : ${local_build_mode})"
									;;
						base)		if [ x"$(grep ${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}:ADD ${SYSTEM_PATH_BUILDTOOLS}/${local_build_mode}/TAGFILE)" != x"" ]; then
										package_selected_by_tag=true
										add_reason="(GROUP=TRUE    : ${local_build_mode})"
									fi
									;;
						user)		for package in ${BUILDSYS_INSTALL_USER}; do
										if [ x"${package}" == x"${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}" ]; then
											package_selected_by_tag=true
											add_reason="(SELECTED BY USER)"
											break
										fi
									done
									;;
					esac
					break
				else
					# Include package for the selected group?
					if test -e "${SYSTEM_PATH_GROUP_EXTRAS_CACHE}/${local_build_mode}"; then
						check=$(grep "^[ ]*+${SYSDB_PACKAGE_NAME[${count}]}$" "${SYSTEM_PATH_GROUP_EXTRAS_CACHE}/${local_build_mode}")
						if [ x"${check}" != x"" ]; then
							let count_selected_by_extras=count_selected_by_extras+1
							SYSDB_PACKAGE_EXTRA_FOR_GROUPS[${count}]="${SYSDB_PACKAGE_EXTRA_FOR_GROUPS[${count}]} ${local_build_mode}"
							#package_selected_by_tag=true
							#add_reason="(INCLUDED FOR GROUP: ${local_build_mode})"
						fi
					fi
				fi
			done

			while [ true ]; do

				if [ x"${package_selected_by_tag}" == x"false" ]; then
					skip_reason="(GROUP=FALSE   : ${SYSDB_PACKAGE_GROUP[${count}]})"
					dep_check_passed=false
					break
				fi

				# Just to be sure...
				if [ x"${skip_reason}" != x"" ]; then
					break
				fi

				break

			done

			break

		done

		# Enable/Disable the package
		if ${dep_check_passed}; then
			SYSDB_PACKAGE_ENABLED[${count}]=true
			SYSDB_PACKAGE_AUTOSELECT[${count}]=true
			let SYSDB_PACKAGE_COUNT_ENEBALED=SYSDB_PACKAGE_COUNT_ENEBALED+1
			if ${RUNTIME_OPTION_DEBUG_MODE}; then
				name="${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}${SPACEBAR}"
				echo "OK....${name:0:34} ${add_reason}"  >>${SYSTEM_DEBUG_LOGFILE_BUILDORDER}
			fi
		else
			SYSDB_PACKAGE_ENABLED[${count}]=false
			SYSDB_PACKAGE_AUTOSELECT[${count}]=false
			if ${RUNTIME_OPTION_DEBUG_MODE}; then
				name="${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}${SPACEBAR}"
				echo "Skip..${name:0:34} ${skip_reason}"  >>${SYSTEM_DEBUG_LOGFILE_BUILDORDER}
			fi
		fi

	done

	# Clear the progress bar.
	FUNC_CLEAR_PROGRESS_BAR




	# In case we found some packages recommended for groups display
	# a menu to select/deselect extra packages.
	if [ ${count_selected_by_extras:0} -gt 0 ]; then

		let count=0
		while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
			if [ x"${SYSDB_PACKAGE_EXTRA_FOR_GROUPS[${count}]}" != x"" ]; then
				if ! ${SYSDB_PACKAGE_ENABLED[${count}]:-true}; then
					let local_count_extras=local_count_extras+1
				fi
			fi
			let count=count+1
		done

		if [ ${local_count_extras} -gt 0 ]; then
			BUILDSYS_MODIFY_BUILDORDER_EDIT_LIST "EXTRAS" "${BUTTON_LABEL_CANCEL}" || FUNC_EXIT_BY_USER

			if ${RUNTIME_OPTION_DEBUG_MODE}; then
				if ${RUNTIME_SCRIPT_MODE:-false}; then
					echo "${RULER1}" >>${SYSTEM_DEBUG_LOGFILE_BUILDORDER}
					echo "Packages added by 'EXTRAS' files:" >>${SYSTEM_DEBUG_LOGFILE_BUILDORDER}
					echo "${RULER1}" >>${SYSTEM_DEBUG_LOGFILE_BUILDORDER}

					FUNC_PROGRESS_BOX "Updating package list..."
					FUNC_INIT_PROGRESS_BAR

					let count=0
					while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
						# Update the progress bar.
						let count=count+1
						FUNC_DRAW_PROGRESS_BAR ${count} ${SYSDB_PACKAGE_COUNT}
						if [ x"${SYSDB_PACKAGE_EXTRA_FOR_GROUPS[${count}]}" != x"" ]; then
							if ${SYSDB_PACKAGE_ENABLED[${count}]:-true}; then
								check=$(grep "Skip\.\.${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]} " ${SYSTEM_DEBUG_LOGFILE_BUILDORDER})
								if [ x"${check}" != x"" ]; then
									name="${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}${SPACEBAR}"
									#reason="OK....${name:0:34} (GROUP=EXTRA   : ${SYSDB_PACKAGE_EXTRA_FOR_GROUPS[${count}]## })"
									#sed -i "s,.*\.${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]} .*,${reason}," ${SYSTEM_DEBUG_LOGFILE_BUILDORDER}
									echo "OK....${name:0:34} (GROUP=EXTRA   : ${SYSDB_PACKAGE_EXTRA_FOR_GROUPS[${count}]## })" >>${SYSTEM_DEBUG_LOGFILE_BUILDORDER}
								fi
							fi
						fi
					done
				fi
			fi

			# Clear the progress bar.
			FUNC_CLEAR_PROGRESS_BAR

			# Search for recommended packages.
			# These packages can be specified in the .config file
			# using ADDPKG:name. These packages will automatically
			# be enabled during creation of the buildorder.
			if ${RUNTIME_OPTION_DEBUG_MODE}; then
				echo "${RULER1}" >>${SYSTEM_DEBUG_LOGFILE_BUILDORDER}
				echo "Automatically added packages:" >>${SYSTEM_DEBUG_LOGFILE_BUILDORDER}
				echo "${RULER1}" >>${SYSTEM_DEBUG_LOGFILE_BUILDORDER}
			fi

			FUNC_PROGRESS_BOX "Adding recommended packages..."

			while [ true ]; do
				FUNC_INIT_PROGRESS_BAR

				let count=0
				package_selected=false

				while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

					# Update the progress bar.
					let count=count+1
					FUNC_DRAW_PROGRESS_BAR ${count} ${SYSDB_PACKAGE_COUNT}

					# Package enabled?
					if ${SYSDB_PACKAGE_ENABLED[${count}]:-true}; then
						# Check for extra packages to be selected.
						for dependency in ${SYSDB_PACKAGE_ADDPKG[${count}]}; do
							# Search package be name.
							if FUNC_FIND_PACKAGE_BY_NAME ${dependency} ${SYSDB_PACKAGE_NAME[${count}]}; then
								# Package found, enabled?
								if ! ${SYSDB_PACKAGE_ENABLED[${RETURN_VALUE}]}; then
									# No, autoselect package.
									add_reason="(ADDED BY ${SYSDB_PACKAGE_NAME[${count}]})"
									name="${SYSDB_PACKAGE_ORIGINAL_NAME[${RETURN_VALUE}]}${SPACEBAR}"
									if ${RUNTIME_OPTION_DEBUG_MODE}; then
										echo "OK....${name:0:34} ${add_reason}" >>${SYSTEM_DEBUG_LOGFILE_BUILDORDER}
										if [ x"${SYSDB_PACKAGE_INSTALLED[${RETURN_VALUE}]}" != x"" ]; then
											for package in ${SYSDB_PACKAGE_INSTALLED[${RETURN_VALUE}]}; do
												echo "                                    < ${package}" >>${SYSTEM_DEBUG_LOGFILE_BUILDORDER}
											done
											name="${SYSDB_PACKAGE_NAME[${RETURN_VALUE}]}"
											name="${name}-${SYSDB_PACKAGE_VERSION[${RETURN_VALUE}]}${SYSDB_PACKAGE_RELEASE[${RETURN_VALUE}]}"
											name="${name}-${SYSDB_PACKAGE_ARCH[${RETURN_VALUE}]}"
											name="${name}-${SYSDB_PACKAGE_BUILD[${RETURN_VALUE}]}"
											name="${name}${PACKAGE_TAG}"
											echo "                                    > ${name}" >>${SYSTEM_DEBUG_LOGFILE_BUILDORDER}
										fi
									fi
									SYSDB_PACKAGE_AUTOSELECT[${RETURN_VALUE}]=true
									SYSDB_PACKAGE_ENABLED[${RETURN_VALUE}]=true
									package_selected=true
								fi
							fi
						done
					fi
				done

				# Any packages enabled?
				if ${package_selected:-false}; then
					# Yes, check again for packages to
					# be enabled automatically.
					continue
				fi
				# No new packages found, all done.
				break

			done

		fi

	fi




	# Display debug info.
	if ${RUNTIME_OPTION_DEBUG_MODE}; then
		dialog	--title "${PROJECT_NAME} - DEBUGGING INFORMATION" \
				--backtitle "${PROJECT_BACKTITLE}" \
				--cr-wrap \
				--exit-label "${BUTTON_LABEL_OK}" \
				--textbox ${SYSTEM_DEBUG_LOGFILE_BUILDORDER} 20 75
	fi




	# Write the automatically selected packages to logfile.
	FUNC_PROGRESS_BOX "Add package list to logfile..."

	echo "Automatically selected packages:" >${SYSTEM_LOGFILE_AUTOSELECT}
	echo "${RULER1}" >>${SYSTEM_LOGFILE_AUTOSELECT}

	count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
		let count=count+1
		if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
			echo " => ${SYSDB_PACKAGE_GROUP[${count}]}/${SYSDB_PACKAGE_NAME[${count}]}-${SYSDB_PACKAGE_VERSION[${count}]}${SYSDB_PACKAGE_RELEASE[${count}]}  [${count}]" >>${SYSTEM_LOGFILE_AUTOSELECT}
		fi
	done

	FUNC_PLEASE_WAIT

	# No error.
	return 0

}

# Check for required and optional packages,
BUILDSYS_CREATE_BUILDORDER_CHECK_DEPOPTS_WARNING() {
	dialog	--title "${PROJECT_NAME} - EXTRA PACKAGES" \
			--backtitle "${PROJECT_BACKTITLE}" \
			--cr-wrap \
			--msgbox "\n\
${PROJECT_NAME} will now search for optional and required packages. \
If any extra packages were found then you will get asked to add or to remove packages to the build order.\n\
\n\
        DOING THAT IS NOT RECOMMENDED!\n\
\n\
If you are unsure then just hit <RETURN> when you get asked for optional or required packages.\n\
" 14 75
}

# Check if all packages are enabled...
BUILDSYS_CHECK_ALL_PACKAGES_ENABLED() {
	local count=0
	local enabled=0

	# Count enabled packages...
	count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
		let count=count+1
		# Package enabled?
		if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
			let enabled=enabled+1
		else
			# If the package is disabled like the intel drivers count them anyway...
			if ${SYSDB_PACKAGE_DISABLED[${count}]}; then
				let enabled=enabled+1
			else
				# Was this package removed by the user?
				if ${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}; then
					let enabled=enabled+1
				fi
			fi
			
		fi
	done

	# All packages enabled?
	if [ ${count:0} -eq ${enabled:0} ]; then
		# Yes, no changes, exit...
		return 0
	else
		return 1
	fi
}

# Check for required and optional packages,
BUILDSYS_CREATE_BUILDORDER_RESET_DEPOPTS() {
	local count=0

	# Reset everything to the defaults.
	count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
		let count=count+1
		SYSDB_PACKAGE_OPTIONAL_FOR_PACKAGE[${count}]=""
		SYSDB_PACKAGE_REBUILD_FOR_PACKAGE[${count}]=""
		SYSDB_PACKAGE_REQUIRED_FOR_PACKAGE[${count}]=""
		SYSDB_PACKAGE_DEPOPT_CHECKED[${count}]=false
	done
}

# Create entry for debug logfile for missing packages.
# $1 package name
# $2 Debug text #2 'Not enabled'
# $3 Debug text #3 'Missing'
# $4 Package info
# $5 count
BUILDSYS_DEBUG_CHECK_DEPOPTS_ENTRY() {
	local installed_package=""
	local package_info=0
	if FUNC_FIND_PACKAGE_BY_NAME $1 ${SYSDB_PACKAGE_NAME[$5]}; then
		# Package found in configuration file.
		# Is package enabled?
		if ! ${SYSDB_PACKAGE_ENABLED[${RETURN_VALUE}]}; then
			# Package not enabled.
			if $4; then
				echo "Error!" >>"${SYSTEM_DEBUG_LOGFILE_DEPOPT_MISSING}"
				package_info=1
			fi
			installed_package=""
			if [ x"${SYSDB_PACKAGE_INSTALLED[${RETURN_VALUE}]}" != x"" ]; then
				installed_package="${SYSDB_PACKAGE_INSTALLED[${RETURN_VALUE}]// */}"
			fi
			echo "$2 $1" >>"${SYSTEM_DEBUG_LOGFILE_DEPOPT_MISSING}"
			if [ x"${installed_package}" != x"" ]; then
				echo "      Installed   -> ${installed_package}" >>"${SYSTEM_DEBUG_LOGFILE_DEPOPT_MISSING}"
			fi
		fi
	else
		# Package not found in configuration file.
		# Happens for example when using INCLUDE.conf files.
		if $4; then
			echo "Error!" >>"${SYSTEM_DEBUG_LOGFILE_DEPOPT_MISSING}"
			package_info=1
		fi
		installed_package=""
		if FUNC_CHECK_PACKAGE_INSTALLED $1; then
			installed_package="${RETURN_VALUE// */}"
		fi
		echo "$3 $1" >>"${SYSTEM_DEBUG_LOGFILE_DEPOPT_MISSING}"
		if [ x"${installed_package}" != x"" ]; then
			echo "      Installed   -> ${installed_package}" >>"${SYSTEM_DEBUG_LOGFILE_DEPOPT_MISSING}"
		fi
	fi
	return ${package_info}
}

# Create debug logfile for missing deps, opts and rebuild packages.
BUILDSYS_DEBUG_CHECK_DEPOPTS() {
	local count=0
	local package=""
	local package_ok=true

	# Delete existing debug file.
	rm -f "${SYSTEM_DEBUG_LOGFILE_DEPOPT_MISSING}"
	echo "Missing packages: " >"${SYSTEM_DEBUG_LOGFILE_DEPOPT_MISSING}"
	echo "${RULER1}" >>"${SYSTEM_DEBUG_LOGFILE_DEPOPT_MISSING}"

	# Check for dependencies and optional packages... This is the 4th generation of the code.
	FUNC_PROGRESS_BOX "Checking for missing packages..."
	FUNC_INIT_PROGRESS_BAR

	count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do

		# Draw the progress bar.
		let count=count+1
		FUNC_DRAW_PROGRESS_BAR ${count} ${SYSDB_PACKAGE_COUNT}

		if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
			echo -n "Checking ${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}... " >>"${SYSTEM_DEBUG_LOGFILE_DEPOPT_MISSING}"
			package_ok=true
			# Check for missing packages that should be rebuild.
			for package in ${SYSDB_PACKAGE_REBUILD[${count}]}; do
				BUILDSYS_DEBUG_CHECK_DEPOPTS_ENTRY \
					${package} \
					"  REB/Not enabled ->" \
					"  REB/missing     ->" \
					${package_ok} \
					${count}
				if test $? -eq 1; then
					package_ok=false
				fi
			done
			# Check for missing automatically added packages.
			for package in ${SYSDB_PACKAGE_ADDPKG[${count}]}; do
				BUILDSYS_DEBUG_CHECK_DEPOPTS_ENTRY \
					${package} \
					"  ADD/Not enabled ->" \
					"  ADD/missing     ->" \
					${package_ok} \
					${count}
				if test $? -eq 1; then
					package_ok=false
				fi
			done
			# Check for missing required packages.
			for package in ${SYSDB_PACKAGE_REQUIRED[${count}]}; do
				BUILDSYS_DEBUG_CHECK_DEPOPTS_ENTRY \
					${package} \
					"  REQ/Not enabled ->" \
					"  REQ/missing     ->" \
					${package_ok} \
					${count}
				if test $? -eq 1; then
					package_ok=false
				fi
			done
			# Check for missing optional packages.
			for package in ${SYSDB_PACKAGE_OPTIONAL[${count}]}; do
				BUILDSYS_DEBUG_CHECK_DEPOPTS_ENTRY \
					${package} \
					"  OPT/Not enabled ->" \
					"  OPT/missing     ->" \
					${package_ok} \
					${count}
				if test $? -eq 1; then
					package_ok=false
				fi
			done
			if ${package_ok:-false}; then
				echo "OK!" >>"${SYSTEM_DEBUG_LOGFILE_DEPOPT_MISSING}"
			fi
		fi

	done

	# Clear the progress bar.
	FUNC_CLEAR_PROGRESS_BAR

	# Display debug info.
	dialog	--title "${PROJECT_NAME} - DEBUGGING INFORMATION" \
			--backtitle "${PROJECT_BACKTITLE}" \
			--cr-wrap \
			--exit-label "${BUTTON_LABEL_OK}" \
			--textbox "${SYSTEM_DEBUG_LOGFILE_DEPOPT_MISSING}" 20 75
}

# Check for required and optional packages,
BUILDSYS_CREATE_BUILDORDER_CHECK_DEPOPTS() {
	local count=0
	local depopt_count=0
	local package=0
	local optional=""
	local required=""
	local rebuild=""
	local entries=0
	local mode=""
	local entry_found=false
	local selected=false
	local selected_package_list=""
	local changes=false
	local package_text=""
	local pre_selected_packages=""
	local extra_packages=""
	local newoptpkg=false
	local newreqpkg=false
	local checknewopt=""
	local checknewreq=""
	local checkorigname=""
	local debug_text_opt=""
	local debug_text_req=""

	# Reset everything to the defaults.
	BUILDSYS_CREATE_BUILDORDER_RESET_DEPOPTS

	# Search for extra packages.
	while [ true ]; do

		# When in debug mode initialize logfile.
		if ${RUNTIME_OPTION_DEBUG_MODE}; then
			rm -f ${SYSTEM_DEBUG_LOGFILE_DEPOPT_INFO}
		fi

		# Check for dependencies and optional packages... This is the 4th generation of the code.
		FUNC_PROGRESS_BOX "Checking for extra packages..."
		FUNC_INIT_PROGRESS_BAR

		count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do

			# Draw the progress bar.
			let count=count+1
			FUNC_DRAW_PROGRESS_BAR ${count} ${SYSDB_PACKAGE_COUNT}

			# Package not selected for menu.
			SYSDB_PACKAGE_RUNTIME_DEPOPT_STATUS[${count}]=""

			# Was this package already removed by the user?
			if ${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}; then
				continue
			fi

			if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
				if ! ${SYSDB_PACKAGE_DEPOPT_CHECKED[${count}]}; then
					# Any packages recommended for rebuild?
					for rebuild in ${SYSDB_PACKAGE_REBUILD[${count}]}; do
						if FUNC_FIND_PACKAGE_BY_NAME ${rebuild} ${SYSDB_PACKAGE_NAME[${count}]}; then
							# Do we have this package already in the list?
							FUNC_CHECK_DUPLICATES ${SYSDB_PACKAGE_REBUILD_FOR_PACKAGE[${RETURN_VALUE}]} ${count}
							if [ $? -eq 0 ]; then
								# Add current package as an recommended package for rebuild.
								SYSDB_PACKAGE_REBUILD_FOR_PACKAGE[${RETURN_VALUE}]="${SYSDB_PACKAGE_REBUILD_FOR_PACKAGE[${RETURN_VALUE}]} ${count}"
								let depopt_count=depopt_count+1
							fi
						else
							echo "${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}: Missing REB ${rebuild}" >>${SYSTEM_DEBUG_LOGFILE_DEPOPT_CHECK}
						fi
					done
					# Any required packages?
					for required in ${SYSDB_PACKAGE_REQUIRED[${count}]}; do
						if FUNC_FIND_PACKAGE_BY_NAME ${required} ${SYSDB_PACKAGE_NAME[${count}]}; then
							# Do we have this package already in the list?
							FUNC_CHECK_DUPLICATES ${SYSDB_PACKAGE_REQUIRED_FOR_PACKAGE[${RETURN_VALUE}]} ${count}
							if [ $? -eq 0 ]; then
								# Add current package as an required package.
								SYSDB_PACKAGE_REQUIRED_FOR_PACKAGE[${RETURN_VALUE}]="${SYSDB_PACKAGE_REQUIRED_FOR_PACKAGE[${RETURN_VALUE}]} ${count}"
								let depopt_count=depopt_count+1
							fi
						else
							echo "${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}: Missing REQ ${required}" >>${SYSTEM_DEBUG_LOGFILE_DEPOPT_CHECK}
						fi
					done
					# Any optional packages?
					if ${OPTION_FIND_OPTIONAL_PACKAGES}; then
						for optional in ${SYSDB_PACKAGE_OPTIONAL[${count}]}; do
							if FUNC_FIND_PACKAGE_BY_NAME ${optional} ${SYSDB_PACKAGE_NAME[${count}]}; then
								# Do we have this package already in the list?
								FUNC_CHECK_DUPLICATES ${SYSDB_PACKAGE_OPTIONAL_FOR_PACKAGE[${RETURN_VALUE}]} ${count}
								if [ $? -eq 0 ]; then
									# Add current package as an optional package.
									SYSDB_PACKAGE_OPTIONAL_FOR_PACKAGE[${RETURN_VALUE}]="${SYSDB_PACKAGE_OPTIONAL_FOR_PACKAGE[${RETURN_VALUE}]} ${count}"
									let depopt_count=depopt_count+1
								fi
							else
								echo "${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}: Missing OPT ${optional}" >>${SYSTEM_DEBUG_LOGFILE_DEPOPT_CHECK}
							fi
						done
					fi
					if ${SYSDB_PACKAGE_AUTOSELECT[${count}]}; then
						SYSDB_PACKAGE_DEPOPT_CHECKED[${count}]=true
					fi
				fi
			fi
		done

		# Clear the progress bar.
		FUNC_CLEAR_PROGRESS_BAR

		if [ ${depopt_count} -eq 0 ]; then
			break
		fi

		# Create checklist
		FUNC_PROGRESS_BOX "Creating package list..."
		FUNC_INIT_PROGRESS_BAR

		rm -f ${DIALOG_TEMP_FILE1}
		rm -f ${DIALOG_TEMP_FILE2}

		count=0
		entries=0
		extra_packages=""
		debug_text_opt=""
		debug_text_req=""
		while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

			# Draw the progress bar.
			let count=count+1
			FUNC_DRAW_PROGRESS_BAR ${count} ${SYSDB_PACKAGE_COUNT}

			entry_found=false
			package_text=""
			# Is current package already selected?
			if ! ${SYSDB_PACKAGE_ENABLED[${count}]}; then
				# No, is this package recommended to rebuild for other packages?
				if [ x"${SYSDB_PACKAGE_REBUILD_FOR_PACKAGE[${count}]}" != x"" ]; then
					# Yes, check if these packages are selected and if
					# so enable this recommended package also.
					for package in ${SYSDB_PACKAGE_REBUILD_FOR_PACKAGE[${count}]}; do
						if ${SYSDB_PACKAGE_ENABLED[${package}]}; then
							mode="on"
							entry_found=true
							package_text="REB"
							break
						fi
					done
				else
					# Is this package required/recommended for other packages?
					if [ x"${SYSDB_PACKAGE_REQUIRED_FOR_PACKAGE[${count}]}" != x"" ]; then
						# Yes, check if these packages are auto-selected and if
						# so enable this required package also.
						for package in ${SYSDB_PACKAGE_REQUIRED_FOR_PACKAGE[${count}]}; do
							# Was the target package already checked? If so
							# ignore this optional package and do not list it
							# again
							newreqpkg=true
							for chknewreq in ${SYSDB_PACKAGE_REQUIRED_FOR_PACKAGE_CHECKED[${count}]}; do
								if [ x"${package}" == x"${chknewreq}" ]; then
									newreqpkg=false
									break
								fi
							done
							# List this package?
							if ${newreqpkg}; then
								if ${SYSDB_PACKAGE_ENABLED[${package}]}; then
									# AUTOSELECT check disabled since that would
									# always enable required packages.
									#if ${SYSDB_PACKAGE_AUTOSELECT[${package}]}; then
									#	mode="on"
									#	entry_found=true
									#	package_text="REQ"
									#	break
									#else
										# The target package was manually added to the buildorder.
										# In this case only add the required/recommended package if
										# it is not installed.
										if [ x"${SYSDB_PACKAGE_INSTALLED[${count}]}" != x"" ]; then
											# Package is installed, does package version/arch/build match
											# the buildtool included in the buildsystem?
											if [ x"${SYSDB_PACKAGE_INSTALLED[${count}]}" == x"${SYSDB_PACKAGE_NAME[${count}]}-${SYSDB_PACKAGE_VERSION[${count}]}${SYSDB_PACKAGE_RELEASE[${count}]}-${SYSDB_PACKAGE_ARCH[${count}]}-${SYSDB_PACKAGE_BUILD[${count}]}${PACKAGE_TAG}" ]; then
												# Yes, unselect package.
												mode="off"
												entry_found=true
												if [ x"${SYSDB_PACKAGE_OPTIONAL_FOR_PACKAGE[${count}]}" != x"" ]; then
													package_text="OPT"
												else
													package_text="REQ"
												fi
											else
												# Package with different version is installed, update the package.
												mode="on"
												entry_found=true
												package_text="REQ"
												break
											fi
										else
											# The target package was manually added to the buildorder but
											# the current required/recommended package is not installed.
											# Add this package to the buildorder.
											mode="on"
											entry_found=true
											package_text="REQ"
											break
										fi
									#fi
								fi
							else
								# When in debug mode keep a list of
								# ignored optional packages.
								if ${RUNTIME_OPTION_DEBUG_MODE}; then
									debug_text_req="${debug_text_req}\n${SYSDB_PACKAGE_NAME[${package}]} => ${SYSDB_PACKAGE_NAME[${count}]}"
								fi
							fi
						done
					else
						# Is this package optional for other packages?
						if [ x"${SYSDB_PACKAGE_OPTIONAL_FOR_PACKAGE[${count}]}" != x"" ]; then
							# Yes, check if these packages are auto-selected and if
							# so enable this optional package also.
							for package in ${SYSDB_PACKAGE_OPTIONAL_FOR_PACKAGE[${count}]}; do
								# Was the target package already checked? If so
								# ignore this optional package and do not list it
								# again
								newoptpkg=true
								for chknewopt in ${SYSDB_PACKAGE_OPTIONAL_FOR_PACKAGE_CHECKED[${count}]}; do
									if [ x"${package}" == x"${chknewopt}" ]; then
										newoptpkg=false
										break
									fi
								done
								# List this package?
								if ${newoptpkg}; then
									# Yes, is target package enabled?
									if ${SYSDB_PACKAGE_ENABLED[${package}]}; then
										# Yes, was current optional package auto-selected?
										if ${SYSDB_PACKAGE_AUTOSELECT[${count}]}; then
											# Yes, enable it by default.
											mode="on"
											entry_found=true
											package_text="OPT"
											break
										else
											# No, disable it by default.
											mode="off"
											entry_found=true
											package_text="OPT"
											break
										fi
									fi
								else
									# When in debug mode keep a list of
									# ignored optional packages.
									if ${RUNTIME_OPTION_DEBUG_MODE}; then
										debug_text_opt="${debug_text_opt}\n${SYSDB_PACKAGE_NAME[${package}]} => ${SYSDB_PACKAGE_NAME[${count}]}"
									fi
								fi
							done
						fi
					fi
				fi
			fi

			# Did we find a dependency, required or optional package?
			if ${entry_found}; then
				# Yes, was it removed by user in a previous run?
				if ${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}; then
					# Yes, disable it by default.
					#mode="off"
					# If it was removed we will not show it again.
					continue
				fi
				# Reload previous settings?
				if [ x"${pre_selected_packages}" != x"" ]; then
					# Yes, get saved mode for the selected package.
					mode="off"
					for package in ${pre_selected_packages}; do
						if [ x"${package}" == x"${SYSDB_PACKAGE_NAME[${count}]}" ]; then
							mode="on"
						fi
					done
				fi
				# Package selected?
				case ${mode} in
					"on")	SYSDB_PACKAGE_RUNTIME_DEPOPT_STATUS[${count}]=true;;
					"off")	SYSDB_PACKAGE_RUNTIME_DEPOPT_STATUS[${count}]=false;;
				esac
				# Get package status (installed, new build and such...)
				INITIALIZE_PACKAGE_STATUS ${count}
				echo "\"${SYSDB_PACKAGE_NAME[${count}]}\" \"${package_text}, ${SYSDB_PACKAGE_STATUS[${count}]}\"  \"${mode}\" \"${SYSDB_PACKAGE_DESC[${count}]}\" \\" >>${DIALOG_TEMP_FILE1}
				extra_packages="${extra_packages} ${SYSDB_PACKAGE_NAME[${count}]} "
				let entries=entries+1
			fi

		done

		# Clear the progress bar.
		FUNC_CLEAR_PROGRESS_BAR

		# When in debug mode display ignored required/optional packages.
		if ${RUNTIME_OPTION_DEBUG_MODE}; then
			if [ x"${debug_text_req}" != x"" -o x"${debug_text_opt}" != x"" ]; then
				let RUNTIME_DEPOPT_STAGE=RUNTIME_DEPOPT_STAGE+1
			fi
			if [ x"${debug_text_req}" != x"" ]; then
				echo "Ignored required packages during Stage${RUNTIME_DEPOPT_STAGE}:" >${SYSTEM_DEBUG_LOGFILE_DEPOPT_INFO}
				echo "${RULER1}" >>${SYSTEM_DEBUG_LOGFILE_DEPOPT_INFO}
				echo -e "${debug_text_req}" | sort | grep "." >>${SYSTEM_DEBUG_LOGFILE_DEPOPT_INFO}
				dialog	--title "${PROJECT_NAME} - DEBUGGING INFORMATION" \
						--backtitle "${PROJECT_BACKTITLE}" \
						--cr-wrap \
						--exit-label "${BUTTON_LABEL_OK}" \
						--textbox ${SYSTEM_DEBUG_LOGFILE_DEPOPT_INFO} 20 75
				cat ${SYSTEM_DEBUG_LOGFILE_DEPOPT_INFO} >>${SYSTEM_DEBUG_LOGFILE_DEPOPT}
				echo "${RULER1}" >>${SYSTEM_DEBUG_LOGFILE_DEPOPT}
				echo "" >>${SYSTEM_DEBUG_LOGFILE_DEPOPT}
				echo "" >>${SYSTEM_DEBUG_LOGFILE_DEPOPT}
				rm -f ${SYSTEM_DEBUG_LOGFILE_DEPOPT_INFO}
			fi
			if [ x"${debug_text_opt}" != x"" ]; then
				echo "Ignored optional packages during Stage${RUNTIME_DEPOPT_STAGE}:" >${SYSTEM_DEBUG_LOGFILE_DEPOPT_INFO}
				echo "${RULER1}" >>${SYSTEM_DEBUG_LOGFILE_DEPOPT_INFO}
				echo -e "${debug_text_opt}" | sort | grep "." >>${SYSTEM_DEBUG_LOGFILE_DEPOPT_INFO}
				dialog	--title "${PROJECT_NAME} - DEBUGGING INFORMATION" \
						--backtitle "${PROJECT_BACKTITLE}" \
						--cr-wrap \
						--exit-label "${BUTTON_LABEL_OK}" \
						--textbox ${SYSTEM_DEBUG_LOGFILE_DEPOPT_INFO} 20 75
				cat ${SYSTEM_DEBUG_LOGFILE_DEPOPT_INFO} >>${SYSTEM_DEBUG_LOGFILE_DEPOPT}
				echo "${RULER1}" >>${SYSTEM_DEBUG_LOGFILE_DEPOPT}
				echo "" >>${SYSTEM_DEBUG_LOGFILE_DEPOPT}
				echo "" >>${SYSTEM_DEBUG_LOGFILE_DEPOPT}
				rm -f ${SYSTEM_DEBUG_LOGFILE_DEPOPT_INFO}
			fi
		fi

		# Do we have any packages not yet enabled?
		if [ ${entries} -eq 0 ]; then
			changes=false
			break
		fi

		# Yes, allow the user to enable additional packages.
		cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "${PROJECT_NAME} - OPTIONAL/REQUIRED PACKAGES" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--cr-wrap \\
		--extra-button \\
		--extra-label "${BUTTON_LABEL_OPTIONS}" \\
		--help-button \\
		--help-label "${BUTTON_LABEL_EVERYTHING}" \\
		--ok-label "${BUTTON_LABEL_OK}" \\
		--cancel-label "${BUTTON_LABEL_CANCEL}" \\
		--item-help \\
		--checklist "\\
${PROJECT_NAME} found some extra packages. Some helpful hints:\n\
REB=Rebuild recommended, REQ=Required package, OPT=Optional package\n\
Use the spacebar to select/unselect a package, and the UP/DOWN arrow keys to scroll up and down through the entire list.\n" 20 75 10 \\
EOF

		# List required updates first.
		grep "\"REQ, Update" ${DIALOG_TEMP_FILE1} | sort >>${DIALOG_TEMP_FILE2}
		grep "\"REB, Update" ${DIALOG_TEMP_FILE1} | sort >>${DIALOG_TEMP_FILE2}
		grep "\"RE., " ${DIALOG_TEMP_FILE1} | grep -v "\"RE., Update" | sort >>${DIALOG_TEMP_FILE2}
		grep "\"OPT, " ${DIALOG_TEMP_FILE1} | sort >>${DIALOG_TEMP_FILE2}
		cat ${DIALOG_TEMP_FILE2} >>${DIALOG_SCRIPT}

		cat <<EOF >>${DIALOG_SCRIPT}
 2> "${DIALOG_RETURN_VALUE}"
EOF

		# Start the script and offer the version select dialog.
		. "${DIALOG_SCRIPT}"

		DIALOG_KEYCODE=$?
		DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
		rm -f "${DIALOG_SCRIPT}"
		rm -f "${DIALOG_TEMP_FILE1}"
		rm -f "${DIALOG_TEMP_FILE2}"
		rm -f "${DIALOG_RETURN_VALUE}"

		# Select all packages?
		if test ${DIALOG_KEYCODE} -eq 2; then
			FUNC_PROGRESS_BOX "Selecting packages..."
			for package in $(for package in ${extra_packages}; do echo ${package}; done ); do
				entry_found=false
				count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
					let count=count+1
					if [ x"${package}" == x"${SYSDB_PACKAGE_NAME[${count}]}" ]; then
						entry_found=true
						break
					fi
				done
				# If we found a dependency which is not available
				# for the current platform then skip this package.
				if ! ${entry_found}; then
					continue
				fi
				SYSDB_PACKAGE_ENABLED[${count}]=true
			done
			continue
		fi

		# Select all packages?
		if test ${DIALOG_KEYCODE} -eq 3; then

			dialog	--title "${PROJECT_NAME} - OPTIONS" \
					--backtitle "${PROJECT_BACKTITLE}" \
					--cr-wrap \
					--ok-label "${BUTTON_LABEL_OK}" \
					--cancel-label "${BUTTON_LABEL_GOBACK}" \
					--menu "\
To modify the automatically created buildorder is not recommended and can break the build of the selected packages!\n\
\n\
Select 'EXTRA' to remove packages that depend on other packages. \
Select 'ADD' or 'REMOVE' to modify the complete package buildorder. \n\
\n\
Select '${BUTTON_LABEL_GOBACK}' to go back to the previous menu.\n " 20 75 6 \
"INFO"   "Display extra packages list" \
"EXTRA" "Remove packages that require extra packages" \
"ADD"    "Add exra packages not yet selected" \
"REMOVE" "Remove packages from the build order" \
"EXIT"    "Exit ${PROJECT_NAME}" \
 2> ${DIALOG_RETURN_VALUE}

			DIALOG_KEYCODE=$?
			DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
			rm -f "${DIALOG_RETURN_VALUE}"

			# 'CANCEL' or 'ESC' ?
			if test ${DIALOG_KEYCODE} -ne 0; then
				continue
			fi

			case ${DIALOG_REPLY} in
				EXIT)		exit 1;;
				ADD)		BUILDSYS_MODIFY_BUILDORDER_EDIT_LIST "ADD" "BACK"
							# Reset everything to the defaults and re-check for depopts.
							BUILDSYS_CREATE_BUILDORDER_RESET_DEPOPTS
							continue;;
				EXTRA)		BUILDSYS_MODIFY_BUILDORDER_EDIT_LIST "REMDPKG" "BACK"
							# Reset everything to the defaults and re-check for depopts.
							BUILDSYS_CREATE_BUILDORDER_RESET_DEPOPTS
							continue;;
				REMOVE)		BUILDSYS_MODIFY_BUILDORDER_EDIT_LIST "REMOVE" "BACK"
							continue;;
				INFO)		FUNC_PROGRESS_BOX "Collecting data..."

							rm -f ${DIALOG_TEMP_FILE1}
							for package in $(for package in ${extra_packages}; do echo ${package}; done | sort ); do
								entry_found=false
								count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
									let count=count+1
									if [ x"${package}" == x"${SYSDB_PACKAGE_NAME[${count}]}" ]; then
										entry_found=true
										break
									fi
								done

								# If we found a dependency which is not available
								# for the current platform then skip this package.
								if ! ${entry_found}; then
									continue
								fi

								# Does buildtool match package name? If not display both names.
								if [ x"${SYSDB_PACKAGE_NAME[${count}]}" != x"${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}" ]; then
									checkorigname=" (${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]})"
								else
									checkorigname=""
								fi

								cat <<EOF >>${DIALOG_TEMP_FILE1}
Package : ${SYSDB_PACKAGE_NAME[${count}]}${checkorigname}
Version : ${SYSDB_PACKAGE_VERSION[${count}]}${SYSDB_PACKAGE_RELEASE[${count}]}
Status  : ${SYSDB_PACKAGE_STATUS[${count}]}
EOF

								if [ x"${SYSDB_PACKAGE_REBUILD_FOR_PACKAGE[${count}]}" != x"" ]; then
									cat <<EOF >>${DIALOG_TEMP_FILE1}

  Re-compile this package is recommended for:
EOF
									for dependency in ${SYSDB_PACKAGE_REBUILD_FOR_PACKAGE[${count}]}; do
										echo "            -${SYSDB_PACKAGE_NAME[${dependency}]}" >>${DIALOG_TEMP_FILE1}
									done
								fi

								if [ x"${SYSDB_PACKAGE_REQUIRED_FOR_PACKAGE[${count}]}" != x"" ]; then
									cat <<EOF >>${DIALOG_TEMP_FILE1}

  This package is required for:
EOF
									for required in ${SYSDB_PACKAGE_REQUIRED_FOR_PACKAGE[${count}]}; do
										echo "            -${SYSDB_PACKAGE_NAME[${required}]}" >>${DIALOG_TEMP_FILE1}
									done
								fi

								if [ x"${SYSDB_PACKAGE_OPTIONAL_FOR_PACKAGE[${count}]}" != x"" ]; then
									cat <<EOF >>${DIALOG_TEMP_FILE1}

  This package is optional for:
EOF
									for optional in ${SYSDB_PACKAGE_OPTIONAL_FOR_PACKAGE[${count}]}; do
										echo "            -${SYSDB_PACKAGE_NAME[${optional}]}" >>${DIALOG_TEMP_FILE1}
									done
								fi

								cat <<EOF >>${DIALOG_TEMP_FILE1}
${RULER2}

EOF

							done

							dialog	--title "${PROJECT_NAME} - PACKAGE INFORMATION" \
									--backtitle "${PROJECT_BACKTITLE}" \
									--cr-wrap \
									--exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${DIALOG_TEMP_FILE1} 20 75

							continue;;
				*)			continue;;
			esac
		fi


		# 'CANCEL' or 'ESC' ?
		if test ${DIALOG_KEYCODE} -ne 0; then
			return 1
		fi

		selected=false
		selected_package_list="${DIALOG_REPLY}"

		FUNC_PROGRESS_BOX "Analyzing selected packages..."
		FUNC_INIT_PROGRESS_BAR

		count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

			# Draw the progress bar
			let count=count+1
			FUNC_DRAW_PROGRESS_BAR ${count} ${SYSDB_PACKAGE_COUNT}

			# Check if the current package was selected.
			if [ x"${SYSDB_PACKAGE_RUNTIME_DEPOPT_STATUS[${count}]}" != x"" ]; then
				selected=false
				for package in ${selected_package_list}; do
					if [ x"${package}" == x"${SYSDB_PACKAGE_NAME[${count}]}" ]; then
						selected=true
						break
					fi
				done
				if ${selected}; then
					if [ x"${SYSDB_PACKAGE_REBUILD_FOR_PACKAGE[${count}]}" != x"" ]; then
						SYSDB_PACKAGE_DEPOPT_SELECT[${count}]=true
					else
						if [ x"${SYSDB_PACKAGE_REQUIRED_FOR_PACKAGE[${count}]}" != x"" ]; then
							SYSDB_PACKAGE_DEPOPT_SELECT[${count}]=true
						fi
					fi
					SYSDB_PACKAGE_ENABLED[${count}]=true
					changes=true
				else
					if ${SYSDB_PACKAGE_RUNTIME_DEPOPT_STATUS[${count}]}; then
						if [ x"${SYSDB_PACKAGE_REBUILD_FOR_PACKAGE[${count}]}" != x"" ]; then
							if ! ${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}; then
								SYSDB_PACKAGE_REMOVED_BY_USER[${count}]=true
								changes=true
							fi
						else
							if [ x"${SYSDB_PACKAGE_REQUIRED_FOR_PACKAGE[${count}]}" != x"" ]; then
								if ! ${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}; then
									SYSDB_PACKAGE_REMOVED_BY_USER[${count}]=true
									changes=true
								fi
							fi
						fi
					fi
					if [ x"${SYSDB_PACKAGE_REQUIRED_FOR_PACKAGE[${count}]}" != x"" ]; then
						for package in ${SYSDB_PACKAGE_REQUIRED_FOR_PACKAGE[${count}]}; do
							newreqpkg=true
							for chknewreq in ${SYSDB_PACKAGE_REQUIRED_FOR_PACKAGE_CHECKED[${count}]}; do
								if [ x"${package}" == x"${chknewreq}" ]; then
									newreqpkg=false
									break
								fi
							done
							if ${newreqpkg}; then
								SYSDB_PACKAGE_REQUIRED_FOR_PACKAGE_CHECKED[${count}]="${SYSDB_PACKAGE_REQUIRED_FOR_PACKAGE_CHECKED[${count}]} ${package}"
							fi
						done
					fi
					if [ x"${SYSDB_PACKAGE_OPTIONAL_FOR_PACKAGE[${count}]}" != x"" ]; then
						for package in ${SYSDB_PACKAGE_OPTIONAL_FOR_PACKAGE[${count}]}; do
							newoptpkg=true
							for chknewopt in ${SYSDB_PACKAGE_OPTIONAL_FOR_PACKAGE_CHECKED[${count}]}; do
								if [ x"${package}" == x"${chknewopt}" ]; then
									newoptpkg=false
									break
								fi
							done
							if ${newoptpkg}; then
								SYSDB_PACKAGE_OPTIONAL_FOR_PACKAGE_CHECKED[${count}]="${SYSDB_PACKAGE_OPTIONAL_FOR_PACKAGE_CHECKED[${count}]} ${package}"
							fi
						done
					fi
				fi
			fi

		done

		# Clear the progress bar.
		FUNC_CLEAR_PROGRESS_BAR

		break

	done

	case ${changes} in
		true)	return 2;;
		*)		return 0;;
	esac

}

# Backup list of installed packages.
BUILDSYS_SETUP_CREATE_PACKAGE_INSTALLED_LIST() {
	case $1 in
		"1")	ls -1 /var/log/packages | sort >${SYSTEM_DEBUG_LOGFILE_INSTALLED_STAGE1};;
		"2")	ls -1 /var/log/packages | sort >${SYSTEM_DEBUG_LOGFILE_INSTALLED_STAGE2};;
		"3")	ls -1 /var/log/packages | sort >${SYSTEM_DEBUG_LOGFILE_INSTALLED_STAGE3};;
	esac
}

# Check for packages that will be updated.
BUILDSYS_SETUP_CREATE_PACKAGE_UPDATE_LIST() {

	local count=0
	local count_update=0
	local text=""
	local source=""
	local first_package=true

	FUNC_PROGRESS_BOX "Checking installed packages..."
	FUNC_WRITE_UPDATED_PACKAGES_TO_LOGFILE "${SYSTEM_LOGFILE_PACKAGE_UPDATES}"

	return 0
}

# Check for downgrading packages
BUILDSYS_SETUP_CHECK_DOWNGRADE_PACKAGES() {

	local downgrade_packages=""
	local count=0
	local default_item=""
	local package=""
	local package_old=""
	local package_new=""
	local exit_dialog=false
	local version_package=0
	local version_installed=0
	local check=""

	FUNC_PROGRESS_BOX "Checking installed versions..."

	# Init the progress bar.
	FUNC_INIT_PROGRESS_BAR

	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do

		# Draw the progress bar.
		FUNC_DRAW_PROGRESS_BAR ${count} ${SYSDB_PACKAGE_COUNT}
		let count=count+1

		# Only check for enabled packages.
		if ${SYSDB_PACKAGE_ENABLED[${count}]}; then

			# Someone uses some really dumb version numbers... skip these packages.
			case  ${SYSDB_PACKAGE_NAME[${count}]} in
				"polkit-kde-agent-1")		continue;;
				"polkit-kde-kcmodules-1")	continue;;
				"xf86-video-nouveau")		continue;;
			esac

			# Did we find any installed packages during initialization?
			if [ x"${SYSDB_PACKAGE_INSTALLED[${count}]}" != x"" ]; then

				# Get the numeric version of the package and the installed package.
				FUNC_GET_PACKAGE_NUMERIC_VERSION_ALL ${count}

				# Ignore revision version information like qt-r12345 since
				# this may be older or newer then a regular version.
				# FUNC_CALC_NUMERIC_VERSION: 90.000.000.000 / 1.000.000.000 = 90.x.y
				if test ${NUMPKG_PACKAGE_VERSION} -lt 90000000000 && test ${NUMPKG_INSTALLED_VERSION} -lt 90000000000 ; then
					# Very stupid way of checking versions... but it works...
					if test ${NUMPKG_PACKAGE_VERSION} -lt ${NUMPKG_INSTALLED_VERSION}; then
						check=$(grep ^${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}$ ${SYSTEM_CONFIG_DOWNGRADE_WHITELIST})
						if [ x"${check}" == x"" ]; then
							downgrade_packages="${downgrade_packages} ${count}"
							echo "Downgrade  : ${SYSDB_PACKAGE_GROUP[${count}]}/${SYSDB_PACKAGE_INSTALLED[${count}]} (New version: ${SYSDB_PACKAGE_VERSION[${count}]}${SYSDB_PACKAGE_RELEASE[${count}]})" >>${SYSTEM_LOGFILE_DOWNGRADES}
						else
							echo "Whitelisted: ${SYSDB_PACKAGE_GROUP[${count}]}/${SYSDB_PACKAGE_INSTALLED[${count}]} (New version: ${SYSDB_PACKAGE_VERSION[${count}]}${SYSDB_PACKAGE_RELEASE[${count}]})" >>${SYSTEM_LOGFILE_DOWNGRADES}
						fi
					fi
				fi

			fi
		fi
	done

	# Clear the progress bar.
	FUNC_CLEAR_PROGRESS_BAR


	## Did we find any packages installed newer to the versions we want to build?
	if [ x"${downgrade_packages}" != x"" ]; then

		default_item="LIST"
		while [ true ]; do
			dialog	--title "${PROJECT_NAME} - DOWNGRADE PACKAGES" \
					--backtitle "${PROJECT_BACKTITLE}" \
					--default-item "${default_item}" \
					--ok-label "${BUTTON_LABEL_OK}" \
					--cancel-label "${BUTTON_LABEL_EXIT}" \
					--menu "\
\n\
Warning:\n\
\n\
You have some packages already installed newer then the packages included in this buildsystem or some package versions could not be auto-detected.\n\
That means you are running this buildsystem on a unsupported system! Continue at your own risk!\n\
\n\
 Select one of the following options:\n" 20 75 4 \
"LIST"      "Display list of packages to be downgraded" \
"ASK"       "Ask me for each package what to do" \
"SKIP"      "Skip the packages and keep newer package" \
"REPLACE"   "Replace installed packages" 2> ${DIALOG_RETURN_VALUE}

			DIALOG_KEYCODE=$?
			DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
			rm -f "${DIALOG_RETURN_VALUE}"

			# 'CANCEL' or 'ESC' ?
			if test ${DIALOG_KEYCODE} -ne 0; then
				return 1
			fi

			# Display a list of packages installed with a newer version.
			if [ x"${DIALOG_REPLY}" == x"LIST" ]; then
				cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "${PROJECT_NAME} - DOWNGRADE PACKAGES" \
		--backtitle "${PROJECT_BACKTITLE}" \
		--cr-wrap \
		--msgbox "\
You have some packages already installed newer then the packages included in this buildsystem or some package versions could not be auto-detected.\n\
${RULER2}\n
EOF
				for count in ${downgrade_packages} ; do
					echo "    ${PROJECT_NAME}: ${SYSDB_PACKAGE_GROUP[${count}]}/${SYSDB_PACKAGE_NAME[${count}]}-${SYSDB_PACKAGE_VERSION[${count}]}${SYSDB_PACKAGE_RELEASE[${count}]}-${SYSDB_PACKAGE_ARCH[${count}]}-${SYSDB_PACKAGE_BUILD[${count}]}${PACKAGE_TAG}\n" >>${DIALOG_SCRIPT}
					for package in ${SYSDB_PACKAGE_INSTALLED[${count}]}; do
						echo "    Installed  : ${package}\n" >>${DIALOG_SCRIPT}
					done
					echo "\n" >>${DIALOG_SCRIPT}
				done
				echo "${RULER2}\n\" 20 75 2> ${DIALOG_RETURN_VALUE}" >>${DIALOG_SCRIPT}

				sh "${DIALOG_SCRIPT}"
				rm -f "${DIALOG_RETURN_VALUE}"
				rm -f "${DIALOG_SCRIPT}"

				default_item="ASK"
				continue
			fi

			# Ask for each package if we should downgrade or not.
			if [ x"${DIALOG_REPLY}" == x"ASK" ]; then
				exit_dialog=false
				for count in ${downgrade_packages}; do

					# Only show first entry in installed package list.
					package_old=${SYSDB_PACKAGE_INSTALLED[${count}]// */}

					# Name of the old package.
					package_new=${SYSDB_PACKAGE_NAME[${count}]}-${SYSDB_PACKAGE_VERSION[${count}]}${SYSDB_PACKAGE_RELEASE[${count}]}-${SYSDB_PACKAGE_ARCH[${count}]}-${SYSDB_PACKAGE_BUILD[${count}]}${PACKAGE_TAG}

					# Bring up the dialog and let the user decide.
					dialog	--title "${PROJECT_NAME} - DOWNGRADE PACKAGES" \
							--backtitle "${PROJECT_BACKTITLE}" \
							--default-item "KEEP" \
							--ok-label "${BUTTON_LABEL_OK}" \
							--cancel-label "${BUTTON_LABEL_GOBACK}" \
							--menu "\
\n\
Warning:\n\
\n\
To downgrade already installed packages is not recommended and can break already installed applications!\n\
\n\
Downgrade the following package?\n\
\n\
Installed  : ${package_old}\n\
New package: ${package_new}\n\
\n\
 Select one of the following options:\n" 20 75 2 \
"KEEP"    "Keep the newer package installed on this system" \
"REPLACE" "Replace the installed package with the older version" 2> ${DIALOG_RETURN_VALUE}

					DIALOG_KEYCODE=$?
					DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
					rm -f "${DIALOG_RETURN_VALUE}"

					# 'CANCEL' or 'ESC' ?
					if test ${DIALOG_KEYCODE} -ne 0; then
						exit_dialog=true
						break
					fi

					# Does the user want to replace the package? No...
					if [ x"${DIALOG_REPLY}" == x"KEEP" ]; then
						SYSDB_PACKAGE_ENABLED[${count}]=false
					else
						SYSDB_PACKAGE_ENABLED[${count}]=true
					fi

				done

				# Go back to main menu?
				if ${exit_dialog}; then
					continue
				fi

				break
			fi

			# Skip packages with a new package already installed.
			if [ x"${DIALOG_REPLY}" == x"SKIP" ]; then
				for count in ${downgrade_packages}; do
					SYSDB_PACKAGE_ENABLED[${count}]=false
				done
				break
			fi

			# Replace newer package allready installed by
			# an old package from the buildsystem.
			if [ x"${DIALOG_REPLY}" == x"REPLACE" ]; then
				for count in ${downgrade_packages}; do
					SYSDB_PACKAGE_ENABLED[${count}]=true
				done
				break
			fi

		done

	fi

	return 0

}
# Add entry to list of packages to be removed.
# Check for package already in the list, check if package
# is installed and check if package will be compiled/replaced.
#   $1: package or buildtool name.
#   $2: package mode code
#   $3: Text for hint
#   $4: Mode
BUILDSYS_SETUP_REMOVE_EXTRA_ADD_ENTRY() {
	local package_name=$1
	local package_mode_code=$2
	local package_hint=$3
	local package_enabled=$4
	local count_remove=0
	local package_list=""
	local package_mode=""
	local installed_pkg_list=""
	local count=0
	local found=false

	# Check if we have a package enabled with the same name.
	count=0
	found=false
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
		let count=count+1
		if [ x"${SYSDB_PACKAGE_ENABLED[${count}]}" == x"true" ]; then
			if [ x"${SYSDB_PACKAGE_NAME[${count}]}" == x"${package_name}" ]; then
				found=true
				break
			fi
			for package in ${SYSDB_PACKAGE_REPLACE[${count}]}; do
				if [ x"${package}" == x"${package_name}" ]; then
					found=true
					break
				fi
			done
			if ${found}; then
				break
			fi
		fi
	done

	# Do we have the package name already in the list?
	check=`grep "^${package_name} " ${DIALOG_SCRIPT}`
	if [ x"$check" != x"" ]; then
		found=true
	fi

	# Is package in the queue to be installed/replaced?
	if ! ${found}; then
		# Check if package is installed.
		if FUNC_CHECK_PACKAGE_INSTALLED ${package_name}; then
			for installed_pkg_list in ${RETURN_VALUE}; do
				pkg_remove_name=${installed_pkg_list%-*}	# Remove build/tag
				pkg_remove_name=${pkg_remove_name%-*}		# Remove arch
				pkg_remove_name=${pkg_remove_name%-*}		# Remove version/release

				# Package already in the list?
				check=`grep "\"${package_name}\"" ${DIALOG_SCRIPT}`
				if [ x"${check}" == x"" ]; then
					# Is installed package a buildtool?
					if [ x"${RETURN_VALUE}" != x"${RETURN_VALUE%${PACKAGE_TAG}}" ]; then
						# yes
						package_mode="[${package_mode_code}] SLP   >>"
					else
						# No
						package_mode="[${package_mode_code}] NoSLP >>"
					fi

					# Create menu entry.
					cat <<EOF >>${DIALOG_SCRIPT}
"${package_name}"  "${package_mode}${RETURN_VALUE}"  "${package_enabled}"  "${package_hint}: ${RETURN_VALUE}"	\\
EOF
					let count_remove=count_remove+1
					package_list="${package_list} ${package_name}"
				fi
			done
		fi
	fi

	# Return the new count/list for packages to be removed.
	RETURN_VALUE="${package_list}"
	return ${count_remove}
}

# Remove extra packages from remove.conf.
BUILDSYS_SETUP_REMOVE_EXTRA_PACKAGES() {
	local count=0
	local count_removed=0
	local count_max_remove=0
	local percent_removed=0
	local line=""
	local package_mode=""
	local package=""
	local found=false
	local remove_package=""
	local installed_package=""
	local remove_package_list=""
	local return_add_entry_count=0
	local count_max_entries=0

	# When upgrade/update/sync packages do not remove installed
	# packages or installing packages may fail because of missing
	# libraries of optional/required dependencies.
	case ${RUNTIME_SETUP_MODE} in
		UPDATE|UPGRADE|SYNC)	return 0;;
	esac

	# Check for extra packages to be removed?
	check_remove_conf=false
	if ${OPTION_REMOVE_EXTRA_PACKAGES:-false}; then
		case ${RUNTIME_SETUP_MODE} in
			SOURCE|SINGLE) check_remove_conf=true;;
		esac
	fi

	# Check for extra packages to be removed?
	check_remove_extras=false
	case ${RUNTIME_SETUP_MODE} in
		SOURCE) check_remove_extras=true;;
	esac

	# Check for include.conf files with extra packages to be removed?
	check_remove_include=false
	if ${RUNTIME_USE_INCLUDE_CONF_FILES_OVERWRITE:-false}; then
		if test -e ${SYSTEM_CONFIG_BUILDTOOLS_FILTER}; then
			check_remove_include=true
		fi
	fi

	# Do we have allready some packages in the list?
	check_remove_other_packages=false
	if [ x"${RUNTIME_REMOVE_EXTRA_PACKAGES}" != x"" ]; then
		check_remove_other_packages=true
	fi

	# Endless menu loop...
	while true; do

		FUNC_PROGRESS_BOX_LARGE "Searching for extra packages to remove..."

		# Yes, allow the user to enable additional packages.
		cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "${PROJECT_NAME} - REMOVE EXTRA PACKAGES" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--cr-wrap \\
		--ok-label "${BUTTON_LABEL_OK}" \\
		--cancel-label "${BUTTON_LABEL_NOTHING}" \\
		--extra-button \\
		--extra-label "${BUTTON_LABEL_EVERYTHING}" \\
		--item-help \\
		--checklist "\\
There are some extra packages to be removed by ${PROJECT_NAME}.\n\
Note: SLP=Buildtool installed, NoSLP=Slackware/3rd party package.\n\
      [S] Suggested, [R]/[I] Recommended, [O] Requested\n\
\n\
Use the spacebar to select/unselect a package, and the UP/DOWN arrow keys to scroll up and down through the entire list." 20 75 8 \\
EOF

		# Remove extra packages from 'remove.conf' ?
		if ${check_remove_conf:-false}; then
			if [ x"${SYSTEM_CONFIG_REMOVE}" != x"" ]; then
				if test -e "${SYSTEM_CONFIG_REMOVE}"; then

					# Do we have to check 'remove.conf'?
					count_max_entries=`grep -v "#" ${SYSTEM_CONFIG_REMOVE} | grep -c "."`
					if test ${count_max_entries} -gt 0; then

						# Init the progress bar.
						FUNC_INIT_PROGRESS_BAR
						let count=0

						let count_max_remove=0
						remove_package_list=""
						while read line; do

							# Draw the progress bar.
							FUNC_DRAW_PROGRESS_BAR ${count} ${count_max_entries}
							let count=count+1

							# Ignore comments and blank lines in remove.conf.
							if [ x"${line}" == x"${line//#/}" -a x"${line}" != x"" ]; then
								BUILDSYS_SETUP_REMOVE_EXTRA_ADD_ENTRY "${line}" "S" "Remove extra package" "off"
								return_add_entry_count=$?
								if test ${return_add_entry_count} -gt 0; then
									let count_max_remove=count_max_remove+return_add_entry_count
									remove_package_list="${remove_package_list} ${RETURN_VALUE}"
								fi
							fi
						done < "${SYSTEM_CONFIG_REMOVE}"

					fi

				fi
			fi
		fi

		# Remove extra packages from 'EXTRAS' in selected groups ?
		if ${check_remove_extras:-false}; then
			# Check selected groups for an existing 'EXTRAS' file.
			for local_build_mode in ${BUILDSYS_INSTALL_GROUP}; do
				# Include package for the selected group?
				if test -e "${SYSTEM_PATH_GROUP_EXTRAS_CACHE}/${local_build_mode}"; then

					# Do we have to check EXTRAS?
					count_max_entries=`grep "^[ ]*=" "${SYSTEM_PATH_GROUP_EXTRAS_CACHE}/${local_build_mode}" | grep -c "."`
					if test ${count_max_entries} -gt 0; then

						# Init the progress bar.
						FUNC_INIT_PROGRESS_BAR
						let count=0

						# Read all packages to be removed from 'EXTRAS' file.
						check=`grep "^[ ]*=" "${SYSTEM_PATH_GROUP_EXTRAS_CACHE}/${local_build_mode}" | sed "s,.*=,,g"`
						for package in ${check}; do

							# Draw the progress bar.
							FUNC_DRAW_PROGRESS_BAR ${count} ${count_max_entries}
							let count=count+1

							BUILDSYS_SETUP_REMOVE_EXTRA_ADD_ENTRY "${package}" "R" "Remove for category [${local_build_mode}]" "on"
							return_add_entry_count=$?
							if test ${return_add_entry_count} -gt 0; then
								let count_max_remove=count_max_remove+return_add_entry_count
								remove_package_list="${remove_package_list} ${RETURN_VALUE}"
							fi
						done

					fi

				fi
			done
		fi

		# Remove extra packages from 'include.conf' files ?
		if ${check_remove_include:-false}; then
			# Do we have to check 'include.conf`?
			count_max_entries=`grep -H "^=" "${SYSTEM_PATH_INCLUDES_CONFIG_COPY}"/*.conf 2>/dev/null | sort | uniq | grep -c "."`
			if test ${count_max_entries} -gt 0; then
				# Init the progress bar.
				FUNC_INIT_PROGRESS_BAR
				let count=0

				for entry in `grep -H "^=" "${SYSTEM_PATH_INCLUDES_CONFIG_COPY}"/*.conf 2>/dev/null | sort | uniq`; do

					# Draw the progress bar.
					FUNC_DRAW_PROGRESS_BAR ${count} ${count_max_entries}
					let count=count+1

					package=${entry##*=}

					# Remove all packages from a group?
					if [ x"${package#@}" != x"${package}" ]; then
						# Add group to the remove package list.
						# Get a list of buildtools of the requested group.
						buildtool_list=`grep "^${package#@}/" ${SYSTEM_CONFIG_BUILDTOOLS} | sed "s,.*/,,g"`
						for buildtool_name in ${buildtool_list}; do
							BUILDSYS_SETUP_REMOVE_EXTRA_ADD_ENTRY "${buildtool_name}" "I" "Remove included [${entry##*/}]" "on"
							return_add_entry_count=$?
							if test ${return_add_entry_count} -gt 0; then
								let count_max_remove=count_max_remove+return_add_entry_count
								remove_package_list="${remove_package_list} ${RETURN_VALUE}"
							fi
						done
					else
						BUILDSYS_SETUP_REMOVE_EXTRA_ADD_ENTRY "${package}" "I" "Remove included [${entry##*/}]" "on"
						return_add_entry_count=$?
						if test ${return_add_entry_count} -gt 0; then
							let count_max_remove=count_max_remove+return_add_entry_count
							remove_package_list="${remove_package_list} ${RETURN_VALUE}"
						fi
					fi

				done
			fi
		fi

		# Remove other packages?
		if ${check_remove_other_packages:-false}; then
			for package in ${RUNTIME_REMOVE_EXTRA_PACKAGES}; do

				BUILDSYS_SETUP_REMOVE_EXTRA_ADD_ENTRY "${package}" "O" "Remove other packages" "on"
				return_add_entry_count=$?
				if test ${return_add_entry_count} -gt 0; then
					let count_max_remove=count_max_remove+return_add_entry_count
					remove_package_list="${remove_package_list} ${RETURN_VALUE}"
				fi

			done
		fi

		cat <<EOF >>${DIALOG_SCRIPT}
 2> "${DIALOG_RETURN_VALUE}"
EOF

		# Any packages left to remove?
		if test ${count_max_remove} -gt 0; then
			# Start the script and offer the version select dialog.
			. "${DIALOG_SCRIPT}"

			DIALOG_KEYCODE=$?
			DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
			rm -f "${DIALOG_TEMP_FILE1}"
			rm -f "${DIALOG_RETURN_VALUE}"

			# Extra button?
			if test ${DIALOG_KEYCODE} -eq 3; then
				DIALOG_REPLY="${remove_package_list}"
				DIALOG_KEYCODE=0
				break
			fi
		else
			DIALOG_KEYCODE=1
		fi
		rm -f "${DIALOG_SCRIPT}"
		break
	done

	# 'OK' ?
	if test ${DIALOG_KEYCODE} -eq 0; then
		# kde-l10n-* ends up as kde-l10n-\*
		RUNTIME_REMOVE_EXTRA_PACKAGES=${DIALOG_REPLY//\\\*/\*}
	fi

	# Show some debug info...
	if ${RUNTIME_OPTION_DEBUG_MODE}; then
		count_max_entries=0
		for entry in ${RUNTIME_REMOVE_EXTRA_PACKAGES}; do
			let count_max_entries=count_max_entries+1
		done
		if test ${count_max_entries:-1} -ne 0; then
			FUNC_DISPLAY_PACKAGE_LIST "${RUNTIME_REMOVE_EXTRA_PACKAGES}" "EXTRA PACKAGES TO REMOVE"
		fi
	fi

	# Packages removed, cleaning up...
	FUNC_PLEASE_WAIT

	return 0

}

# Check if we have packages selected to be installed.
BUILDSYS_SETUP_CHECK_NOTHING_TO_INSTALL() {

	FUNC_PROGRESS_BOX "Counting packages to be installed..."

	FUNC_CHECK_INSTALL_PACKAGES

	# Did we find any packages to be installed?
	if [ ${RUNTIME_INSTALL_PACKAGES} -eq 0 ]; then
		# No display an error message, exit the buildsystem.
		FUNC_MESSAGE_BOX "ERROR" "No packages selected.\nNothing to build.\n\nExiting now!"
		FUNC_CLEAR_SCREEN
		return 1
	fi

	return 0
}

# Setup the build environment.
BUILDSYS_SETUP_BUILD_ENVIRONMENT() {
	local check=""
	local cpus=0

	# Setup debug mode.
	if ${RUNTIME_OPTION_DEBUG_MODE}; then
		export BUILDTOOL_DEBUG_MODE=true
	else
		export BUILDTOOL_DEBUG_MODE=false
	fi

	# Set debug options.
	export BUILDTOOL_STRIP_BINARIES=${BUILDTOOL_STRIP_BINARIES:-${OPTION_STRIP_BINARIES:-true}}

	# Verbose mode?
	export BUILDTOOL_QUIET_MODE=false
	export BUILDTOOL_VERBOSE_MODE=${OPTION_VERBOSE_MODE:-true}

	# Create 'slack-required' files?
	BUILDTOOL_RBUILDER_CREATE=${BUILDTOOL_RBUILDER_CREATE:-${OPTION_RBUILDER_CREATE:-false}}

	if ${BUILDTOOL_RBUILDER_CREATE}; then
		BUILDTOOL_RBUILDER_FLAGS=${BUILDTOOL_RBUILDER_FLAGS:-${OPTION_RBUILDER_FLAGS:-"-y"}}
		BUILDTOOL_RBUILDER_DATABASE_PATH=${BUILDTOOL_RBUILDER_DATABASE_PATH:-${OPTION_RBUILDER_DATABASE_PATH:-""}}
		if [ x"${BUILDTOOL_RBUILDER_DATABASE_PATH}" != x"" ]; then
			mkdir -p "${BUILDTOOL_RBUILDER_DATABASE_PATH}"
		fi
		BUILDTOOL_RBUILDER_CONFIG_PATH=${BUILDTOOL_RBUILDER_CONFIG_PATH:-${OPTION_RBUILDER_CONFIG_PATH:-""}}
		if [ x"${BUILDTOOL_RBUILDER_CONFIG_PATH}" != x"" ]; then
			mkdir -p "${BUILDTOOL_RBUILDER_CONFIG_PATH}"
		fi
		BUILDTOOL_RBUILDER_COPY_PATH=${BUILDTOOL_RBUILDER_COPY_PATH:-${OPTION_RBUILDER_COPY_PATH:-""}}
		if [ x"${BUILDTOOL_RBUILDER_COPY_PATH}" != x"" ]; then
			mkdir -p "${BUILDTOOL_RBUILDER_COPY_PATH}"
		fi
		BUILDTOOL_RBUILDER_FILTER=${BUILDTOOL_RBUILDER_FILTER:-${OPTION_RBUILDER_FILTER:-false}}
		if ${BUILDTOOL_RBUILDER_FILTER:-false}; then
			BUILDTOOL_RBUILDER_FILTER_PATH=${BUILDTOOL_RBUILDER_FILTER_PATH:-${OPTION_RBUILDER_FILTER_PATH:-${SYSTEM_CONFIG}}}
			if [ x"${BUILDTOOL_RBUILDER_FILTER_PATH}" != x"" ]; then
				mkdir -p "${BUILDTOOL_RBUILDER_FILTER_PATH}"
			fi
		else
			BUILDTOOL_RBUILDER_FILTER_PATH=""
		fi
	else
		BUILDTOOL_RBUILDER_FLAGS=""
		BUILDTOOL_RBUILDER_DATABASE_PATH=""
		BUILDTOOL_RBUILDER_CONFIG_PATH=""
		BUILDTOOL_RBUILDER_COPY_PATH=""
		BUILDTOOL_RBUILDER_FILTER=false
		BUILDTOOL_RBUILDER_FILTER_PATH=""
	fi

	# Export the settings for creating 'slack-required' files.
	export BUILDTOOL_RBUILDER_CREATE
	export BUILDTOOL_RBUILDER_FLAGS
	export BUILDTOOL_RBUILDER_DATABASE_PATH
	export BUILDTOOL_RBUILDER_CONFIG_PATH
	export BUILDTOOL_RBUILDER_COPY_PATH
	export BUILDTOOL_RBUILDER_FILTER
	export BUILDTOOL_RBUILDER_FILTER_PATH

	# Setup the package tag.
	export PACKAGE_TAG=${PACKAGE_TAG:-${OPTION_PACKAGE_TAG:-"slp"}}

	# Include buildfiles.
	export BUILDTOOL_INCLUDE_BUILDFILES=${BUILDTOOL_INCLUDE_BUILDFILES:-${OPTION_INCLUDE_BUILDFILES:-false}}

	# Export KDEDIR and KDEDIRS.
	export KDEDIR
	export KDEDIRS

	# Set defaults for QT.
	export QTDIR
	export QT4DIR
	export QT5DIR

	# Set default for package extension.
	export BUILDTOOL_PACKAGE_EXTENSION

	# Set default for BUILDARCH-warnings logfile.
	export LOGFILE_WARNING_BUILDARCH=${SYSTEM_LOGFILE_BUILDARCH_WARNINGS}

	# Set default for SUID-warnings logfile.
	export LOGFILE_WARNING_SUID=${SYSTEM_LOGFILE_SUID_WARNINGS}

	# Set default for RPATH-warnings logfile.
	export LOGFILE_WARNING_RPATH=${SYSTEM_LOGFILE_RPATH_WARNINGS}

	# Set default for SHAREDLIBS-warnings logfile.
	export LOGFILE_WARNING_SHAREDLIBS=${SYSTEM_LOGFILE_SHAREDLIBS_WARNINGS}

	# Set default for EMPTYDIR-warnings logfile.
	export LOGFILE_WARNING_EMPTYDIR=${SYSTEM_LOGFILE_EMPTYDIR_WARNINGS}

	# Set default for DOINST-warnings logfile.
	export LOGFILE_WARNING_DOINST=${SYSTEM_LOGFILE_DOINST_WARNINGS}

	# Set default for docfile warnings logfile.
	export LOGFILE_WARNING_DOCFILES=${SYSTEM_LOGFILE_DOCFILES_WARNINGS}

	# Set default for usrlocal warnings logfile.
	export LOGFILE_WARNING_USRLOCAL=${SYSTEM_LOGFILE_USRLOCAL_WARNINGS}

	# Set default for overwrite files warnings logfile.
	export LOGFILE_WARNING_OVERWRITE_FILES=${SYSTEM_LOGFILE_OVERWRITE_FILES_WARNINGS}

	# Set default for applied patches logfile.
	export LOGFILE_APPLIED_PATCHES=${SYSTEM_LOGFILE_APPLIED_PATCHES}

	# Set default for updated icon cache files logfile.
	export LOGFILE_GTK_ICON_CACHE=${SYSTEM_LOGFILE_GTK_ICON_CACHE}

	# Set default for removed *.la files logfile.
	export LOGFILE_DEBUG_RM_LA_FILES=${SYSTEM_LOGFILE_RM_LA_FILES}

	# Set default for remaining *.la files logfile.
	export LOGFILE_DEBUG_CHECK_LA_FILES=${SYSTEM_LOGFILE_CHECK_LA_FILES}

	# Use multi-compile/distcc ?
	if ${OPTION_MULTIPLE_CPU_COMPILE:-false}; then
		if [ x"${OPTION_MAX_COMPILE_JOBS:-auto}" == x"auto" ]; then
			cpus=$(grep -c "^processor" /proc/cpuinfo)
			let cpus=cpus+1
			if test ${cpus} -lt 1; then
				cpus=1
			fi
			FUNC_CLEAN_MAKEOPTS_JOBS "${MAKEOPTS}" "-j${cpus}"
			export RUNTIME_MAKEOPTS="${RETURN_VALUE}"
		else
			if test ${OPTION_MAX_COMPILE_JOBS:-1} -gt ${MAX_COMPILE_JOBS} ]; then
				# Illegal value for $OPTION_MAX_COMPILE_JOBS.
				OPTION_MAX_COMPILE_JOBS=${MAX_COMPILE_JOBS}
			fi
			if test ${OPTION_MAX_COMPILE_JOBS:-1} -lt 1; then
				# Illegal value for $OPTION_MAX_COMPILE_JOBS.
				OPTION_MAX_COMPILE_JOBS=1
			fi
			FUNC_CLEAN_MAKEOPTS_JOBS "${MAKEOPTS}" "-j${OPTION_MAX_COMPILE_JOBS:-1}"
			export RUNTIME_MAKEOPTS="${RETURN_VALUE}"
		fi
	else
		# If the user does not allow distcc then overwrite it.
		FUNC_CLEAN_MAKEOPTS_JOBS "${MAKEOPTS}" "-j1"
		export RUNTIME_MAKEOPTS="${RETURN_VALUE}"
	fi
}

# Write packages to build to the jobfile.
BUILDSYS_SETUP_CREATE_JOBFILE() {
	local count=0

	# Create a job-file including all packages left to build
	rm -f ${SYSTEM_JOB_PACKAGES}
	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
		let count=count+1
		if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
			echo "${SYSDB_PACKAGE_GROUP[${count}]}/${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}" >>"${SYSTEM_JOB_PACKAGES}"
		fi
	done

}

# Save settings for a restart after we maybe have rebooted the system and bash variables have been cleared.
BUILDSYS_SETUP_CREATE_JOBSETTINGS() {

	# Use the cache? Yes, get the cache directory...
	if ${OPTION_MOVE_INSTALLED_PACKAGES}; then
		if [ x"${SYSTEM_PATH_PKGDIR}" == x"" ]; then
			if [ x${RUNTIME_SYSTEM_USERMODE} == xtrue -o x${RUNTIME_SYSTEM_CUSTOM_CONFIG} == xtrue ]; then
				SYSTEM_PATH_PKGDIR="${OPTION_MOVE_INSTALLED_PACKAGES_TO_PATH}/${OS_PLATFORM_CODE}/${PROJECT_VERSION}-$(date +%s)-CUSTOM/packages"
			else
				case ${RUNTIME_SETUP_MODE} in
					UPDATE|UPGRADE)		SYSTEM_PATH_PKGDIR="${OPTION_MOVE_INSTALLED_PACKAGES_TO_PATH}/${OS_PLATFORM_CODE}/${PROJECT_VERSION}-$(date +%s)-UPDATES/packages";;
					*)					SYSTEM_PATH_PKGDIR="${OPTION_MOVE_INSTALLED_PACKAGES_TO_PATH}/${OS_PLATFORM_CODE}/${PROJECT_VERSION}-$(date +%s)/packages";;
				esac
			fi
		fi
	else
		SYSTEM_PATH_PKGDIR=""
	fi

	# These options can be overwritten using environment variables.
	cat <<EOF >"${SYSTEM_JOB_SETTINGS}"
export PACKAGE_TAG="${PACKAGE_TAG}"

export BUILDTOOL_PACKAGE_EXTENSION=${BUILDTOOL_PACKAGE_EXTENSION}
export BUILDTOOL_DEBUG_MODE=${BUILDTOOL_DEBUG_MODE}
export BUILDTOOL_QUIET_MODE=false
export BUILDTOOL_VERBOSE_MODE=${BUILDTOOL_VERBOSE_MODE}
export BUILDTOOL_RBUILDER_CREATE=${BUILDTOOL_RBUILDER_CREATE}
export BUILDTOOL_RBUILDER_FLAGS="${BUILDTOOL_RBUILDER_FLAGS}"
export BUILDTOOL_RBUILDER_DATABASE_PATH="${BUILDTOOL_RBUILDER_DATABASE_PATH}"
export BUILDTOOL_RBUILDER_CONFIG_PATH="${BUILDTOOL_RBUILDER_CONFIG_PATH}"
export BUILDTOOL_RBUILDER_COPY_PATH="${BUILDTOOL_RBUILDER_COPY_PATH}"
export BUILDTOOL_RBUILDER_FILTER="${BUILDTOOL_RBUILDER_FILTER}"
export BUILDTOOL_RBUILDER_FILTER_PATH="${BUILDTOOL_RBUILDER_FILTER_PATH}"
export BUILDTOOL_STRIP_BINARIES=${BUILDTOOL_STRIP_BINARIES}
export BUILDTOOL_INCLUDE_BUILDFILES=${BUILDTOOL_INCLUDE_BUILDFILES}

EOF

	# These options were autodetected during setup.
	cat <<EOF >>"${SYSTEM_JOB_SETTINGS}"
export OS_PLATFORM="${OS_PLATFORM}"
export OS_PLATFORM_CODE="${OS_PLATFORM_CODE}"
export OS_TYPE="${OS_TYPE}"
export OS_VERSION="${OS_VERSION}"
export OS_VERSION_CODE="${OS_VERSION_CODE}"
export OS_ARCH="${OS_ARCH}"

export MPLAYER_GUI_MANPAGE_LANGUAGE="${MPLAYER_GUI_MANPAGE_LANGUAGE}"

export SYSTEM_PATH_PKGDIR="${SYSTEM_PATH_PKGDIR}"

export MAKEOPTS="${RUNTIME_MAKEOPTS}"

export KDEDIR="${KDEDIR}"
export KDEDIRS="${KDEDIRS}"
export QTDIR="${QTDIR}"
export QT4DIR="${QT4DIR}"
export QT5DIR="${QT5DIR}"

export BUILDSYS_INSTALL_MODE="${BUILDSYS_INSTALL_MODE}"
export BUILDSYS_INSTALL_GROUP="${BUILDSYS_INSTALL_GROUP}"
export BUILDSYS_INSTALL_USER="${BUILDSYS_INSTALL_USER}"
export BUILDSYS_INSTALL_EVERYTHING=${BUILDSYS_INSTALL_EVERYTHING}

export BUILDSYS_GTK_ICON_CACHE="${BUILDSYS_GTK_ICON_CACHE}"

export BUILDTOOL_SOURCE_MIRROR_GNOME="${BUILDTOOL_SOURCE_MIRROR_GNOME:-$MIRROR_GNOME}"
export BUILDTOOL_SOURCE_MIRROR_KDE="${BUILDTOOL_SOURCE_MIRROR_KDE:-$MIRROR_KDE}"
export BUILDTOOL_SOURCE_MIRROR_XORG="${BUILDTOOL_SOURCE_MIRROR_XORG:-$MIRROR_XORG}"
export BUILDTOOL_SOURCE_MIRROR_SOURCEFORGE="${BUILDTOOL_SOURCE_MIRROR_SOURCEFORGE:-$MIRROR_SOURCEFORGE}"
export BUILDTOOL_SOURCE_MIRROR_GENTOO_DISTFILES="${BUILDTOOL_SOURCE_MIRROR_GENTOO_DISTFILES:-$MIRROR_GENTOO_DISTFILES}"
EOF

}

# Create logfile that can be reused as packages.conf.overwrite.
BUILDSYS_SETUP_CREATE_PKG_CONF_OVERWRITE() {
	local count=0

	cat <<EOF >${SYSTEM_LOGFILE_PKG_CONF_OVERWRITE}
###
### Automatically created logfile
### created by ${PROJECT_NAME} for Slackware ${OS_PLATFORM_CODE}
### Rename this file to '$(basename ${SYSTEM_CONFIG_OVERWRITE})'
### and place the file into ${PROJECT_NAME}'s root directory
### to use this buildorder to install the same packages again.
###
EOF

	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
		let count=count+1

		if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
			echo -n "### ${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]}-${SYSDB_PACKAGE_VERSION[${count}]}${SYSDB_PACKAGE_RELEASE[${count}]}" >>${SYSTEM_LOGFILE_PKG_CONF_OVERWRITE}
			if [ x"${SYSDB_PACKAGE_INSTALLED[${count}]}" != x"" ]; then
				echo -n " (** Update **)" >>${SYSTEM_LOGFILE_PKG_CONF_OVERWRITE}
			fi
			echo "" >>${SYSTEM_LOGFILE_PKG_CONF_OVERWRITE}
			echo "${SYSDB_PACKAGE_GROUP[${count}]}/${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}" >>${SYSTEM_LOGFILE_PKG_CONF_OVERWRITE}
		fi
	done
}

# Save runtime settings to logfile.
BUILDSYS_SETUP_CREATE_SETTINGS_LOGFILE() {

	cat <<EOF >>${SYSTEM_LOGFILE_SETTINGS}
# generic system settings:
$(uname -a)
$(/usr/bin/gcc --version)

# Options for creating 'slack-required' files:
BUILDTOOL_RBUILDER_CREATE                 : ${BUILDTOOL_RBUILDER_CREATE}
BUILDTOOL_RBUILDER_FLAGS                  : ${BUILDTOOL_RBUILDER_FLAGS}
BUILDTOOL_RBUILDER_COPY_PATH              : ${BUILDTOOL_RBUILDER_COPY_PATH}
BUILDTOOL_RBUILDER_CONFIG_PATH            : ${BUILDTOOL_RBUILDER_CONFIG_PATH}
BUILDTOOL_RBUILDER_DATABASE_PATH          : ${BUILDTOOL_RBUILDER_DATABASE_PATH}
BUILDTOOL_RBUILDER_FILTER                 : ${BUILDTOOL_RBUILDER_FILTER}
BUILDTOOL_RBUILDER_FILTER_PATH            : ${BUILDTOOL_RBUILDER_FILTER_PATH}

# Options for customize the buildtools:
BUILDTOOL_INCLUDE_BUILDFILES              : ${BUILDTOOL_INCLUDE_BUILDFILES}
BUILDTOOL_STRIP_BINARIES                  : ${BUILDTOOL_STRIP_BINARIES}

# Buildsystem options:
OPTION_REMOVE_ALL_PKG_BEFORE_INSTALL      : ${OPTION_REMOVE_ALL_PKG_BEFORE_INSTALL}
OPTION_AUTO_REMOVE_PACKAGES               : ${OPTION_AUTO_REMOVE_PACKAGES}
OPTION_REMOVE_EXTRA_PACKAGES              : ${OPTION_REMOVE_EXTRA_PACKAGES}
OPTION_USE_NTP                            : ${OPTION_USE_NTP}
OPTION_CREATE_PKGMD5                      : ${OPTION_CREATE_PKGMD5}
OPTION_CREATE_PKGTXT                      : ${OPTION_CREATE_PKGTXT}
OPTION_DELETE_INSTALLED_SOURCES           : ${OPTION_DELETE_INSTALLED_SOURCES}
OPTION_BUILD_EXTERNAL_SOURCES             : ${OPTION_BUILD_EXTERNAL_SOURCES}
OPTION_BUILD_GERMAN_PACKAGES              : ${OPTION_BUILD_GERMAN_PACKAGES}
OPTION_BUILD_MPLAYER_GUI_LANG             : ${OPTION_BUILD_MPLAYER_GUI_LANG}

BUILDSYS_INSTALL_GROUP                    : ${BUILDSYS_INSTALL_GROUP}
BUILDSYS_INSTALL_MODE                     : ${BUILDSYS_INSTALL_MODE}
BUILDSYS_INSTALL_USER                     : ${BUILDSYS_INSTALL_USER}
BUILDSYS_INSTALL_EVERYTHING               : ${BUILDSYS_INSTALL_EVERYTHING}

EOF
}

# Save md5sum of sudoers file.
BUILDSYS_SETUP_CREATE_SUDOERS_BACKUP() {
	if [ -r /etc/sudoers ]; then
		md5sum /etc/sudoers >${SYSTEM_PATH_LOGFILES_SYSTEM}/etc_sudoers.md5
		cat /etc/sudoers >${SYSTEM_PATH_LOGFILES_SYSTEM}/etc_sudoers.orig
	else
		echo "ERROR!" >${SYSTEM_PATH_LOGFILES_SYSTEM}/etc_sudoers.md5
	fi
}

# Create a list of user selected packages.
BUILDSYS_SETUP_CREATE_USER_MODIFICATIONS_LIST() {

	local count=0
	local count_custom=0
	local text=""
	local mode=""
	local check=true
	local skip_category=""

	FUNC_PROGRESS_BOX "Checking for user modifications..."

	rm -f ${SYSTEM_LOGFILE_USERSELECT_RUNTIME}

	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
		let count=count+1
		check=false
		if ${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]}; then
			mode="Removed"
			check=true
			for blacklist in ${SYSTEM_CATEGORY_BLACKLIST}; do
				if [ x"${SYSDB_PACKAGE_GROUP[${count}]}" == x"${blacklist}" ]; then
					mode="Blacklisted"
					break
				fi
			done
		else
			if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
				if ${SYSDB_PACKAGE_DEPOPT_SELECT[${count}]}; then
					mode="Required"
					# Ignore packages that got added during
					# dependency checking.
					check=false
				else
					if ! ${SYSDB_PACKAGE_AUTOSELECT[${count}]}; then
						mode="Added"
						check=true
					fi
				fi
			fi
		fi

		if ${check}; then
			let count_custom=count_custom+1
			text="0000${count_custom}"
			text="[${text:${#text}-4:4}] ${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]}${SPACEBAR}"
			text="${text:0:40} ${mode}"
			echo "${text}" >>"${SYSTEM_LOGFILE_USERSELECT_RUNTIME}"
		fi
	done

	if test ${count_custom} -gt 0; then
		sed -i  "1 iCustom build order modifications (${count_custom} total):" "${SYSTEM_LOGFILE_USERSELECT_RUNTIME}"
		sed -i  "1 a${RULER2}" "${SYSTEM_LOGFILE_USERSELECT_RUNTIME}"

		echo "${RULER2}" >>"${SYSTEM_LOGFILE_USERSELECT_RUNTIME}"
		echo "" >>"${SYSTEM_LOGFILE_USERSELECT_RUNTIME}"
	fi
}

# Warn user about source packages to be deleted.
BUILDSYS_SETUP_WARN_USER_DELETE_SOURCE_PACKAGES() {

	if ${OPTION_DELETE_INSTALLED_SOURCES}; then

		dialog	--title "${PROJECT_NAME} - DELETE INSTALLED SOURCE FILES" \
				--backtitle "${PROJECT_BACKTITLE}" \
				--yes-label "${BUTTON_LABEL_YES}" \
				--no-label "${BUTTON_LABEL_NO}" \
				--ok-label "${BUTTON_LABEL_YES}" \
				--cancel-label "${BUTTON_LABEL_NO}" \
				--defaultno \
				--yesno "\n\
You have configured ${PROJECT_NAME} to delete the source files after the package was installed succesfully.\n\n\
Note: This is only recommended when you are running out of disk space.\n\n\
Do you really want to delete the source packages?" 12 75 2>/dev/null

    # Are we sure we do not want to build Mono?
    if [ $? = 0 ]; then
      OPTION_DELETE_INSTALLED_SOURCES=true
    else
      OPTION_DELETE_INSTALLED_SOURCES=false
    fi

  fi

}

# Check if we want to build everything and we do not have Retries enabled.
BUILDSYS_SETUP_WARN_USER_DISTCC_RETRIES() {

	if ! ${RUNTIME_OPTION_DEBUG_MODE}; then
		return 0
	fi

	if [ x"$(ps -e | grep distccd)" == x"" ]; then
		return 0
	fi

	if test ${OPTION_MULTIPLE_CPU_RETRIES:-0} -gt 0; then
		return 0
	fi

	dialog	--title "${PROJECT_NAME} - WARNING" \
			--backtitle "${PROJECT_BACKTITLE}" \
			--cr-wrap \
			--yes-label "${BUTTON_LABEL_YES}" \
			--no-label "${BUTTON_LABEL_NO}" \
			--ok-label "${BUTTON_LABEL_YES}" \
			--cancel-label "${BUTTON_LABEL_NO}" \
			--yesno "\n\
You have selected to build all packages included in this buildsystem. \
This will take a very long time even when using distcc for distributed computing using multiple computers.\n\
\n\
Since distcc may fail to compile sometimes it is recommended to set the following option in the buildsystem configuration file:\n\
\n\
  OPTION_MULTIPLE_CPU_RETRIES=2\n\
\n\
This may help to continue the build if distcc has failed to compile a package in case of code that is not compatible with disttc.\n\
This will not help if the build has failed because of missing dependencies or other source code related problems.\n\
\n\
Enable 'OPTION_MULTIPLE_CPU_RETRIES' now (Recommended)?\n\
" 21 75

	DIALOG_KEYCODE=$?

	case ${DIALOG_KEYCODE} in
		0)	OPTION_MULTIPLE_CPU_RETRIES=2
			;;
	esac

	return 0
}


# Warn user about installed icon cache files.
BUILDSYS_SETUP_WARN_USER_UPDATE_ICON_CACHE() {

	## This code will be reused in the installtgz.sh script...

	## LABEL:WARNGIC

	if [ x"${OPTION_UPDATE_GTK_ICON_CACHE:-false}" != x"true" -a x"${BUILDSYS_GTK_ICON_CACHE:-ask}" == x"ask" ]; then

		# Do we have some icon cache file already installed?
		check=$(find /usr/share/icons/ -type f -name "icon-theme.cache")
		if [ x"${check}" != x"" ]; then

			dialog	--title "${PROJECT_NAME} - WARNING: GTK ICON CACHE" \
					--backtitle "${PROJECT_BACKTITLE}" \
					--yes-label "${BUTTON_LABEL_UPDATE}" \
					--no-label "${BUTTON_LABEL_CONTINUE}" \
					--ok-label "${BUTTON_LABEL_UPDATE}" \
					--cancel-label "${BUTTON_LABEL_CONTINUE}" \
					--extra-button \
					--extra-label "${BUTTON_LABEL_REMOVE}" \
					--yesno "\n\
A GTK+ icon cache includes information about all icons for the selected icon theme and can improove loading required icons when you start a GTK+ based application.\n\
\n\
You have selected not to update the GTK icon cache but you have already installed some icon cache files on your system.\n\
It is recommended to enable updating the icon cache for all icon themes or some icons may be missing when you try to launch applications based on GTK+. Your other choice would be to delete existing cache files.\n\
\n\
Select '${BUTTON_LABEL_UPDATE}' to update the GTK icon cache, select '${BUTTON_LABEL_REMOVE}' to delete existing icon cache files or select '${BUTTON_LABEL_CONTINUE}' to keep the existing cache files and continue on your own risk.\n\
" 20 75 2>/dev/null

			DIALOG_KEYCODE=$?

			# Update GTK icon cache?
			if test ${DIALOG_KEYCODE} -eq 0; then
				OPTION_UPDATE_GTK_ICON_CACHE=true
				BUILDSYS_GTK_ICON_CACHE="update"
			else
				if test ${DIALOG_KEYCODE} -eq 3; then
					OPTION_UPDATE_GTK_ICON_CACHE=false
					BUILDSYS_GTK_ICON_CACHE="remove"
				fi
			fi
		fi
	fi

	## LABEL:WARNGIC

	## This code will be reused in the installtgz.sh script...

}

# Caclulate remaing build time.
BUILDSYS_SETUP_INIT_STATISTICS() {
	FUNC_CALCULATE_ETA
	let SELECTED_PACKAGES_ETA_REMAINING=SELECTED_PACKAGES_ETA
	FUNC_COUNT_ENABLED_PACKAGES
}

# Setup cache dir.
BUILDSYS_SETUP_INIT_PKGDIR() {

	# Use the cache? Yes, make sure we have the directory...
	if ${OPTION_MOVE_INSTALLED_PACKAGES}; then
		if [ x"${SYSTEM_PATH_PKGDIR}" != x"" ]; then
			mkdir -p "${SYSTEM_PATH_PKGDIR}"
		fi
	else
		SYSTEM_PATH_PKGDIR=""
	fi

	return 0

}

# Display the packages to be installed
BUILDSYS_LAST_WARNING_DISPLAY_PACKAGES() {

	local count=0
	local count_install=0
	local name=""

	FUNC_PROGRESS_BOX "Analyzing packages..."

	rm -f ${DIALOG_TEMP_FILE1}
	rm -f ${DIALOG_TEMP_FILE2}

	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
		let count=count+1
		if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
			let count_install=count_install+1
			name="    ${SYSDB_PACKAGE_NAME[${count}]}${SPACEBAR}"
			name="${name:0:37} ${SYSDB_PACKAGE_VERSION[${count}]}${SYSDB_PACKAGE_RELEASE[${count}]}${SPACEBAR}"
			echo "${name:0:57} ${SYSDB_PACKAGE_GROUP[${count}]}" >>${DIALOG_TEMP_FILE1}
		fi
	done

	sort ${DIALOG_TEMP_FILE1} >${DIALOG_TEMP_FILE2}
	sed -i  "1 iPackages that will be installed (${count_install} total):" ${DIALOG_TEMP_FILE2}
	sed -i  "1 aPackage name                          Version             Group" ${DIALOG_TEMP_FILE2}
	sed -i  "2 a${RULER2}" ${DIALOG_TEMP_FILE2}

	echo "${RULER2}" >>${DIALOG_TEMP_FILE2}
	echo "" >>${DIALOG_TEMP_FILE2}

	dialog	--title "${PROJECT_NAME} - PACKAGES TO INSTALL" \
			--backtitle "${PROJECT_BACKTITLE}" \
			--cr-wrap \
			--exit-label "${BUTTON_LABEL_OK}" \
			--textbox ${DIALOG_TEMP_FILE2} 20 75

	rm -f ${DIALOG_TEMP_FILE1}
	rm -f ${DIALOG_TEMP_FILE2}

	return 0

}

# Display the sources to be downloaded.
BUILDSYS_LAST_WARNING_DISPLAY_MISSING_SOURCES() {

	local count=0
	local count_download=0
	local text=""
	local source=""

	FUNC_PROGRESS_BOX "Analyzing packages..."

	rm -f ${DIALOG_TEMP_FILE1}

	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
		let count=count+1
		if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
			if ! ${SYSDB_PACKAGE_SOURCES_CHECKED[${count}]}; then
				for source in ${SYSDB_PACKAGE_SOURCE[${count}]//@@@/ } ; do
					if [ ! -e ${SYSTEM_PATH_SOURCES}/${SYSDB_PACKAGE_GROUP[${count}]}/${source} ]; then
						let count_download=count_download+1
						text="0000${count_download}"
						echo "    [${text:${#text}-4:4}] ${SYSDB_PACKAGE_NAME[${count}]}" >>${DIALOG_TEMP_FILE1}
						echo "          URL   :${SYSDB_PACKAGE_URL1[${count}]}" >>${DIALOG_TEMP_FILE1}
						echo "          SOURCE:${source}" >>${DIALOG_TEMP_FILE1}
						echo " " >>${DIALOG_TEMP_FILE1}
					fi
				done
			fi
		fi
	done

	sed -i  "1 iPackages that will be downloaded (${count_download} total):" ${DIALOG_TEMP_FILE1}
	sed -i  "1 a${RULER2}" ${DIALOG_TEMP_FILE1}

	echo "${RULER2}" >>${DIALOG_TEMP_FILE1}
	echo "" >>${DIALOG_TEMP_FILE1}

	dialog	--title "${PROJECT_NAME} - PACKAGES TO DOWNLOAD" \
			--backtitle "${PROJECT_BACKTITLE}" \
			--cr-wrap \
			--exit-label "${BUTTON_LABEL_OK}" \
			--textbox ${DIALOG_TEMP_FILE1} 20 75

	rm -f ${DIALOG_TEMP_FILE1}

	return 0

}

# Display the packages to be updated.
BUILDSYS_LAST_WARNING_DISPLAY_UPDATE_PACKAGES() {

	FUNC_PROGRESS_BOX "Analyzing packages..."
	FUNC_WRITE_UPDATED_PACKAGES_TO_LOGFILE "${DIALOG_TEMP_FILE1}"

	dialog	--title "${PROJECT_NAME} - PACKAGES TO BE UPDATED" \
			--backtitle "${PROJECT_BACKTITLE}" \
			--cr-wrap \
			--exit-label "${BUTTON_LABEL_OK}" \
			--textbox "${DIALOG_TEMP_FILE1}" 20 75

	rm -f "${DIALOG_TEMP_FILE1}"

	return 0

}

# Display the packages added/removed by user.
BUILDSYS_LAST_WARNING_DISPLAY_CUSTOM_CHANGES() {

	dialog	--title "${PROJECT_NAME} - CUSTOM CHANGES" \
			--backtitle "${PROJECT_BACKTITLE}" \
			--cr-wrap \
			--exit-label "${BUTTON_LABEL_OK}" \
			--textbox ${SYSTEM_LOGFILE_USERSELECT_RUNTIME} 20 75

	return 0

}

# Last warning.
BUILDSYS_LAST_WARNING() {
	local count=0
	local check=true
	local gcc_text1=""
	local gcc_text_plain1=""
	local gcc_text2=""
	local gcc_text_plain2=""
	local rbuilder_text1=""
	local rbuilder_text2=""
	local multicpu_text=""
	local eta_min_total=0
	local eta_min=0
	local eta_hour=0
	local spacer=""

	FUNC_PROGRESS_BOX "Setting up build environment..."

	# Get runtime settings to display in dialog.
	if [ -z ${SLACK_ARCH} ]; then
		gcc_text1="[ ]"
		gcc_text_plain1="default"
	else
		gcc_text1="[X]"
		gcc_text_plain1="${SLACK_ARCH}"
	fi

	if [ -z ${SLACK_OPT} ]; then
		gcc_text2="[ ]"
		gcc_text_plain2="default"
	else
		gcc_text2="[X]"
		gcc_text_plain2="${SLACK_OPT}"
	fi

	# Create 'slack-required' files?
	if ${BUILDTOOL_RBUILDER_CREATE}; then
		rbuilder_text1="Enabled with RBUILDER_FLAGS=${BUILDTOOL_RBUILDER_FLAGS}"
		if ${BUILDTOOL_RBUILDER_FILTER}; then
			rbuilder_text2="${BUILDTOOL_RBUILDER_FILTER_PATH:0:40}"
		fi
	else
		rbuilder_text1="Disabled"
		rbuilder_text2=""
	fi

	# Define distcc settings.
	if ${OPTION_MULTIPLE_CPU_COMPILE}; then
		multicpu_text="[X]"
	else
		multicpu_text="[ ]"
	fi

	# Initializing when not restarting a previous setup.
	if ! ${RUNTIME_SYSTEM_RESTART}; then
		# Fill up the logfile with more data.
		cat <<EOF >>${SYSTEM_LOGFILE_SETTINGS}

Create 'slack-required' files             : ${rbuilder_text1}
Filter 'slack-required' files             : ${rbuilder_text2}
GCC Compiler (\$SLACK_ARCH)                : ${gcc_text_plain1}
GCC Compiler (\$SLACKOPT)                  : ${gcc_text_plain2}
Using multiple CPUs/computers             : ${OPTION_MULTIPLE_CPU_COMPILE} / ${RUNTIME_MAKEOPTS}
EOF
	fi

	# Check source packages.
	FUNC_PROGRESS_BOX "Calculating: Source packages..."
	FUNC_CHECK_SOURCE_PACKAGES

	# Check packages to update.
	FUNC_PROGRESS_BOX "Calculating: Package updates..."
	FUNC_CHECK_UPDATE_PACKAGES

	# Check packages to remove.
	FUNC_PROGRESS_BOX "Calculating: Removing packages..."
	FUNC_CHECK_REMOVE_PACKAGES

	# Check packages to install.
	FUNC_PROGRESS_BOX "Calculating: Install packages..."
	FUNC_CHECK_INSTALL_PACKAGES

	# This is the last warning...
	while [ true ] ; do

		cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "${PROJECT_NAME} - SYSTEM CHECK FINISHED" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--help-button \\
		--help-label "${BUTTON_LABEL_HELP}" \\
		--ok-label "${BUTTON_LABEL_OK}" \\
		--cancel-label "${BUTTON_LABEL_EXIT}" \\
		--cr-wrap \\
		--menu "\\
Selected platform      : ${OS_PLATFORM_CODE}\n\\
EOF

		let eta_min_total=SELECTED_PACKAGES_ETA/60
		let eta_hour=eta_min_total/60
		let eta_min=eta_min_total-eta_hour*60
		cat <<EOF >>${DIALOG_SCRIPT}
ETA (for a 3GHz CPU)   : ${eta_min_total}min. (${eta_hour}h${eta_min}m)\n\\
EOF

		cat <<EOF >>${DIALOG_SCRIPT}
Packages to...  : download:${RUNTIME_MISSING_SOURCE_PACKAGES:-0}  update:${RUNTIME_UPDATE_PACKAGES:-0}  remove:${RUNTIME_REMOVE_PACKAGES:-0}  install:${RUNTIME_INSTALL_PACKAGES}\n\
GCC Compiler    : ${gcc_text1} SLACK_ARCH  ${gcc_text2} SLACK_OPT  ${multicpu_text} MULTICPU\n\
RBuilder        : ${rbuilder_text1}\n\\
EOF
		if [ x"${rbuilder_text2}" != x"" ]; then
			cat <<EOF >>${DIALOG_SCRIPT}
RBuilder filter : ${rbuilder_text2}\n\\
EOF
		else
			cat <<EOF >>${DIALOG_SCRIPT}
\n\\
EOF
		fi


		spacer="\n"

		cat <<EOF >>${DIALOG_SCRIPT}
${spacer}\\
\nSelect one of the following options:" 20 75 5 \\
"START"  "Start building selected packages" \\
EOF

		if [ ${RUNTIME_MISSING_SOURCE_PACKAGES} -gt 0 ]; then
			cat <<EOF >>${DIALOG_SCRIPT}
"DOWNLOADS" "Display a list of packages to be downloaded" \\
EOF
		fi

		if [ ${RUNTIME_UPDATE_PACKAGES} -gt 0 -o ${RUNTIME_REMOVE_PACKAGES} -gt 0 ]; then
			cat <<EOF >>${DIALOG_SCRIPT}
"UPDATES" "Display a list of packages to be updated/removed" \\
EOF
		fi

		if [ -e ${SYSTEM_LOGFILE_USERSELECT_RUNTIME} ]; then
			cat <<EOF >>${DIALOG_SCRIPT}
"USERPKG" "Display a list of user selected packages" \\
EOF
		fi

		cat <<EOF >>${DIALOG_SCRIPT}
"PACKAGES" "Display a list of packages to be installed" \\
EOF

		cat <<EOF >>${DIALOG_SCRIPT}
 2> "${DIALOG_RETURN_VALUE}"
EOF

		# Start the script and offer the version select dialog.
		. "${DIALOG_SCRIPT}"

		DIALOG_KEYCODE=$?
		DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}")
		rm -f "${DIALOG_SCRIPT}"
		rm -f "${DIALOG_RETURN_VALUE}"

		# Need some help?
		if test ${DIALOG_KEYCODE} -eq 2; then
			dialog	--title "${PROJECT_NAME} - HELP" \
					--backtitle "${PROJECT_BACKTITLE}" \
					--cr-wrap \
					--msgbox "\n\
This is (nearly) the last warning before ${PROJECT_NAME} (maybe) will start building packages for you. Some notes:\n\n
'ETA' is just a guess and is based on a Intel CoreDuo 1.73GHz CPU. \
If you have some other CPUs then this is just a first calculation. \
The ETA will be corrected while compiling the selected packages.\n\n
If an error occures you should check the logfiles in build/logfiles. \
You can skip the package that failed to build by edit packages.job and remove the very first line of the file. \
Save the file and restart ${PROJECT_NAME} using the following command (${PROJECT_NAME} will try to resume the build process):\n
\n
      sh ${PROJECT_SCRIPT_NAME}" 20 75
			continue
		fi

		# 'CANCEL' or 'ESC' ?
		if test ${DIALOG_KEYCODE} -ne 0; then
			return 1
		fi

		case ${DIALOG_REPLY} in
			"DOWNLOADS")	BUILDSYS_LAST_WARNING_DISPLAY_MISSING_SOURCES
							continue
							;;

			"UPDATES")		BUILDSYS_LAST_WARNING_DISPLAY_UPDATE_PACKAGES
							continue
							;;

			"USERPKG")		BUILDSYS_LAST_WARNING_DISPLAY_CUSTOM_CHANGES
							continue
							;;

			"PACKAGES")		BUILDSYS_LAST_WARNING_DISPLAY_PACKAGES
							continue
							;;

			"START")		break
							;;
		esac

	done

  return 0
}

# Just a separator...
SYS____________________BUILD_PACKAGES() {
	true
}

# Save configuration.
BUILDSYS_SAVE_CONFIGURATION() {

	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_RBUILDER_CREATE"					"${OPTION_RBUILDER_CREATE}"

	if ${OPTION_RBUILDER_CREATE}; then
		CONFIG_EDITOR_SAVE_INPUT_OPTION	"OPTION_RBUILDER_FLAGS"						"${OPTION_RBUILDER_FLAGS}"
		CONFIG_EDITOR_SAVE_INPUT_OPTION	"OPTION_RBUILDER_DATABASE_PATH"				"${OPTION_RBUILDER_DATABASE_PATH}"
		CONFIG_EDITOR_SAVE_INPUT_OPTION	"OPTION_RBUILDER_CONFIG_PATH"				"${OPTION_RBUILDER_CONFIG_PATH}"
		CONFIG_EDITOR_SAVE_INPUT_OPTION	"OPTION_RBUILDER_COPY_PATH"					"${OPTION_RBUILDER_COPY_PATH}"
		CONFIG_EDITOR_SAVE_MENU_OPTION	"OPTION_RBUILDER_FILTER"					"${OPTION_RBUILDER_FILTER}"
		CONFIG_EDITOR_SAVE_INPUT_OPTION	"OPTION_RBUILDER_FILTER_PATH"				"${OPTION_RBUILDER_FILTER_PATH}"
	fi

	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_PACKAGE_TAG"						"${PACKAGE_TAG}"
	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_PACKAGE_EXT"						"${BUILDTOOL_PACKAGE_EXTENSION}"

	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_USE_NTP"							"${OPTION_USE_NTP}"
	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_CREATE_PKGMD5"						"${OPTION_CREATE_PKGMD5}"
	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_CREATE_PKGTXT"						"${OPTION_CREATE_PKGTXT}"
	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_FIND_OPTIONAL_PACKAGES"				"${OPTION_FIND_OPTIONAL_PACKAGES}"

	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_REMOVE_ALL_PKG_BEFORE_INSTALL"		"${OPTION_REMOVE_ALL_PKG_BEFORE_INSTALL}"
	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_AUTO_REMOVE_PACKAGES"				"${OPTION_AUTO_REMOVE_PACKAGES}"
	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_REMOVE_EXTRA_PACKAGES"				"${OPTION_REMOVE_EXTRA_PACKAGES}"
	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_STRIP_BINARIES"						"${OPTION_STRIP_BINARIES}"
	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_INCLUDE_BUILDFILES"					"${OPTION_INCLUDE_BUILDFILES}"
	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_UPDATE_GTK_ICON_CACHE"				"${OPTION_UPDATE_GTK_ICON_CACHE}"
	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_ONLY_SHOW_APPS_IN"					"${OPTION_ONLY_SHOW_APPS_IN}"

	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_BUILD_GERMAN_PACKAGES"				"${OPTION_BUILD_GERMAN_PACKAGES}"
	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_BUILD_MPLAYER_GUI_LANG"				"${OPTION_BUILD_MPLAYER_GUI_LANG}"
	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_BUILD_EXTERNAL_SOURCES"				"${OPTION_BUILD_EXTERNAL_SOURCES}"


	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_DELETE_INSTALLED_SOURCES"			"${OPTION_DELETE_INSTALLED_SOURCES}"

	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_DEBUG_OVERWRITE_CHECK"				"${OPTION_DEBUG_OVERWRITE_CHECK}"
	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_DEBUG_OVERWRITE_CHECK_HALT"			"${OPTION_DEBUG_OVERWRITE_CHECK_HALT}"
	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_MOVE_INSTALLED_PACKAGES"			"${OPTION_MOVE_INSTALLED_PACKAGES}"
	CONFIG_EDITOR_SAVE_INPUT_OPTION		"OPTION_MOVE_INSTALLED_PACKAGES_TO_PATH"	"${OPTION_MOVE_INSTALLED_PACKAGES_TO_PATH}"

	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_VERBOSE_MODE"						"${OPTION_VERBOSE_MODE}"

	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_MULTIPLE_CPU_COMPILE"				"${OPTION_MULTIPLE_CPU_COMPILE}"
	CONFIG_EDITOR_SAVE_INPUT_OPTION		"OPTION_MULTIPLE_CPU_RETRIES"				"${OPTION_MULTIPLE_CPU_RETRIES}"
	CONFIG_EDITOR_SAVE_MENU_OPTION		"OPTION_SET_MAX_COMPILE_JOBS"				"${OPTION_SET_MAX_COMPILE_JOBS}"
	CONFIG_EDITOR_SAVE_INPUT_OPTION		"OPTION_MAX_COMPILE_JOBS"					"${OPTION_MAX_COMPILE_JOBS}"

	return 0
}

# Copy database values to logfile.
BUILDSYS_SAVE_DATABASE_TO_LOGFILE() {

	# Only save the database when we have not restarted the buildsystem.
	if ! ${RUNTIME_SYSTEM_RESTART}; then

		local count=0

		rm -f ${SYSTEM_LOGFILE_DATABASE}

		FUNC_PROGRESS_BOX "Writing database to logfile..."
		FUNC_INIT_PROGRESS_BAR

		while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
			let count=count+1
			FUNC_DRAW_PROGRESS_BAR ${count} ${SYSDB_PACKAGE_COUNT}
			cat <<EOF >>${SYSTEM_LOGFILE_DATABASE}
${SYSDB_PACKAGE_NAME[${count}]},\
${SYSDB_PACKAGE_NAME_SOURCE[${count}]},\
${SYSDB_PACKAGE_SEARCH_NAME[${count}]},\
${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]},\
${SYSDB_PACKAGE_GROUP[${count}]},\
${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]},\
${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]},\
${SYSDB_PACKAGE_BUILDTOOL_SCRIPT[${count}]},\
${SYSDB_PACKAGE_VERSION[${count}]},\
${SYSDB_PACKAGE_VERSION_SOURCE[${count}]},\
${SYSDB_PACKAGE_RELEASE[${count}]},\
${SYSDB_PACKAGE_RELEASE_SOURCE[${count}]},\
${SYSDB_PACKAGE_ARCH[${count}]},\
${SYSDB_PACKAGE_TYPE[${count}]},\
${SYSDB_PACKAGE_TAG[${count}]},\
${SYSDB_PACKAGE_BUILD[${count}]},\
${SYSDB_PACKAGE_ETA[${count}]},\
${SYSDB_PACKAGE_FLAGS[${count}]},\
${SYSDB_PACKAGE_REPLACE[${count}]},\
${SYSDB_PACKAGE_REMOVE[${count}]},\
${SYSDB_PACKAGE_EXTERNAL[${count}]},\
${SYSDB_PACKAGE_EXTRAPKG[${count}]},\
${SYSDB_PACKAGE_OPTIONAL[${count}]},\
${SYSDB_PACKAGE_OPTIONAL_FOR_PACKAGE[${count}]},\
${SYSDB_PACKAGE_REQUIRED[${count}]},\
${SYSDB_PACKAGE_REQUIRED_FOR_PACKAGE[${count}]},\
${SYSDB_PACKAGE_REBUILD[${count}]},\
${SYSDB_PACKAGE_REBUILD_FOR_PACKAGE[${count}]},\
${SYSDB_PACKAGE_ADDPKG[${count}]},\
${SYSDB_PACKAGE_ENABLED[${count}]},\
${SYSDB_PACKAGE_DISABLED[${count}]},\
${SYSDB_PACKAGE_AUTOSELECT[${count}]},\
${SYSDB_PACKAGE_REMOVED_BY_USER[${count}]},\
${SYSDB_PACKAGE_LANG[${count}]},\
${SYSDB_PACKAGE_INSTALLED[${count}]},\
${SYSDB_PACKAGE_INSTALLED_VERSION[${count}]},\
${SYSDB_PACKAGE_INSTALLED_RELEASE[${count}]},\
${SYSDB_PACKAGE_INSTALLED_ARCH[${count}]},\
${SYSDB_PACKAGE_INSTALLED_BUILD[${count}]},\
${SYSDB_PACKAGE_INSTALLED_TAG[${count}]},\
${SYSDB_PACKAGE_DESC[${count}]},\
${SYSDB_PACKAGE_STATUS[${count}]},\
${SYSDB_PACKAGE_SOURCES_CHECKED[${count}]},\
${SYSDB_PACKAGE_SKIP_MD5[${count}]},\
${SYSDB_PACKAGE_SOURCE[${count}]},\
${SYSDB_PACKAGE_URL1[${count}]},\
${SYSDB_PACKAGE_URL2[${count}]},\
${SYSDB_PACKAGE_URL3[${count}]},\
${SYSDB_PACKAGE_URL4[${count}]}
EOF
		done

		# Clear the progress bar.
		FUNC_CLEAR_PROGRESS_BAR

	fi

}

# Backup build settings.
BUILDSYS_SAVE_BUILD_SETTINGS() {
	local logfile=""

	case ${RUNTIME_SYSTEM_RESTART} in
		false)	logfile="${SYSTEM_LOGFILE_BUILDSYS_SETTINGS}.startup";;
		*)		logfile="${SYSTEM_LOGFILE_BUILDSYS_SETTINGS}.restart";;
	esac

	echo $(uname -a) >${logfile}
	echo $(gcc --version) >>${logfile}

	cat <<EOF >>${logfile}
OPTION_BUILDSYS_FIRSTTIME_USER        : ${OPTION_BUILDSYS_FIRSTTIME_USER}
OPTION_BUILD_GERMAN_PACKAGES          : ${OPTION_BUILD_GERMAN_PACKAGES}
OPTION_BUILD_MPLAYER_GUI_LANG         : ${OPTION_BUILD_MPLAYER_GUI_LANG}
OPTION_BUILD_EXTERNAL_SOURCES         : ${OPTION_BUILD_EXTERNAL_SOURCES}
OPTION_RBUILDER_CREATE                : ${OPTION_RBUILDER_CREATE}
OPTION_RBUILDER_FLAGS                 : ${OPTION_RBUILDER_FLAGS}
OPTION_RBUILDER_DATABASE_PATH         : ${OPTION_RBUILDER_DATABASE_PATH}
OPTION_RBUILDER_CONFIG_PATH           : ${OPTION_RBUILDER_CONFIG_PATH}
OPTION_RBUILDER_COPY_PATH             : ${OPTION_RBUILDER_COPY_PATH}
OPTION_RBUILDER_FILTER                : ${OPTION_RBUILDER_FILTER}
OPTION_RBUILDER_FILTER_PATH           : ${OPTION_RBUILDER_FILTER_PATH}
OPTION_PACKAGE_TAG                    : ${OPTION_PACKAGE_TAG}
OPTION_USE_NTP                        : ${OPTION_USE_NTP}
OPTION_USE_NTP_SERVER                 : ${OPTION_USE_NTP_SERVER}
OPTION_CREATE_PKGMD5                  : ${OPTION_CREATE_PKGMD5}
OPTION_CREATE_PKGTXT                  : ${OPTION_CREATE_PKGTXT}
OPTION_FIND_OPTIONAL_PACKAGES         : ${OPTION_FIND_OPTIONAL_PACKAGES}
OPTION_VERBOSE_MODE                   : ${OPTION_VERBOSE_MODE}
OPTION_MULTIPLE_CPU_COMPILE           : ${OPTION_MULTIPLE_CPU_COMPILE}
OPTION_SET_MAX_COMPILE_JOBS           : ${OPTION_SET_MAX_COMPILE_JOBS}
OPTION_MAX_COMPILE_JOBS               : ${OPTION_MAX_COMPILE_JOBS}
OPTION_MULTIPLE_CPU_RETRIES           : ${OPTION_MULTIPLE_CPU_RETRIES}
OPTION_GCC_RETRIES                    : ${OPTION_GCC_RETRIES}
OPTION_REMOVE_ALL_PKG_BEFORE_INSTALL  : ${OPTION_REMOVE_ALL_PKG_BEFORE_INSTALL}
OPTION_AUTO_REMOVE_PACKAGES           : ${OPTION_AUTO_REMOVE_PACKAGES}
OPTION_REMOVE_EXTRA_PACKAGES          : ${OPTION_REMOVE_EXTRA_PACKAGES}
OPTION_INCLUDE_BUILDFILES             : ${OPTION_INCLUDE_BUILDFILES}
OPTION_STRIP_BINARIES                 : ${OPTION_STRIP_BINARIES}
OPTION_UPDATE_GTK_ICON_CACHE          : ${OPTION_UPDATE_GTK_ICON_CACHE}
OPTION_ONLY_SHOW_APPS_IN              : ${OPTION_ONLY_SHOW_APPS_IN}
OPTION_SKIP_MISSING_SOURCE_PACKAGES   : ${OPTION_SKIP_MISSING_SOURCE_PACKAGES}
OPTION_DELETE_INSTALLED_SOURCES       : ${OPTION_DELETE_INSTALLED_SOURCES}
OPTION_DEBUG_OVERWRITE_CHECK          : ${OPTION_DEBUG_OVERWRITE_CHECK}
OPTION_DEBUG_OVERWRITE_HALT           : ${OPTION_DEBUG_OVERWRITE_HALT}
OPTION_MOVE_INSTALLED_PACKAGES        : ${OPTION_MOVE_INSTALLED_PACKAGES}
OPTION_MOVE_INSTALLED_PACKAGES_TO_PATH: ${OPTION_MOVE_INSTALLED_PACKAGES_TO_PATH}
MIRROR_GNOME                          : ${MIRROR_GNOME}
MIRROR_KDE                            : ${MIRROR_KDE}
MIRROR_XORG                           : ${MIRROR_XORG}
MIRROR_SOURCEFORGE                    : ${MIRROR_SOURCEFORGE}
MIRROR_GENTOO_DISTFILES               : ${MIRROR_GENTOO_DISTFILES}
EOF

}

# Download missing sources.
BUILDSYS_DOWNLOAD_MISSING_SOURCES() {
	local count=0
	local source=""
	local download_mode=""
	local count_missing_sources=0
	local default_item="SHOW"
	local cancel=false
	local text1=""
	local text2=""
	local package_count=0
	local count_incomplete_sources=0
	local count_incomplete_sources_remaining=0

	FUNC_CLEAR_SCREEN

	while [ true ]; do

		cat <<EOF >>${SYSTEM_LOGFILE_DOWNLOADS}

${RULER1}
$(date +%c)
Downloading missing source-packages...
${RULER1}
EOF

		# Show some info...
		FUNC_PROGRESS_BOX "Checking for missing sources..."

		# This is only a quick-check if all source package are already there.
		# We check the MD5SUM later to speed up a system-restart.
		count_incomplete_sources=0
		count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
			let count=count+1
			if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
				if ! ${SYSDB_PACKAGE_SOURCES_CHECKED[${count}]}; then
					export SOURCE_PATH=${SYSTEM_PATH_SOURCES}/${SYSDB_PACKAGE_GROUP[${count}]}
					if ( cd ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]} \
							&& sh ${SYSDB_PACKAGE_BUILDTOOL_SCRIPT[${count}]} -q -C ); then
						continue
					fi
					# The sources for the current package are not there...
					let count_incomplete_sources=count_incomplete_sources+1
				fi
			fi
		done

		# Any buildtools with missing source packages?
		if [ ${count_incomplete_sources} -eq 0 ]; then
			# No, exit...
			cancel=false
			break
		fi

		FUNC_CLEAR_SCREEN
		echo "Downloading missing source packages..."
		echo "Please wait..."

		# Set the counter for remaining packages.
		let count_missing_sources=0
		let count_incomplete_sources_remaining=count_incomplete_sources
		let package_count=1
		text1="0000${count_incomplete_sources}"
		text1="${text1:${#text1}-4:4}"

		# Download the missing source packages.
		count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
			let count=count+1
			if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
				if ! ${SYSDB_PACKAGE_SOURCES_CHECKED[${count}]}; then

					echo -n "${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]}... " >> ${SYSTEM_LOGFILE_DOWNLOADS}

					clear
					text2="0000${package_count}"
					echo "Downloading missing source packages..."
					echo "[${text2:${#text2}-4:4}/${text1}] ${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]}... "

					# Skip checking for MD5?
					if ${SYSDB_PACKAGE_SKIP_MD5[${count}]}; then
						download_mode="-d"
					else
						if ( grep "00000000000000000000000000000000" ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/MD5SUM &>/dev/null ); then
							download_mode="-n"
						else
							download_mode="-m"
						fi
					fi

					export SOURCE_PATH=${SYSTEM_PATH_SOURCES}/${SYSDB_PACKAGE_GROUP[${count}]}
					export SOURCE_CACHE=${SYSTEM_PATH_DOWNLOADS}/${SYSDB_PACKAGE_GROUP[${count}]}
					mkdir -p ${SOURCE_CACHE}

					export LOGFILE_DOWNLOAD_ERROR=${SYSTEM_LOGFILE_DOWNLOAD_ERROR}

					# Download the missing sources using buildtool functions.
					if ( cd ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]} \
							&& sh ${SYSDB_PACKAGE_BUILDTOOL_SCRIPT[${count}]} -q ${download_mode} ); then
						# Download successful...
						echo "OK!" >> ${SYSTEM_LOGFILE_DOWNLOADS}
						SYSDB_PACKAGE_SOURCES_CHECKED[${count}]=true
						let package_count=package_count+1
						let count_incomplete_sources_remaining=count_incomplete_sources_remaining-1
					else
						# Download failed...
						echo "Failed!" >> ${SYSTEM_LOGFILE_DOWNLOADS}
						SYSDB_PACKAGE_SOURCES_CHECKED[${count}]=false
						let package_count=package_count+1
						# If download failed because of bad MD5SUM, delete the downloaded files.
						( cd ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]} \
							&& sh ${SYSDB_PACKAGE_BUILDTOOL_SCRIPT[${count}]} -q --purgesrc
						)
					fi
					# All packages checked? => Skip remaining packages...
					if [ ${package_count} -gt ${count_incomplete_sources} ]; then
						break
					fi
				fi
			fi
		done

		# All missing source packages downloaded?
		if [ ${count_incomplete_sources_remaining} -lt 1 ]; then
			# Yes, exit...
			cancel=false
			DIALOG_REPL=""
			break
		fi

		# Display menu and ask what to do...
		default_item="SHOW"
		while [ true ]; do
			dialog	--title "${PROJECT_NAME} - MISSING SOURCES" \
					--backtitle "${PROJECT_BACKTITLE}" \
					--default-item "${default_item}" \
					--no-cancel \
					--menu "\n\
Some source packages could not be download automatically. Skip those packages during build process?\n\
\n\
Warning: Skip core packages could break the build process. \
If you are sure that those packages which could not be downloaded are not necessary, then select 'SKIP' to continue.\n\
 " 18 75 4 \
"SHOW"      "Show a list of missing source packages" \
"RETRY"     "Try again to download the missing source packages" \
"SKIP"      "Skip packages when the source is not available" \
"EXIT"      "Do not continue and exit this script" \
2> ${DIALOG_RETURN_VALUE}

			DIALOG_KEYCODE=$?
			DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
			rm -f "${DIALOG_RETURN_VALUE}"

			# 'CANCEL' or 'ESC' ?
			if test ${DIALOG_KEYCODE} -ne 0; then
				cancel=true
				break
			fi

			# Exit.
			if [ x"${DIALOG_REPLY}" == x"EXIT" ]; then
				cancel=true
				break
			fi

			# Skip packages with missing sources.
			if [ x"${DIALOG_REPLY}" == x"SKIP" ]; then
				count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
					let count=count+1
					if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
						if ! ${SYSDB_PACKAGE_SOURCES_CHECKED[${count}]}; then
							SYSDB_PACKAGE_ENABLED[${count}]=false
						fi
					fi
				done
				cancel=false
				break
			fi

			# Display list of packages with missing sources.
			if [ x"${DIALOG_REPLY}" == x"SHOW" ]; then
				BUILDSYS_LAST_WARNING_DISPLAY_MISSING_SOURCES
				default_item="RETRY"
				continue
			fi

			# Download packages with missing sources again.
			if [ x"${DIALOG_REPLY}" == x"RETRY" ]; then
				break
			fi

		done

		# Download missing sources again?
		if [ x"${DIALOG_REPLY}" == x"RETRY" ]; then
			# Yes, continue...
			continue
		else
			break
		fi

	done

	FUNC_CLEAR_SCREEN

	case ${cancel} in
		false)	return 0;;
		*)		return 1;;
	esac

}

# Last MD5SUM check...
BUILDSYS_DOWNLOAD_CHECK_MD5SUM() {

	local count=0
	local count_bad_md5sum=0
	local source=""
	local backup=0
	local verify_mode=""
	local default_item="SHOW"
	local text1=""
	local text2=""

	while [ true ]; do

		rm -f ${SYSTEM_LOGFILE_MD5SUM}

		FUNC_PROGRESS_BOX "Checking MD5SUM data..."
		FUNC_INIT_PROGRESS_BAR

		let count_bad_md5sum=0

		count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
			let count=count+1
			FUNC_DRAW_PROGRESS_BAR ${count} ${SYSDB_PACKAGE_COUNT}

			if ${SYSDB_PACKAGE_ENABLED[${count}]}; then

				# Add package name to logfile...
				text="0000${count}"
				text="[${text:${#text}-4:4}] ${SYSDB_PACKAGE_NAME[${count}]}${SPACEBAR}"
				echo -n "${text:0:50}" >> ${SYSTEM_LOGFILE_MD5SUM}

				# MD5 supported for the current package?
				if ${SYSDB_PACKAGE_SKIP_MD5[${count}]}; then
					# No, only check for an existing source package.
					verify_mode="-C"
				else
					# Yes, check MD5SUM of the source package.
					verify_mode="-V"
				fi

				export SOURCE_PATH=${SYSTEM_PATH_SOURCES}/${SYSDB_PACKAGE_GROUP[${count}]}
				export SOURCE_CACHE=${SOURCE_PATH}

				if ( cd ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]} && sh ${SYSDB_PACKAGE_BUILDTOOL_SCRIPT[${count}]} ${verify_mode} --quiet 1>>${SYSTEM_LOGFILE_MD5SUM} 2>>${SYSTEM_LOGFILE_MD5SUM} ); then
					echo "OK!" >> ${SYSTEM_LOGFILE_MD5SUM}
					SYSDB_PACKAGE_SOURCES_CHECKED[${count}]=true
					continue
				else
					echo "Bad MD5!" >> ${SYSTEM_LOGFILE_MD5SUM}
					SYSDB_PACKAGE_SOURCES_CHECKED[${count}]=false
					for source in ${SYSDB_PACKAGE_SOURCE[${count}]//@@@/ }; do
						if [ -e ${SYSTEM_PATH_SOURCES}/${SYSDB_PACKAGE_GROUP[${count}]}/${source} ]; then
							if ! ( cd ${SYSTEM_PATH_SOURCES}/${SYSDB_PACKAGE_GROUP[${count}]} && cat ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/MD5SUM | grep "${source}" | md5sum -c &>/dev/null ); then
								mv ${SYSTEM_PATH_SOURCES}/${SYSDB_PACKAGE_GROUP[${count}]}/${source} ${SYSTEM_PATH_DOWNLOADS}/
								let count_bad_md5sum=count_bad_md5sum+1
							fi
						fi
					done
					continue
				fi

			fi

		done

		# Clear the progress bar.
		FUNC_CLEAR_PROGRESS_BAR

		if [ ${count_bad_md5sum} -eq 0 ]; then
			cancel=false
			break
		fi


		default_item="SHOW"

		while [ true ]; do
			dialog	--title "${PROJECT_NAME} - PACKAGES WITH BAD MD5SUM" \
					--backtitle "${PROJECT_BACKTITLE}" \
					--default-item "${default_item}" \
					--no-cancel \
					--menu "\n\
Some source packages were downloaded, but the MD5 checksum did not match the downloaded package.\n\
This could happen if the package was updated without version number being changed. \
For example FlashPlayer or GoogleEarth do not include detailed version number information in the package name.\n\
\n\
Continue using the downloaded packages? " 20 75 6 \
"SHOW"     "Show a list of packages with bad MD5 checksum" \
"RETRY"    "Try download packages with bad MD5 checksum again" \
"CONTINUE" "Continue using the downloaded packages" \
"NEWMD5"   "Create new MD5 checksum for the packages and continue" \
"EXIT"     "Do not continue and exit this script" \
"SKIP"     "Skip packages with bad MD5 checksum and continue" \
2> ${DIALOG_RETURN_VALUE}

			DIALOG_KEYCODE=$?
			DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
			rm -f "${DIALOG_RETURN_VALUE}"

			# 'CANCEL' or 'ESC' ?
			if test ${DIALOG_KEYCODE} -ne 0; then
				cancel=true
				break
			fi

			if [ x"${DIALOG_REPLY}" == x"EXIT" ]; then
				cancel=true
				break
			fi

			if [ x"${DIALOG_REPLY}" == x"SKIP" ]; then
				count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
					let count=count+1
					if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
						if ! ${SYSDB_PACKAGE_SOURCES_CHECKED[${count}]}; then
							SYSDB_PACKAGE_ENABLED[${count}]=false
						fi
					fi
				done
				cancel=false
				break
			fi

			if [ x"${DIALOG_REPLY}" == x"SHOW" ]; then
				BUILDSYS_LAST_WARNING_DISPLAY_MISSING_SOURCES
				default_item="RETRY"
				continue
			fi

			if [ x"${DIALOG_REPLY}" == x"RETRY" ]; then
				FUNC_PROGRESS_BOX "Re-download source package..."
				text1="0000${SYSDB_PACKAGE_COUNT}"
				text1="${text1:${#text1}-4:4}"
				count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
					let count=count+1
					if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
						if ! ${SYSDB_PACKAGE_SOURCES_CHECKED[${count}]}; then
							FUNC_CLEAR_SCREEN
							echo "Downloading missing source packages..."
							text2="0000${count}"
							echo "[${text2:${#text2}-4:4}/${text1}] ${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]}... "
							export SOURCE_PATH=${SYSTEM_PATH_SOURCES}/${SYSDB_PACKAGE_GROUP[${count}]}
							export SOURCE_CACHE=${SOURCE_PATH}
							if ( cd ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]} && sh ${SYSDB_PACKAGE_BUILDTOOL_SCRIPT[${count}]} -D ); then
								SYSDB_PACKAGE_SOURCES_CHECKED[${count}]=true
							fi
						fi
					fi
				done
				cancel=false
				break
			fi

			if [ x"${DIALOG_REPLY}" == x"CONTINUE" ]; then
				count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
					let count=count+1
					if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
						if ! ${SYSDB_PACKAGE_SOURCES_CHECKED[${count}]}; then
							for source in ${SYSDB_PACKAGE_SOURCE[${count}]//@@@/ }; do
								if [ ! -e ${SYSTEM_PATH_SOURCES}/${SYSDB_PACKAGE_GROUP[${count}]}/${source} ]; then
									if [ -e {SYSTEM_PATH_DOWNLOADS}/${source} ]; then
										mv ${SYSTEM_PATH_DOWNLOADS}/${source} ${SYSTEM_PATH_SOURCES}/${SYSDB_PACKAGE_GROUP[${count}]}/
									fi
								fi
							done
						fi
					fi
				done
				cancel=false
				break
			fi

			if [ x"${DIALOG_REPLY}" == x"NEWMD5" ]; then
				FUNC_PROGRESS_BOX "Creating new MD5SUM..."
				text1="0000${SYSDB_PACKAGE_COUNT}"
				text1="${text1:${#text1}-4:4}"
				count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
					let count=count+1
					if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
						if ! ${SYSDB_PACKAGE_SOURCES_CHECKED[${count}]}; then
							FUNC_CLEAR_SCREEN
							echo "Updating MD5SUM checksum data..."
							text2="0000${count}"
							echo "[${text2:${#text2}-4:4}/${text1}] ${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]}... "
							export SOURCE_PATH=${SYSTEM_PATH_SOURCES}/${SYSDB_PACKAGE_GROUP[${count}]}
							export SOURCE_CACHE=${SOURCE_PATH}
							if ( cd ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]} && sh ${SYSDB_PACKAGE_BUILDTOOL_SCRIPT[${count}]} -n ); then
								SYSDB_PACKAGE_SOURCES_CHECKED[${count}]=true
							fi
						fi
					fi
				done
				DIALOG_REPLY="RETRY"
				cancel=false
				break
			fi

		done

		if [ x"${DIALOG_REPLY}" == x"RETRY" ]; then
			continue
		else
			break
		fi

	done

	FUNC_CLEAR_SCREEN

	case ${cancel} in
		false)	return 0;;
		*)		return 1;;
	esac

}

# Remove installed packages.
BUILDSYS_REMOVE_INSTALLED_PACKAGES() {

	local count=0
	local count_removed=0
	local percent_removed=0
	local default_item=""
	local package=""

	# Stop all running services that maybe get removed/re-installed
	FUNC_PROGRESS_BOX "Stopping services..."

	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
		let count=count+1

		if ${SYSDB_PACKAGE_ENABLED[${count}]}; then

			case ${SYSDB_PACKAGE_NAME[${count}]} in
				avahi)
					if [ -x /etc/rc.d/rc.avahidaemon ]; then
						sh /etc/rc.d/rc.avahidaemon stop &>/dev/null
					fi
					if [ -x /etc/rc.d/rc.avahidnsconfd ]; then
						sh /etc/rc.d/rc.avahidnsconfd stop &>/dev/null
					fi
					;;
				dbus)
					if [ -x /etc/rc.d/rc.messagebus ]; then
						sh /etc/rc.d/rc.messagebus stop &>/dev/null
					fi
					;;
			esac

		fi

	done

	# When upgrade/update/sync packages do not remove installed
	# packages or installing packages may fail because of missing
	# libraries of optional/required dependencies.
	case ${RUNTIME_SETUP_MODE} in
		UPDATE|UPGRADE|SYNC)	return 0;;
	esac

	# Any packages already installed?
	FUNC_CHECK_UPDATE_PACKAGES
	if [ ${RUNTIME_UPDATE_PACKAGES} -ne 0 ]; then

		# Remove all packages before we start to compile?
		if ${OPTION_REMOVE_ALL_PKG_BEFORE_INSTALL}; then

			# Do we have to show the 'Remove packages' warning?
			if ! ${OPTION_AUTO_REMOVE_PACKAGES}; then

				default_item="SHOW"

				while [ true ]; do
					dialog	--title "${PROJECT_NAME} - REMOVE PACKAGES THAT WILL BE UPDATED" \
							--backtitle "${PROJECT_BACKTITLE}" \
							--default-item "${default_item}" \
							--no-cancel \
							--menu "\n\
Some packages that will be compiled are already installed (${RUNTIME_UPDATE_PACKAGES} total).\n\
\n\
Select 'CONT' to continue and remove already installed packages. This is recommended when compile packages from source but may also break your system!\n\
\n\
Select 'SKIP' if you are unsure and remove packages during install which is possibly unsafe and can break the build process.\n " 20 75 4 \
"SHOW" "Show a list of packages that will be removed" \
"CONT" "Remove packages to be updated and continue (recommended)" \
"SKIP" "Skip this step and continue anyway (unsafe!)" \
"EXIT" "Do not remove anything and exit" 2> ${DIALOG_RETURN_VALUE}

					DIALOG_KEYCODE=$?
					DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
					rm -f "${DIALOG_RETURN_VALUE}"

					# 'CANCEL' or 'ESC' ?
					if test ${DIALOG_KEYCODE} -ne 0; then
						return 1
					fi

					if [ x"${DIALOG_REPLY}" == x"EXIT" ]; then
						return 1
					fi

					if [ x"${DIALOG_REPLY}" == x"SKIP" ]; then
						OPTION_REMOVE_ALL_PKG_BEFORE_INSTALL=false
						break
					fi

					if [ x"${DIALOG_REPLY}" == x"CONT" ]; then
						break
					fi

					if [ x"${DIALOG_REPLY}" == x"SHOW" ]; then
						BUILDSYS_LAST_WARNING_DISPLAY_UPDATE_PACKAGES
						default_item="CONT"
					fi

				done

			fi

		fi

		# Start removing all packages that will be updated/replaced.
		if ${OPTION_REMOVE_ALL_PKG_BEFORE_INSTALL}; then
			(	count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
					let count=count+1
					if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
						if [ x"${SYSDB_PACKAGE_REMOVE[${count}]}" != x"" ]; then
							for package in ${SYSDB_PACKAGE_REMOVE[${count}]}; do
								if [ -e /var/log/packages/${package} ]; then
									let percent_removed=count_removed*100/RUNTIME_UPDATE_PACKAGES
									let count_removed=count_removed+1
									echo ${percent_removed}
									echo "XXX"
									echo "Currently removing: ${SYSDB_PACKAGE_NAME[${count}]}"
									echo "Package: ${package}"
									echo "XXX"
									removepkg "${package}" &>/dev/null
									package_name=${package%-*}			# Remove build
									package_name=${package_name%-*}		# Remove arch
									package_name=${package_name%-*}		# Remove version
									if [ x"${package_name}" != x"${SYSDB_PACKAGE_NAME[${count}]}" ]; then
										if [ x"${SYSDB_PACKAGE_SEARCH_NAME[${count}]%-\*}" == x"${SYSDB_PACKAGE_SEARCH_NAME[${count}]}" ]; then
											echo "  => REMOVED by '${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}': ${package}" >>${SYSTEM_LOGFILE_REMOVED}
										else
											echo "UPGRADED: ${package}" >>${SYSTEM_LOGFILE_REMOVED}
										fi
									else
										echo "UPGRADED: ${package}" >>${SYSTEM_LOGFILE_REMOVED}
									fi
								fi
							done
						fi
					fi
				done
			) | dialog	--title "${PROJECT_NAME} - REMOVING PACKAGES TO BE UPDATED" \
						--backtitle "${PROJECT_BACKTITLE}" \
						--gauge "Removing packages..." 8 60 0
		fi
	fi

	# Remove extra packages from remove.conf...
	if [ x"${RUNTIME_REMOVE_EXTRA_PACKAGES}" != x"" ]; then
		for package in ${RUNTIME_REMOVE_EXTRA_PACKAGES}; do
			let count_max_remove=count_max_remove+1
		done
		rm -f ${SYSTEM_PATH_PACKAGES}/$(basename ${SYSTEM_CONFIG_REMOVE} .orig)
		touch ${SYSTEM_PATH_PACKAGES}/$(basename ${SYSTEM_CONFIG_REMOVE} .orig)
		(	let count=0
			for package in ${RUNTIME_REMOVE_EXTRA_PACKAGES}; do
				let count=count+1

				if FUNC_CHECK_PACKAGE_INSTALLED ${package}; then
					remove_package="${RETURN_VALUE}"
				else
					remove_package=""
				fi
				let percent_removed=count*100/count_max_remove
				echo ${percent_removed}
				echo "XXX"
				echo "Currently removing: ${package}"
				echo "Package: ${remove_package}"
				echo "XXX"
				if [ x"${remove_package}" != x"" ]; then
					for installed_package in ${remove_package}; do
						removepkg "${installed_package}" &>/dev/null
						echo "EXTRAPKG: ${installed_package}" >>${SYSTEM_LOGFILE_REMOVED}
					done
					echo "${package}" >>${SYSTEM_PATH_PACKAGES}/$(basename ${SYSTEM_CONFIG_REMOVE} .orig)
				fi
			done
		) | dialog	--title "${PROJECT_NAME} - REMOVING EXTRA PACKAGES" \
					--backtitle "${PROJECT_BACKTITLE}" \
					--gauge "Removing packages..." 8 60 0
	fi

	# Packages removed, cleaning up...
	FUNC_PLEASE_WAIT

	# Update library database.
	ldconfig 1>>${SYSTEM_LOGFILE_BUILDSYS_CONFIG} 2>>${SYSTEM_LOGFILE_BUILDSYS_ERROR}

	return 0

}

# Add a package to joblist and tontinue building packages...
BUILDSYS_JOBLIST_ADD_PACKAGE() {
	local package_config_file=""

	if [ ! -e ${SYSTEM_JOB_PACKAGES} ]; then
		# We need a job file to add packages.
		echo
		echo "This option can only be used when resume a previous"
		echo "build session. Exiting now!"
		return 1
	else
		# Any packages specified?
		if [ x"${RUNTIME_CLI_PARAMETER// /}" == x"" ]; then
			echo
			echo "You must specify a package name to be added to the job list when using"
			echo "the '--add' option. Example:"
			echo "  sh ${PROJECT_SCRIPT_NAME} --add boost [,pkg2,pkg3,...]"
			echo ""
			echo "Exiting now!"
			return 1
		else
			# Check all buildtools we do have in the buildtool configuration file.
			if [ -e "${SYSTEM_CONFIG_PACKAGE_OPTDATA}" ]; then
				package_config_file="${SYSTEM_CONFIG_PACKAGE_OPTDATA}"
			else
				package_config_file="${SYSTEM_CONFIG_BUILDTOOLS}"
			fi
			packages_requested="@EOP@${RUNTIME_CLI_PARAMETER//,/@EOP@}@EOP@"
			for config_package in $(grep "." ${package_config_file} | grep -v "^#"); do
				found=false
				# Check all packages specified on the command line separated by commas. example -a pkg1,pkg2,pkg3
				remaining_packages=${packages_requested//@EOP@/ }
				for package in ${remaining_packages}; do
					if [ x"${config_package##*/}" == x"${package}" ]; then
						if [ -d "${SYSTEM_PATH_BUILDTOOLS}/${config_package}" ]; then
							packages_requested="${packages_requested//@EOP@${package}@EOP@/@EOP@}"
							found=true
							break
						fi
					fi
				done
				# Found a requested package?
				if ${found}; then
					let buildtool_count=buildtool_count+1
					buildtool_found[${buildtool_count}]="${config_package}"
				fi
			done
			# Any requested packages not found?
			if [ x"${packages_requested// /}" != x"@EOP@" ]; then
				# Yes, Exit...
				echo ""
				echo "Some requested packages could not be found:"
				for package in ${packages_requested//@EOP@/ }; do
					echo " => ${package##*/}"
				done
				echo ""
				echo "Exiting now!"
				return 1
			fi
			# No, add all requested buildtools to the job list.
			echo ""
			echo " => Adding requested package(s) to the job list..."
			echo "    NOTE: $(basename ${SCRIPT_NAME_INSTALLPKG}) will not be updated!"
			count_package=0
			count=0
			while [ ${count} -lt ${buildtool_count} ]; do
				let count=count+1
				if [ x"$(grep ^${buildtool_found[${count}]}$ ${SYSTEM_JOB_PACKAGES})" == x"" ]; then
					let count_package=count_package+1
					sed -i "${count_package} i${buildtool_found[${count}]}" ${SYSTEM_JOB_PACKAGES}
					echo "    Added package  : ${buildtool_found[${count}]}"
				else
					echo "    Ignored package: ${buildtool_found[${count}]}"
					echo "      !! Package is already in the job list !!"
				fi
			done
			echo ""
			echo "Build will continue, please wait..."
			echo ""
			SLEEPMODE 3
			return 0
		fi
	fi
	return 0
}

# Remove a package from joblist and tontinue building packages...
BUILDSYS_JOBLIST_SKIP_PACKAGE() {
	if [ ! -e ${SYSTEM_JOB_PACKAGES} ]; then
		# We need a job file to skip packages.
		echo
		echo "This option can only be used when resume a previous"
		echo "build session. Exiting now!"
		return 1
	else
		# Remove requested packages.
		echo
		echo " => Skipping requested package(s)..."
		echo "    NOTE: $(basename ${SCRIPT_NAME_INSTALLPKG}) will not be updated!"
		if [ x"${2}" != x"" ]; then
			# Check all packages specified on the command line separated by commas. example -a pkg1,pkg2,pkg3
			for package in ${2//,/ }; do
				if [ x"$(grep /${package}$ ${SYSTEM_JOB_PACKAGES})" != x"" ]; then
					sed -i "/\/${package}$/ d" ${SYSTEM_JOB_PACKAGES}
					echo "    Removed: ${package}"
				else
					echo "    Error: Cannot find package ${package}!"
					error=true
				fi
			done
			# Did we find any errors?
			if ${error}; then
				# Yes, Exit...
				echo ""
				echo "Not all requested packages could be removed from the job list."
				echo "Try again or use 'sh ${PROJECT_SCRIPT_NAME}' with no options to continue."
				echo "Exiting now!"
				return 1
			fi
		else
			package=$(grep -m 1 "/" ${SYSTEM_JOB_PACKAGES})
			echo "    Removed: ${package}"
			sed -i "1 d" ${SYSTEM_JOB_PACKAGES}
		fi
		echo ""
		echo "Build will continue, please wait..."
		echo ""
		SLEEPMODE 3
		RUNTIME_QUICK_RESUME=true
		return 0
	fi
	return 0
}

# Finally build and install the packages...
BUILDSYS_COMPILE_PACKAGES() {
	local remaining_seconds=${SELECTED_PACKAGES_ETA_REMAINING}
	local remaining_minutes_all=0
	local remaining_minutes=0
	local remaining_hours=0
	local build_seconds=${SELECTED_PACKAGES_ETA}
	local build_minutes_all=0
	local build_minutes=0
	local build_hours=0
	local timer_start=0
	local timer_end=0
	local timer_seconds=0
	local timer_minutes=0
	local count=0
	local build_error=0
	local logfile=""
	local check=""
	local build_finished=0

	# Let the buildtools know that we are using the buildsystem mode.
	export BUILDSYSTEM_BUILD_MODE=true

	# Disable update functions since we do that at once when build has finished.
	export DISABLE_SCHEMAS_UPDATE=true
	export DISABLE_DESKTOPFILES_UPDATE=true
	export DISABLE_MIMEINFO_UPDATE=true
	export DISABLE_ICON_CACHE_UPDATE=true
	export DISABLE_GIOMODULES_UPDATE=true
	export DISABLE_GLIBSCHEMAS_UPDATE=true

	# If we do not update the gtk icon cache we also do not
	# need the code to update the cache when creating packages.
	if test x"${OPTION_UPDATE_GTK_ICON_CACHE:-false}" == x"false"; then
		export DISABLE_SUPPORT_GTK_ICON_CACHE=true
	else
		export DISABLE_SUPPORT_GTK_ICON_CACHE=false
	fi

	FUNC_CLEAR_SCREEN

	if ! ${RUNTIME_SYSTEM_RESTART}; then
		cat <<EOF >${SYSTEM_LOGFILE_BUILDSYS_MASTER}
Packages to be downloaded: ${RUNTIME_MISSING_SOURCE_PACKAGES}
Packages to be updated   : ${RUNTIME_UPDATE_PACKAGES}
Packages to be installed : ${SYSDB_PACKAGE_COUNT}
${RULER2}

EOF
	else
		if ${OPTION_VERBOSE_MODE:-true}; then

			cat <<EOF >>${SYSTEM_LOGFILE_BUILDSYS_MASTER}

${RULER2}
Continue building packages...
${RULER2}

EOF
		else
			echo "" >>${SYSTEM_LOGFILE_BUILDSYS_MASTER}
		fi
	fi

	rm -f ${SYSTEM_BUILD_FAILED}

	# Update the timer to calculate build-time.
	if ${OPTION_USE_NTP}; then
		echo "Syncing system time using NTP time server..."
		echo "Please wait..."
		FUNC_SYNC_SYSTEM_TIME
	fi

	echo -e "\nNow building packages..."

	# Calculate initial values for remaining build time.
	let build_minutes_all=build_seconds/60
	if [ ${build_minutes_all} -eq 0 ]; then
		build_minutes_all=1
	fi
	let build_hours=build_minutes_all/60
	let build_minutes=build_minutes_all-build_hours*60

	# Calculate time when build might be finished.
	let build_finished=`date +%s`
	let build_finished=build_finished+build_seconds

	# Print some status information.
	echo -e "ETA (based on last build)                : ${build_minutes_all}min. (${build_hours}h${build_minutes}m)" |\
		tee -a ${SYSTEM_LOGFILE_BUILDSYS_MASTER}
	echo -e "Calculated end of the compilation process: $(date +'%Y-%m-%d %H:%M' --date=@$build_finished)\n" |\
		tee -a ${SYSTEM_LOGFILE_BUILDSYS_MASTER}

	# Get the current time.
	let timer_start=$(date +%s)

	(	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
			let count=count+1

			# Is package enabled?
			if ${SYSDB_PACKAGE_ENABLED[${count}]}; then
				# Compile the package...
				let RUNTIME_PACKAGES_COMPILED=RUNTIME_PACKAGES_COMPILED+1
				BUILDSYS_COMPILE_PACKAGES_BUILD ${count}
				build_error=$?

				# Overwriting files... exit.
				if [ ${build_error} -eq 2 ]; then
					touch ${SYSTEM_BUILD_FAILED}
					break
				fi

				if [ ${build_error} -ne 0 ]; then
					echo "BUILD FAILED" >${SYSTEM_BUILD_FAILED}
					echo
					echo
					echo "Build failed!"
					echo ${RULER2}
					if [ ${build_error} -eq 1 ]; then
						logfile=${SYSTEM_PATH_LOGFILES_BUILDTOOLS}/${SYSDB_PACKAGE_GROUP[${count}]}/log.${SYSDB_PACKAGE_NAME[${count}]}
						check=$(grep -c "." ${logfile}.stage2.build 2>/dev/null)
						if [ ${check:-0} -gt 2 ]; then
							tail -n 20 ${logfile}.stage2.build
						else
							tail -n 20 ${logfile}.stage1.config
						fi
						echo ${RULER2}
					fi
					echo
					touch ${SYSTEM_BUILD_FAILED}
					break
				fi

				# Remove the package from job list.
				sed -i "1 d" ${SYSTEM_JOB_PACKAGES}
			fi

		done
	) | tee -a ${SYSTEM_LOGFILE_BUILDSYS_MASTER}


	# Did a package fail to build? If so, stop the script.
	if [ -e ${SYSTEM_BUILD_FAILED} ]; then
		return 1
	fi

	# Was the last build finished?
	if [ -e ${BUILD_PATH_TEMP}/COMPILE ]; then
		echo "Build failed!"
		echo "BUILD FAILED" >${SYSTEM_BUILD_FAILED}
		return 1
	fi

	# Remove (hopefully) empty job-list.
	check=$(grep "." ${SYSTEM_JOB_PACKAGES})
	if [ x"${check}" != x"" ]; then
		echo "Build cancelled!"
		echo "BUILD CANCELLED" >${SYSTEM_BUILD_FAILED}
		return 1
	else
		rm -f ${SYSTEM_JOB_SETTINGS}
		rm -f ${SYSTEM_JOB_PACKAGES}
	fi

	# Print status info.
	cat <<EOF >>${SYSTEM_LOGFILE_BUILDSYS_MASTER}

${RULER1}
EOF

	# Update the timer to calculate build-time.
	if ${OPTION_USE_NTP}; then
		echo "Syncing system time using NTP time server..."
		FUNC_SYNC_SYSTEM_TIME
	fi

	# Calculate time needed to compile the packages.
	let timer_end=$(date +%s)
	let timer_seconds=timer_end-timer_start
	let timer_minutes=timer_seconds/60
	if [ ${timer_minutes} -eq 0 ]; then
		timer_minutes=1
	fi

	cat <<EOF >>${SYSTEM_LOGFILE_BUILDSYS_MASTER}
Packages downloaded: ${RUNTIME_MISSING_SOURCE_PACKAGES}
Packages updated   : ${RUNTIME_UPDATE_PACKAGES}
Packages installed : ${RUNTIME_PACKAGES_COMPILED}
Calculated ETA     : ${build_minutes_all}min.
Build finished in  : ${timer_minutes}min.
EOF

	# Create a logfile of original build for installtgz.sh script.
	ls /var/log/packages | sort >${SYSTEM_PATH_PACKAGES_DATA}

	return 0

}

# Build single package.
BUILDSYS_COMPILE_PACKAGES_BUILD() {
	local count=$1
	local buildtool_name=${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]}
	local buildtool_group=${SYSDB_PACKAGE_GROUP[${count}]}
	local lastrun_seconds=0
	local lastrun_seconds_text=""
	local lastrun_minutes=0
	local calculated_seconds=0
	local calculated_minutes=0
	local calculated_seconds_rest=0
	local calculated_seconds_diff=0
	local speed_factor=0
	local speed_percent=0
	local remaining_seconds=0
	local remaining_minutes=0
	local remaining_hours=0
	local text_count1=""
	local text_count2=""
	local text_count3=""
	local text_remain=""
	local text_remain_time=""
	local text_speed_percent=""
	local packages_percent=0
	local timer_start=0
	local timer_end=0
	local timer_diff_seconds=0
	local timer_diff_seconds_text=""
	local timer_diff_minutes=0
	local timer_check_seconds=0
	local package_speed_factor=0
	local build_try=0
	local build_finished=false
	local build_verbose_mode=""
	local package_supports_multicpu=false
	local package=""
	local package_name=""
	local package_compression_type=""
	local package_name_temp=""
	local package_desc=""
	local package_speed_factor=0
	local package_speed_factor_text=""
	local pkginfo=""
	local package_info_width=""
	local remove_package=""
	local remove_package_notice=true
	local compiled_package_list=""
	local build_result=false
	local package_md5=""
	local move_to_cache_retry_count=0
	local uncompressed=0
	local uncompressedb=0
	local pkgsizeb=0

	local screen_width=`dialog --stdout --print-maxsize | sed "s,.* ,,g"`
	if ${RUNTIME_OPTION_DEBUG_MODE}; then
		if test -f "${SYSTEM_ROOT}/SCREEN_WIDTH"; then
			local screen_width=`grep -m1 "." ${SYSTEM_ROOT}/SCREEN_WIDTH`
		fi
	fi

	echo "${SYDB_PACKAGE_NAME[${count}]}" >${SYSTEM_PATH_TEMP}/COMPILE

	# These variables will be used from inside the buildtools.
	export SOURCE_PATH="${SYSTEM_PATH_SOURCES}/${buildtool_group}"
	export BUILD_BINARY_DIR="${SYSTEM_PATH_TEMP_PACKAGE}"
	export BUILD_LOGFILE_DIR="${SYSTEM_PATH_LOGFILES_BUILDTOOLS}/${buildtool_group}"
	export BUILD_TMP="${SYSTEM_PATH_TEMP_BUILD}"



	# Get buildtime of the package.
	lastrun_seconds=${SYSDB_PACKAGE_ETA[${count}]}

	# Calculate the current speed factor.
	let speed_factor=BUILDTIME_SECONDS_ALL*100/BUILDTIME_SECONDS_ALL_LASTRUN

	# How long will the current package take to compile?
	let calculated_seconds=lastrun_seconds*speed_factor/100
	let calculated_minutes=calculated_seconds/60
	let calculated_seconds_rest=calculated_seconds-calculated_minutes*60
	if [ ${calculated_seconds_rest} -gt 0 ]; then
		let calculated_minutes=calculated_minutes+1
	fi
	if [ ${calculated_minutes} -eq 0 ]; then
		calculated_minutes=1
	fi

	# How long did this package take the last time?
	let lastrun_minutes=lastrun_seconds/60
	let calculated_seconds_diff=lastrun_seconds-lastrun_minutes*60
	if [ ${calculated_seconds_diff} -gt 0 ]; then
		let lastrun_minutes=lastrun_minutes+1
	fi

	# Create the output.
	text_eta="ETA:${calculated_minutes}m(${calculated_seconds}s) LastRun:${lastrun_minutes}m(${lastrun_seconds}s) "

	# Calculate the remaining time to finish the build.
	let speed_percent=10000/speed_factor
	let remaining_seconds=SELECTED_PACKAGES_ETA_REMAINING*speed_factor/100
	let remaining_minutes=remaining_seconds/60
	if [ ${remaining_minutes} -eq 0 ]; then
		remaining_minutes=1
	fi
	let remaining_hours=0

	# in case of more then 60 minutes... calculate hours.
	if [ ${remaining_minutes} -ge 100 ]; then
		let remaining_hours=remaining_minutes/60
		let remaining_minutes=remaining_minutes-remaining_hours*60
	fi

	# Calculate package numbers.
	text_count1="0000${RUNTIME_PACKAGES_COMPILED}"
	text_count2="0000${RUNTIME_INSTALL_PACKAGES}"
	let packages_percent=RUNTIME_PACKAGES_COMPILED-1
	let packages_percent=packages_percent*100/RUNTIME_INSTALL_PACKAGES
	let text_count3=RUNTIME_INSTALL_PACKAGES-RUNTIME_PACKAGES_COMPILED
	text_count3="0000${text_count3}"
	text_speed_percent="   ${speed_percent}"
	text_speed_percent="${text_speed_percent:${#text_speed_percent}-3:3}%"




	# Verbose mode?
	if ${OPTION_VERBOSE_MODE:-true}; then
		# Create remaining text depending on rest time less then 100min or not.
		if [ ${remaining_hours} -eq 0 ]; then
			text_remain_time="Remaining:${remaining_minutes}m"
			text_speed_percent="Speed:${speed_percent}% "
			text_remain="$text_remain_time $text_speed_percent"
		else
			text_remain_time="Remaining:${remaining_hours}h${remaining_minutes}m"
			text_speed_percent="Speed:${speed_percent}% "
			text_remain="$text_remain_time $text_speed_percent"
		fi

		# Print package information.
		echo    "${RULER1}"
		echo -n "["$(date +%H:%M)"]"
		echo -n "  Package:${text_count1:${#text_count1}-4:4}/${text_count2:${#text_count2}-4:4} (${packages_percent}%) "
		echo    "  *** ${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]}-${SYSDB_PACKAGE_VERSION[${count}]}${SYSDB_PACKAGE_RELEASE[${count}]} ***"
		echo    "         ${text_eta}${text_remain}"
	else
		# Create remaining text depending on rest time less then 120min or not.
		if [ ${remaining_hours} -lt 1 ]; then
			text_remain_time="  ${remaining_minutes}"
			text_remain_time="${text_remain_time:${#text_remain_time}-2:2}m"
			text_remain="        $text_remain_time"
			text_remain="${text_remain:${#text_remain}-6:6}"
		else
			text_remain_time="  ${remaining_hours}"
			text_remain_time="${text_remain_time:${#text_remain_time}-2:2}h"
			text_remain="00${remaining_minutes}"
			text_remain="${text_remain_time}${text_remain:${#text_remain}-2:2}m"
		fi
		# Add current compile speed to package info only on wide screens.
		if [ ${screen_width:-0} -gt 80 ]; then
			text_remain="${text_remain} | $text_speed_percent"
		fi
		# On low resolution screens reduce package information.
		# >80 : 1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
		#       9999 | 23h59m | 100% | package-version: package-description........................ [999m] [0000 K]
		# >40 : 12345678901234567890123456789012345678901234567890123456789012345678901234567890
		#       9999 | 23h59m | 100% | package-version: package-description........... [0000 K]
		# <40 : 1234567890123456789012345678901234567890
		#       9999 | package-version........ [0000 K]
		if [ ${screen_width:-0} -gt 40 ]; then
			# On wide screens print calculated time needed to build the package.
			calculated_minutes_text="    ${calculated_minutes}m"
			calculated_minutes_text=" [${calculated_minutes_text:${#calculated_minutes_text}-4:4}]"
			if [ ${screen_width:-0} -gt 80 ]; then
				# [xxxx/yyyy] | remaning...
				# pkginfo="$(date +%H:%M) | ${text_count1:${#text_count1}-4:4}/${text_count2:${#text_count2}-4:4}"
				# [zzzz] | remaning...
				pkginfo="$(date +%H:%M) | ${text_count3:${#text_count3}-4:4}"
				pkginfo="${pkginfo} | $text_remain"
				#pkginfo="${pkginfo} | ${SYSDB_PACKAGE_GROUP[${count}]}/${SYSDB_PACKAGE_NAME[${count}]}-${SYSDB_PACKAGE_VERSION[${count}]}${SYSDB_PACKAGE_RELEASE[${count}]}: "
				pkginfo="${pkginfo} | ${SYSDB_PACKAGE_GROUP[${count}]}/${SYSDB_PACKAGE_NAME[${count}]}: "
				# Create short package description.
				package_desc=`cat ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/slack-desc | grep -m1 "^${SYSDB_PACKAGE_NAME[${count}]}:" | sed -e "s,^${SYSDB_PACKAGE_NAME[${count}]}: ,," -e "s,.* (,," -e "s,)$,,"`
				pkginfo="${pkginfo}${package_desc}"
			else
				# [zzzz] | remaning...
				pkginfo="${text_count3:${#text_count3}-4:4}"
				pkginfo="${pkginfo} | $text_remain"
				pkginfo="${pkginfo} | ${SYSDB_PACKAGE_GROUP[${count}]}/${SYSDB_PACKAGE_NAME[${count}]}: "
				# Create short package description.
				package_desc=`cat ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/slack-desc | grep -m1 "^${SYSDB_PACKAGE_NAME[${count}]}:" | sed -e "s,^${SYSDB_PACKAGE_NAME[${count}]}: ,," -e "s,.* (,," -e "s,)$,,"`
				pkginfo="${pkginfo}${package_desc}"
			fi
		else
			# On small screens do not print calculated time needed to build the package.
			calculated_minutes_text=""
			# [zzzz] | remaning...
			pkginfo="${text_count3:${#text_count3}-4:4}"
			pkginfo="${pkginfo} $text_remain ${SYSDB_PACKAGE_NAME[${count}]}"
		fi
		# Print short package info while compiling the package.
		# Shorten the package info to leave some space for package size info like "... [999m] [0000 K]"
		let package_info_width=screen_width-13-${#calculated_minutes_text}
		pkginfo="${pkginfo}................................................................................"
		pkginfo="${pkginfo}................................................................................"
		echo -n "${pkginfo:0:$package_info_width}...${calculated_minutes_text} "
	fi




	# Add current package name to master logfiles for config/error messages.
	cat <<EOF >>${SYSTEM_LOGFILE_BUILDSYS_CONFIG}
${RULER1}
${buildtool_group}/${buildtool_name}  [$(date +%H:%M) T:${lastrun_minutes}m/E:${calculated_minutes}m ${text_speed_percent}]
EOF
	cat <<EOF >>${SYSTEM_LOGFILE_BUILDSYS_ERROR}
${RULER1}
${buildtool_group}/${buildtool_name}
EOF




	# Since build may fail when using distcc we will try it at least two times...
	export MAKEOPTS="${RUNTIME_MAKEOPTS}"
	build_finished=false
	build_try=0; while [ true ]; do

		# Update the timer to calculate build-time.
		FUNC_SYNC_SYSTEM_TIME

		let timer_start=$(date +%s)

		if test -d ${SYSTEM_PATH_CACHE}; then
			cache_package_name=$(find ${SYSTEM_PATH_CACHE}/ -maxdepth 1 -mindepth 1 -name ${SYSDB_PACKAGE_NAME[${count}]}-${SYSDB_PACKAGE_VERSION[${count}]}${SYSDB_PACKAGE_RELEASE[${count}]}-${SYSDB_PACKAGE_ARCH[${count}]}-${SYSDB_PACKAGE_BUILD[${count}]}${PACKAGE_TAG}.t[xglb]z -printf "%f\n")
		else
			cache_package_name=""
		fi
		if [ x"${cache_package_name}" != x"" ]; then
			# Install the package from cache.
			for cache_pkg_file in ${cache_package_name}; do
				rm -rf ${BUILD_BINARY_DIR}
				mkdir -p ${BUILD_BINARY_DIR}
				cp ${SYSTEM_PATH_CACHE}/${cache_pkg_file} ${BUILD_BINARY_DIR}/
				#upgradepkg --install-new --reinstall ${SYSTEM_PATH_CACHE}/${cache_pkg_file} \
				#	1>${SYSTEM_BUILD_INSTALL_PACKAGE} \
				#	2>${SYSTEM_BUILD_INSTALL_PACKAGE_ERRORS}
				echo "Installed ${SYSTEM_PATH_CACHE}/${cache_pkg_file}" >>${SYSTEM_LOGFILE_BUILDSYS_CONFIG}
			done
			build_finished=true
			break
		else
			# Build the package
			rm -rf ${BUILD_BINARY_DIR}
			(	cd ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]} \
				&& sh ${SYSDB_PACKAGE_BUILDTOOL_SCRIPT[${count}]} --build 1>${SYSTEM_BUILD_LOGFILE_TEMP1} 2>${SYSTEM_BUILD_LOGFILE_TEMP2}
			)
			build_result=$?
			cat ${SYSTEM_BUILD_LOGFILE_TEMP1} >>${SYSTEM_LOGFILE_BUILDSYS_CONFIG}
			cat ${SYSTEM_BUILD_LOGFILE_TEMP2} >>${SYSTEM_LOGFILE_BUILDSYS_ERROR}
			if test ${build_result} -eq 0; then
				build_finished=true
				break
			fi
		fi

		# Print error to screen.
		echo
		cat ${SYSTEM_BUILD_LOGFILE_TEMP2}
		echo "Waiting 3 seconds..."
		SLEEPMODE 3

		# Build failed.
		package_supports_multicpu=$(grep "^PACKAGE_SUPPORTS_DISTCC=false" "${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/${SYSDB_PACKAGE_BUILDTOOL_SCRIPT[${count}]}")
		if [ x"${package_supports_multicpu}" != x"" -o x"${OPTION_MULTIPLE_CPU_COMPILE}" == x"false" ]; then
			if [ ${build_try:-1} -ge ${OPTION_GCC_RETRIES:-1} ]; then
				break
			fi
		else
			if [ ${build_try:-3} -ge ${OPTION_MULTIPLE_CPU_RETRIES:-3} ]; then
				break
			fi
		fi

		# Build failed, when using distcc give it another try...
		# Reset MAKEOPTS to use only one job at a time which should be a safe
		# choice if package is not distcc compatible.
		FUNC_CLEAN_MAKEOPTS_JOBS "${MAKEOPTS}" "-j1"
		export MAKEOPTS="${RETURN_VALUE}"
		let build_try=build_try+1

		echo
		echo    "         Build failed... will give it another try..."
		echo

		# Add it to config logfile for those who like to use 'tail -f $SYSTEM_LOGFILE_BUILDSYS_CONFIG' :-)
		echo "  => Build failed... will give it another try..." 1>>${SYSTEM_LOGFILE_BUILDSYS_CONFIG}

	done

	# Just in case the buildtool did exit with return code '0' and
	# we do not have a compiled package, exit the buildsystem...
	compiled_package_list=$(find ${BUILD_BINARY_DIR}/ -type f -name "*t[x|g|l|b]z" -printf "%f\n")
	if [ x"${compiled_package_list}" == x"" ]; then
		build_finished=false
	fi

	# Did the package build fine?
	if ! ${build_finished}; then
		echo "Error! Exiting now..."
		return 1
	else
		# Check for already existing files also included in the compiled package.
		if ${OPTION_DEBUG_OVERWRITE_CHECK}; then
			build_verbose_mode="--quiet"
			if ${OPTION_VERBOSE_MODE:-true}; then
				echo -n "       "
				build_verbose_mode="--noverbose"
			fi
			if ! ( export LOGFILE_OVERWRITE_FILES_WARNINGS=${SYSTEM_LOGFILE_OVERWRITE_FILES_WARNINGS} \
					&& cd ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]} \
					&& sh ${SYSDB_PACKAGE_BUILDTOOL_SCRIPT[${count}]} --overwrite ${build_verbose_mode} ); then
				# If we found any existing files and HALT option is enabled: stop the build.
				if ${OPTION_DEBUG_OVERWRITE_HALT}; then
					echo
					echo "This package will overwrite existing files!"
					echo "Logfile: ${SYSTEM_LOGFILE_OVERWRITE_FILES_WARNINGS}"
					echo "Option 'OPTION_DEBUG_OVERWRITE_HALT' is enabled, exiting now!"
					return 2
				fi

			fi
		fi

		# Remove extra packages that will not be updated.
		if [ ${OPTION_REMOVE_ALL_PKG_BEFORE_INSTALL} == false ]; then
			# Any packages to remove?
			if [ x"${SYSDB_PACKAGE_REMOVE[${count}]}" != x"" ]; then
				# Yes, check for packages other then those that will be installed...
				remove_package_notice=true
				for package in $( find ${BUILD_BINARY_DIR}/ -type f -name "*t[x|g|l|b]z" -printf "%f\n"); do
					# Get the name of the package:
					package_name=${package%-*}			# Remove build
					package_name=${package_name%-*}		# Remove arch
					package_name=${package_name%-*}		# Remove version
					for remove_package in ${SYSDB_PACKAGE_REMOVE[${count}]}; do
						# Does package still exist?
						if [ -e /var/log/packages/${remove_package} ]; then
							# Yes, will the package be replaced?
							package_name_temp=${remove_package%-*}			# Remove build
							package_name_temp=${package_name_temp%-*}		# Remove arch
							package_name_temp=${package_name_temp%-*}		# Remove version
							if [ x"${package_name}" != x"${package_name_temp}" ]; then
								# No, remove extra package...
								if ${remove_package_notice:-false}; then
									echo ""
									echo "Removing installed package(s):"
									remove_package_notice=false
								fi
								echo "  => ${remove_package}"
								removepkg "${remove_package}" &>/dev/null
								echo "  => REMOVED by '${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}': ${remove_package}" >>${SYSTEM_LOGFILE_REMOVED}
							else
								echo "UPGRADED: ${remove_package}" >>${SYSTEM_LOGFILE_REMOVED}
							fi
						fi
					done
				done
			fi
		fi

		# If not in verbose mode calculate uncompressed package size.
		if ! ${OPTION_VERBOSE_MODE:-true}; then
			let uncompressedb=0
			for package in $( find ${BUILD_BINARY_DIR}/ -type f -name "*t[x|g|l|b]z" -print); do
				# Calculate the uncompressed size of the package in bytes/kbytes.
				case ${package} in
					*txz)	pkgsizeb="$(xz     -dc ${package} | dd 2>&1 | tail -n 1 | cut -f 1 -d ' ')"
							;;
					*tgz)	pkgsizeb="$(gunzip -dc ${package} | dd 2>&1 | tail -n 1 | cut -f 1 -d ' ')"
							;;
					*tlz)	pkgsizeb="$(lzip   -dc ${package} | dd 2>&1 | tail -n 1 | cut -f 1 -d ' ')"
							;;
					*tbz)	pkgsizeb="$(bzip2  -dc ${package} | dd 2>&1 | tail -n 1 | cut -f 1 -d ' ')"
							;;
				esac
				let uncompressedb=uncompressedb+pkgsizeb
			done

			# If size is > 1024Bytes calculate KBytes/MBytes.
			if [ ${uncompressedb:-0} -gt 1024 ]; then
				if [ ${uncompressedb:-0} -gt 1048576 ]; then
					uncompressed="    $(expr ${uncompressedb} / 1024 / 1024)"
					uncompressed="${uncompressed:${#uncompressed}-4:4} M"
				else
					uncompressed="    $(expr ${uncompressedb} / 1024)"
					uncompressed="${uncompressed:${#uncompressed}-4:4} K"
				fi
			else
				uncompressed="    $(expr ${uncompressedb})"
				uncompressed="${uncompressed:${#uncompressed}-4:4} B"
			fi
			echo "[$uncompressed]"
		fi

		# Install all packages, move them to package directory and display build-time.
		for package in $( find ${BUILD_BINARY_DIR}/ -type f -name "*t[x|g|l|b]z" -printf "%f\n"); do

			# Get the name of the package:
			package_name=${package%-*}		# Remove build
			package_name=${package_name%-*}		# Remove arch
			package_name=${package_name%-*}		# Remove version

			# Get package compression type
			package_compression_type=${package##*.}

			# Move new package to package directory.
			mkdir -p ${SYSTEM_PATH_PACKAGES}/${buildtool_group}
			mv ${BUILD_BINARY_DIR}/${package} ${SYSTEM_PATH_PACKAGES}/${buildtool_group}

			# Support for MD5SUMs for each created package.
			if ${OPTION_CREATE_PKGMD5}; then
				( cd ${SYSTEM_PATH_PACKAGES}/${buildtool_group} && md5sum ${package} >${package%.${package_compression_type}}.md5 )
			fi

			# Support for 'package-version-arch.txt', used for slack repositories.
			if ${OPTION_CREATE_PKGTXT}; then
				( cd ${SYSTEM_PATH_PACKAGES}/${buildtool_group} && tar --to-stdout -xf ${package} "install/slack-desc" | grep "^${SYSDB_PACKAGE_NAME[${count}]}:" >${package%.${package_compression_type}}.txt )
			fi

			# Nuke existing logfiles, just to make sure.
			rm -f ${SYSTEM_BUILD_INSTALL_PACKAGE}
			rm -f ${SYSTEM_BUILD_INSTALL_PACKAGE_ERRORS}

			# Install the package
			( cd ${SYSTEM_PATH_PACKAGES} \
				&& upgradepkg --install-new --reinstall ${buildtool_group}/${package} \
					1>${SYSTEM_BUILD_INSTALL_PACKAGE} \
					2>${SYSTEM_BUILD_INSTALL_PACKAGE_ERRORS}
			)

			# Package installed successfully?
			if [ ! -e /var/log/packages/${package%.t[x|g|l|b]z} ]; then
				# Should not happen, but just in case it happens we
				# leave a ERROR message in the logfile.
				echo "  ** ERROR **" >>${SYSTEM_BUILD_INSTALL_PACKAGE_ERRORS}
				echo "  Package ${package} not installed!" >>${SYSTEM_BUILD_INSTALL_PACKAGE_ERRORS}
			fi

			# Print package description.
			if ${OPTION_VERBOSE_MODE:-true}; then
				cat ${SYSTEM_BUILD_INSTALL_PACKAGE} | sed "s,^${package_name}:,>>,"
			fi

			# If we got an error while install the packages print
			# the messages to the error logfile.
			cat ${SYSTEM_BUILD_INSTALL_PACKAGE_ERRORS} | tee -a ${SYSTEM_LOGFILE_BUILDSYS_ERROR}

			# Delete temporary logfiles.
			rm -f ${SYSTEM_BUILD_INSTALL_PACKAGE}
			rm -f ${SYSTEM_BUILD_INSTALL_PACKAGE_ERRORS}

			# Add package name to logfile.
			echo "${package%.${package_compression_type}}" >>${SYSTEM_LOGFILE_NEW_PACKAGES}

			# Delete the installed sources.
			if ${OPTION_DELETE_INSTALLED_SOURCES}; then
				for source in ${SYSDB_PACKAGE_SOURCE[${count}]//@@@/ }; do
					rm -vf "${SOURCE_PATH}/${source}" 1>>${SYSTEM_LOGFILE_REMOVED_SOURCES} 2>>${SYSTEM_LOGFILE_REMOVED_SOURCES}
				done
			fi

			# Move packages to NFS share to save some disk space.
			if ${OPTION_MOVE_INSTALLED_PACKAGES}; then
				mkdir -p "${SYSTEM_PATH_PKGDIR}/${buildtool_group}"
				package_md5=$(md5sum "${SYSTEM_PATH_PACKAGES}/${buildtool_group}/${package}" | sed "s, .*,,g")
				for move_to_cache_retry_count in 1 2 3; do
					cp "${SYSTEM_PATH_PACKAGES}/${buildtool_group}/${package}" "${SYSTEM_PATH_PKGDIR}/${buildtool_group}/"
					if [ x$(md5sum "${SYSTEM_PATH_PKGDIR}/${buildtool_group}/${package}" | sed "s, .*,,g")  == x${package_md5} ]; then
						rm "${SYSTEM_PATH_PACKAGES}/${buildtool_group}/${package}"
						break
					fi
					# Maybe network problems? Wait a few seconds and retry...
					echo ${RULER1}
					echo "OOps! Package ${buildtool_group}/${package} not moved to cache!"
					echo "Will retry in 5 seconds..."
					echo ${RULER1}
					SLEEPMODE 5
				done
				# Package moved to cache?
				if test -e "${SYSTEM_PATH_PACKAGES}/${buildtool_group}/${package}"; then
					echo ${RULER1}
					echo "Error! Package ${buildtool_group}/${package} not moved to cache!"
					echo ${RULER1}
					return 1
				fi
			fi

		done

		# Run ldconfig to update library-database.
		ldconfig 1>>${SYSTEM_LOGFILE_BUILDSYS_CONFIG} 2>>${SYSTEM_LOGFILE_BUILDSYS_ERROR}




		# Update the timer to calculate build-time.
		FUNC_SYNC_SYSTEM_TIME

		# Display build time.
		let timer_end=$(date +%s)
		let timer_diff_seconds=timer_end-timer_start
		if [ ${timer_diff_seconds} -eq 0 ]; then
			let timer_diff_seconds=1
		fi

		# Package installed from cache?
		if test -d ${SYSTEM_PATH_CACHE}; then
			cache_package_name=$(find ${SYSTEM_PATH_CACHE}/ -maxdepth 1 -mindepth 1 -name ${SYSDB_PACKAGE_NAME[${count}]}-${SYSDB_PACKAGE_VERSION[${count}]}${SYSDB_PACKAGE_RELEASE[${count}]}-${SYSDB_PACKAGE_ARCH[${count}]}-${SYSDB_PACKAGE_BUILD[${count}]}${PACKAGE_TAG}.t[xglb]z -printf "%f\n")
		else
			cache_package_name=""
		fi
		if [ x"${cache_package_name}" != x"" ]; then
			let timer_diff_seconds=lastrun_seconds
		fi

		# Reset buildtime?
		if test -e ${SYSTEM_PATH_TEMP}/RESET; then
			rm ${SYSTEM_PATH_TEMP}/RESET
			let timer_diff_seconds=lastrun_seconds
			let BUILDTIME_SECONDS_ALL=BUILDTIME_SECONDS_ALL_LASTRUN
		fi

		# Sometimes we get a difference >21600 (6hours)...
		# Happens on virtual machines that get paused for some hours.
		let timer_check_seconds=timer_diff_seconds-lastrun_seconds
		if [ ${timer_check_seconds} -gt 21600 ]; then
			if [ ${speed_percent:-100} -gt 10 ]; then
				echo "timer_start: ${timer_start}"					 >${SYSTEM_PATH_LOGFILES_SYSTEM}/DEBUG_CACHE_${buildtool_name}
				echo "timer_end  : ${timer_end}"					>>${SYSTEM_PATH_LOGFILES_SYSTEM}/DEBUG_CACHE_${buildtool_name}
				echo "timer_diff_seconds : ${timer_diff_seconds}"	>>${SYSTEM_PATH_LOGFILES_SYSTEM}/DEBUG_CACHE_${buildtool_name}
				let timer_diff_seconds=lastrun_seconds
			fi
		fi

		let timer_diff_minutes=timer_diff_seconds/60
		if [ ${timer_diff_minutes} -eq 0 ]; then
			timer_diff_minutes=1
		fi
		let package_speed_factor=lastrun_seconds*100/timer_diff_seconds

		# How long did it take to build/compile the package?
		if ${OPTION_VERBOSE_MODE:-true}; then
			# Only print this info to screen in verbose mode.
			echo "Time needed to build this package: ${timer_diff_minutes}min. (Ratio:${lastrun_seconds}/${timer_diff_seconds} Speed:${package_speed_factor}%)"
		fi
		# Always print this info to the logfile.
		# 12345678901234567890123456789012345678901234567890123456789012345678901234567890
		# Time needed to build 'buildtool-name'............... :99999s [ 99999s |9999% ]
		pkginfo="Time needed to build '${buildtool_name}'................................................................................"
		pkginfo="${pkginfo:0:52}"
		lastrun_seconds_text="${lastrun_seconds}s      "
		lastrun_seconds_text="${lastrun_seconds_text:0:6}"
		timer_diff_seconds_text="     ${timer_diff_seconds}"
		timer_diff_seconds_text="${timer_diff_seconds_text:${#timer_diff_seconds_text}-5:5}s"
		package_speed_factor_text="    ${package_speed_factor}"
		package_speed_factor_text="${package_speed_factor_text:${#package_speed_factor_text}-4:4}%"
		echo "${pkginfo} : ${timer_diff_seconds_text} [ ${lastrun_seconds_text}|${package_speed_factor_text} ]" >>${SYSTEM_LOGFILE_BUILDTIME}

		# Update package ETA.
		sed -i "s,^ETA:.*,ETA:${timer_diff_seconds}," ${SYSDB_PACKAGE_BUILDTOOL_PATH[${count}]}/.config

		# Calculate remaining time according to difference on last package.
		let SELECTED_PACKAGES_ETA_REMAINING=SELECTED_PACKAGES_ETA_REMAINING-lastrun_seconds
		let BUILDTIME_SECONDS_ALL=BUILDTIME_SECONDS_ALL+timer_diff_seconds
		let BUILDTIME_SECONDS_ALL_LASTRUN=BUILDTIME_SECONDS_ALL_LASTRUN+lastrun_seconds




		# If not in verbose mode... all done!
		if ${OPTION_VERBOSE_MODE:-true}; then
			echo "Done!"
			echo
		fi
	fi

	rm -rf ${BUILD_BINARY_DIR}
	rm -rf ${SYSTEM_PATH_TEMP_BUILD}
	rm -f ${SYSTEM_BUILD_LOGFILE_TEMP1}
	rm -f ${SYSTEM_BUILD_LOGFILE_TEMP2}
	rm -f ${SYSTEM_PATH_TEMP}/COMPILE
	return 0
}

# Finalize the build.
BUILDSYS_FINALIZE_BUILD() {

	## Build finished, do some maintenance.
	FUNC_SYSTEM_UPDATE_DESKTOP_DATABASE
	FUNC_SYSTEM_UPDATE_GCONF_DATABASE
	FUNC_SYSTEM_UPDATE_GIOMODULES_CACHE
	FUNC_SYSTEM_UPDATE_GLIB_SCHEMAS
	FUNC_SYSTEM_UPDATE_ICON_CACHE
	FUNC_SYSTEM_CREATE_DUMMY_GTK_BOOKMARKS

	## Check for new configuration files.
	BUILDSYS_CHECK_ETC_NEW_FILES

	## Check for changes in /etc/sudoers.
	BUILDSYS_CHECK_ETC_SUDOERS

	return 0
}

# Build finished.
BUILDSYS_CONGRATULATIONS() {
	dialog	--title "${PROJECT_NAME} - INFORMATION" \
			--backtitle "${PROJECT_BACKTITLE}" \
			--cr-wrap \
			--msgbox "\n\
Congratulations!\n\
\n\
The ${PROJECT_NAME} has successfully installed all packages you have selected. Your system should now be ready for a reboot.\n\
\n\
You will find a copy of all installed packages in the temp/packages directory incl. a small script named '$( basename ${SCRIPT_NAME_INSTALLPKG} )' that you can use to reinstall all packages.\n\
\n\
Have fun!.\n" 15 75
}

# Display the Logfiles created during build process
BUILDSYS_SHOW_LOGFILES() {
	local count=0
	local line=""
	local line_temp1=""

	cat <<EOF >${DIALOG_SCRIPT}
		dialog	--title "${PROJECT_NAME} - BUILD FINISHED" \\
				--backtitle "${PROJECT_BACKTITLE}" \\
				--cr-wrap \\
				--ok-label "${BUTTON_LABEL_OK}" \\
				--cancel-label "${BUTTON_LABEL_EXIT}" \\
				--menu "\\
There are a few logfiles available that show the things that have been done during this build. \\
Select 'EXIT' to quit.\n\\
\n\\
Select one of the following options:" 20 75 10 \\
"MASTER"    "Display the master logfile" \\
"CONFIG"    "List of build errors while configure the packages" \\
"ERROR"     "List of build errors while compile the packages" \\
EOF

	if test -e ${SYSTEM_LOGFILE_OVERWRITE_FILES_WARNINGS}; then
		cat <<EOF >>${DIALOG_SCRIPT}
"OVERWRITE"  "List of replaced files included in other packages" \\
EOF
	fi

	if test -e ${SYSTEM_LOGFILE_BUILDARCH_WARNINGS}; then
		cat <<EOF >>${DIALOG_SCRIPT}
"BUILDARCH"  "List of packages that do not use --BUILD=ARCH option" \\
EOF
	fi

	if test -e ${SYSTEM_LOGFILE_USRLOCAL_WARNINGS}; then
		cat <<EOF >>${DIALOG_SCRIPT}
"USRLOCAL"  "List of packages that install files to /usr/local" \\
EOF
	fi

	if test -e ${SYSTEM_LOGFILE_SUID_WARNINGS}; then
		cat <<EOF >>${DIALOG_SCRIPT}
"SUID"       "List of packages that include files with SUID" \\
EOF
	fi

	if test -e ${SYSTEM_LOGFILE_RPATH_WARNINGS}; then
		cat <<EOF >>${DIALOG_SCRIPT}
"RPATH"      "List of packages that include files with RPATH" \\
EOF
	fi

	if test -e ${SYSTEM_LOGFILE_SHAREDLIBS_WARNINGS}; then
		cat <<EOF >>${DIALOG_SCRIPT}
"SHAREDLIBS" "List of packages including non-executable shared libraries." \\
EOF
	fi

	if test -e ${SYSTEM_LOGFILE_EMPTYDIR_WARNINGS}; then
		cat <<EOF >>${DIALOG_SCRIPT}
"EMPTYDIR"   "List of packages with warnings about empty directories" \\
EOF
	fi

	if test -e ${SYSTEM_LOGFILE_DOINST_WARNINGS}; then
		cat <<EOF >>${DIALOG_SCRIPT}
"DOINST"     "List of packages with bad doinst.sh scripts" \\
EOF
	fi

	if test -e ${SYSTEM_LOGFILE_DOCFILES_WARNINGS}; then
		cat <<EOF >>${DIALOG_SCRIPT}
"DOCFILES"   "List of packages with documentation warnings" \\
EOF
	fi

	if test -e ${SYSTEM_LOGFILE_APPLIED_PATCHES}; then
		cat <<EOF >>${DIALOG_SCRIPT}
"PATCHES"   "List of packages with applied patches" \\
EOF
	fi

	if test -e ${SYSTEM_LOGFILE_GTK_ICON_CACHE}; then
		cat <<EOF >>${DIALOG_SCRIPT}
"ICACHE"    "List of GTK icon cache updates" \\
EOF
	fi

	if test -e ${SYSTEM_LOGFILE_RM_LA_FILES}; then
		cat <<EOF >>${DIALOG_SCRIPT}
"RMLAFILES" "List of removed '*.la' files" \\
EOF
	fi

	if test -e ${SYSTEM_LOGFILE_CHECK_LA_FILES}; then
		cat <<EOF >>${DIALOG_SCRIPT}
"LAFILES"   "List of remaining '*.la' files" \\
EOF
	fi

	if test -e ${SYSTEM_LOGFILE_REMOVED}; then
		cat <<EOF >>${DIALOG_SCRIPT}
"REMOVED"   "Show a list of packages that have been removed" \\
EOF
	fi

	if test -e ${SYSTEM_LOGFILE_NEW_PACKAGES}; then
		cat <<EOF >>${DIALOG_SCRIPT}
"PACKAGES"  "Show a list of packages that have been installed" \\
EOF
	fi

	if test -e ${SYSTEM_LOGFILE_PACKAGE_UPDATES}; then
		cat <<EOF >>${DIALOG_SCRIPT}
"UPDATES"   "Show a list of packages that have been updated" \\
EOF
	fi

	if test -e ${SYSTEM_LOGFILE_DOWNLOADS}; then
		cat <<EOF >>${DIALOG_SCRIPT}
"DOWNLOADS" "Show a list of packages that have been downloaded" \\
EOF
	fi

	if test -e ${SYSTEM_LOGFILE_BUILDTIME}; then
		cat <<EOF >>${DIALOG_SCRIPT}
"BUILDTIME" "How much time needed to build a package?" \\
EOF
	fi

	cat <<EOF >>${DIALOG_SCRIPT}
"PKGLIST"   "Copy list of packages to 'packages.conf.overwrite'" \\
2> ${DIALOG_RETURN_VALUE}
EOF

	while [ true ] ; do

		# Start the script and offer the version select dialog.
		. "${DIALOG_SCRIPT}"

		DIALOG_KEYCODE=$?
		DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
		rm -f "${DIALOG_RETURN_VALUE}"

		# 'CANCEL' or 'ESC' ?
		if test ${DIALOG_KEYCODE} -ne 0; then
			FUNC_CLEAR_SCREEN
			break
		fi


		case ${DIALOG_REPLY} in
			"EXIT")			FUNC_CLEAR_SCREEN
							break
							;;

			"DOWNLOADS")	dialog	--title "${PROJECT_NAME} - DOWNLOAD-LOGFILE" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_DOWNLOADS} 20 75
							continue
							;;

			"MASTER")		dialog	--title "${PROJECT_NAME} - MASTER-LOGFILE" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_BUILDSYS_MASTER} 20 75
							continue
							;;

			"REMOVED")		dialog	--title "${PROJECT_NAME} - REMOVED PACKAGES" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_REMOVED} 20 75
							continue
							;;

			"PACKAGES")		dialog	--title "${PROJECT_NAME} - INSTALLED PACKAGES" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_NEW_PACKAGES} 20 75
							continue
							;;

			"CONFIG")		dialog	--title "${PROJECT_NAME} - CONFIG-LOGFILE" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_BUILDSYS_CONFIG} 20 75
							continue
							;;

			"ERROR")		dialog	--title "${PROJECT_NAME} - ERROR-LOGFILE" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_BUILDSYS_ERROR} 20 75
							continue
							;;

			"BUILDARCH")	dialog	--title "${PROJECT_NAME} - BUILDARCH-WARNINGS" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_BUILDARCH_WARNINGS} 20 75
							continue
							;;

			"USRLOCAL")		dialog	--title "${PROJECT_NAME} - USRLOCAL-WARNINGS" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_USRLOCAL_WARNINGS} 20 75
							continue
							;;

			"SUID")			dialog	--title "${PROJECT_NAME} - SUID-WARNINGS" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_SUID_WARNINGS} 20 75
							continue
							;;

			"RPATH")		dialog	--title "${PROJECT_NAME} - RPATH-WARNINGS" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_RPATH_WARNINGS} 20 75
							continue
							;;

			"SHAREDLIBS")		dialog	--title "${PROJECT_NAME} - SHAREDLIBS-WARNINGS" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_SHAREDLIBS_WARNINGS} 20 75
							continue
							;;

			"EMPTYDIR")		dialog	--title "${PROJECT_NAME} - EMPTYDIR-WARNINGS" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_EMPTYDIR_WARNINGS} 20 75
							continue
							;;

			"DOINST")		dialog	--title "${PROJECT_NAME} - DOINST-WARNINGS" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_DOINST_WARNINGS} 20 75
							continue
							;;

			"DOCFILES")		dialog	--title "${PROJECT_NAME} - DOCFILE-WARNINGS" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_DOCFILES_WARNINGS} 20 75
							continue
							;;

			"PATCHES")		dialog	--title "${PROJECT_NAME} - APPLIED PATCHES" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_APPLIED_PATCHES} 20 75
							continue
							;;

			"ICACHE")		dialog	--title "${PROJECT_NAME} - GTK ICON CACHE UPDATES" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_GTK_ICON_CACHE} 20 75
							continue
							;;

			"RMLAFILES")		dialog	--title "${PROJECT_NAME} - REMOVED *.LA FILES" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_RM_LA_FILES} 20 75
							continue
							;;

			"LAFILES")		dialog	--title "${PROJECT_NAME} - REMAINING *.LA FILES" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_CHECK_LA_FILES} 20 75
							continue
							;;

			"UPDATES")		dialog	--title "${PROJECT_NAME} - PACKAGES UPDATED" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_PACKAGE_UPDATES} 20 75
							continue
							;;

			"OVERWRITE")	dialog	--title "${PROJECT_NAME} - REPLACED FILES" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_OVERWRITE_FILES_WARNINGS} 20 75
							continue
							;;

			"BUILDTIME")	dialog	--title "${PROJECT_NAME} - TIME NEEDED FOR EACH PACKAGE" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap --exit-label "${BUTTON_LABEL_OK}" \
									--textbox ${SYSTEM_LOGFILE_BUILDTIME} 20 75
							continue
							;;

			"PKGLIST")		dialog	--title "${PROJECT_NAME}" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap \
									--infobox "\nCreating config file...\n " 5 40
							cat ${SYSTEM_PATH_LOGFILES_SYSTEM}/$(basename ${SYSTEM_JOB_PACKAGES}) >${SYSTEM_CONFIG_OVERWRITE}
							dialog	--title "${PROJECT_NAME} - INFORMATION" --backtitle "${PROJECT_BACKTITLE}" --cr-wrap \
									--msgbox "\n\
File '$(basename ${SYSTEM_CONFIG_OVERWRITE})' has been created. Until you do not remove this file only a customized package list will be used.\n\n\
If you want to start a new build with all packages then remove this file:\n\
\n\
${SYSTEM_CONFIG_OVERWRITE}\n " 0 0
							continue
							;;

		esac

	done

	# Delete dialog script.
	rm -f "${DIALOG_SCRIPT}"

}

# Just a separator...
SYS____________________MAIN_MENU() {
	true
}

# Main menu.
BUILDSYS_MAIN_MENU() {
	local default_item="${RUNTIME_MAIN_MENU_OPTION}"

	cat <<EOF >${DIALOG_SCRIPT}
dialog	--title "WELCOME TO ${PROJECT_NAME} ${PROJECT_VERSION}" \\
		--backtitle "${PROJECT_BACKTITLE}" \\
		--ok-label "${BUTTON_LABEL_OK}" \\
		--cancel-label "${BUTTON_LABEL_EXIT}" \\
		--default-item "${default_item}" \\
		--item-help \\
		--menu "\\
This script will help you to compile applications and libraries for your (fresh) installation of Slackware Linux. \\
${PROJECT_NAME} should only be used when you want to compile multiple packages from source, for example to build the KDE desktop.\n\\
\n\\
	If you want to install the KDE desktop select 'SOURCE' to start.\n" 20 75 8 \\
	"README"   "First time users should start here"                       "If this is the first time you are using ${PROJECT_NAME} then you should read this first!" \\
EOF

	if [ x"${RUNTIME_PACKAGE_CONFIG_FILE}" != x"${SYSTEM_CONFIG_BUILDTOOLS_TEMP}" ]; then
		cat <<EOF >>${DIALOG_SCRIPT}
	"SOURCE"    "Build packages from source using Buildtools"              "Build and install multiple packages, for example the KDE desktop" \\
EOF
	fi

	cat <<EOF >>${DIALOG_SCRIPT}
	"SINGLE"    "Build individual/selected packages"                       "Compile single packages from source using Buildtools" \\
	"PACKAGES"  "Show a list of packages included in the ${PROJECT_NAME}"  "Show a list of packages included in the ${PROJECT_NAME}" \\
	"DOWNLOAD"  "Build source package database (3Gb of downloads)"         "Download all source packages and show a list off missing files" \\
EOF

	cat <<EOF >>${DIALOG_SCRIPT}
	"BUILDTOOL" "Download latest buildtools     ** EXPERIMENTAL **"        "Get the latest builodtools from the online archive **EXPERIMENTAL**" \\
	"UPDATE"    "Update installed packages      ** EXPERIMENTAL **"        "Search for installed packages and check for newer buildtools **EXPERIMENTAL**" \\
	"SYNC"      "Sync installed packages        ** EXPERIMENTAL **"        "Search for installed packages and check for different buildtools **EXPERIMENTAL**" \\
EOF

	if ${RUNTIME_OPTION_DEBUG_MODE}; then
		cat <<EOF >>${DIALOG_SCRIPT}
	"CHKSRC"    "DEBUG: Check source packages and update buildtools"       "Check source package database and update buildtool scripts if neccessary" \\
	"UPGRADE"   "DEBUG: Update existing packages and install new packages" "Search for older packages to be updated or packages not yet installed" \\
EOF
	fi

	cat <<EOF >>${DIALOG_SCRIPT}
	"CONFIG"    "Edit the ${PROJECT_NAME} configuration file"              "Edit the ${PROJECT_NAME} configuration file and change some default settings." \\
	"PROFILE"   "Create/Edit your profile/buildtool options"               "Create/Edit your profile in /etc/profile.d to set some default buildtool options." \\
	"SCRIPTS"   "Rebuild the scripts to re-install/remove packages"        "Create scripts to automatically install or remove previously build packages" \\
	"ICACHE"    "Update the GTK+ icon cache"                               "Update the GTK+ icon cache" \\
EOF

	cat <<EOF >>${DIALOG_SCRIPT}
2> ${DIALOG_RETURN_VALUE}
EOF

	# Start the script and offer the version select dialog.
	. "${DIALOG_SCRIPT}"

	DIALOG_KEYCODE=$?
	DIALOG_REPLY=$(cat "${DIALOG_RETURN_VALUE}" | sed "s,\\\",,g")
	rm -f "${DIALOG_SCRIPT}"
	rm -f "${DIALOG_RETURN_VALUE}"

	# 'CANCEL' or 'ESC' ?
	if test ${DIALOG_KEYCODE} -ne 0; then
		FUNC_CLEAR_SCREEN
		return 1
	fi

	RUNTIME_SETUP_MODE=${DIALOG_REPLY}
	return 0
}

# Download all source packages.
BUILDSYS_MAIN_DOWNLOAD_ALL_SOURCE_PACKAGES() {
	local list_missing_sources=""
	local list_missing_external_sources=""
	local list_missing_x86_64_sources=""
	local list_missing_x86_32_sources=""
	local count=0
	local text0=""
	local text1=""
	local text2=""
	local text3=""
	local text4=""
	local package_count=0
	local package_list=""
	local download_mode=""
	local force_download=""
	local key=""
	local language=""
	local package_dir=""
	local package=""
	local pkginfo_file=${SYSTEM_PATH_TEMP}/pkginfo
	local check=""

	if ${RUNTIME_DOWNLOAD_ALL_SOURCES_QUIET_MODE}; then
		FUNC_CLEAR_SCREEN
		echo
		echo ${RULER1}
		echo "Checking buildtool configuration file..."
		echo ${RULER1}
		echo
	fi

	# Check buildtool configuration file.
	CONFIGURE_CHECK_BUILDTOOL_CONFIGURATION_FILE || exit 1
	if ${RUNTIME_DOWNLOAD_ALL_SOURCES_QUIET_MODE}; then
		echo
		echo "Done!"
		echo
	fi

	if ! ${RUNTIME_DOWNLOAD_ALL_SOURCES_QUIET_MODE}; then
		dialog	--title "${PROJECT_NAME} - WARNING" \
				--backtitle "${PROJECT_BACKTITLE}" \
				--cr-wrap \
				--yes-label "${BUTTON_LABEL_YES}" \
				--no-label "${BUTTON_LABEL_NO}" \
				--ok-label "${BUTTON_LABEL_YES}" \
				--cancel-label "${BUTTON_LABEL_NO}" \
				--defaultno \
				--yesno "\
\n
This script will now try to download all available source packages, even for those packages you maybe do not want to build. \
The complete download can take up to 3Gb. Make sure you have a fast internet connection and enough free disk space.\n
\n
Building the source package database first may have at least one advantage: \
You will see if there is something missing before you start to play with build/install the packages you want.\n
\n
If you select 'SOURCE' from the main menu only the source packages required will be downloaded, but you have to go through the complete setup before you will know if there is something missing.\n
\n
Start downloading all missing source-packages now?\n
" 20 75

		DIALOG_KEYCODE=$?

		# Cancel or ESC?
		if test ${DIALOG_KEYCODE} -ne 0; then
			return 0
		fi

	fi

	if ! ${RUNTIME_DOWNLOAD_ALL_SOURCES_QUIET_MODE}; then
		dialog	--title "${PROJECT_NAME}" \
				--backtitle "${PROJECT_BACKTITLE}" \
				--infobox "\nMoving existing source packages into source package pool...\n " 6 40
	else
		echo
		echo ${RULER1}
		echo "Moving existing source packages into source package pool..."
		echo ${RULER1}
		echo
	fi

	#find ${SYSTEM_PATH_SOURCES}/ -type f -exec mv {} ${SYSTEM_PATH_DOWNLOADS} \;
	for package_dir in $(find ${SYSTEM_PATH_SOURCES}/ -type d -maxdepth 1 -mindepth 1 -printf "%f\n" | grep "." | sort); do
		mkdir -p ${SYSTEM_PATH_DOWNLOADS}/${package_dir}
		for package in $(find ${SYSTEM_PATH_SOURCES}/${package_dir}/ -type f -print | grep "." | sort); do
			mv ${package} ${SYSTEM_PATH_DOWNLOADS}/${package_dir}/
		done
		rm -rf ${SYSTEM_PATH_SOURCES}/${package_dir} &>/dev/null
	done

	if ! ${RUNTIME_DOWNLOAD_ALL_SOURCES_QUIET_MODE}; then
		FUNC_CLEAR_SCREEN
	else
		echo
		echo "Done!"
		echo
	fi

	echo
	echo ${RULER1}
	echo "Validating source package database..."
	echo ${RULER1}
	echo

	# Delete existing logfile.
	rm -f ${SYSTEM_LOGFILE_DOWNLOADS}
	rm -f ${SYSTEM_LOGFILE_DOWNLOAD_ERROR}
	rm -f ${DIALOG_TEMP_FILE1}
	rm -f ${DIALOG_TEMP_FILE2}

	package_list=$(grep -v "#" ${SYSTEM_CONFIG_BUILDTOOLS} | grep ".")
	package_count=$(grep -v "#" ${SYSTEM_CONFIG_BUILDTOOLS} | grep -c ".")

	# Initialize logfile.
	cat <<EOF >${SYSTEM_LOGFILE_DOWNLOADS}

${RULER1}
$(date +%c)
Downloading missing source-packages...
${RULER1}
EOF

	for package in ${package_list}; do
		let count=count+1

		# Download only files included in single.conf file?
		if ${RUNTIME_DOWNLOAD_SINGLE_MODE_ONLY}; then
			if [ x"${SYSDB_PACKAGE_ORIGINAL_NAME[${count}]}" == x"" ]; then
				SYSDB_PACKAGE_ORIGINAL_NAME[${count}]=${package##*/}
			fi
			FUNC_CHECK_SINGLE_MODE_SELECTED ${count}
			if [ x"${RETURN_VALUE}" == x"off" ]; then
				continue
			fi
		fi

		# Running in DEBUG mode?
		if ! ${RUNTIME_OPTION_DEBUG_MODE}; then
			# No, check for english/german packages.
			language=$(grep -m1 "^LANGUAGE:" ${SYSTEM_PATH_BUILDTOOLS}/${package}/.config )
			if [ x"${language}" != x"" ]; then
				case ${language##*:} in
					GERMAN)		if ! ${OPTION_BUILD_GERMAN_PACKAGES}; then continue; fi;;
					ENGLISH)	if   ${OPTION_BUILD_GERMAN_PACKAGES}; then continue; fi;;
				esac
			fi
		else
			# In debug mode always skip english packages since me is german ;)
			language=$(grep -m1 "^LANGUAGE:" ${SYSTEM_PATH_BUILDTOOLS}/${package}/.config )
			if [ x"${language}" != x"" ]; then
				case ${language##*:} in
					ENGLISH)	continue;;
				esac
			fi
		fi

		export SOURCE_PATH="${SYSTEM_PATH_SOURCES}/${package%%/*}"
		export SOURCE_CACHE="${SYSTEM_PATH_DOWNLOADS}/${package%%/*}"
		export LOGFILE_DOWNLOAD_ERROR=${SYSTEM_LOGFILE_DOWNLOAD_ERROR}

		# Creat source path.
		mkdir -p ${SOURCE_PATH}

		# Get package info from buildtool.
		group="${package%%/*}"
		buildtool="${package##*/}"
		buildtool_path="${SYSTEM_PATH_BUILDTOOLS}/${group}/${buildtool}"
		buildtool_script="${SYSTEM_PATH_BUILDTOOLS}/${group}/${buildtool}/${buildtool}.${BUILDTOOL_SUFFIX}"
		grep "^PACKAGE_.*=" ${buildtool_script} >${pkginfo_file}
		. ${pkginfo_file}

		# Print status message...
		text0="0000${count}"
		text1="0000${package_count}"
		text2="[${text0:${#text0}-4:4}/${text1:${#text1}-4:4}]"
		text3="${package##*/}${SPACEBAR}"
		text3="${text3:0:33}"
		text4="${PACKAGE_VERSION}${PACKAGE_RELEASE_TAG}${SPACEBAR}"
		text4="${text4:0:13}"
		echo -n "${text2}  ${text3} ${text4} > " | tee -a ${SYSTEM_LOGFILE_DOWNLOADS}

		# Skip checking of MD5SUM? If true, only download the sources.
		if ( grep "^SKIPMD5" ${SYSTEM_PATH_BUILDTOOLS}/${package}/.config &>/dev/null ); then
			download_mode="-d"
			force_download="-D"
		else
			download_mode="-m"
			force_download="-M"
		fi

		# Package blacklisted?
		check=$(grep "^${package##*/}$" ${SYSTEM_CONFIG_PACKAGE_BLACKLIST})
		if [ x"${check}" != x"" ]; then
			echo "Blacklisted!" | tee -a ${SYSTEM_LOGFILE_DOWNLOADS}
			continue
		else
			# Use the buildtool to check for the source packages.
			if ( cd ${SYSTEM_PATH_BUILDTOOLS}/${package} && sh ${package##*/}.${BUILDTOOL_SUFFIX} --checksrc --quiet 2>/dev/null ); then
				if ( cd ${SYSTEM_PATH_BUILDTOOLS}/${package} && sh ${package##*/}.${BUILDTOOL_SUFFIX} ${download_mode} --quiet 2>/dev/null ); then
					echo "OK!" | tee -a ${SYSTEM_LOGFILE_DOWNLOADS}
					continue
				fi
			fi

			# Some sources are not there, download them.
			# Packages with "external" sources?
			check=$(grep "^FLAGS:EXTERNAL$" ${SYSTEM_PATH_BUILDTOOLS}/${package}/.config )
			if [ x"${check}" != x"" ]; then
				echo "External..."
				continue
			fi
			echo "Oops!" | tee -a ${SYSTEM_LOGFILE_DOWNLOADS}
			echo -n "${SPACEBAR:0:13}Downloading..." | tee -a ${SYSTEM_LOGFILE_DOWNLOADS}
			if ( cd ${SYSTEM_PATH_BUILDTOOLS}/${package} && sh ${package##*/}.${BUILDTOOL_SUFFIX} ${force_download} --quiet 2>/dev/null ); then
				echo "OK!" | tee -a ${SYSTEM_LOGFILE_DOWNLOADS}
				continue
			fi

			# Download failed.
			( cd ${SYSTEM_PATH_BUILDTOOLS}/${package} && sh ${package##*/}.${BUILDTOOL_SUFFIX} --purgesrc --quiet 2>/dev/null )

			# Failed because of EXTERNAL sources?
			if ( grep "^FLAGS:EXTERNAL" ${SYSTEM_PATH_BUILDTOOLS}/${package}/.config &>/dev/null ); then
				echo "Missing external source!" | tee -a ${SYSTEM_LOGFILE_DOWNLOADS}
				list_missing_external_sources="${list_missing_external_sources} ${package}"
				( cd ${SYSTEM_PATH_BUILDTOOLS}/${package} && sh ${package##*/}.${BUILDTOOL_SUFFIX} --verify 2>/dev/null | grep "FAILED" | sed "s,:.*,,g" 1>${DIALOG_TEMP_FILE2} )
				if ( grep "." ${DIALOG_TEMP_FILE2} &>/dev/null ); then
					echo "  ${package}:" >>${DIALOG_TEMP_FILE1}
					cat ${DIALOG_TEMP_FILE2} | sed "s,^,    => ,g" >>${DIALOG_TEMP_FILE1}
				fi
				continue
			fi

			# Not required for x86_64?
			if ( grep "^PLATFORM:SKIP_FOR_X86_64" ${SYSTEM_PATH_BUILDTOOLS}/${package}/.config &>/dev/null ); then
				echo "Not required for x86_64!" | tee -a ${SYSTEM_LOGFILE_DOWNLOADS}
				list_missing_x86_64_sources="${list_missing_x86_64_sources} ${package}"
				( cd ${SYSTEM_PATH_BUILDTOOLS}/${package} && sh ${package##*/}.${BUILDTOOL_SUFFIX} --verify 2>/dev/null | grep "FAILED" | sed "s,:.*,,g" 1>${DIALOG_TEMP_FILE2} )
				if ( grep "." ${DIALOG_TEMP_FILE2} &>/dev/null ); then
					echo "  ${package}:" >>${DIALOG_TEMP_FILE1}
					cat ${DIALOG_TEMP_FILE2} | sed "s,^,    => ,g" >>${DIALOG_TEMP_FILE1}
				fi
				continue
			fi

			# Not required for x86_32?
			if ( grep "^PLATFORM:BUILD_ONLY_FOR_X86_64" ${SYSTEM_PATH_BUILDTOOLS}/${package}/.config &>/dev/null ); then
				echo "Not required for x86_32!" | tee -a ${SYSTEM_LOGFILE_DOWNLOADS}
				list_missing_x86_32_sources="${list_missing_x86_32_sources} ${package}"
				( cd ${SYSTEM_PATH_BUILDTOOLS}/${package} && sh ${package##*/}.${BUILDTOOL_SUFFIX} --verify 2>/dev/null | grep "FAILED" | sed "s,:.*,,g" 1>${DIALOG_TEMP_FILE2} )
				if ( grep "." ${DIALOG_TEMP_FILE2} &>/dev/null ); then
					echo "  ${package}:" >>${DIALOG_TEMP_FILE1}
					cat ${DIALOG_TEMP_FILE2} | sed "s,^,    => ,g" >>${DIALOG_TEMP_FILE1}
				fi
				continue
			fi

			# Download error.
			list_missing_sources="${list_missing_sources} ${package}"
			echo "Failed!" | tee -a ${SYSTEM_LOGFILE_DOWNLOADS}
			( cd ${SYSTEM_PATH_BUILDTOOLS}/${package} && sh ${package##*/}.${BUILDTOOL_SUFFIX} --verify 2>/dev/null | grep "FAILED" | sed "s,:.*,,g" 1>${DIALOG_TEMP_FILE2} )
			if ( grep "." ${DIALOG_TEMP_FILE2} &>/dev/null ); then
				echo "  ${package}:" >>${DIALOG_TEMP_FILE1}
				cat ${DIALOG_TEMP_FILE2} | sed "s,^,    => ,g" >>${DIALOG_TEMP_FILE1}
			fi
		fi

	done

	# Remove temporary package info file.
	rm -f ${pkginfo_file}




	# Do we miss some source packages?
	if [ x"${list_missing_sources}${list_missing_external_sources}${list_missing_x86_64_sources}${list_missing_x86_32_sources}" == x"" ]; then
		if ! ${RUNTIME_DOWNLOAD_ALL_SOURCES_QUIET_MODE}; then

			# No, all sources there...
			dialog	--title "${PROJECT_NAME} - INFORMATION" \
					--backtitle "${PROJECT_BACKTITLE}" \
					--cr-wrap \
					--msgbox "\n\
Congratulations!\n\
\n\
Your source package database seem to be complete. You have now all required source packages to compile all buildtools included in this ${PROJECT_NAME}.\n\
\n\
You can now continue to build packages and select 'SOURCE' from the main menu.\n\
\n\
Have fun!" 15 60

			return 0
		else
			cat <<EOF
${RULER1}

Congratulations!

Your source package database seem to be complete. You have now all
required source packages to compile all buildtools included in this
buildsystem release.

Have fun!

EOF
			return 0
		fi
	fi

	## Do we miss some (required) source packages?
	if [ x"${list_missing_sources}" != x"" ]; then
		if ! ${RUNTIME_DOWNLOAD_ALL_SOURCES_QUIET_MODE}; then

			## Yes, create a dialog and show a list of missing packages.
			cat <<EOF >${DIALOG_TEMP_FILE2}
${RULER1}

Sorry!

The source package database isn't  complete.  If you  want to include
the packages listed below  then download the sources manually to this
directory:

  ${SYSTEM_PATH_DOWNLOADS}

Sometimes download servers are down for some reasons. For some of the
servers you can specify an other download mirror.  Have a look to the
'$(basename ${SYSTEM_CONFIG_SETTINGS})' configuration file.
${RULER2}

EOF
		else

			## Yes, create a dialog and show a list of missing packages.
			cat <<EOF
${RULER1}

Sorry!

The source package database isn't  complete.  If you  want to include
the packages listed below  then download the sources manually to this
directory:

  ${SYSTEM_PATH_DOWNLOADS}

Sometimes download servers are down for some reasons. For some of the
servers you can specify an other download mirror.  Have a look to the
'$(basename ${SYSTEM_CONFIG_SETTINGS})' configuration file.

EOF
		fi

	else

		if ! ${RUNTIME_DOWNLOAD_ALL_SOURCES_QUIET_MODE}; then

			## No, create a dialog and show a list of missing packages.
			cat <<EOF >${DIALOG_TEMP_FILE2}
${RULER1}

Congratulations!

Nearly all source packages have been downloaded. A few extra packages
will be missing such as binary applications (will not be installed on
the x86_64 platform) or  'EXTERNAL'  source packages which require to
accept a license  before you can download the sources  (such as SUN's
Java JRE or JDK). These packages are not required to install KDE.

If you want to include packages tagged with  'EXTERNAL' then download
the sources manually to this directory:

  ${SYSTEM_PATH_DOWNLOADS}

${RULER2}

EOF

		else

			## No, create a dialog and show a list of missing packages.
			cat <<EOF
${RULER1}

Congratulations!

Nearly all source packages have been downloaded. A few extra packages
will be missing such as binary applications (will not be installed on
the x86_64 platform) or  'EXTERNAL'  source packages which require to
accept a license  before you can download the sources  (such as SUN's
Java JRE or JDK). These packages are not required to install KDE.

If you want to include packages tagged with  'EXTERNAL' then download
the sources manually to this directory:

  ${SYSTEM_PATH_DOWNLOADS}

The source packages will then be added to the database next time.

EOF

		fi

	fi

	if ! ${RUNTIME_DOWNLOAD_ALL_SOURCES_QUIET_MODE}; then
		cat ${DIALOG_TEMP_FILE1} >>${DIALOG_TEMP_FILE2}

		echo "${RULER2}" >>${DIALOG_TEMP_FILE2}

		dialog	--title "${PROJECT_NAME} - MISSING SOURCES" \
				--backtitle "${PROJECT_BACKTITLE}" \
				--cr-wrap \
				--exit-label "${BUTTON_LABEL_OK}" \
				--textbox ${DIALOG_TEMP_FILE2} 20 75

	else
		cat <<EOF

Press <ENTER> to get a list of missing packages.
EOF
		read key
		more ${DIALOG_TEMP_FILE1}
		echo
	fi

	rm -f ${DIALOG_TEMP_FILE1}
	rm -f ${DIALOG_TEMP_FILE2}

	return 0

}

# Check for updates
BUILDSYS_MAIN_CHECK_FOR_UPDATES() {
	local count=0
	local update=0
	local temp1=""
	local xorg_version_major=0
	local xorg_version_minor=0
	local xorg_installed_version_major=0
	local xorg_installed_version_minor=0

	local upgrade_mode=$1
	local sync_mode=$2
	local sync_installed=""
	local sync_buildtool=""

	# Check for installed packages...
	INITIALIZE_CHECK_INSTALLED_PACKAGES

	# Display a infobox...
	FUNC_PROGRESS_BOX "Searching for packages to update..."
	FUNC_INIT_PROGRESS_BAR

	# Search for updates/upgrades...
	count=0; while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
		let count=count+1
		FUNC_DRAW_PROGRESS_BAR ${count} ${SYSDB_PACKAGE_COUNT}

		# Disable all packages by default.
		SYSDB_PACKAGE_ENABLED[${count}]=false
		FUNC_GET_PACKAGE_NUMERIC_VERSION_ALL ${count}

		# Is the package already installed?
		if [ x"${SYSDB_PACKAGE_INSTALLED[${count}]}" != x"" ]; then
			# xorg-server?
			if [ x"${SYSDB_PACKAGE_NAME[${count}]}" == x"xorg-server" ]; then
				# yes, only check if the major/minor version matches the installed version.
				# Update xorg-server to 1.19.1 from 1.19 but do not update
				# xorg-server to 1.19.1 from 1.17.
				FUNC_CALC_NUMERIC_VERSION "${SYSDB_PACKAGE_VERSION[${count}]}"
				let xorg_version_major=${RETURN_VERSION_MAJOR}
				let xorg_version_minor=${RETURN_VERSION_MINOR}
				FUNC_CALC_NUMERIC_VERSION "${SYSDB_PACKAGE_INSTALLED_VERSION[${count}]}"
				let xorg_installed_version_major=${RETURN_VERSION_MAJOR}
				let xorg_installed_version_minor=${RETURN_VERSION_MINOR}
				if test ${xorg_version_major} -eq ${xorg_installed_version_major}; then
					if test ${xorg_version_minor} -ne ${xorg_installed_version_minor}; then
						continue
					fi
				else
					continue
				fi
			fi

			# Yes, package installed by a buildtool?
			if [ x"${SYSDB_PACKAGE_TAG[${count}]}" != x"${SYSDB_PACKAGE_INSTALLED_TAG[${count}]}" ]; then
				# No... upgrade?
				if ! ${upgrade_mode:-false}; then
					# Update only, ignore this package.
					continue
				else
					# Yes, upgrade the slackware package to latest buildtool.
					SYSDB_PACKAGE_ENABLED[${count}]=true
					let update=update+1
					continue
				fi
			fi

			# Sync mode?
			if ${sync_mode:-false}; then
				sync_buildtool=${SYSDB_PACKAGE_VERSION[${count}]}-${SYSDB_PACKAGE_RELEASE[${count}]}-${SYSDB_PACKAGE_ARCH[${count}]}-${SYSDB_PACKAGE_BUILD[${count}]}${SYSDB_PACKAGE_TAG[${count}]}
				sync_installed=${SYSDB_PACKAGE_INSTALLED_VERSION[${count}]}-${SYSDB_PACKAGE_INSTALLED_RELEASE[${count}]}-${SYSDB_PACKAGE_INSTALLED_ARCH[${count}]}-${SYSDB_PACKAGE_INSTALLED_BUILD[${count}]}${SYSDB_PACKAGE_INSTALLED_TAG[${count}]}
				if [ x"${sync_buildtool}" != x"${sync_installed}" ]; then
					SYSDB_PACKAGE_ENABLED[${count}]=true
					let update=update+1
					continue
				fi
			fi

			# Newer buildtool available?
			if [ ${NUMPKG_INSTALLED_VERSION} -eq ${NUMPKG_PACKAGE_VERSION} ]; then
				# No, different release/arch?
				if [ x"${SYSDB_PACKAGE_RELEASE[${count}]}-${SYSDB_PACKAGE_ARCH[${count}]}" != x"${SYSDB_PACKAGE_INSTALLED_RELEASE[${count}]}-${SYSDB_PACKAGE_INSTALLED_ARCH[${count}]}" ]; then
					# Yes, update the package.
					SYSDB_PACKAGE_ENABLED[${count}]=true
					let update=update+1
					continue
				else
					# No, newer build available?
					if [ ${SYSDB_PACKAGE_BUILD[${count}]} -gt ${SYSDB_PACKAGE_INSTALLED_BUILD[${count}]} ]; then
						SYSDB_PACKAGE_ENABLED[${count}]=true
						let update=update+1
						continue
					fi
				fi
			else
				# Buildtool newer then the installed version?
				if [ ${NUMPKG_INSTALLED_VERSION} -lt ${NUMPKG_PACKAGE_VERSION} ]; then
					SYSDB_PACKAGE_ENABLED[${count}]=true
					let update=update+1
					continue
				fi
			fi
		else
			# Package not yet installed, check for special packages.
			if ${upgrade_mode:-false}; then
				# Install the package.
				SYSDB_PACKAGE_ENABLED[${count}]=true
				let update=update+1
			fi
		fi
	done

	# Clear the progress bar.
	FUNC_CLEAR_PROGRESS_BAR

	# Updates available?
	if [ ${update} -eq 0 ]; then
		# No... everything is up2date.
		FUNC_MESSAGE_BOX "INFORMATION" "\nCongratulations!\nAll packages are up-to-date!"
		return 0
	else
		# More then one update available?
		if [ ${update} -gt 1 ]; then
			temp1="There are ${update} updates available!"
		else
			temp1="There is 1 update available!"
		fi
		# Display a infobox and warning.
		dialog	--title "${PROJECT_NAME} - INFORMATION" \
				--backtitle "${PROJECT_BACKTITLE}" \
				--cr-wrap \
				--yes-label "${BUTTON_LABEL_UPDATE}" \
				--no-label "${BUTTON_LABEL_CANCEL}" \
				--ok-label "${BUTTON_LABEL_UPDATE}" \
				--cancel-label "${BUTTON_LABEL_CANCEL}" \
				--defaultno \
				--yesno "\n\
${temp1}\n\
\n\
${PROJECT_NAME} will now create a list of packages that can be updated. \
Select the packages you want to update.\n\
\n\
WARNING!\n\
This is only for development and might break your installed OS!\n\
 " 13 75
		DIALOG_KEYCODE=$?

		# Continue?
		if test ${DIALOG_KEYCODE} -eq 0; then
			# Yes...
			return 2
		else
			# Exit...
			FUNC_CLEAR_SCREEN
			return 1
		fi
	fi

	# Back to menu.
	return 0
}

# Show hints and tips.
BUILDSYS_MAIN_DISPLAY_README_FILE() {

	dialog	--title "${PROJECT_NAME} - HINTS AND TIPS" \
			--backtitle "${PROJECT_BACKTITLE}" \
			--cr-wrap \
			--exit-label "${BUTTON_LABEL_OK}" \
			--textbox ${SYSTEM_ROOT}/docs/README 20 75

	OPTION_BUILDSYS_FIRSTTIME_USER=false
	CONFIG_EDITOR_SAVE_MENU_OPTION  "OPTION_BUILDSYS_FIRSTTIME_USER" "false"

	return 0

}

# List included buildtools.
BUILDSYS_MAIN_DISPLAY_INCLUDED_BUILDTOOLS() {
	local count=0

	rm -f ${DIALOG_TEMP_FILE1}
	rm -f ${DIALOG_TEMP_FILE2}

	while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ];do
		let count=count+1
		echo "    "${SYSDB_PACKAGE_BUILDTOOL_NAME[${count}]}-${SYSDB_PACKAGE_VERSION[${count}]}${SYSDB_PACKAGE_RELEASE[${count}]} >>${DIALOG_TEMP_FILE1}
	done

	sort ${DIALOG_TEMP_FILE1} >${DIALOG_TEMP_FILE2}

	sed -i  "1 iPackages included in this ${PROJECT_NAME} (${SYSDB_PACKAGE_COUNT} total)" ${DIALOG_TEMP_FILE2}
	sed -i  "1 a${RULER2}" ${DIALOG_TEMP_FILE2}
	echo "${RULER2}" >>${DIALOG_TEMP_FILE2}
	echo "" >>${DIALOG_TEMP_FILE2}

	dialog	--title "${PROJECT_NAME} - PACKAGES INCLUDED" \
			--backtitle "${PROJECT_BACKTITLE}" \
			--cr-wrap \
			--exit-label "${BUTTON_LABEL_OK}" \
			--textbox ${DIALOG_TEMP_FILE2} 20 75

	rm -f ${DIALOG_TEMP_FILE1}
	rm -f ${DIALOG_TEMP_FILE2}

	return 0
}

# Update buildtools.
BUILDSYS_MAIN_UPDATE_BUILDTOOLS() {
	local latest=""
	local year=""
	local month=""
	local update_status=1
	local proceed=""
	local conftype=""
	local confname=""
	local md5_local=""
	local md5_installed=""
	local version_required=0
	local version_required_major=0
	local version_required_minor=0
	local version_required_micro=0
	local version_system=0
	local version_system_major=0
	local version_system_minor=0
	local version_system_micro=0
	local version_system_code=0
	local buildtools_installed=""
	local buildtools_available=""
	local buildtools_available_build=""
	local buildtools_system_build=""
	local buildtools_status=""
	local buildtool_update=false
	local buildtool_archive_name="buildtool_archive"
	local buildtool_force_update=false
	local buildtool_local_archive=""
	local download_failed=true
	local download_error=255

	# Don't update the original system tree...
	if [ -e .bzr ]; then
		echo ""
		echo "ERROR!"
		echo "Do not run update on system directory!"
		echo "Exiting now..."
		echo ""
		exit 1
	fi

	# First update / initial buildtools download?
	if test -d ${SYSTEM_PATH_BUILDTOOLS}; then
		buildtool_force_update=false
		# Do we have a recent archive info?
		if [ ! -e ${SYSTEM_PATH_BTARCHIVE} ]; then
			if ${RUNTIME_CLI_MODE}; then
				echo ""
				echo "ERROR!"
				echo "${SYSTEM_PATH_BTARCHIVE} not found!"
				echo "Maybe you need to update ${PROJECT_NAME}?"
				echo "Exiting now..."
				echo ""
			else
				FUNC_MESSAGE_BOX "ERROR" "$(basename ${SYSTEM_PATH_BTARCHIVE}) not found!\nUnable to update (Error#2).\n\nExiting now!"
				FUNC_CLEAR_SCREEN
			fi
			exit 2
		else
			buildtools_installed=$(grep "." ${SYSTEM_PATH_BTARCHIVE} | sed "s,.*-,,g" | sed "s,.tar.bz2,,g")
		fi
	else
		if ${RUNTIME_CLI_MODE}; then
			cat <<EOF

${PROJECT_NAME} Version ${PROJECT_VERSION} - Initializing...
${RULER1}

Welcome to ${PROJECT_NAME}!
Before packages can be compiled and installed you need to
download and install the latest buildtools.

${PROJECT_NAME} will now download the latest buildtool archive
from the project website. Please wait...

EOF
		else
			FUNC_PROGRESS_BOX "Initializing, please wait..."
		fi
		buildtool_force_update=true
		buildtools_installed="none"
	fi

	RUNTIME_BUILDTOOL_UPDATE=false
	(	# user notification...
		if ${RUNTIME_CLI_MODE}; then
			echo "Checking buildtool status..."
		else
			FUNC_PROGRESS_BOX "Checking buildtool status..."
		fi

		# Initialize update environment.
		cd ${SYSTEM_PATH_TEMP}
		rm -rf buildtool_update
		mkdir -p buildtool_update
		cd buildtool_update
		latest=""

		# Do we have an archive in the main directory?
		buildtool_local_archive=$(find ${SYSTEM_ROOT}/ -type f -mindepth 1 -maxdepth 1 -name "${buildtool_archive_name}-*tar.bz2" -printf "%f\n" | sort -r | grep -m1 ".")
		if [ x"${buildtool_local_archive}" != x"" ]; then
			# Yes, do we have some buildtools allready installed?
			if [ -e ${SYSTEM_PATH_BTARCHIVE} ]; then
				# Yes, check local buildtool status.
				md5_installed=$(grep " ${buildtool_local_archive}$" ${SYSTEM_PATH_BTARCHIVE})
				md5_local=$(cd ${SYSTEM_ROOT}/; md5sum ${buildtool_local_archive})
				if [ x"${md5_installed}" == x"${md5_local}" ]; then
					buildtool_local_archive=""
				fi
			fi
			# Move local archive to temporary directory.
			mv ${SYSTEM_ROOT}/$buildtool_local_archive .
			# Set local archive as new buildtool archive to install.
			latest=$buildtool_local_archive
			download_failed=false
			download_error=0
			if ${RUNTIME_CLI_MODE}; then
				echo ""
				echo "Found local buildtool archive..."
				echo "  => ${SYSTEM_ROOT}/$buildtool_local_archive"
				echo ""
			else
				FUNC_PROGRESS_BOX "Found local buildtool archive..."
			fi
		else
			# No local archive found, try to download from the project website.
			download_failed=true
			for download_server in ${SYSTEM_CONFIG_LATEST_URL}; do
				if ${RUNTIME_CLI_MODE}; then
					echo "Checking server: ${download_server}"
				else
					FUNC_PROGRESS_BOX_LARGE "Server: ${download_server}"
				fi
				rm -f LATEST
				download_error=1
				wget ${download_server} &>/dev/null || continue
				# We need to check LATEST file for a valid content i.e.
				# a valid buildtool-archive name.
				latest=$(grep "${buildtool_archive_name}-.*.tar.bz2" LATEST)
				if [ x"${latest}" != x"" ]; then
					rm -f ${latest}
					download_error=10
					wget ${SYSTEM_CONFIG_ARCHIVE_URL}/${latest} &>/dev/null || continue
					# Download of archive failed?
					if ! test -e ${latest}; then
						download_error=4
						continue
					fi
					download_error=11
					wget ${SYSTEM_CONFIG_ARCHIVE_URL}/${latest//tar.bz2/md5} &>/dev/null || continue
					# Download of md5 hash failed?
					if ! test -e ${latest//tar.bz2/md5}; then
						download_error=4
						continue
					fi
					# MD5 checksum failed?
					if ! ( md5sum -c ${latest//tar.bz2/md5} &>/dev/null ); then
						download_error=4
						continue
					fi
					# Does MD5 od the downloaded archive match the installed archive?
					if ! ${buildtool_force_update}; then
						if ( md5sum -c ${SYSTEM_PATH_BTARCHIVE} &>/dev/null ); then
							# No updates available.
							exit 5
						fi
					fi
				else
					download_error=3
					continue
				fi
				download_failed=false
				break
			done
		fi

		# Download successful?
		if ${download_failed:true}; then
			case ${download_error} in
				1)	if ${RUNTIME_CLI_MODE}; then
						echo ""
						echo "Unable to get buildtool archive info."
						echo "Download failed! (Error#${download_error})"
						echo "Exiting now..."
						echo ""
					else
						FUNC_MESSAGE_BOX "ERROR" "Unable to get buildtool archive info.\nDownload failed! (Error #${download_error})\n\nExiting now..."
						FUNC_CLEAR_SCREEN
					fi
					;;
				3)	if ${RUNTIME_CLI_MODE}; then
						echo ""
						echo "Unable to get buildtool archive name."
						echo "Download failed! (Error#${download_error})"
						echo "Exiting now..."
						echo ""
					else
						FUNC_MESSAGE_BOX "ERROR" "Unable to get buildtool archive name.\nDownload failed! (Error #${download_error})\n\nExiting now..."
						FUNC_CLEAR_SCREEN
					fi
					;;
				4)	if ${RUNTIME_CLI_MODE}; then
						echo ""
						echo "Latest buildtool archive: ${latest}"
						echo "Download failed! (Error#${download_error})"
						echo "Exiting now..."
						echo ""
					else
						FUNC_MESSAGE_BOX "ERROR" "Latest buildtool archive:\n${latest}\nDownload failed! (Error#${download_error})\nExiting now..."
						FUNC_CLEAR_SCREEN
					fi
					;;
				10)	if ${RUNTIME_CLI_MODE}; then
						echo ""
						echo "Unable to get archive from server."
						echo "Download failed! (Error#${download_error})"
						echo "Exiting now..."
						echo ""
					else
						FUNC_MESSAGE_BOX "ERROR" "Unable to get archive from server.\nDownload failed! (Error #${download_error})\n\nExiting now..."
						FUNC_CLEAR_SCREEN
					fi
					;;
				11)	if ${RUNTIME_CLI_MODE}; then
						echo ""
						echo "Unable to get MD5 data from server."
						echo "Download failed! (Error#${download_error})"
						echo "Exiting now..."
						echo ""
					else
						FUNC_MESSAGE_BOX "ERROR" "Unable to get MD5 data from server.\nDownload failed! (Error #${download_error})\n\nExiting now..."
						FUNC_CLEAR_SCREEN
					fi
					;;
				*)	if ${RUNTIME_CLI_MODE}; then
						echo ""
						echo "Unable to get latest buildtool archive."
						echo "Download failed! (Error#${download_error})"
						echo "Exiting now..."
						echo ""
					else
						FUNC_MESSAGE_BOX "ERROR" "Unable to get latest buildtool archive.\nDownload failed! (Error #${download_error})\n\nExiting now..."
						FUNC_CLEAR_SCREEN
					fi
					;;
			esac
			exit 4
		fi

		# Unpack the archive.
		(	mkdir -p update
			cd update
			tar xf ../${latest}
		)

		# Do we have a required buildsystem version in the archive?
		if [ ! -e update/VERSION.TXT ]; then
			# No, bad archive.
			if ${RUNTIME_CLI_MODE}; then
				echo ""
				echo "No valid buildtool archive found."
				echo "Download failed! (Error#6)"
				echo "Exiting now..."
				echo ""
			else
				FUNC_MESSAGE_BOX "ERROR" "No valid buildtool archive found.\nDownload failed! (Error #6)\n\nExiting now..."
				FUNC_CLEAR_SCREEN
			fi
			exit 6
		fi

		# Split VERSION_REQUIRED into MAJOR, MINOR and MICRO version.
		version_required=$(grep "." update/VERSION.TXT)
		version_required_major=$(echo ${version_required} | awk -F. '{ print $1 }')
		version_required_minor=$(echo ${version_required} | awk -F. '{ print $2 }')
		version_required_minor=${version_required_minor:-0}
		version_required_micro=$(echo ${version_required} | awk -F. '{ print $3 }')
		version_required_micro=${version_required_micro:-0}
		let version_required_code=version_required_major*1000000+version_required_minor*1000+version_required_micro
		# Split VERSION_SYSTEM into MAJOR, MINOR and MICRO version.
		version_system=$(grep "." ${SYSTEM_ROOT}/VERSION)
		version_system_major=$(echo ${version_system} | awk -F. '{ print $1 }')
		version_system_minor=$(echo ${version_system} | awk -F. '{ print $2 }')
		version_system_minor=${version_system_minor:-0}
		version_system_micro=$(echo ${version_system} | awk -F. '{ print $3 }')
		version_system_micro=${version_system_micro:-0}
		let version_system_code=version_system_major*1000000+version_system_minor*1000+version_system_micro
		# Required buildsystem version installed?
		if test ${version_system_code} -lt ${version_required_code}; then
			if ${RUNTIME_CLI_MODE}; then
				echo ""
				echo "ERROR!"
				echo "Required ${PROJECT_NAME} >= ${version_required} not installed!"
				echo "(Installed: ${version_system})"
				echo ""
			else
				FUNC_MESSAGE_BOX "ERROR" "\nUpdate failed:\n${PROJECT_NAME} >= ${version_required} not installed!\n\nExiting now..."
				FUNC_CLEAR_SCREEN
			fi
			exit 7
		fi
		# Buildtool update available?
		#buildtools_installed=$(grep "." ${SYSTEM_PATH_BTARCHIVE} | sed "s,.*-,,g" | sed "s,.tar.bz2,,g")
		buildtools_available=$(echo ${latest} | sed "s,.*-,,g" | sed "s,.tar.bz2,,g")
		buildtools_available_build=$(grep "." update/BUILD.TXT)
		buildtools_system_build="$(grep "." ${SYSTEM_ROOT}/VERSION_BUILD)"
		buildtools_system_build="${buildtools_system_build##0}"
		buildtools_system_build="${buildtools_system_build##0}"
		buildtools_system_build="${buildtools_system_build:-0}"
		let buildtools_system_build=version_system_code*1000+buildtools_system_build
		if ! ${buildtool_force_update}; then
			# Is online archive newer then installed buildtools?
			if test ${buildtools_available_build} -gt ${buildtools_system_build}; then
				buildtools_status="Update available!  "
				buildtool_update=true
			else
				# Buildtool update available?
				if test ${buildtools_available//./} -gt ${buildtools_installed//./}; then
					buildtools_status="Update available!  "
					buildtool_update=true
				else
					buildtools_status="No update available!  "
				fi
			fi
			# Is online archive newer then installed buildtools?
			if ! ${buildtool_update}; then
				# No updates available.
				if ${RUNTIME_CLI_MODE}; then
					echo ""
					echo "INFORMATION!"
					echo "No updates availabale!"
					echo"   Installed: ${buildtools_system_build}"
					echo"   Available: ${buildtools_available_build}"
					echo ""
				else
					FUNC_MESSAGE_BOX "INFORMATION" "\nNo update available:\n Installed: ${buildtools_system_build}\n Available: ${buildtools_available_build}\nExiting now..."
					FUNC_CLEAR_SCREEN
				fi
				exit 5
			fi
		else
			# We do not have any buildtools installed or we have
			# requested to install an local buildtools archive.
			buildtools_status="Update available!  "
		fi
		# Request user confirmation.
		if ! ${RUNTIME_CLI_MODE}; then
			dialog	--title "${PROJECT_NAME} - VERSION STATUS" \
					--backtitle "${PROJECT_BACKTITLE}" \
					--cr-wrap \
					--yes-label "${BUTTON_LABEL_YES}" \
					--no-label "${BUTTON_LABEL_NO}" \
					--ok-label "${BUTTON_LABEL_YES}" \
					--cancel-label "${BUTTON_LABEL_NO}" \
					--defaultno \
					--yesno "\
There is a newer buildtool archive available for install:\n\
\n\
Update information:\n\
${RULER2}\n\
Buildtools installed : ${buildtools_installed}\n\
Build                : ${buildtools_system_build}\n\
${PROJECT_NAME} installed: ${version_system}\n\
\n\
Buildtools available : ${buildtools_available}\n\
Build                : ${buildtools_available_build}\n\
${PROJECT_NAME} required : ${version_required}\n\
${RULER2}\n\
\n\
${buildtools_status}\n\
Install buildtool archive now? Select 'yes' to proceed...\n\
" 20 75
			DIALOG_KEYCODE=$?
			if test ${DIALOG_KEYCODE:-0} -eq 0; then
				proceed="yes"
			else
				proceed="no"
			fi
		else
			echo ""
			echo "Update information:"
			echo "${RULER2}"
			echo "Buildtools installed : ${buildtools_installed}"
			echo "Build                : ${buildtools_system_build}"
			echo "${PROJECT_NAME} installed   : ${version_system}"
			echo ""
			echo "Buildtools available : ${buildtools_available}"
			echo "Build                : ${buildtools_available_build}"
			echo "${PROJECT_NAME} required    : ${version_required}"
			echo "${RULER2}"
			echo "${buildtools_status}"
			echo "Update now? Type 'yes' to proceed..."
			read proceed
		fi
		# Update buildtools?
		if [ x"${proceed}" == x"yes" ]; then
			# Install buildtools.
			if test -d "${SYSTEM_PATH_BUILDTOOLS}"; then
				mv "${SYSTEM_PATH_BUILDTOOLS}" \
				   "${SYSTEM_PATH_BUILDTOOLS}.${buildtools_installed}"
			fi
			mv "update" \
			   "${SYSTEM_PATH_BUILDTOOLS}"
			# Install includes-config files.
			if test -d "${SYSTEM_PATH_INCLUDES_CONFIG}/config"; then
				if test -d "${SYSTEM_PATH_BUILDTOOLS}/.includes-config"; then
					mv "${SYSTEM_PATH_INCLUDES_CONFIG}/config" \
					   "${SYSTEM_PATH_INCLUDES_CONFIG}/config.${buildtools_installed}"
				fi
			fi
			if test -d "${SYSTEM_PATH_BUILDTOOLS}/.includes-config"; then
				mkdir -p ${SYSTEM_PATH_INCLUDES_CONFIG}
				mv "${SYSTEM_PATH_BUILDTOOLS}/.includes-config" \
				   "${SYSTEM_PATH_INCLUDES_CONFIG}/config"
			fi
			# Install configuration files.
			if test -d "${SYSTEM_PATH_BUILDTOOLS}/.config"; then
				for conftype in packages remove single; do
					for confname in $(find "${SYSTEM_PATH_BUILDTOOLS}/.config/" -name "${conftype}.conf*" -printf "%f\n"); do
						if test -e "${SYSTEM_CONFIG}/${confname}"; then
							mkdir -p "${SYSTEM_CONFIG}.${buildtools_installed}"
							cat "${SYSTEM_CONFIG}/${confname}" \
							   >"${SYSTEM_CONFIG}.${buildtools_installed}/${confname}"
						fi
						cat ${SYSTEM_PATH_BUILDTOOLS}/.config/${confname} \
						   >${SYSTEM_CONFIG}/${confname}
					done
				done
				# Replace existing packages.conf if not replaced by buildtool archive.
				if [ -e "${SYSTEM_CONFIG}/packages.conf" ]; then
					if [ ! -e "${SYSTEM_PATH_BUILDTOOLS}/.config/packages.conf" ]; then
						if [ -e "${SYSTEM_PATH_BUILDTOOLS}/.config/packages.conf.orig" ]; then
							cat ${SYSTEM_PATH_BUILDTOOLS}/.config/packages.conf.orig \
								>${SYSTEM_CONFIG}/packages.conf
						fi
					fi
				fi
			fi
			# Delete outdated cache file.
			rm -f ${SYSTEM_CONFIG_BUILDTOOLS_CACHE} &>/dev/null
			# Create new logfile of installed buildtool archive.
			if [ -e "${SYSTEM_PATH_BTARCHIVE}" ]; then
				mv "${SYSTEM_PATH_BTARCHIVE}" \
				   "${SYSTEM_PATH_BTARCHIVE}.${buildtools_installed}"
			fi
			md5sum ${latest} >${SYSTEM_PATH_BTARCHIVE}
			exit 0
		else
			exit 9
		fi
	)

	# Update?
	update_status=$?
	if test ${update_status} -eq 0; then
		# Yes
		if ! ${RUNTIME_CLI_MODE}; then
			# Re-Load package configuration.
			SYSTEM_CONFIG_BUILDTOOLS="${SYSTEM_CONFIG_BUILDTOOLS//.orig/}"
			RUNTIME_PACKAGE_CONFIG_FILE="${SYSTEM_CONFIG_BUILDTOOLS}"
			FUNC_PROGRESS_BOX "Update finished succesfully!"
			SLEEPMODE 2
			RUNTIME_BUILDTOOL_UPDATE=true
		else
			echo "${RULER1}"
			echo "Update finished!"
			echo ""
			SLEEPMODE 2
		fi
	else
		# No
		if test ${update_status} -eq 9; then
			update_status=0
		else
			if test ${update_status} -eq 5; then
				if ! ${RUNTIME_CLI_MODE}; then
					FUNC_MESSAGE_BOX "INFORMATION" "\nNo updates available!\nEverything is up to date."
				else
					echo "${RULER1}"
					echo "No updates available!"
					echo "Everything is up to date."
					echo ""
					SLEEPMODE 2
				fi
				update_status=0
			fi
		fi
	fi
	if ${buildtool_force_update}; then
		if ! test -d ${SYSTEM_PATH_BUILDTOOLS}; then
			if ${RUNTIME_CLI_MODE}; then
				echo ""
				echo "Without buildtools ${PROJECT_NAME} will not be able to continue."
				echo "Exiting now..."
				echo ""
			else
				FUNC_MESSAGE_BOX "ERROR" "Without buildtools ${PROJECT_NAME} will not be able to continue.\n\nExiting now!"
				FUNC_CLEAR_SCREEN
			fi
			update_status=1
		fi
	fi
	return ${update_status}
}


# Just a separator...
SYS____________________MAIN_LOOP() {
	true
}




# Main loop... Initialize the buildsystem.
CONFIGURE_DEFINE_RUNTIME_SETTINGS || exit 1

# Some system checks...
CONFIGURE_CHECK_SYSTEM_REQUIREMENTS || exit 1

# Make sure all system files are there...
CONFIGURE_CHECK_SYSTEM_FILES || exit 1

# Check for command line options and
# check for system files if needed...
CONFIGURE_CHECK_CLI_OPTIONS $* || exit 0

# Do we have a local copy of buildtools package?
CONFIGURE_CHECK_BUILDTOOL_FILES || exit 1

# Execute command line jobs...
case ${RUNTIME_CLI_OPTION} in
	DLALL)		RUNTIME_DOWNLOAD_ALL_SOURCES_QUIET_MODE=true;
				BUILDSYS_MAIN_DOWNLOAD_ALL_SOURCE_PACKAGES;
				exit 0;;
	DLSINGLE)	RUNTIME_DOWNLOAD_ALL_SOURCES_QUIET_MODE=true;
				RUNTIME_DOWNLOAD_SINGLE_MODE_ONLY=true;
				BUILDSYS_MAIN_DOWNLOAD_ALL_SOURCE_PACKAGES;
				exit 0;;
	UPDATEBT)	BUILDSYS_MAIN_UPDATE_BUILDTOOLS;
				exit 0;;
	ADD)		BUILDSYS_JOBLIST_ADD_PACKAGE  || exit 1;;
	SKIP)		BUILDSYS_JOBLIST_SKIP_PACKAGE || exit 1;;
esac

# Check if we have restarted the buildsystem.
CONFIGURE_SESSION_MODE || exit 1

# Print auto-detected platform information...
if ! ${RUNTIME_SYSTEM_RESTART}; then
	FUNC_INFO_BOX "Please wait..." "Autodetected platform:\n\nSystem   : ${OS_PLATFORM}\nVersion  : ${OS_VERSION}\nArch     : ${OS_ARCH}\nLibraries: ${SYSTEM_LIBDIR}"

	# Copy platform data into logfile.
	cat <<EOF >${SYSTEM_DEBUG_LOGFILE_PLATFORM}
OS_PLATFORM     : ${OS_PLATFORM}
OS_PLATFORM_CODE: ${OS_PLATFORM_CODE}
OS_TYPE         : ${OS_TYPE}
OS_VERSION      : ${OS_VERSION}
OS_VERSION_CODE : ${OS_VERSION_CODE}
OS_ARCH         : ${OS_ARCH}

Autodetected platform:
System          : ${OS_PLATFORM}
Version         : ${OS_VERSION}
Arch            : ${OS_ARCH}
Libraries       : ${SYSTEM_LIBDIR}

EOF
	if test -e /etc/os-release; then
		cat /etc/os-release >>${SYSTEM_DEBUG_LOGFILE_PLATFORM}
	fi

	SLEEPMODE 2
fi

# Validate the buildorder configuration file.
FUNC_PROGRESS_BOX "Checking configuration file..."
if ! ${RUNTIME_QUICK_RESUME}; then
	CONFIGURE_CHECK_BUILDTOOL_CONFIGURATION_FILE | exit 1
fi

# Load the buildsystem configuration file.
CONFIGURE_LOAD_SYSTEM_SETTINGS || exit 1

# Load package configuration.
INITIALIZE_BUILDTOOLS || exit 1

FUNC_CLEAR_SCREEN




# Are we in restart mode?
if ${RUNTIME_SYSTEM_RESTART}; then
	RUNTIME_SETUP_MODE="RESTART"
	if ! ${RUNTIME_QUICK_RESUME}; then
		INITIALIZE_CHECK_INSTALLED_PACKAGES
	fi
else
	if ${RUNTIME_SYSTEM_USERMODE}; then
		RUNTIME_SETUP_MODE="SINGLE"
		INITIALIZE_CHECK_INSTALLED_PACKAGES
	else
		while [ true ]; do

			# First-time users should read the readme first.
			if ${OPTION_BUILDSYS_FIRSTTIME_USER}; then
				RUNTIME_MAIN_MENU_OPTION="README"
			else
				case ${RUNTIME_SYSTEM_CUSTOM_CONFIG} in
					true)	RUNTIME_MAIN_MENU_OPTION="SINGLE";;
					*)		RUNTIME_MAIN_MENU_OPTION="SOURCE";;
				esac
			fi

			# Display main menu.
			BUILDSYS_MAIN_MENU || FUNC_EXIT_BY_USER

			# No matter if the user has read the README file,
			# skip the README file on next start.
			OPTION_BUILDSYS_FIRSTTIME_USER=false
			CONFIG_EDITOR_SAVE_MENU_OPTION  "OPTION_BUILDSYS_FIRSTTIME_USER" "false"

			# Check selected menu option.
			case ${RUNTIME_SETUP_MODE} in
				README)		BUILDSYS_MAIN_DISPLAY_README_FILE
							RUNTIME_MAIN_MENU_OPTION="SOURCE"
							continue;;

				ICACHE)		SYSVAR_MODE="ICONCACHE"
							OPTION_UPDATE_GTK_ICON_CACHE=true
							FUNC_SYSTEM_UPDATE_ICON_CACHE
							continue;;

				CONFIG)		CONFIG_EDITOR_MAIN_MENU || exit 1
							continue;;

				PROFILE)	PROFILE_EDITOR_MAIN_MENU || exit 1
							continue;;

				SOURCE)		INITIALIZE_CHECK_INSTALLED_PACKAGES
							break;;

				SINGLE)		INITIALIZE_CHECK_INSTALLED_PACKAGES
							break;;

				SCRIPTS)	SYSVAR_MODE="SCRIPTS"
							UTILITY_CREATE_SCRIPT_INSTALLTGZ
							UTILITY_CREATE_SCRIPT_REMOVETGZ
							UTILITY_CREATE_SCRIPT_MAKEINFO
							continue;;

				DOWNLOAD)	SYSVAR_MODE="DOWNLOAD"
							BUILDSYS_MAIN_DOWNLOAD_ALL_SOURCE_PACKAGES || exit 1
							continue;;

				UPDATE)		SYSVAR_MODE="UPDATE"
							OPTION_REMOVE_EXTRA_PACKAGES=false
							BUILDSYS_MAIN_CHECK_FOR_UPDATES "false" "false"
							case $? in
							1)	exit 1;;
							2)	break;;
							*)	continue;;
							esac;;

				UPGRADE)	SYSVAR_MODE="UPDATE"
							OPTION_REMOVE_EXTRA_PACKAGES=false
							BUILDSYS_MAIN_CHECK_FOR_UPDATES "true" "false"
							case $? in
							1)	exit 1;;
							2)	break;;
							*)	continue;;
							esac;;

				BUILDTOOL)	SYSVAR_MODE="BUILDTOOL"
							RUNTIME_CLI_MODE=false
							BUILDSYS_MAIN_UPDATE_BUILDTOOLS || exit 1
							if ${RUNTIME_BUILDTOOL_UPDATE:-false}; then
								INITIALIZE_BUILDTOOLS || exit 1
							fi
							continue;;

				SYNC)		SYSVAR_MODE="UPDATE"
							OPTION_REMOVE_EXTRA_PACKAGES=false
							BUILDSYS_MAIN_CHECK_FOR_UPDATES "false" "true"
							case $? in
							1)	exit 1;;
							2)	break;;
							*)	continue;;
							esac;;

				PACKAGES)	SYSVAR_MODE="PACKAGES"
							BUILDSYS_MAIN_DISPLAY_INCLUDED_BUILDTOOLS
							continue;;

				CHKSRC)		SYSVAR_MODE="CHKSRC"
							count=0
							while [ ${count} -lt ${SYSDB_PACKAGE_COUNT} ]; do
								let count=count+1
								SYSDB_PACKAGE_SOURCES_CHECKED[${count}]=false
							done
							BUILDSYS_CHECK_SOURCES
							case $? in
							1)	exit 1;;
							*)	continue;;
							esac;;
			esac
		done
	fi
fi




# Are we in restart mode?
if ! ${RUNTIME_SYSTEM_RESTART}; then
	# No, select some buildsystem options...
	BUILDSYS_SELECT_BUILDOPTIONS || FUNC_EXIT_BY_USER
	BUILDSYS_SAVE_CONFIGURATION
fi

# Select build categories/features.
case ${RUNTIME_SETUP_MODE} in
	SOURCE)			while [ true ]; do
						BUILDSYS_SELECT_BUILDTOOL_GROUPS || FUNC_EXIT_BY_USER
						break
					done
					BUILDSYS_CREATE_BUILDORDER
					;;
esac

# Check/modify the buildorder.
case ${RUNTIME_SETUP_MODE} in
	SOURCE)		# Check if "SOURCE" has auto-selected any packages.
				BUILDSYS_SETUP_CHECK_NOTHING_TO_INSTALL || FUNC_EXIT_BY_USER
				if ${RUNTIME_SYSTEM_USERMODE}; then
					BUILDSYS_MODIFY_BUILDORDER_EDIT_LIST "REMOVE" "${BUTTON_LABEL_CANCEL}" || FUNC_EXIT_BY_USER
				else
					BUILDSYS_MODIFY_BUILDORDER_MENU || FUNC_EXIT_BY_USER
				fi
				# Check if we have multiple MESA packages enabled.
				BUILDSYS_MODIFY_SELECT_MESA || FUNC_EXIT_BY_USER
				# Check if we have multiple XOrg X server packages enabled.
				BUILDSYS_MODIFY_SELECT_XORG_SERVER || FUNC_EXIT_BY_USER
				# If XOrg drivers should be installed, choose the drivers to build.
				if FUNC_CHECK_GROUP_XORGDRV_SELECTED; then
					BUILDSYS_MODIFY_SELECT_XORG_DRIVER_MENU || FUNC_EXIT_BY_USER
				fi
				# Check for other xorg packages to be removed
				BUILDSYS_CHECK_XORG_DRIVERS_REMAINING
				BUILDSYS_CHECK_XORG_PACKAGES_REMAINING
				# Check if we have duplicate packages enabled.
				BUILDSYS_MODIFY_SELECT_DUPLICATE_PACKAGES || FUNC_EXIT_BY_USER
				# Do not check for extra package when restarted since not all deps/opts may be available in the packages.job file anymore.
				if ${OPTION_FIND_OPTIONAL_PACKAGES:-true}; then
					if ! ${RUNTIME_SYSTEM_RESTART}; then
						while [ true ]; do
							if ! BUILDSYS_CHECK_ALL_PACKAGES_ENABLED; then
								BUILDSYS_CREATE_BUILDORDER_CHECK_DEPOPTS_WARNING
								while [ true ]; do
									BUILDSYS_CREATE_BUILDORDER_CHECK_DEPOPTS
									case $? in
										0)	break;;
										1)	FUNC_EXIT_BY_USER;;
										*)	continue;;
									esac
								done
							fi
							break
						done
					fi
				fi
				;;
	SINGLE)		# Check if "SOURCE" has auto-selected any packages.
				BUILDSYS_SETUP_CHECK_NOTHING_TO_INSTALL  || FUNC_EXIT_BY_USER
				BUILDSYS_MODIFY_BUILDORDER_EDIT_LIST "SINGLE" "${BUTTON_LABEL_CANCEL}" || FUNC_EXIT_BY_USER
				# Check if we have multiple MESA packages enabled.
				BUILDSYS_MODIFY_SELECT_MESA || FUNC_EXIT_BY_USER
				# Check if we have multiple XOrg X server packages enabled.
				BUILDSYS_MODIFY_SELECT_XORG_SERVER || FUNC_EXIT_BY_USER
				# Check for other xorg packages to be removed
				BUILDSYS_CHECK_XORG_DRIVERS_REMAINING
				BUILDSYS_CHECK_XORG_PACKAGES_REMAINING
				# Check if we have duplicate packages enabled.
				BUILDSYS_MODIFY_SELECT_DUPLICATE_PACKAGES || FUNC_EXIT_BY_USER
				# Do not check for extra package when restarted since not all deps/opts may be available in the job.packages file anymore.
				if ${OPTION_FIND_OPTIONAL_PACKAGES:-true}; then
					if ! ${RUNTIME_SYSTEM_RESTART}; then
						if ! BUILDSYS_CHECK_ALL_PACKAGES_ENABLED; then
							BUILDSYS_CREATE_BUILDORDER_CHECK_DEPOPTS_WARNING
							while [ true ]; do
								BUILDSYS_CREATE_BUILDORDER_CHECK_DEPOPTS
								case $? in
									0)	break;;
									1)	FUNC_EXIT_BY_USER "reset_job";;
									*)	continue;;
								esac
							done
						fi
					fi
				fi
				;;
	RESTART)	if ! ${RUNTIME_QUICK_RESUME}; then
					BUILDSYS_MODIFY_BUILDORDER_EDIT_LIST "RESTART" "${BUTTON_LABEL_CANCEL}" || FUNC_EXIT_BY_USER
				fi
				;;
	UPDATE)		BUILDSYS_MODIFY_BUILDORDER_EDIT_LIST "SYSUPDATE" "${BUTTON_LABEL_CANCEL}" || FUNC_EXIT_BY_USER
				# Check if we have multiple MESA packages enabled.
				BUILDSYS_MODIFY_SELECT_MESA || FUNC_EXIT_BY_USER
				# Check if we have multiple XOrg X server packages enabled.
				BUILDSYS_MODIFY_SELECT_XORG_SERVER || FUNC_EXIT_BY_USER
				# Check if we have duplicate packages enabled.
				BUILDSYS_MODIFY_SELECT_DUPLICATE_PACKAGES || FUNC_EXIT_BY_USER
				# Check for new required/optional packages.
				if ${OPTION_FIND_OPTIONAL_PACKAGES:-true}; then
					BUILDSYS_CREATE_BUILDORDER_CHECK_DEPOPTS_WARNING
					while [ true ]; do
						BUILDSYS_CREATE_BUILDORDER_CHECK_DEPOPTS
						case $? in
							0)	break;;
							1)	FUNC_EXIT_BY_USER "reset_job";;
							*)	continue;;
						esac
					done
				fi
				;;
	SYNC)		BUILDSYS_MODIFY_BUILDORDER_EDIT_LIST "SYSUPDATE" "${BUTTON_LABEL_CANCEL}" || FUNC_EXIT_BY_USER
				# Check if we have multiple MESA packages enabled.
				BUILDSYS_MODIFY_SELECT_MESA || FUNC_EXIT_BY_USER
				# Check if we have multiple XOrg X server packages enabled.
				BUILDSYS_MODIFY_SELECT_XORG_SERVER || FUNC_EXIT_BY_USER
				# Check if we have duplicate packages enabled.
				BUILDSYS_MODIFY_SELECT_DUPLICATE_PACKAGES || FUNC_EXIT_BY_USER
				;;
	UPGRADE)	BUILDSYS_MODIFY_BUILDORDER_EDIT_LIST "SYSUPDATE" "${BUTTON_LABEL_CANCEL}" || FUNC_EXIT_BY_USER
				# Check if we have multiple MESA packages enabled.
				BUILDSYS_MODIFY_SELECT_MESA || FUNC_EXIT_BY_USER
				# Check if we have multiple XOrg X server packages enabled.
				BUILDSYS_MODIFY_SELECT_XORG_SERVER || FUNC_EXIT_BY_USER
				# Check if we have duplicate packages enabled.
				BUILDSYS_MODIFY_SELECT_DUPLICATE_PACKAGES || FUNC_EXIT_BY_USER
				;;
esac

# Debug only: Check for missing deps/opt/rebs...
if ${RUNTIME_OPTION_DEBUG_MODE}; then
	if ! ${RUNTIME_SYSTEM_RESTART}; then
		# When upgrade/update/sync packages there is no
		# need to check for missing.
		case ${RUNTIME_SETUP_MODE} in
			UPDATE|UPGRADE|SYNC)	true;;
			*)						BUILDSYS_DEBUG_CHECK_DEPOPTS;;
		esac
	fi
fi

# Are we in restart mode?
if ! ${RUNTIME_SYSTEM_RESTART}; then
	# Install MPlayer?
	BUILDSYS_CHECK_MPLAYER_GUI_LANG
fi

# Any packages to downgrade?
if ! ${RUNTIME_QUICK_RESUME}; then
	BUILDSYS_SETUP_CHECK_DOWNGRADE_PACKAGES || FUNC_EXIT_BY_USER
fi

# Remove extra packages.
if ! ${RUNTIME_SYSTEM_RESTART}; then
	BUILDSYS_SETUP_REMOVE_EXTRA_PACKAGES
fi

# Check if 'SOURCE' or 'SINGLE' has selected any packages to build.
BUILDSYS_SETUP_CHECK_NOTHING_TO_INSTALL || FUNC_EXIT_BY_USER

# Create environment variables required for the build.
BUILDSYS_SETUP_BUILD_ENVIRONMENT

# Are we in restart mode?
if ! ${RUNTIME_SYSTEM_RESTART}; then
	# No, backup/create some files.
	BUILDSYS_SETUP_CREATE_SUDOERS_BACKUP
	BUILDSYS_SETUP_CREATE_SETTINGS_LOGFILE
	BUILDSYS_SETUP_CREATE_PKG_CONF_OVERWRITE
	BUILDSYS_SETUP_CREATE_PACKAGE_INSTALLED_LIST "1"
	BUILDSYS_SETUP_CREATE_PACKAGE_UPDATE_LIST
fi

# Check for packages added/removed by user.
BUILDSYS_SETUP_CREATE_USER_MODIFICATIONS_LIST

# Check for the source packages.
if ! ${RUNTIME_QUICK_RESUME}; then
	BUILDSYS_CHECK_SOURCES || exit 1
fi

# Get some statistics.
BUILDSYS_SETUP_INIT_STATISTICS

# Are we in restart mode?
if ! ${RUNTIME_SYSTEM_RESTART}; then
	# Do some warnings...
	BUILDSYS_SETUP_WARN_USER_DELETE_SOURCE_PACKAGES
	BUILDSYS_SETUP_WARN_USER_UPDATE_ICON_CACHE
	BUILDSYS_SETUP_WARN_USER_DISTCC_RETRIES
fi

# Create jobfiles so when an error occures when downloading
# source packages we can resume from here...
BUILDSYS_SETUP_CREATE_JOBFILE
BUILDSYS_SETUP_CREATE_JOBSETTINGS
if ! ${RUNTIME_SYSTEM_RESTART}; then
	# Backup the job list
	cp "${SYSTEM_JOB_PACKAGES}" "${SYSTEM_PATH_LOGFILES_SYSTEM}"
	# Backup the job setup.
	cp "${SYSTEM_JOB_SETTINGS}" "${SYSTEM_PATH_LOGFILES_SYSTEM}"
	# We should create installtgz & co when upgrading packages
	# since we might have new packages also.
	case ${RUNTIME_SETUP_MODE} in
		SOURCE|\
		SINGLE|\
		UPDATE|\
		UPGRADE)	UTILITY_CREATE_SCRIPT_INSTALLTGZ; \
					UTILITY_CREATE_SCRIPT_REMOVETGZ; \
					UTILITY_CREATE_SCRIPT_MAKEINFO
					;;
	esac
fi

# Display last warning
BUILDSYS_LAST_WARNING || FUNC_EXIT_BY_USER

# Save configuration.
BUILDSYS_SAVE_CONFIGURATION
BUILDSYS_SAVE_DATABASE_TO_LOGFILE
BUILDSYS_SAVE_BUILD_SETTINGS

# Any source packages missing?
# Only check when not using 'SETUP -q' for quick restart or missing sources > 0.
if ! ${RUNTIME_QUICK_RESUME:-false} || [ ${RUNTIME_MISSING_SOURCE_PACKAGES:-0} -ne 0 ] ; then
	BUILDSYS_DOWNLOAD_MISSING_SOURCES || exit 1
fi

# Are we in restart mode?
if ! ${RUNTIME_SYSTEM_RESTART}; then
	# No... check MD5SUM of source packages...
	BUILDSYS_DOWNLOAD_CHECK_MD5SUM || exit 1
fi

# Remove installed packages.
BUILDSYS_REMOVE_INSTALLED_PACKAGES || exit 1

# Are we in restart mode?
if ! ${RUNTIME_SYSTEM_RESTART}; then
	BUILDSYS_SETUP_CREATE_PACKAGE_INSTALLED_LIST "2"
fi

# Create directory for installed packages.
BUILDSYS_SETUP_INIT_PKGDIR

# Build packages... failed because of an error?
BUILDSYS_COMPILE_PACKAGES
if [ -e ${SYSTEM_BUILD_FAILED} -o $? -ne 0 ]; then
	cat <<EOF

${RULER1}
ERROR: $(grep -m1 "." ${SYSTEM_JOB_PACKAGES}):
Build has failed. You can skip the package which has failed to build
by using the following command to resume the install process:
  sh ${PROJECT_SCRIPT_NAME} --skip
If there's a package missing you may want to try:
  sh ${PROJECT_SCRIPT_NAME} --add pkgname[,pkg2,pkg3...]
If you want to help to improve this project then send me the logfiles
which can help me to find out why the build has failed:
  tar cjf log-system.tar.bz2 temp/logfiles.system
  tar cjf log-buildtools.tar.bz2 temp/logfiles.buildtools
Attach the newly created archives to a mail and send to:
  ${PROJECT_EMAIL}
Sorry for any problems you got by using this experimental project.
${RULER1}
EOF
	# Yes, exit...
	exit 1
fi

# Create logfile with all installed packages.
BUILDSYS_SETUP_CREATE_PACKAGE_INSTALLED_LIST "3"

# Do some maintenance...
BUILDSYS_FINALIZE_BUILD

# All done!
BUILDSYS_CONGRATULATIONS

# Show some logfiles...
BUILDSYS_SHOW_LOGFILES

exit 0

