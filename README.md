# pkgBuildBot
pkgBuildBot is a collection of scripts to build/install KDE, XFCE and many other applications from the official source packages designed to work with Slackware Linux.

## Building
#### Stable
Download latest stable release from **[here](http://mkslack.gitlab.io/html/content/buildsystem/index.html)**.
```
wget http://mkslack.gitlab.io/html/archive/pkgbuildbot/pkgBuildBot-latest.tar.bz2
tar xf pkgBuildBot-*.tar.bz2
cd pkgBuildBot-*
sh SETUP.sh
```
The latest available buildtools will then be downloaded from the project website.
#### Unstable
Download latest development release:
```
wget https://gitlab.com/mkslack/pkgBuildBot/repository/master/archive.zip
unzip -q master.zip
cd pkgBuildBot-master
sh SETUP.sh
```
The development snapshot allready includes all available buildtools.

## License
pkgBuildBot is under **[GNU General Public License 3](https://www.gnu.org/licenses/gpl-3.0.de.html).**
